<?php
/**
 * RadioShowTime
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * RadioShowTime Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RadioShowTime implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'RadioShowTime';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'startTime' => 'string',
        'endTime' => 'string',
        'weekday' => 'double',
        'id' => 'double',
        'radioShowId' => 'double',
        'radioShow' => 'object'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'startTime' => 'startTime',
        'endTime' => 'endTime',
        'weekday' => 'weekday',
        'id' => 'id',
        'radioShowId' => 'radioShowId',
        'radioShow' => 'radioShow'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'startTime' => 'setStartTime',
        'endTime' => 'setEndTime',
        'weekday' => 'setWeekday',
        'id' => 'setId',
        'radioShowId' => 'setRadioShowId',
        'radioShow' => 'setRadioShow'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'startTime' => 'getStartTime',
        'endTime' => 'getEndTime',
        'weekday' => 'getWeekday',
        'id' => 'getId',
        'radioShowId' => 'getRadioShowId',
        'radioShow' => 'getRadioShow'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['startTime'] = isset($data['startTime']) ? $data['startTime'] : '0000';
        $this->container['endTime'] = isset($data['endTime']) ? $data['endTime'] : '2359';
        $this->container['weekday'] = isset($data['weekday']) ? $data['weekday'] : 1.0;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['radioShowId'] = isset($data['radioShowId']) ? $data['radioShowId'] : null;
        $this->container['radioShow'] = isset($data['radioShow']) ? $data['radioShow'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        if (!is_null($this->container['weekday']) && ($this->container['weekday'] > 7.0)) {
            $invalid_properties[] = "invalid value for 'weekday', must be smaller than or equal to 7.0.";
        }

        if (!is_null($this->container['weekday']) && ($this->container['weekday'] < 1.0)) {
            $invalid_properties[] = "invalid value for 'weekday', must be bigger than or equal to 1.0.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        if ($this->container['weekday'] > 7.0) {
            return false;
        }
        if ($this->container['weekday'] < 1.0) {
            return false;
        }
        return true;
    }


    /**
     * Gets startTime
     * @return string
     */
    public function getStartTime()
    {
        return $this->container['startTime'];
    }

    /**
     * Sets startTime
     * @param string $startTime Time when the show starts, format HHmm
     * @return $this
     */
    public function setStartTime($startTime)
    {
        $this->container['startTime'] = $startTime;

        return $this;
    }

    /**
     * Gets endTime
     * @return string
     */
    public function getEndTime()
    {
        return $this->container['endTime'];
    }

    /**
     * Sets endTime
     * @param string $endTime Time when the show ends, format HHmm
     * @return $this
     */
    public function setEndTime($endTime)
    {
        $this->container['endTime'] = $endTime;

        return $this;
    }

    /**
     * Gets weekday
     * @return double
     */
    public function getWeekday()
    {
        return $this->container['weekday'];
    }

    /**
     * Sets weekday
     * @param double $weekday Day of the week when the show is live, 1 = Monday / 7 = Sunday
     * @return $this
     */
    public function setWeekday($weekday)
    {

        if ($weekday > 7.0) {
            throw new \InvalidArgumentException('invalid value for $weekday when calling RadioShowTime., must be smaller than or equal to 7.0.');
        }
        if ($weekday < 1.0) {
            throw new \InvalidArgumentException('invalid value for $weekday when calling RadioShowTime., must be bigger than or equal to 1.0.');
        }
        $this->container['weekday'] = $weekday;

        return $this;
    }

    /**
     * Gets id
     * @return double
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param double $id
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets radioShowId
     * @return double
     */
    public function getRadioShowId()
    {
        return $this->container['radioShowId'];
    }

    /**
     * Sets radioShowId
     * @param double $radioShowId
     * @return $this
     */
    public function setRadioShowId($radioShowId)
    {
        $this->container['radioShowId'] = $radioShowId;

        return $this;
    }

    /**
     * Gets radioShow
     * @return object
     */
    public function getRadioShow()
    {
        return $this->container['radioShow'];
    }

    /**
     * Sets radioShow
     * @param object $radioShow
     * @return $this
     */
    public function setRadioShow($radioShow)
    {
        $this->container['radioShow'] = $radioShow;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this));
    }
}


