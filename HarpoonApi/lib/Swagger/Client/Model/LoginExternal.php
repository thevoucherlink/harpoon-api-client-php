<?php
/**
 * LoginExternal
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * LoginExternal Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LoginExternal implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'LoginExternal';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'apiKey' => 'string',
        'version' => 'string',
        'requestUrl' => 'string',
        'authorizeUrl' => 'string',
        'accessUrl' => 'string'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'apiKey' => 'apiKey',
        'version' => 'version',
        'requestUrl' => 'requestUrl',
        'authorizeUrl' => 'authorizeUrl',
        'accessUrl' => 'accessUrl'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'apiKey' => 'setApiKey',
        'version' => 'setVersion',
        'requestUrl' => 'setRequestUrl',
        'authorizeUrl' => 'setAuthorizeUrl',
        'accessUrl' => 'setAccessUrl'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'apiKey' => 'getApiKey',
        'version' => 'getVersion',
        'requestUrl' => 'getRequestUrl',
        'authorizeUrl' => 'getAuthorizeUrl',
        'accessUrl' => 'getAccessUrl'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['apiKey'] = isset($data['apiKey']) ? $data['apiKey'] : null;
        $this->container['version'] = isset($data['version']) ? $data['version'] : null;
        $this->container['requestUrl'] = isset($data['requestUrl']) ? $data['requestUrl'] : null;
        $this->container['authorizeUrl'] = isset($data['authorizeUrl']) ? $data['authorizeUrl'] : null;
        $this->container['accessUrl'] = isset($data['accessUrl']) ? $data['accessUrl'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        if ($this->container['apiKey'] === null) {
            $invalid_properties[] = "'apiKey' can't be null";
        }
        if ($this->container['requestUrl'] === null) {
            $invalid_properties[] = "'requestUrl' can't be null";
        }
        if ($this->container['authorizeUrl'] === null) {
            $invalid_properties[] = "'authorizeUrl' can't be null";
        }
        if ($this->container['accessUrl'] === null) {
            $invalid_properties[] = "'accessUrl' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        if ($this->container['apiKey'] === null) {
            return false;
        }
        if ($this->container['requestUrl'] === null) {
            return false;
        }
        if ($this->container['authorizeUrl'] === null) {
            return false;
        }
        if ($this->container['accessUrl'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets apiKey
     * @return string
     */
    public function getApiKey()
    {
        return $this->container['apiKey'];
    }

    /**
     * Sets apiKey
     * @param string $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->container['apiKey'] = $apiKey;

        return $this;
    }

    /**
     * Gets version
     * @return string
     */
    public function getVersion()
    {
        return $this->container['version'];
    }

    /**
     * Sets version
     * @param string $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->container['version'] = $version;

        return $this;
    }

    /**
     * Gets requestUrl
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->container['requestUrl'];
    }

    /**
     * Sets requestUrl
     * @param string $requestUrl
     * @return $this
     */
    public function setRequestUrl($requestUrl)
    {
        $this->container['requestUrl'] = $requestUrl;

        return $this;
    }

    /**
     * Gets authorizeUrl
     * @return string
     */
    public function getAuthorizeUrl()
    {
        return $this->container['authorizeUrl'];
    }

    /**
     * Sets authorizeUrl
     * @param string $authorizeUrl
     * @return $this
     */
    public function setAuthorizeUrl($authorizeUrl)
    {
        $this->container['authorizeUrl'] = $authorizeUrl;

        return $this;
    }

    /**
     * Gets accessUrl
     * @return string
     */
    public function getAccessUrl()
    {
        return $this->container['accessUrl'];
    }

    /**
     * Sets accessUrl
     * @param string $accessUrl
     * @return $this
     */
    public function setAccessUrl($accessUrl)
    {
        $this->container['accessUrl'] = $accessUrl;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this));
    }
}


