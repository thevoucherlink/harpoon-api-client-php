<?php
/**
 * CustomerNotification
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * CustomerNotification Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CustomerNotification implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'CustomerNotification';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'message' => 'string',
        'cover' => 'string',
        'coverUpload' => '\Swagger\Client\Model\MagentoImageUpload',
        'from' => '\Swagger\Client\Model\NotificationFrom',
        'related' => '\Swagger\Client\Model\NotificationRelated',
        'link' => 'string',
        'actionCode' => 'string',
        'status' => 'string',
        'sentAt' => '\DateTime',
        'id' => 'double'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'message' => 'message',
        'cover' => 'cover',
        'coverUpload' => 'coverUpload',
        'from' => 'from',
        'related' => 'related',
        'link' => 'link',
        'actionCode' => 'actionCode',
        'status' => 'status',
        'sentAt' => 'sentAt',
        'id' => 'id'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'message' => 'setMessage',
        'cover' => 'setCover',
        'coverUpload' => 'setCoverUpload',
        'from' => 'setFrom',
        'related' => 'setRelated',
        'link' => 'setLink',
        'actionCode' => 'setActionCode',
        'status' => 'setStatus',
        'sentAt' => 'setSentAt',
        'id' => 'setId'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'message' => 'getMessage',
        'cover' => 'getCover',
        'coverUpload' => 'getCoverUpload',
        'from' => 'getFrom',
        'related' => 'getRelated',
        'link' => 'getLink',
        'actionCode' => 'getActionCode',
        'status' => 'getStatus',
        'sentAt' => 'getSentAt',
        'id' => 'getId'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['message'] = isset($data['message']) ? $data['message'] : null;
        $this->container['cover'] = isset($data['cover']) ? $data['cover'] : null;
        $this->container['coverUpload'] = isset($data['coverUpload']) ? $data['coverUpload'] : null;
        $this->container['from'] = isset($data['from']) ? $data['from'] : null;
        $this->container['related'] = isset($data['related']) ? $data['related'] : null;
        $this->container['link'] = isset($data['link']) ? $data['link'] : null;
        $this->container['actionCode'] = isset($data['actionCode']) ? $data['actionCode'] : 'post';
        $this->container['status'] = isset($data['status']) ? $data['status'] : 'unread';
        $this->container['sentAt'] = isset($data['sentAt']) ? $data['sentAt'] : null;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets message
     * @return string
     */
    public function getMessage()
    {
        return $this->container['message'];
    }

    /**
     * Sets message
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->container['message'] = $message;

        return $this;
    }

    /**
     * Gets cover
     * @return string
     */
    public function getCover()
    {
        return $this->container['cover'];
    }

    /**
     * Sets cover
     * @param string $cover
     * @return $this
     */
    public function setCover($cover)
    {
        $this->container['cover'] = $cover;

        return $this;
    }

    /**
     * Gets coverUpload
     * @return \Swagger\Client\Model\MagentoImageUpload
     */
    public function getCoverUpload()
    {
        return $this->container['coverUpload'];
    }

    /**
     * Sets coverUpload
     * @param \Swagger\Client\Model\MagentoImageUpload $coverUpload
     * @return $this
     */
    public function setCoverUpload($coverUpload)
    {
        $this->container['coverUpload'] = $coverUpload;

        return $this;
    }

    /**
     * Gets from
     * @return \Swagger\Client\Model\NotificationFrom
     */
    public function getFrom()
    {
        return $this->container['from'];
    }

    /**
     * Sets from
     * @param \Swagger\Client\Model\NotificationFrom $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->container['from'] = $from;

        return $this;
    }

    /**
     * Gets related
     * @return \Swagger\Client\Model\NotificationRelated
     */
    public function getRelated()
    {
        return $this->container['related'];
    }

    /**
     * Sets related
     * @param \Swagger\Client\Model\NotificationRelated $related
     * @return $this
     */
    public function setRelated($related)
    {
        $this->container['related'] = $related;

        return $this;
    }

    /**
     * Gets link
     * @return string
     */
    public function getLink()
    {
        return $this->container['link'];
    }

    /**
     * Sets link
     * @param string $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->container['link'] = $link;

        return $this;
    }

    /**
     * Gets actionCode
     * @return string
     */
    public function getActionCode()
    {
        return $this->container['actionCode'];
    }

    /**
     * Sets actionCode
     * @param string $actionCode
     * @return $this
     */
    public function setActionCode($actionCode)
    {
        $this->container['actionCode'] = $actionCode;

        return $this;
    }

    /**
     * Gets status
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets sentAt
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->container['sentAt'];
    }

    /**
     * Sets sentAt
     * @param \DateTime $sentAt
     * @return $this
     */
    public function setSentAt($sentAt)
    {
        $this->container['sentAt'] = $sentAt;

        return $this;
    }

    /**
     * Gets id
     * @return double
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param double $id
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this));
    }
}


