<?php
/**
 * UdropshipVendorProduct
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * UdropshipVendorProduct Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class UdropshipVendorProduct implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'UdropshipVendorProduct';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'double',
        'vendorId' => 'double',
        'productId' => 'double',
        'priority' => 'double',
        'carrierCode' => 'string',
        'vendorSku' => 'string',
        'vendorCost' => 'string',
        'stockQty' => 'string',
        'backorders' => 'double',
        'shippingPrice' => 'string',
        'status' => 'double',
        'reservedQty' => 'string',
        'availState' => 'string',
        'availDate' => '\DateTime',
        'relationshipStatus' => 'string',
        'udropshipVendor' => 'object',
        'catalogProduct' => 'object'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'vendorId' => 'vendorId',
        'productId' => 'productId',
        'priority' => 'priority',
        'carrierCode' => 'carrierCode',
        'vendorSku' => 'vendorSku',
        'vendorCost' => 'vendorCost',
        'stockQty' => 'stockQty',
        'backorders' => 'backorders',
        'shippingPrice' => 'shippingPrice',
        'status' => 'status',
        'reservedQty' => 'reservedQty',
        'availState' => 'availState',
        'availDate' => 'availDate',
        'relationshipStatus' => 'relationshipStatus',
        'udropshipVendor' => 'udropshipVendor',
        'catalogProduct' => 'catalogProduct'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'vendorId' => 'setVendorId',
        'productId' => 'setProductId',
        'priority' => 'setPriority',
        'carrierCode' => 'setCarrierCode',
        'vendorSku' => 'setVendorSku',
        'vendorCost' => 'setVendorCost',
        'stockQty' => 'setStockQty',
        'backorders' => 'setBackorders',
        'shippingPrice' => 'setShippingPrice',
        'status' => 'setStatus',
        'reservedQty' => 'setReservedQty',
        'availState' => 'setAvailState',
        'availDate' => 'setAvailDate',
        'relationshipStatus' => 'setRelationshipStatus',
        'udropshipVendor' => 'setUdropshipVendor',
        'catalogProduct' => 'setCatalogProduct'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'vendorId' => 'getVendorId',
        'productId' => 'getProductId',
        'priority' => 'getPriority',
        'carrierCode' => 'getCarrierCode',
        'vendorSku' => 'getVendorSku',
        'vendorCost' => 'getVendorCost',
        'stockQty' => 'getStockQty',
        'backorders' => 'getBackorders',
        'shippingPrice' => 'getShippingPrice',
        'status' => 'getStatus',
        'reservedQty' => 'getReservedQty',
        'availState' => 'getAvailState',
        'availDate' => 'getAvailDate',
        'relationshipStatus' => 'getRelationshipStatus',
        'udropshipVendor' => 'getUdropshipVendor',
        'catalogProduct' => 'getCatalogProduct'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['vendorId'] = isset($data['vendorId']) ? $data['vendorId'] : null;
        $this->container['productId'] = isset($data['productId']) ? $data['productId'] : null;
        $this->container['priority'] = isset($data['priority']) ? $data['priority'] : null;
        $this->container['carrierCode'] = isset($data['carrierCode']) ? $data['carrierCode'] : null;
        $this->container['vendorSku'] = isset($data['vendorSku']) ? $data['vendorSku'] : null;
        $this->container['vendorCost'] = isset($data['vendorCost']) ? $data['vendorCost'] : null;
        $this->container['stockQty'] = isset($data['stockQty']) ? $data['stockQty'] : null;
        $this->container['backorders'] = isset($data['backorders']) ? $data['backorders'] : null;
        $this->container['shippingPrice'] = isset($data['shippingPrice']) ? $data['shippingPrice'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['reservedQty'] = isset($data['reservedQty']) ? $data['reservedQty'] : null;
        $this->container['availState'] = isset($data['availState']) ? $data['availState'] : null;
        $this->container['availDate'] = isset($data['availDate']) ? $data['availDate'] : null;
        $this->container['relationshipStatus'] = isset($data['relationshipStatus']) ? $data['relationshipStatus'] : null;
        $this->container['udropshipVendor'] = isset($data['udropshipVendor']) ? $data['udropshipVendor'] : null;
        $this->container['catalogProduct'] = isset($data['catalogProduct']) ? $data['catalogProduct'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        if ($this->container['id'] === null) {
            $invalid_properties[] = "'id' can't be null";
        }
        if ($this->container['vendorId'] === null) {
            $invalid_properties[] = "'vendorId' can't be null";
        }
        if ($this->container['productId'] === null) {
            $invalid_properties[] = "'productId' can't be null";
        }
        if ($this->container['priority'] === null) {
            $invalid_properties[] = "'priority' can't be null";
        }
        if (!is_null($this->container['carrierCode']) && (strlen($this->container['carrierCode']) > 50)) {
            $invalid_properties[] = "invalid value for 'carrierCode', the character length must be smaller than or equal to 50.";
        }

        if (!is_null($this->container['vendorSku']) && (strlen($this->container['vendorSku']) > 64)) {
            $invalid_properties[] = "invalid value for 'vendorSku', the character length must be smaller than or equal to 64.";
        }

        if ($this->container['backorders'] === null) {
            $invalid_properties[] = "'backorders' can't be null";
        }
        if ($this->container['status'] === null) {
            $invalid_properties[] = "'status' can't be null";
        }
        if (!is_null($this->container['availState']) && (strlen($this->container['availState']) > 32)) {
            $invalid_properties[] = "invalid value for 'availState', the character length must be smaller than or equal to 32.";
        }

        if (!is_null($this->container['relationshipStatus']) && (strlen($this->container['relationshipStatus']) > 255)) {
            $invalid_properties[] = "invalid value for 'relationshipStatus', the character length must be smaller than or equal to 255.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        if ($this->container['id'] === null) {
            return false;
        }
        if ($this->container['vendorId'] === null) {
            return false;
        }
        if ($this->container['productId'] === null) {
            return false;
        }
        if ($this->container['priority'] === null) {
            return false;
        }
        if (strlen($this->container['carrierCode']) > 50) {
            return false;
        }
        if (strlen($this->container['vendorSku']) > 64) {
            return false;
        }
        if ($this->container['backorders'] === null) {
            return false;
        }
        if ($this->container['status'] === null) {
            return false;
        }
        if (strlen($this->container['availState']) > 32) {
            return false;
        }
        if (strlen($this->container['relationshipStatus']) > 255) {
            return false;
        }
        return true;
    }


    /**
     * Gets id
     * @return double
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param double $id
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets vendorId
     * @return double
     */
    public function getVendorId()
    {
        return $this->container['vendorId'];
    }

    /**
     * Sets vendorId
     * @param double $vendorId
     * @return $this
     */
    public function setVendorId($vendorId)
    {
        $this->container['vendorId'] = $vendorId;

        return $this;
    }

    /**
     * Gets productId
     * @return double
     */
    public function getProductId()
    {
        return $this->container['productId'];
    }

    /**
     * Sets productId
     * @param double $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->container['productId'] = $productId;

        return $this;
    }

    /**
     * Gets priority
     * @return double
     */
    public function getPriority()
    {
        return $this->container['priority'];
    }

    /**
     * Sets priority
     * @param double $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->container['priority'] = $priority;

        return $this;
    }

    /**
     * Gets carrierCode
     * @return string
     */
    public function getCarrierCode()
    {
        return $this->container['carrierCode'];
    }

    /**
     * Sets carrierCode
     * @param string $carrierCode
     * @return $this
     */
    public function setCarrierCode($carrierCode)
    {
        if (strlen($carrierCode) > 50) {
            throw new \InvalidArgumentException('invalid length for $carrierCode when calling UdropshipVendorProduct., must be smaller than or equal to 50.');
        }
        $this->container['carrierCode'] = $carrierCode;

        return $this;
    }

    /**
     * Gets vendorSku
     * @return string
     */
    public function getVendorSku()
    {
        return $this->container['vendorSku'];
    }

    /**
     * Sets vendorSku
     * @param string $vendorSku
     * @return $this
     */
    public function setVendorSku($vendorSku)
    {
        if (strlen($vendorSku) > 64) {
            throw new \InvalidArgumentException('invalid length for $vendorSku when calling UdropshipVendorProduct., must be smaller than or equal to 64.');
        }
        $this->container['vendorSku'] = $vendorSku;

        return $this;
    }

    /**
     * Gets vendorCost
     * @return string
     */
    public function getVendorCost()
    {
        return $this->container['vendorCost'];
    }

    /**
     * Sets vendorCost
     * @param string $vendorCost
     * @return $this
     */
    public function setVendorCost($vendorCost)
    {
        $this->container['vendorCost'] = $vendorCost;

        return $this;
    }

    /**
     * Gets stockQty
     * @return string
     */
    public function getStockQty()
    {
        return $this->container['stockQty'];
    }

    /**
     * Sets stockQty
     * @param string $stockQty
     * @return $this
     */
    public function setStockQty($stockQty)
    {
        $this->container['stockQty'] = $stockQty;

        return $this;
    }

    /**
     * Gets backorders
     * @return double
     */
    public function getBackorders()
    {
        return $this->container['backorders'];
    }

    /**
     * Sets backorders
     * @param double $backorders
     * @return $this
     */
    public function setBackorders($backorders)
    {
        $this->container['backorders'] = $backorders;

        return $this;
    }

    /**
     * Gets shippingPrice
     * @return string
     */
    public function getShippingPrice()
    {
        return $this->container['shippingPrice'];
    }

    /**
     * Sets shippingPrice
     * @param string $shippingPrice
     * @return $this
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->container['shippingPrice'] = $shippingPrice;

        return $this;
    }

    /**
     * Gets status
     * @return double
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     * @param double $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets reservedQty
     * @return string
     */
    public function getReservedQty()
    {
        return $this->container['reservedQty'];
    }

    /**
     * Sets reservedQty
     * @param string $reservedQty
     * @return $this
     */
    public function setReservedQty($reservedQty)
    {
        $this->container['reservedQty'] = $reservedQty;

        return $this;
    }

    /**
     * Gets availState
     * @return string
     */
    public function getAvailState()
    {
        return $this->container['availState'];
    }

    /**
     * Sets availState
     * @param string $availState
     * @return $this
     */
    public function setAvailState($availState)
    {
        if (strlen($availState) > 32) {
            throw new \InvalidArgumentException('invalid length for $availState when calling UdropshipVendorProduct., must be smaller than or equal to 32.');
        }
        $this->container['availState'] = $availState;

        return $this;
    }

    /**
     * Gets availDate
     * @return \DateTime
     */
    public function getAvailDate()
    {
        return $this->container['availDate'];
    }

    /**
     * Sets availDate
     * @param \DateTime $availDate
     * @return $this
     */
    public function setAvailDate($availDate)
    {
        $this->container['availDate'] = $availDate;

        return $this;
    }

    /**
     * Gets relationshipStatus
     * @return string
     */
    public function getRelationshipStatus()
    {
        return $this->container['relationshipStatus'];
    }

    /**
     * Sets relationshipStatus
     * @param string $relationshipStatus
     * @return $this
     */
    public function setRelationshipStatus($relationshipStatus)
    {
        if (strlen($relationshipStatus) > 255) {
            throw new \InvalidArgumentException('invalid length for $relationshipStatus when calling UdropshipVendorProduct., must be smaller than or equal to 255.');
        }
        $this->container['relationshipStatus'] = $relationshipStatus;

        return $this;
    }

    /**
     * Gets udropshipVendor
     * @return object
     */
    public function getUdropshipVendor()
    {
        return $this->container['udropshipVendor'];
    }

    /**
     * Sets udropshipVendor
     * @param object $udropshipVendor
     * @return $this
     */
    public function setUdropshipVendor($udropshipVendor)
    {
        $this->container['udropshipVendor'] = $udropshipVendor;

        return $this;
    }

    /**
     * Gets catalogProduct
     * @return object
     */
    public function getCatalogProduct()
    {
        return $this->container['catalogProduct'];
    }

    /**
     * Sets catalogProduct
     * @param object $catalogProduct
     * @return $this
     */
    public function setCatalogProduct($catalogProduct)
    {
        $this->container['catalogProduct'] = $catalogProduct;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this));
    }
}


