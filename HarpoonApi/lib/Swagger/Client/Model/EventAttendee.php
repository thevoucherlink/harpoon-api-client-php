<?php
/**
 * EventAttendee
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * EventAttendee Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class EventAttendee implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'EventAttendee';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'joinedAt' => '\DateTime',
        'email' => 'string',
        'firstName' => 'string',
        'lastName' => 'string',
        'dob' => 'string',
        'gender' => 'string',
        'password' => 'string',
        'profilePicture' => 'string',
        'cover' => 'string',
        'profilePictureUpload' => '\Swagger\Client\Model\MagentoImageUpload',
        'coverUpload' => '\Swagger\Client\Model\MagentoImageUpload',
        'metadata' => 'object',
        'connection' => '\Swagger\Client\Model\CustomerConnection',
        'followingCount' => 'double',
        'followerCount' => 'double',
        'isFollowed' => 'bool',
        'isFollower' => 'bool',
        'notificationCount' => 'double',
        'authorizationCode' => 'string',
        'id' => 'double'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'joinedAt' => 'joinedAt',
        'email' => 'email',
        'firstName' => 'firstName',
        'lastName' => 'lastName',
        'dob' => 'dob',
        'gender' => 'gender',
        'password' => 'password',
        'profilePicture' => 'profilePicture',
        'cover' => 'cover',
        'profilePictureUpload' => 'profilePictureUpload',
        'coverUpload' => 'coverUpload',
        'metadata' => 'metadata',
        'connection' => 'connection',
        'followingCount' => 'followingCount',
        'followerCount' => 'followerCount',
        'isFollowed' => 'isFollowed',
        'isFollower' => 'isFollower',
        'notificationCount' => 'notificationCount',
        'authorizationCode' => 'authorizationCode',
        'id' => 'id'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'joinedAt' => 'setJoinedAt',
        'email' => 'setEmail',
        'firstName' => 'setFirstName',
        'lastName' => 'setLastName',
        'dob' => 'setDob',
        'gender' => 'setGender',
        'password' => 'setPassword',
        'profilePicture' => 'setProfilePicture',
        'cover' => 'setCover',
        'profilePictureUpload' => 'setProfilePictureUpload',
        'coverUpload' => 'setCoverUpload',
        'metadata' => 'setMetadata',
        'connection' => 'setConnection',
        'followingCount' => 'setFollowingCount',
        'followerCount' => 'setFollowerCount',
        'isFollowed' => 'setIsFollowed',
        'isFollower' => 'setIsFollower',
        'notificationCount' => 'setNotificationCount',
        'authorizationCode' => 'setAuthorizationCode',
        'id' => 'setId'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'joinedAt' => 'getJoinedAt',
        'email' => 'getEmail',
        'firstName' => 'getFirstName',
        'lastName' => 'getLastName',
        'dob' => 'getDob',
        'gender' => 'getGender',
        'password' => 'getPassword',
        'profilePicture' => 'getProfilePicture',
        'cover' => 'getCover',
        'profilePictureUpload' => 'getProfilePictureUpload',
        'coverUpload' => 'getCoverUpload',
        'metadata' => 'getMetadata',
        'connection' => 'getConnection',
        'followingCount' => 'getFollowingCount',
        'followerCount' => 'getFollowerCount',
        'isFollowed' => 'getIsFollowed',
        'isFollower' => 'getIsFollower',
        'notificationCount' => 'getNotificationCount',
        'authorizationCode' => 'getAuthorizationCode',
        'id' => 'getId'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['joinedAt'] = isset($data['joinedAt']) ? $data['joinedAt'] : null;
        $this->container['email'] = isset($data['email']) ? $data['email'] : null;
        $this->container['firstName'] = isset($data['firstName']) ? $data['firstName'] : null;
        $this->container['lastName'] = isset($data['lastName']) ? $data['lastName'] : null;
        $this->container['dob'] = isset($data['dob']) ? $data['dob'] : null;
        $this->container['gender'] = isset($data['gender']) ? $data['gender'] : 'other';
        $this->container['password'] = isset($data['password']) ? $data['password'] : null;
        $this->container['profilePicture'] = isset($data['profilePicture']) ? $data['profilePicture'] : null;
        $this->container['cover'] = isset($data['cover']) ? $data['cover'] : null;
        $this->container['profilePictureUpload'] = isset($data['profilePictureUpload']) ? $data['profilePictureUpload'] : null;
        $this->container['coverUpload'] = isset($data['coverUpload']) ? $data['coverUpload'] : null;
        $this->container['metadata'] = isset($data['metadata']) ? $data['metadata'] : null;
        $this->container['connection'] = isset($data['connection']) ? $data['connection'] : null;
        $this->container['followingCount'] = isset($data['followingCount']) ? $data['followingCount'] : 0.0;
        $this->container['followerCount'] = isset($data['followerCount']) ? $data['followerCount'] : 0.0;
        $this->container['isFollowed'] = isset($data['isFollowed']) ? $data['isFollowed'] : false;
        $this->container['isFollower'] = isset($data['isFollower']) ? $data['isFollower'] : false;
        $this->container['notificationCount'] = isset($data['notificationCount']) ? $data['notificationCount'] : 0.0;
        $this->container['authorizationCode'] = isset($data['authorizationCode']) ? $data['authorizationCode'] : null;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        if ($this->container['email'] === null) {
            $invalid_properties[] = "'email' can't be null";
        }
        if ($this->container['firstName'] === null) {
            $invalid_properties[] = "'firstName' can't be null";
        }
        if ($this->container['lastName'] === null) {
            $invalid_properties[] = "'lastName' can't be null";
        }
        if ($this->container['password'] === null) {
            $invalid_properties[] = "'password' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        if ($this->container['email'] === null) {
            return false;
        }
        if ($this->container['firstName'] === null) {
            return false;
        }
        if ($this->container['lastName'] === null) {
            return false;
        }
        if ($this->container['password'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets joinedAt
     * @return \DateTime
     */
    public function getJoinedAt()
    {
        return $this->container['joinedAt'];
    }

    /**
     * Sets joinedAt
     * @param \DateTime $joinedAt
     * @return $this
     */
    public function setJoinedAt($joinedAt)
    {
        $this->container['joinedAt'] = $joinedAt;

        return $this;
    }

    /**
     * Gets email
     * @return string
     */
    public function getEmail()
    {
        return $this->container['email'];
    }

    /**
     * Sets email
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->container['email'] = $email;

        return $this;
    }

    /**
     * Gets firstName
     * @return string
     */
    public function getFirstName()
    {
        return $this->container['firstName'];
    }

    /**
     * Sets firstName
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->container['firstName'] = $firstName;

        return $this;
    }

    /**
     * Gets lastName
     * @return string
     */
    public function getLastName()
    {
        return $this->container['lastName'];
    }

    /**
     * Sets lastName
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->container['lastName'] = $lastName;

        return $this;
    }

    /**
     * Gets dob
     * @return string
     */
    public function getDob()
    {
        return $this->container['dob'];
    }

    /**
     * Sets dob
     * @param string $dob
     * @return $this
     */
    public function setDob($dob)
    {
        $this->container['dob'] = $dob;

        return $this;
    }

    /**
     * Gets gender
     * @return string
     */
    public function getGender()
    {
        return $this->container['gender'];
    }

    /**
     * Sets gender
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->container['gender'] = $gender;

        return $this;
    }

    /**
     * Gets password
     * @return string
     */
    public function getPassword()
    {
        return $this->container['password'];
    }

    /**
     * Sets password
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->container['password'] = $password;

        return $this;
    }

    /**
     * Gets profilePicture
     * @return string
     */
    public function getProfilePicture()
    {
        return $this->container['profilePicture'];
    }

    /**
     * Sets profilePicture
     * @param string $profilePicture
     * @return $this
     */
    public function setProfilePicture($profilePicture)
    {
        $this->container['profilePicture'] = $profilePicture;

        return $this;
    }

    /**
     * Gets cover
     * @return string
     */
    public function getCover()
    {
        return $this->container['cover'];
    }

    /**
     * Sets cover
     * @param string $cover
     * @return $this
     */
    public function setCover($cover)
    {
        $this->container['cover'] = $cover;

        return $this;
    }

    /**
     * Gets profilePictureUpload
     * @return \Swagger\Client\Model\MagentoImageUpload
     */
    public function getProfilePictureUpload()
    {
        return $this->container['profilePictureUpload'];
    }

    /**
     * Sets profilePictureUpload
     * @param \Swagger\Client\Model\MagentoImageUpload $profilePictureUpload
     * @return $this
     */
    public function setProfilePictureUpload($profilePictureUpload)
    {
        $this->container['profilePictureUpload'] = $profilePictureUpload;

        return $this;
    }

    /**
     * Gets coverUpload
     * @return \Swagger\Client\Model\MagentoImageUpload
     */
    public function getCoverUpload()
    {
        return $this->container['coverUpload'];
    }

    /**
     * Sets coverUpload
     * @param \Swagger\Client\Model\MagentoImageUpload $coverUpload
     * @return $this
     */
    public function setCoverUpload($coverUpload)
    {
        $this->container['coverUpload'] = $coverUpload;

        return $this;
    }

    /**
     * Gets metadata
     * @return object
     */
    public function getMetadata()
    {
        return $this->container['metadata'];
    }

    /**
     * Sets metadata
     * @param object $metadata
     * @return $this
     */
    public function setMetadata($metadata)
    {
        $this->container['metadata'] = $metadata;

        return $this;
    }

    /**
     * Gets connection
     * @return \Swagger\Client\Model\CustomerConnection
     */
    public function getConnection()
    {
        return $this->container['connection'];
    }

    /**
     * Sets connection
     * @param \Swagger\Client\Model\CustomerConnection $connection
     * @return $this
     */
    public function setConnection($connection)
    {
        $this->container['connection'] = $connection;

        return $this;
    }

    /**
     * Gets followingCount
     * @return double
     */
    public function getFollowingCount()
    {
        return $this->container['followingCount'];
    }

    /**
     * Sets followingCount
     * @param double $followingCount
     * @return $this
     */
    public function setFollowingCount($followingCount)
    {
        $this->container['followingCount'] = $followingCount;

        return $this;
    }

    /**
     * Gets followerCount
     * @return double
     */
    public function getFollowerCount()
    {
        return $this->container['followerCount'];
    }

    /**
     * Sets followerCount
     * @param double $followerCount
     * @return $this
     */
    public function setFollowerCount($followerCount)
    {
        $this->container['followerCount'] = $followerCount;

        return $this;
    }

    /**
     * Gets isFollowed
     * @return bool
     */
    public function getIsFollowed()
    {
        return $this->container['isFollowed'];
    }

    /**
     * Sets isFollowed
     * @param bool $isFollowed
     * @return $this
     */
    public function setIsFollowed($isFollowed)
    {
        $this->container['isFollowed'] = $isFollowed;

        return $this;
    }

    /**
     * Gets isFollower
     * @return bool
     */
    public function getIsFollower()
    {
        return $this->container['isFollower'];
    }

    /**
     * Sets isFollower
     * @param bool $isFollower
     * @return $this
     */
    public function setIsFollower($isFollower)
    {
        $this->container['isFollower'] = $isFollower;

        return $this;
    }

    /**
     * Gets notificationCount
     * @return double
     */
    public function getNotificationCount()
    {
        return $this->container['notificationCount'];
    }

    /**
     * Sets notificationCount
     * @param double $notificationCount
     * @return $this
     */
    public function setNotificationCount($notificationCount)
    {
        $this->container['notificationCount'] = $notificationCount;

        return $this;
    }

    /**
     * Gets authorizationCode
     * @return string
     */
    public function getAuthorizationCode()
    {
        return $this->container['authorizationCode'];
    }

    /**
     * Sets authorizationCode
     * @param string $authorizationCode
     * @return $this
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->container['authorizationCode'] = $authorizationCode;

        return $this;
    }

    /**
     * Gets id
     * @return double
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param double $id
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this));
    }
}


