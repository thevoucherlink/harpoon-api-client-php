<?php
/**
 * HarpoonHpublicVendorconfig
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * HarpoonHpublicVendorconfig Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class HarpoonHpublicVendorconfig implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'HarpoonHpublicVendorconfig';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'double',
        'type' => 'double',
        'category' => 'string',
        'logo' => 'string',
        'cover' => 'string',
        'facebookPageId' => 'string',
        'facebookPageToken' => 'string',
        'facebookEventEnabled' => 'double',
        'facebookEventUpdatedAt' => '\DateTime',
        'facebookEventCount' => 'double',
        'facebookFeedEnabled' => 'double',
        'facebookFeedUpdatedAt' => '\DateTime',
        'facebookFeedCount' => 'double',
        'stripePublishableKey' => 'string',
        'stripeRefreshToken' => 'string',
        'stripeAccessToken' => 'string',
        'feesEventTicket' => 'string',
        'stripeUserId' => 'string',
        'vendorconfigId' => 'double',
        'createdAt' => '\DateTime',
        'updatedAt' => '\DateTime'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'type' => 'type',
        'category' => 'category',
        'logo' => 'logo',
        'cover' => 'cover',
        'facebookPageId' => 'facebookPageId',
        'facebookPageToken' => 'facebookPageToken',
        'facebookEventEnabled' => 'facebookEventEnabled',
        'facebookEventUpdatedAt' => 'facebookEventUpdatedAt',
        'facebookEventCount' => 'facebookEventCount',
        'facebookFeedEnabled' => 'facebookFeedEnabled',
        'facebookFeedUpdatedAt' => 'facebookFeedUpdatedAt',
        'facebookFeedCount' => 'facebookFeedCount',
        'stripePublishableKey' => 'stripePublishableKey',
        'stripeRefreshToken' => 'stripeRefreshToken',
        'stripeAccessToken' => 'stripeAccessToken',
        'feesEventTicket' => 'feesEventTicket',
        'stripeUserId' => 'stripeUserId',
        'vendorconfigId' => 'vendorconfigId',
        'createdAt' => 'createdAt',
        'updatedAt' => 'updatedAt'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'type' => 'setType',
        'category' => 'setCategory',
        'logo' => 'setLogo',
        'cover' => 'setCover',
        'facebookPageId' => 'setFacebookPageId',
        'facebookPageToken' => 'setFacebookPageToken',
        'facebookEventEnabled' => 'setFacebookEventEnabled',
        'facebookEventUpdatedAt' => 'setFacebookEventUpdatedAt',
        'facebookEventCount' => 'setFacebookEventCount',
        'facebookFeedEnabled' => 'setFacebookFeedEnabled',
        'facebookFeedUpdatedAt' => 'setFacebookFeedUpdatedAt',
        'facebookFeedCount' => 'setFacebookFeedCount',
        'stripePublishableKey' => 'setStripePublishableKey',
        'stripeRefreshToken' => 'setStripeRefreshToken',
        'stripeAccessToken' => 'setStripeAccessToken',
        'feesEventTicket' => 'setFeesEventTicket',
        'stripeUserId' => 'setStripeUserId',
        'vendorconfigId' => 'setVendorconfigId',
        'createdAt' => 'setCreatedAt',
        'updatedAt' => 'setUpdatedAt'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'type' => 'getType',
        'category' => 'getCategory',
        'logo' => 'getLogo',
        'cover' => 'getCover',
        'facebookPageId' => 'getFacebookPageId',
        'facebookPageToken' => 'getFacebookPageToken',
        'facebookEventEnabled' => 'getFacebookEventEnabled',
        'facebookEventUpdatedAt' => 'getFacebookEventUpdatedAt',
        'facebookEventCount' => 'getFacebookEventCount',
        'facebookFeedEnabled' => 'getFacebookFeedEnabled',
        'facebookFeedUpdatedAt' => 'getFacebookFeedUpdatedAt',
        'facebookFeedCount' => 'getFacebookFeedCount',
        'stripePublishableKey' => 'getStripePublishableKey',
        'stripeRefreshToken' => 'getStripeRefreshToken',
        'stripeAccessToken' => 'getStripeAccessToken',
        'feesEventTicket' => 'getFeesEventTicket',
        'stripeUserId' => 'getStripeUserId',
        'vendorconfigId' => 'getVendorconfigId',
        'createdAt' => 'getCreatedAt',
        'updatedAt' => 'getUpdatedAt'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['category'] = isset($data['category']) ? $data['category'] : null;
        $this->container['logo'] = isset($data['logo']) ? $data['logo'] : null;
        $this->container['cover'] = isset($data['cover']) ? $data['cover'] : null;
        $this->container['facebookPageId'] = isset($data['facebookPageId']) ? $data['facebookPageId'] : null;
        $this->container['facebookPageToken'] = isset($data['facebookPageToken']) ? $data['facebookPageToken'] : null;
        $this->container['facebookEventEnabled'] = isset($data['facebookEventEnabled']) ? $data['facebookEventEnabled'] : null;
        $this->container['facebookEventUpdatedAt'] = isset($data['facebookEventUpdatedAt']) ? $data['facebookEventUpdatedAt'] : null;
        $this->container['facebookEventCount'] = isset($data['facebookEventCount']) ? $data['facebookEventCount'] : null;
        $this->container['facebookFeedEnabled'] = isset($data['facebookFeedEnabled']) ? $data['facebookFeedEnabled'] : null;
        $this->container['facebookFeedUpdatedAt'] = isset($data['facebookFeedUpdatedAt']) ? $data['facebookFeedUpdatedAt'] : null;
        $this->container['facebookFeedCount'] = isset($data['facebookFeedCount']) ? $data['facebookFeedCount'] : null;
        $this->container['stripePublishableKey'] = isset($data['stripePublishableKey']) ? $data['stripePublishableKey'] : null;
        $this->container['stripeRefreshToken'] = isset($data['stripeRefreshToken']) ? $data['stripeRefreshToken'] : null;
        $this->container['stripeAccessToken'] = isset($data['stripeAccessToken']) ? $data['stripeAccessToken'] : null;
        $this->container['feesEventTicket'] = isset($data['feesEventTicket']) ? $data['feesEventTicket'] : null;
        $this->container['stripeUserId'] = isset($data['stripeUserId']) ? $data['stripeUserId'] : null;
        $this->container['vendorconfigId'] = isset($data['vendorconfigId']) ? $data['vendorconfigId'] : null;
        $this->container['createdAt'] = isset($data['createdAt']) ? $data['createdAt'] : null;
        $this->container['updatedAt'] = isset($data['updatedAt']) ? $data['updatedAt'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        if ($this->container['id'] === null) {
            $invalid_properties[] = "'id' can't be null";
        }
        if ($this->container['type'] === null) {
            $invalid_properties[] = "'type' can't be null";
        }
        if ($this->container['category'] === null) {
            $invalid_properties[] = "'category' can't be null";
        }
        if ((strlen($this->container['category']) > 65535)) {
            $invalid_properties[] = "invalid value for 'category', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['logo'] === null) {
            $invalid_properties[] = "'logo' can't be null";
        }
        if ((strlen($this->container['logo']) > 65535)) {
            $invalid_properties[] = "invalid value for 'logo', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['cover'] === null) {
            $invalid_properties[] = "'cover' can't be null";
        }
        if ((strlen($this->container['cover']) > 65535)) {
            $invalid_properties[] = "invalid value for 'cover', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['facebookPageId'] === null) {
            $invalid_properties[] = "'facebookPageId' can't be null";
        }
        if ((strlen($this->container['facebookPageId']) > 65535)) {
            $invalid_properties[] = "invalid value for 'facebookPageId', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['facebookPageToken'] === null) {
            $invalid_properties[] = "'facebookPageToken' can't be null";
        }
        if ((strlen($this->container['facebookPageToken']) > 65535)) {
            $invalid_properties[] = "invalid value for 'facebookPageToken', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['facebookEventEnabled'] === null) {
            $invalid_properties[] = "'facebookEventEnabled' can't be null";
        }
        if ($this->container['facebookEventUpdatedAt'] === null) {
            $invalid_properties[] = "'facebookEventUpdatedAt' can't be null";
        }
        if ($this->container['facebookEventCount'] === null) {
            $invalid_properties[] = "'facebookEventCount' can't be null";
        }
        if ($this->container['facebookFeedEnabled'] === null) {
            $invalid_properties[] = "'facebookFeedEnabled' can't be null";
        }
        if ($this->container['facebookFeedUpdatedAt'] === null) {
            $invalid_properties[] = "'facebookFeedUpdatedAt' can't be null";
        }
        if ($this->container['facebookFeedCount'] === null) {
            $invalid_properties[] = "'facebookFeedCount' can't be null";
        }
        if ($this->container['stripePublishableKey'] === null) {
            $invalid_properties[] = "'stripePublishableKey' can't be null";
        }
        if ((strlen($this->container['stripePublishableKey']) > 65535)) {
            $invalid_properties[] = "invalid value for 'stripePublishableKey', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['stripeRefreshToken'] === null) {
            $invalid_properties[] = "'stripeRefreshToken' can't be null";
        }
        if ((strlen($this->container['stripeRefreshToken']) > 65535)) {
            $invalid_properties[] = "invalid value for 'stripeRefreshToken', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['stripeAccessToken'] === null) {
            $invalid_properties[] = "'stripeAccessToken' can't be null";
        }
        if ((strlen($this->container['stripeAccessToken']) > 65535)) {
            $invalid_properties[] = "invalid value for 'stripeAccessToken', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['feesEventTicket'] === null) {
            $invalid_properties[] = "'feesEventTicket' can't be null";
        }
        if ((strlen($this->container['feesEventTicket']) > 65535)) {
            $invalid_properties[] = "invalid value for 'feesEventTicket', the character length must be smaller than or equal to 65535.";
        }

        if ($this->container['stripeUserId'] === null) {
            $invalid_properties[] = "'stripeUserId' can't be null";
        }
        if ((strlen($this->container['stripeUserId']) > 255)) {
            $invalid_properties[] = "invalid value for 'stripeUserId', the character length must be smaller than or equal to 255.";
        }

        if ($this->container['vendorconfigId'] === null) {
            $invalid_properties[] = "'vendorconfigId' can't be null";
        }
        if ($this->container['createdAt'] === null) {
            $invalid_properties[] = "'createdAt' can't be null";
        }
        if ($this->container['updatedAt'] === null) {
            $invalid_properties[] = "'updatedAt' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        if ($this->container['id'] === null) {
            return false;
        }
        if ($this->container['type'] === null) {
            return false;
        }
        if ($this->container['category'] === null) {
            return false;
        }
        if (strlen($this->container['category']) > 65535) {
            return false;
        }
        if ($this->container['logo'] === null) {
            return false;
        }
        if (strlen($this->container['logo']) > 65535) {
            return false;
        }
        if ($this->container['cover'] === null) {
            return false;
        }
        if (strlen($this->container['cover']) > 65535) {
            return false;
        }
        if ($this->container['facebookPageId'] === null) {
            return false;
        }
        if (strlen($this->container['facebookPageId']) > 65535) {
            return false;
        }
        if ($this->container['facebookPageToken'] === null) {
            return false;
        }
        if (strlen($this->container['facebookPageToken']) > 65535) {
            return false;
        }
        if ($this->container['facebookEventEnabled'] === null) {
            return false;
        }
        if ($this->container['facebookEventUpdatedAt'] === null) {
            return false;
        }
        if ($this->container['facebookEventCount'] === null) {
            return false;
        }
        if ($this->container['facebookFeedEnabled'] === null) {
            return false;
        }
        if ($this->container['facebookFeedUpdatedAt'] === null) {
            return false;
        }
        if ($this->container['facebookFeedCount'] === null) {
            return false;
        }
        if ($this->container['stripePublishableKey'] === null) {
            return false;
        }
        if (strlen($this->container['stripePublishableKey']) > 65535) {
            return false;
        }
        if ($this->container['stripeRefreshToken'] === null) {
            return false;
        }
        if (strlen($this->container['stripeRefreshToken']) > 65535) {
            return false;
        }
        if ($this->container['stripeAccessToken'] === null) {
            return false;
        }
        if (strlen($this->container['stripeAccessToken']) > 65535) {
            return false;
        }
        if ($this->container['feesEventTicket'] === null) {
            return false;
        }
        if (strlen($this->container['feesEventTicket']) > 65535) {
            return false;
        }
        if ($this->container['stripeUserId'] === null) {
            return false;
        }
        if (strlen($this->container['stripeUserId']) > 255) {
            return false;
        }
        if ($this->container['vendorconfigId'] === null) {
            return false;
        }
        if ($this->container['createdAt'] === null) {
            return false;
        }
        if ($this->container['updatedAt'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets id
     * @return double
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param double $id
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets type
     * @return double
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     * @param double $type
     * @return $this
     */
    public function setType($type)
    {
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets category
     * @return string
     */
    public function getCategory()
    {
        return $this->container['category'];
    }

    /**
     * Sets category
     * @param string $category
     * @return $this
     */
    public function setCategory($category)
    {
        if (strlen($category) > 65535) {
            throw new \InvalidArgumentException('invalid length for $category when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['category'] = $category;

        return $this;
    }

    /**
     * Gets logo
     * @return string
     */
    public function getLogo()
    {
        return $this->container['logo'];
    }

    /**
     * Sets logo
     * @param string $logo
     * @return $this
     */
    public function setLogo($logo)
    {
        if (strlen($logo) > 65535) {
            throw new \InvalidArgumentException('invalid length for $logo when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['logo'] = $logo;

        return $this;
    }

    /**
     * Gets cover
     * @return string
     */
    public function getCover()
    {
        return $this->container['cover'];
    }

    /**
     * Sets cover
     * @param string $cover
     * @return $this
     */
    public function setCover($cover)
    {
        if (strlen($cover) > 65535) {
            throw new \InvalidArgumentException('invalid length for $cover when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['cover'] = $cover;

        return $this;
    }

    /**
     * Gets facebookPageId
     * @return string
     */
    public function getFacebookPageId()
    {
        return $this->container['facebookPageId'];
    }

    /**
     * Sets facebookPageId
     * @param string $facebookPageId
     * @return $this
     */
    public function setFacebookPageId($facebookPageId)
    {
        if (strlen($facebookPageId) > 65535) {
            throw new \InvalidArgumentException('invalid length for $facebookPageId when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['facebookPageId'] = $facebookPageId;

        return $this;
    }

    /**
     * Gets facebookPageToken
     * @return string
     */
    public function getFacebookPageToken()
    {
        return $this->container['facebookPageToken'];
    }

    /**
     * Sets facebookPageToken
     * @param string $facebookPageToken
     * @return $this
     */
    public function setFacebookPageToken($facebookPageToken)
    {
        if (strlen($facebookPageToken) > 65535) {
            throw new \InvalidArgumentException('invalid length for $facebookPageToken when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['facebookPageToken'] = $facebookPageToken;

        return $this;
    }

    /**
     * Gets facebookEventEnabled
     * @return double
     */
    public function getFacebookEventEnabled()
    {
        return $this->container['facebookEventEnabled'];
    }

    /**
     * Sets facebookEventEnabled
     * @param double $facebookEventEnabled
     * @return $this
     */
    public function setFacebookEventEnabled($facebookEventEnabled)
    {
        $this->container['facebookEventEnabled'] = $facebookEventEnabled;

        return $this;
    }

    /**
     * Gets facebookEventUpdatedAt
     * @return \DateTime
     */
    public function getFacebookEventUpdatedAt()
    {
        return $this->container['facebookEventUpdatedAt'];
    }

    /**
     * Sets facebookEventUpdatedAt
     * @param \DateTime $facebookEventUpdatedAt
     * @return $this
     */
    public function setFacebookEventUpdatedAt($facebookEventUpdatedAt)
    {
        $this->container['facebookEventUpdatedAt'] = $facebookEventUpdatedAt;

        return $this;
    }

    /**
     * Gets facebookEventCount
     * @return double
     */
    public function getFacebookEventCount()
    {
        return $this->container['facebookEventCount'];
    }

    /**
     * Sets facebookEventCount
     * @param double $facebookEventCount
     * @return $this
     */
    public function setFacebookEventCount($facebookEventCount)
    {
        $this->container['facebookEventCount'] = $facebookEventCount;

        return $this;
    }

    /**
     * Gets facebookFeedEnabled
     * @return double
     */
    public function getFacebookFeedEnabled()
    {
        return $this->container['facebookFeedEnabled'];
    }

    /**
     * Sets facebookFeedEnabled
     * @param double $facebookFeedEnabled
     * @return $this
     */
    public function setFacebookFeedEnabled($facebookFeedEnabled)
    {
        $this->container['facebookFeedEnabled'] = $facebookFeedEnabled;

        return $this;
    }

    /**
     * Gets facebookFeedUpdatedAt
     * @return \DateTime
     */
    public function getFacebookFeedUpdatedAt()
    {
        return $this->container['facebookFeedUpdatedAt'];
    }

    /**
     * Sets facebookFeedUpdatedAt
     * @param \DateTime $facebookFeedUpdatedAt
     * @return $this
     */
    public function setFacebookFeedUpdatedAt($facebookFeedUpdatedAt)
    {
        $this->container['facebookFeedUpdatedAt'] = $facebookFeedUpdatedAt;

        return $this;
    }

    /**
     * Gets facebookFeedCount
     * @return double
     */
    public function getFacebookFeedCount()
    {
        return $this->container['facebookFeedCount'];
    }

    /**
     * Sets facebookFeedCount
     * @param double $facebookFeedCount
     * @return $this
     */
    public function setFacebookFeedCount($facebookFeedCount)
    {
        $this->container['facebookFeedCount'] = $facebookFeedCount;

        return $this;
    }

    /**
     * Gets stripePublishableKey
     * @return string
     */
    public function getStripePublishableKey()
    {
        return $this->container['stripePublishableKey'];
    }

    /**
     * Sets stripePublishableKey
     * @param string $stripePublishableKey
     * @return $this
     */
    public function setStripePublishableKey($stripePublishableKey)
    {
        if (strlen($stripePublishableKey) > 65535) {
            throw new \InvalidArgumentException('invalid length for $stripePublishableKey when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['stripePublishableKey'] = $stripePublishableKey;

        return $this;
    }

    /**
     * Gets stripeRefreshToken
     * @return string
     */
    public function getStripeRefreshToken()
    {
        return $this->container['stripeRefreshToken'];
    }

    /**
     * Sets stripeRefreshToken
     * @param string $stripeRefreshToken
     * @return $this
     */
    public function setStripeRefreshToken($stripeRefreshToken)
    {
        if (strlen($stripeRefreshToken) > 65535) {
            throw new \InvalidArgumentException('invalid length for $stripeRefreshToken when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['stripeRefreshToken'] = $stripeRefreshToken;

        return $this;
    }

    /**
     * Gets stripeAccessToken
     * @return string
     */
    public function getStripeAccessToken()
    {
        return $this->container['stripeAccessToken'];
    }

    /**
     * Sets stripeAccessToken
     * @param string $stripeAccessToken
     * @return $this
     */
    public function setStripeAccessToken($stripeAccessToken)
    {
        if (strlen($stripeAccessToken) > 65535) {
            throw new \InvalidArgumentException('invalid length for $stripeAccessToken when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['stripeAccessToken'] = $stripeAccessToken;

        return $this;
    }

    /**
     * Gets feesEventTicket
     * @return string
     */
    public function getFeesEventTicket()
    {
        return $this->container['feesEventTicket'];
    }

    /**
     * Sets feesEventTicket
     * @param string $feesEventTicket
     * @return $this
     */
    public function setFeesEventTicket($feesEventTicket)
    {
        if (strlen($feesEventTicket) > 65535) {
            throw new \InvalidArgumentException('invalid length for $feesEventTicket when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 65535.');
        }
        $this->container['feesEventTicket'] = $feesEventTicket;

        return $this;
    }

    /**
     * Gets stripeUserId
     * @return string
     */
    public function getStripeUserId()
    {
        return $this->container['stripeUserId'];
    }

    /**
     * Sets stripeUserId
     * @param string $stripeUserId
     * @return $this
     */
    public function setStripeUserId($stripeUserId)
    {
        if (strlen($stripeUserId) > 255) {
            throw new \InvalidArgumentException('invalid length for $stripeUserId when calling HarpoonHpublicVendorconfig., must be smaller than or equal to 255.');
        }
        $this->container['stripeUserId'] = $stripeUserId;

        return $this;
    }

    /**
     * Gets vendorconfigId
     * @return double
     */
    public function getVendorconfigId()
    {
        return $this->container['vendorconfigId'];
    }

    /**
     * Sets vendorconfigId
     * @param double $vendorconfigId
     * @return $this
     */
    public function setVendorconfigId($vendorconfigId)
    {
        $this->container['vendorconfigId'] = $vendorconfigId;

        return $this;
    }

    /**
     * Gets createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->container['createdAt'];
    }

    /**
     * Sets createdAt
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->container['createdAt'] = $createdAt;

        return $this;
    }

    /**
     * Gets updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->container['updatedAt'];
    }

    /**
     * Sets updatedAt
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->container['updatedAt'] = $updatedAt;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Harpoon\Api\ObjectSerializer::sanitizeForSerialization($this));
    }
}


