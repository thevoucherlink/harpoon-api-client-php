# TritonConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationId** | **string** |  | 
**stationName** | **string** |  | [optional] 
**mp3Mount** | **string** |  | [optional] 
**aacMount** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


