# EventCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**\Swagger\Client\Model\EventCheckoutTicketData[]**](EventCheckoutTicketData.md) | Tickets to checkout | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


