# CouponCheckout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **double** |  | [optional] 
**name** | **string** |  | [optional] 
**code** | [**\Swagger\Client\Model\CouponPurchaseCode**](CouponPurchaseCode.md) |  | [optional] 
**isAvailable** | **bool** |  | [optional] [default to false]
**createdAt** | [**\DateTime**](Date.md) |  | [optional] 
**expiredAt** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedAt** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedByTerminal** | **string** |  | [optional] 
**redemptionType** | **string** |  | [optional] 
**status** | **string** |  | [optional] [default to 'unknown']
**unlockTime** | **double** |  | [optional] [default to 15.0]
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


