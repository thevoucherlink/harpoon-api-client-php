# Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**productCount** | **double** |  | [optional] [default to 0.0]
**hasChildren** | **bool** |  | [optional] [default to false]
**parent** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**isPrimary** | **bool** |  | [optional] [default to false]
**children** | [**\Swagger\Client\Model\Category[]**](Category.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


