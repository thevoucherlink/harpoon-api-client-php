# UdropshipVendorProductAssoc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isUdmulti** | **double** |  | [optional] 
**id** | **double** |  | 
**isAttribute** | **double** |  | 
**productId** | **double** |  | 
**vendorId** | **double** |  | 
**udropshipVendor** | **object** |  | [optional] 
**catalogProduct** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


