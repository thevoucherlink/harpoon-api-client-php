# AwEventbookingTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**eventTicketId** | **double** |  | 
**orderItemId** | **double** |  | 
**code** | **string** |  | 
**redeemed** | **double** |  | 
**paymentStatus** | **double** |  | 
**createdAt** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedAt** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedByTerminal** | **string** |  | [optional] 
**fee** | **string** |  | 
**isPassOnFee** | **double** |  | 
**competitionwinnerId** | **double** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


