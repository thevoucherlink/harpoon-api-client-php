# CustomerUpdateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **string** | New first name for the Customer | 
**lastName** | **string** | New last name for the Customer | [optional] 
**email** | **string** | New email for the Customer | [optional] 
**gender** | **string** | New gender for the Customer | [optional] 
**dob** | **string** | New date of birth for the Customer | [optional] 
**metadata** | **object** | New metadata collection for the Customer | [optional] 
**privacy** | [**\Swagger\Client\Model\CustomerPrivacy**](CustomerPrivacy.md) | New privacy settings for the Customer | [optional] 
**profilePictureUpload** | [**\Swagger\Client\Model\MagentoImageUpload**](MagentoImageUpload.md) | New profile picture for the Customer | [optional] 
**coverUpload** | [**\Swagger\Client\Model\MagentoImageUpload**](MagentoImageUpload.md) | New cover photo for the customer | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


