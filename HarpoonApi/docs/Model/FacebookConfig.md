# FacebookConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **string** |  | 
**appToken** | **string** |  | 
**scopes** | **string** |  | [optional] [default to 'first_name, last_name, email, id, gender, cover{source}']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


