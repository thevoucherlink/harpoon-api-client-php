# AwEventbookingEventTicketAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**ticketId** | **double** |  | 
**storeId** | **double** |  | [optional] 
**attributeCode** | **string** |  | [optional] 
**value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


