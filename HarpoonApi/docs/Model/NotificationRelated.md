# NotificationRelated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **double** |  | [optional] [default to 0.0]
**brandId** | **double** |  | [optional] [default to 0.0]
**venueId** | **double** |  | [optional] [default to 0.0]
**competitionId** | **double** |  | [optional] [default to 0.0]
**couponId** | **double** |  | [optional] [default to 0.0]
**eventId** | **double** |  | [optional] [default to 0.0]
**dealPaidId** | **double** |  | [optional] [default to 0.0]
**dealGroupId** | **double** |  | [optional] [default to 0.0]
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


