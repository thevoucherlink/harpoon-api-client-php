# CustomerPasswordUpdateCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentPassword** | **string** | Current Customer Passwrod | 
**newPassword** | **string** | New Customer Password | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


