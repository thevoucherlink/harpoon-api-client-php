# CustomerPrivacy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activity** | **string** |  | [default to 'public']
**notificationPush** | **bool** |  | [optional] [default to true]
**notificationBeacon** | **bool** |  | [optional] [default to true]
**notificationLocation** | **bool** |  | [optional] [default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


