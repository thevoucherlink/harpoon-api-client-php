# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**streetAddress** | **string** | Main street line for the address (first line) | [optional] 
**streetAddressComp** | **string** | Line to complete the address (second line) | [optional] 
**city** | **string** |  | [optional] 
**region** | **string** | Region, County, Province or State | [optional] 
**countryCode** | **string** | Country ISO code (must be max of 2 capital letter) | [optional] [default to 'IE']
**postcode** | **string** |  | [optional] 
**attnFirstName** | **string** | Postal Attn {person first name} | [optional] 
**attnLastName** | **string** | Postal Attn {person last name} | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


