# EventTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**price** | **double** |  | [optional] [default to 0.0]
**qtyBought** | **double** |  | [optional] [default to 0.0]
**qtyTotal** | **double** |  | [optional] [default to 0.0]
**qtyLeft** | **double** |  | [optional] [default to 0.0]
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


