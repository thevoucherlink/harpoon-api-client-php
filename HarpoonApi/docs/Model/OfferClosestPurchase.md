# OfferClosestPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiredAt** | [**\DateTime**](Date.md) |  | [optional] 
**purchasedAt** | [**\DateTime**](Date.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


