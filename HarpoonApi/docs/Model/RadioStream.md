# RadioStream

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**description** | **string** |  | [optional] 
**urlHQ** | **string** |  | 
**urlLQ** | **string** |  | 
**starts** | [**\DateTime**](Date.md) | When the Stream starts of being public | [optional] 
**ends** | [**\DateTime**](Date.md) | When the Stream ceases of being public | [optional] 
**metaDataUrl** | **string** |  | [optional] 
**imgStream** | **string** |  | [optional] 
**sponsorTrack** | **string** | Url of the sponsor MP3 to be played | [optional] 
**tritonConfig** | [**\Swagger\Client\Model\TritonConfig**](TritonConfig.md) |  | [optional] 
**id** | **double** |  | [optional] 
**appId** | **string** |  | [optional] 
**radioShows** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


