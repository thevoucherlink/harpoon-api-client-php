# MenuItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**url** | **string** |  | [optional] 
**image** | **string** |  | [optional] 
**visibility** | **bool** |  | [optional] 
**sortOrder** | **double** |  | [optional] 
**pageAction** | **bool** |  | [optional] 
**pageActionIcon** | **string** |  | [optional] 
**pageActionLink** | **string** |  | [optional] 
**styleCss** | **string** |  | [optional] 
**featureRadioDontShowOnCamera** | **bool** |  | [optional] [default to true]
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


