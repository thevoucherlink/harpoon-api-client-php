# FacebookEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node** | **string** |  | [optional] 
**category** | **string** |  | [optional] 
**updatedTime** | **string** |  | [optional] 
**maybeCount** | **double** |  | [optional] [default to 0.0]
**noreplyCount** | **double** |  | [optional] [default to 0.0]
**attendingCount** | **double** |  | [optional] [default to 0.0]
**place** | **object** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


