# HarpoonHpublicVendorconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**type** | **double** |  | 
**category** | **string** |  | 
**logo** | **string** |  | 
**cover** | **string** |  | 
**facebookPageId** | **string** |  | 
**facebookPageToken** | **string** |  | 
**facebookEventEnabled** | **double** |  | 
**facebookEventUpdatedAt** | [**\DateTime**](Date.md) |  | 
**facebookEventCount** | **double** |  | 
**facebookFeedEnabled** | **double** |  | 
**facebookFeedUpdatedAt** | [**\DateTime**](Date.md) |  | 
**facebookFeedCount** | **double** |  | 
**stripePublishableKey** | **string** |  | 
**stripeRefreshToken** | **string** |  | 
**stripeAccessToken** | **string** |  | 
**feesEventTicket** | **string** |  | 
**stripeUserId** | **string** |  | 
**vendorconfigId** | **double** |  | 
**createdAt** | [**\DateTime**](Date.md) |  | 
**updatedAt** | [**\DateTime**](Date.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


