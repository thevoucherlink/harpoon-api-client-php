# RssFeedGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**sortOrder** | **double** |  | [optional] [default to 0.0]
**id** | **double** |  | [optional] 
**appId** | **string** |  | [optional] 
**rssFeeds** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


