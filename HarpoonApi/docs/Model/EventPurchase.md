# EventPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **double** |  | [optional] 
**name** | **string** |  | [optional] 
**event** | [**\Swagger\Client\Model\Event**](Event.md) |  | [optional] 
**redemptionType** | **string** |  | [optional] 
**unlockTime** | **double** |  | [optional] [default to 15.0]
**code** | **string** |  | [optional] 
**qrcode** | **string** |  | [optional] 
**isAvailable** | **bool** |  | [optional] [default to false]
**status** | **string** |  | [optional] [default to 'unknown']
**createdAt** | [**\DateTime**](Date.md) |  | [optional] 
**expiredAt** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedAt** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedByTerminal** | **string** |  | [optional] 
**basePrice** | **double** |  | [optional] [default to 0.0]
**ticketPrice** | **double** |  | [optional] [default to 9.0]
**currency** | **string** |  | [optional] [default to 'EUR']
**attendee** | [**\Swagger\Client\Model\Customer**](Customer.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


