# AuthToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **string** |  | 
**expiresIn** | **double** |  | [optional] 
**scope** | **string** |  | [optional] 
**refreshToken** | **string** |  | [optional] 
**tokenType** | **string** |  | [optional] 
**kid** | **string** |  | [optional] 
**macAlgorithm** | **string** |  | [optional] 
**macKey** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


