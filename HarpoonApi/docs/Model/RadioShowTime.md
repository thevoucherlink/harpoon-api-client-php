# RadioShowTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **string** | Time when the show starts, format HHmm | [optional] [default to '0000']
**endTime** | **string** | Time when the show ends, format HHmm | [optional] [default to '2359']
**weekday** | **double** | Day of the week when the show is live, 1 &#x3D; Monday / 7 &#x3D; Sunday | [optional] [default to 1.0]
**id** | **double** |  | [optional] 
**radioShowId** | **double** |  | [optional] 
**radioShow** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


