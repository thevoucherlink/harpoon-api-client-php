# HarpoonHpublicv12VendorAppCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**appId** | **double** |  | 
**vendorId** | **double** |  | 
**categoryId** | **double** |  | 
**isPrimary** | **double** |  | [optional] [default to 0.0]
**createdAt** | [**\DateTime**](Date.md) |  | [optional] 
**updatedAt** | [**\DateTime**](Date.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


