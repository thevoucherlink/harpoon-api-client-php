# UrbanAirshipConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**developmentAppKey** | **string** |  | 
**developmentAppSecret** | **string** |  | [optional] 
**developmentLogLevel** | **string** |  | [optional] [default to 'debug']
**productionAppKey** | **string** |  | [optional] 
**productionAppSecret** | **string** |  | [optional] 
**productionLogLevel** | **string** |  | [optional] [default to 'none']
**inProduction** | **bool** |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


