# PlayerConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mute** | **bool** | Available on the following SDKs: JS, iOS | [optional] [default to false]
**autostart** | **bool** | Available on the following SDKs: JS, iOS, Android | [optional] [default to false]
**repeat** | **bool** | Available on the following SDKs: JS, iOS, Android | [optional] [default to false]
**controls** | **bool** | Available on the following SDKs: JS, iOS, Android | [optional] [default to true]
**visualPlaylist** | **bool** | Available on the following SDKs: JS (as visualplaylist) | [optional] [default to true]
**displayTitle** | **bool** | Available on the following SDKs: JS (as displaytitle) | [optional] [default to true]
**displayDescription** | **bool** | Available on the following SDKs: JS (as displaydescription) | [optional] [default to true]
**stretching** | **string** | Available on the following SDKs: JS, iOS, Android (as stretch) | [optional] [default to 'uniform']
**hlshtml** | **bool** | Available on the following SDKs: JS | [optional] [default to false]
**primary** | **string** | Available on the following SDKs: JS | [optional] [default to 'html5']
**flashPlayer** | **string** | Available on the following SDKs: JS (as flashplayer) | [optional] [default to '/']
**baseSkinPath** | **string** | Available on the following SDKs: JS (as base) | [optional] [default to '/']
**preload** | **string** | Available on the following SDKs: JS | [optional] 
**playerAdConfig** | **object** | Available on the following SDKs: JS, iOS, Android | [optional] 
**playerCaptionConfig** | **object** | Available on the following SDKs: JS, iOS, Android | [optional] 
**skinName** | **string** | Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin) | [optional] 
**skinCss** | **string** | Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin) | [optional] 
**skinActive** | **string** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS | [optional] 
**skinInactive** | **string** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS | [optional] 
**skinBackground** | **string** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


