# AwCollpurDealPurchases

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**dealId** | **double** |  | 
**orderId** | **double** |  | 
**orderItemId** | **double** |  | 
**qtyPurchased** | **double** |  | 
**qtyWithCoupons** | **double** |  | 
**customerName** | **string** |  | 
**customerId** | **double** |  | 
**purchaseDateTime** | [**\DateTime**](Date.md) |  | [optional] 
**shippingAmount** | **string** |  | 
**qtyOrdered** | **string** |  | 
**refundState** | **double** |  | [optional] 
**isSuccessedFlag** | **double** |  | [optional] 
**awCollpurCoupon** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


