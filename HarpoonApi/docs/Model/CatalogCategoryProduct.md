# CatalogCategoryProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryId** | **double** |  | 
**productId** | **double** |  | 
**position** | **double** |  | 
**catalogProduct** | **object** |  | [optional] 
**catalogCategory** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


