# CatalogCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**parentId** | **double** |  | 
**createdAt** | [**\DateTime**](Date.md) |  | [optional] 
**updatedAt** | [**\DateTime**](Date.md) |  | [optional] 
**path** | **string** |  | 
**position** | **double** |  | 
**level** | **double** |  | 
**childrenCount** | **double** |  | 
**storeId** | **double** |  | 
**allChildren** | **string** |  | [optional] 
**availableSortBy** | **string** |  | [optional] 
**children** | **string** |  | [optional] 
**customApplyToProducts** | **double** |  | [optional] 
**customDesign** | **string** |  | [optional] 
**customDesignFrom** | [**\DateTime**](Date.md) |  | [optional] 
**customDesignTo** | [**\DateTime**](Date.md) |  | [optional] 
**customLayoutUpdate** | **string** |  | [optional] 
**customUseParentSettings** | **double** |  | [optional] 
**defaultSortBy** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**displayMode** | **string** |  | [optional] 
**filterPriceRange** | **string** |  | [optional] 
**image** | **string** |  | [optional] 
**includeInMenu** | **double** |  | [optional] 
**isActive** | **double** |  | [optional] 
**isAnchor** | **double** |  | [optional] 
**landingPage** | **double** |  | [optional] 
**metaDescription** | **string** |  | [optional] 
**metaKeywords** | **string** |  | [optional] 
**metaTitle** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**pageLayout** | **string** |  | [optional] 
**pathInStore** | **string** |  | [optional] 
**thumbnail** | **string** |  | [optional] 
**ummCatLabel** | **string** |  | [optional] 
**ummCatTarget** | **string** |  | [optional] 
**ummDdBlocks** | **string** |  | [optional] 
**ummDdColumns** | **double** |  | [optional] 
**ummDdProportions** | **string** |  | [optional] 
**ummDdType** | **string** |  | [optional] 
**ummDdWidth** | **string** |  | [optional] 
**urlKey** | **string** |  | [optional] 
**urlPath** | **string** |  | [optional] 
**catalogProducts** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


