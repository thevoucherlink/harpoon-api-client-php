# StripeInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **string** |  | [optional] 
**amountDue** | **double** |  | [optional] 
**applicationFee** | **double** |  | [optional] 
**attemptCount** | **double** |  | [optional] 
**attempted** | **bool** |  | [optional] 
**charge** | **string** |  | [optional] 
**closed** | **bool** |  | [optional] 
**currency** | **string** |  | [optional] 
**date** | **double** |  | [optional] 
**description** | **string** |  | [optional] 
**endingBalance** | **double** |  | [optional] 
**forgiven** | **bool** |  | [optional] 
**livemode** | **bool** |  | [optional] 
**metadata** | **string** |  | [optional] 
**nextPaymentAttempt** | **double** |  | [optional] 
**paid** | **bool** |  | [optional] 
**periodEnd** | **double** |  | [optional] 
**periodStart** | **double** |  | [optional] 
**receiptNumber** | **string** |  | [optional] 
**subscriptionProrationDate** | **double** |  | [optional] 
**subtotal** | **double** |  | [optional] 
**tax** | **double** |  | [optional] 
**taxPercent** | **double** |  | [optional] 
**total** | **double** |  | [optional] 
**webhooksDeliveredAt** | **double** |  | [optional] 
**customer** | **string** |  | 
**discount** | [**\Swagger\Client\Model\StripeDiscount**](StripeDiscount.md) |  | [optional] 
**statmentDescriptor** | **string** |  | [optional] 
**subscription** | [**\Swagger\Client\Model\StripeSubscription**](StripeSubscription.md) |  | [optional] 
**lines** | [**\Swagger\Client\Model\StripeInvoiceItem[]**](StripeInvoiceItem.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


