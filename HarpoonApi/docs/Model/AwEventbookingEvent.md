# AwEventbookingEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**productId** | **double** |  | [optional] 
**isEnabled** | **double** |  | [optional] 
**eventStartDate** | [**\DateTime**](Date.md) |  | [optional] 
**eventEndDate** | [**\DateTime**](Date.md) |  | [optional] 
**dayCountBeforeSendReminderLetter** | **double** |  | [optional] 
**isReminderSend** | **double** |  | 
**isTermsEnabled** | **double** |  | [optional] 
**generatePdfTickets** | **double** |  | 
**redeemRoles** | **string** |  | [optional] 
**location** | **string** |  | [optional] 
**tickets** | **object[]** |  | [optional] 
**attributes** | **object[]** |  | [optional] 
**purchasedTickets** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


