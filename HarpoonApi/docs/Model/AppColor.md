# AppColor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appPrimary** | **string** |  | [default to '#027fd3']
**appSecondary** | **string** |  | [optional] [default to '#26ff00']
**textPrimary** | **string** |  | [optional] [default to '#049975']
**textSecondary** | **string** |  | [optional] [default to '#4e5eea']
**radioNavBar** | **string** |  | [optional] [default to '#87878c']
**radioPlayer** | **string** |  | [optional] [default to '#af2768']
**radioProgressBar** | **string** |  | [optional] [default to '#ff00c9']
**menuBackground** | **string** |  | [optional] [default to '#000000']
**radioTextSecondary** | **string** |  | [optional] [default to '#000000']
**radioTextPrimary** | **string** |  | [optional] [default to '#000000']
**menuItem** | **string** |  | [optional] [default to '#000000']
**appGradientPrimary** | **string** |  | [optional] [default to '#000000']
**feedSegmentIndicator** | **string** |  | [optional] [default to '#ff0bc6']
**radioPlayButton** | **string** |  | [optional] [default to '#000000']
**radioPauseButton** | **string** |  | [optional] [default to '#000000']
**radioPlayButtonBackground** | **string** |  | [optional] [default to '#000000']
**radioPauseButtonBackground** | **string** |  | [optional] [default to '#000000']
**radioPlayerItem** | **string** |  | [optional] [default to '#000000']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


