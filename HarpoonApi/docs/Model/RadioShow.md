# RadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**description** | **string** |  | [optional] 
**content** | **string** |  | [optional] 
**contentType** | **string** |  | [optional] [default to 'image']
**contact** | [**\Swagger\Client\Model\Contact**](Contact.md) | Contacts for this show | [optional] 
**starts** | [**\DateTime**](Date.md) | When the Show starts of being public | [optional] 
**ends** | [**\DateTime**](Date.md) | When the Show ceases of being public | [optional] 
**type** | **string** |  | [optional] 
**radioShowTimeCurrent** | [**\Swagger\Client\Model\RadioShowTime**](RadioShowTime.md) |  | [optional] 
**imgShow** | **string** |  | [optional] 
**sponsorTrack** | **string** | Url of the sponsor MP3 to be played | [optional] 
**id** | **double** |  | [optional] 
**radioStreamId** | **double** |  | [optional] 
**playlistItemId** | **double** |  | [optional] 
**radioPresenters** | **object[]** |  | [optional] 
**radioStream** | **object** |  | [optional] 
**radioShowTimes** | **object[]** |  | [optional] 
**playlistItem** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


