# Brand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**logo** | **string** |  | [optional] 
**cover** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**managerName** | **string** |  | [optional] 
**followerCount** | **double** |  | [optional] [default to 0.0]
**isFollowed** | **bool** |  | [optional] [default to false]
**categories** | [**\Swagger\Client\Model\Category[]**](Category.md) |  | [optional] 
**website** | **string** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


