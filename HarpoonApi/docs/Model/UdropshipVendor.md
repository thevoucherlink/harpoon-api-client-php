# UdropshipVendor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**vendorName** | **string** |  | 
**vendorAttn** | **string** |  | 
**email** | **string** |  | 
**street** | **string** |  | 
**city** | **string** |  | 
**zip** | **string** |  | [optional] 
**countryId** | **string** |  | 
**regionId** | **double** |  | [optional] 
**region** | **string** |  | [optional] 
**telephone** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**status** | **bool** |  | 
**password** | **string** |  | [optional] 
**passwordHash** | **string** |  | [optional] 
**passwordEnc** | **string** |  | [optional] 
**carrierCode** | **string** |  | [optional] 
**notifyNewOrder** | **double** |  | 
**labelType** | **string** |  | 
**testMode** | **double** |  | 
**handlingFee** | **string** |  | 
**upsShipperNumber** | **string** |  | [optional] 
**customDataCombined** | **string** |  | [optional] 
**customVarsCombined** | **string** |  | [optional] 
**emailTemplate** | **double** |  | [optional] 
**urlKey** | **string** |  | [optional] 
**randomHash** | **string** |  | [optional] 
**createdAt** | [**\DateTime**](Date.md) |  | [optional] 
**notifyLowstock** | **double** |  | [optional] 
**notifyLowstockQty** | **string** |  | [optional] 
**useHandlingFee** | **double** |  | [optional] 
**useRatesFallback** | **double** |  | [optional] 
**allowShippingExtraCharge** | **double** |  | [optional] 
**defaultShippingExtraChargeSuffix** | **string** |  | [optional] 
**defaultShippingExtraChargeType** | **string** |  | [optional] 
**defaultShippingExtraCharge** | **string** |  | [optional] 
**isExtraChargeShippingDefault** | **double** |  | [optional] 
**defaultShippingId** | **double** |  | [optional] 
**billingUseShipping** | **double** |  | 
**billingEmail** | **string** |  | 
**billingTelephone** | **string** |  | [optional] 
**billingFax** | **string** |  | [optional] 
**billingVendorAttn** | **string** |  | 
**billingStreet** | **string** |  | 
**billingCity** | **string** |  | 
**billingZip** | **string** |  | [optional] 
**billingCountryId** | **string** |  | 
**billingRegionId** | **double** |  | [optional] 
**billingRegion** | **string** |  | [optional] 
**subdomainLevel** | **double** |  | 
**updateStoreBaseUrl** | **double** |  | 
**confirmation** | **string** |  | [optional] 
**confirmationSent** | **double** |  | [optional] 
**rejectReason** | **string** |  | [optional] 
**backorderByAvailability** | **double** |  | [optional] 
**useReservedQty** | **double** |  | [optional] 
**tiercomRates** | **string** |  | [optional] 
**tiercomFixedRule** | **string** |  | [optional] 
**tiercomFixedRates** | **string** |  | [optional] 
**tiercomFixedCalcType** | **string** |  | [optional] 
**tiershipRates** | **string** |  | [optional] 
**tiershipSimpleRates** | **string** |  | [optional] 
**tiershipUseV2Rates** | **double** |  | [optional] 
**vacationMode** | **double** |  | [optional] 
**vacationEnd** | [**\DateTime**](Date.md) |  | [optional] 
**vacationMessage** | **string** |  | [optional] 
**udmemberLimitProducts** | **double** |  | [optional] 
**udmemberProfileId** | **double** |  | [optional] 
**udmemberProfileRefid** | **string** |  | [optional] 
**udmemberMembershipCode** | **string** |  | [optional] 
**udmemberMembershipTitle** | **string** |  | [optional] 
**udmemberBillingType** | **string** |  | [optional] 
**udmemberHistory** | **string** |  | [optional] 
**udmemberProfileSyncOff** | **double** |  | [optional] 
**udmemberAllowMicrosite** | **double** |  | [optional] 
**udprodTemplateSku** | **string** |  | [optional] 
**vendorTaxClass** | **string** |  | [optional] 
**catalogProducts** | **object[]** |  | [optional] 
**vendorLocations** | **object[]** |  | [optional] 
**config** | **object** |  | [optional] 
**partners** | **object[]** |  | [optional] 
**udropshipVendorProducts** | **object[]** |  | [optional] 
**harpoonHpublicApplicationpartners** | **object[]** |  | [optional] 
**harpoonHpublicv12VendorAppCategories** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


