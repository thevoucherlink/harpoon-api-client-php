# DealGroupRedeemData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseId** | **double** | Group Deal purchase id | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


