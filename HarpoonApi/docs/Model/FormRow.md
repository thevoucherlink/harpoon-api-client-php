# FormRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadataKey** | **string** |  | 
**type** | **string** |  | [default to 'text']
**title** | **string** |  | 
**defaultValue** | **string** |  | [optional] 
**placeholder** | **string** |  | [optional] 
**hidden** | **bool** |  | [optional] [default to false]
**required** | **bool** |  | [optional] [default to true]
**validationRegExp** | **string** |  | [optional] 
**validationMessage** | **string** |  | [optional] 
**selectorOptions** | [**\Swagger\Client\Model\FormSelectorOption[]**](FormSelectorOption.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


