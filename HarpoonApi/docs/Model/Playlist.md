# Playlist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Playlist name | [optional] 
**shortDescription** | **string** | Playlist short description | [optional] 
**image** | **string** | Playlist image | [optional] 
**isMain** | **bool** | If Playlist is included in Main | [optional] [default to false]
**id** | **double** |  | [optional] 
**appId** | **string** |  | [optional] 
**playlistItems** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


