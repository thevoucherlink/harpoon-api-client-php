# DealGroupCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qty** | **double** | Quantity to checkout | [default to 1.0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


