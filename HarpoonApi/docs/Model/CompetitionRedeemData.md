# CompetitionRedeemData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chanceId** | **double** | Competition purchase id | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


