# UdropshipVendorPartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** |  | 
**invitedEmail** | **string** |  | 
**invitedAt** | [**\DateTime**](Date.md) |  | 
**acceptedAt** | [**\DateTime**](Date.md) |  | 
**createdAt** | [**\DateTime**](Date.md) |  | 
**updatedAt** | [**\DateTime**](Date.md) |  | 
**configList** | **double** |  | 
**configFeed** | **double** |  | 
**configNeedFollow** | **double** |  | 
**configAcceptEvent** | **double** |  | 
**configAcceptCoupon** | **double** |  | 
**configAcceptDealsimple** | **double** |  | 
**configAcceptDealgroup** | **double** |  | 
**configAcceptNotificationpush** | **double** |  | 
**configAcceptNotificationbeacon** | **double** |  | 
**isOwner** | **double** |  | 
**vendorId** | **double** |  | 
**partnerId** | **double** |  | 
**id** | **double** |  | 
**udropshipVendor** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


