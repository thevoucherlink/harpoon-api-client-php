# FormSelectorOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** |  | 
**value** | **string** |  | 
**format** | **string** |  | [optional] [default to 'text']
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


