# AwCollpurCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**dealId** | **double** |  | 
**purchaseId** | **double** |  | 
**couponCode** | **string** |  | 
**status** | **string** |  | 
**couponDeliveryDatetime** | [**\DateTime**](Date.md) |  | [optional] 
**couponDateUpdated** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedAt** | [**\DateTime**](Date.md) |  | [optional] 
**redeemedByTerminal** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


