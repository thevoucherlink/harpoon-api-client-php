# Following

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  | 
**firstName** | **string** |  | 
**lastName** | **string** |  | 
**dob** | **string** |  | [optional] 
**gender** | **string** |  | [optional] [default to 'other']
**password** | **string** |  | 
**profilePicture** | **string** |  | [optional] 
**cover** | **string** |  | [optional] 
**profilePictureUpload** | [**\Swagger\Client\Model\MagentoImageUpload**](MagentoImageUpload.md) |  | [optional] 
**coverUpload** | [**\Swagger\Client\Model\MagentoImageUpload**](MagentoImageUpload.md) |  | [optional] 
**metadata** | **object** |  | [optional] 
**connection** | [**\Swagger\Client\Model\CustomerConnection**](CustomerConnection.md) |  | [optional] 
**followingCount** | **double** |  | [optional] [default to 0.0]
**followerCount** | **double** |  | [optional] [default to 0.0]
**isFollowed** | **bool** |  | [optional] [default to false]
**isFollower** | **bool** |  | [optional] [default to false]
**notificationCount** | **double** |  | [optional] [default to 0.0]
**authorizationCode** | **string** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


