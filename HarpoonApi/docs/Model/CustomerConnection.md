# CustomerConnection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**facebook** | [**\Swagger\Client\Model\CustomerConnectionFacebook**](CustomerConnectionFacebook.md) |  | [optional] 
**twitter** | [**\Swagger\Client\Model\CustomerConnectionTwitter**](CustomerConnectionTwitter.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


