# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**cover** | **string** |  | [optional] 
**price** | **double** |  | [optional] [default to 0.0]
**baseCurrency** | **string** |  | [optional] [default to 'EUR']
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


