# CheckoutAgreement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**contentHeight** | **string** |  | [optional] 
**checkboxText** | **string** |  | [optional] 
**isActive** | **double** |  | 
**isHtml** | **double** |  | 
**id** | **double** |  | 
**content** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


