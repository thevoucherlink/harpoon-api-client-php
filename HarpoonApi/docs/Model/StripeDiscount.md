# StripeDiscount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **string** |  | [optional] 
**customer** | **string** |  | [optional] 
**end** | **double** |  | [optional] 
**start** | **double** |  | [optional] 
**subscription** | **string** |  | [optional] 
**coupon** | [**\Swagger\Client\Model\StripeCoupon**](StripeCoupon.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


