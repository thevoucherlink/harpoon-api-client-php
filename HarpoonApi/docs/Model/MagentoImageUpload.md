# MagentoImageUpload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **string** |  | 
**contentType** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


