# AnonymousModel8

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brandId** | **double** |  | [optional] 
**competitionId** | **double** |  | [optional] 
**couponId** | **double** |  | [optional] 
**customerId** | **double** |  | [optional] 
**eventId** | **double** |  | [optional] 
**dealPaid** | **double** |  | [optional] 
**dealGroup** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


