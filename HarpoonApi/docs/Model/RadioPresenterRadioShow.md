# RadioPresenterRadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | [optional] 
**radioPresenterId** | **double** |  | [optional] 
**radioShowId** | **double** |  | [optional] 
**radioPresenter** | **object** |  | [optional] 
**radioShow** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


