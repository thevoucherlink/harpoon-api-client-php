# AppConnectTo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **string** |  | 
**heading** | **string** |  | 
**description** | **string** |  | 
**link** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


