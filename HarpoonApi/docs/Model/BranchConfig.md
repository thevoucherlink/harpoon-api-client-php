# BranchConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branchLiveKey** | **string** |  | [optional] 
**branchTestKey** | **string** |  | [optional] 
**branchLiveUrl** | **string** |  | [optional] 
**branchTestUrl** | **string** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


