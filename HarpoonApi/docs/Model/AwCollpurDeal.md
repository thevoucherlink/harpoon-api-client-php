# AwCollpurDeal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**productId** | **double** |  | 
**productName** | **string** |  | 
**storeIds** | **string** |  | 
**isActive** | **double** |  | 
**isSuccess** | **double** |  | 
**closeState** | **double** |  | 
**qtyToReachDeal** | **double** |  | 
**purchasesLeft** | **double** |  | 
**maximumAllowedPurchases** | **double** |  | 
**availableFrom** | [**\DateTime**](Date.md) |  | [optional] 
**availableTo** | [**\DateTime**](Date.md) |  | [optional] 
**price** | **string** |  | 
**autoClose** | **double** |  | 
**name** | **string** |  | 
**description** | **string** |  | 
**fullDescription** | **string** |  | [optional] 
**dealImage** | **string** |  | 
**isFeatured** | **double** |  | [optional] 
**enableCoupons** | **double** |  | 
**couponPrefix** | **string** |  | 
**couponExpireAfterDays** | **double** |  | [optional] 
**expiredFlag** | **double** |  | [optional] 
**sentBeforeFlag** | **double** |  | [optional] 
**isSuccessedFlag** | **double** |  | [optional] 
**awCollpurDealPurchases** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


