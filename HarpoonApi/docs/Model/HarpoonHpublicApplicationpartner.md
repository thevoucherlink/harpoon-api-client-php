# HarpoonHpublicApplicationpartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**applicationId** | **double** |  | 
**brandId** | **double** |  | 
**status** | **string** |  | 
**invitedEmail** | **string** |  | [optional] 
**invitedAt** | [**\DateTime**](Date.md) |  | [optional] 
**acceptedAt** | [**\DateTime**](Date.md) |  | [optional] 
**createdAt** | [**\DateTime**](Date.md) |  | [optional] 
**updatedAt** | [**\DateTime**](Date.md) |  | [optional] 
**configList** | **double** |  | [optional] 
**configFeed** | **double** |  | [optional] 
**configNeedFollow** | **double** |  | [optional] 
**configAcceptEvent** | **double** |  | [optional] 
**configAcceptCoupon** | **double** |  | [optional] 
**configAcceptDealsimple** | **double** |  | [optional] 
**configAcceptDealgroup** | **double** |  | [optional] 
**configAcceptNotificationpush** | **double** |  | [optional] 
**configAcceptNotificationbeacon** | **double** |  | [optional] 
**isOwner** | **double** |  | [optional] 
**configApproveEvent** | **double** |  | [optional] 
**configApproveCoupon** | **double** |  | [optional] 
**configApproveDealsimple** | **double** |  | [optional] 
**configApproveDealgroup** | **double** |  | [optional] 
**udropshipVendor** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


