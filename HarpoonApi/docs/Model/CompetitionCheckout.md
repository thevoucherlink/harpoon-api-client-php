# CompetitionCheckout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cartId** | **double** |  | [optional] 
**url** | **string** |  | [optional] 
**status** | **string** |  | [default to 'unknown']
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


