# UdropshipVendorProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**vendorId** | **double** |  | 
**productId** | **double** |  | 
**priority** | **double** |  | 
**carrierCode** | **string** |  | [optional] 
**vendorSku** | **string** |  | [optional] 
**vendorCost** | **string** |  | [optional] 
**stockQty** | **string** |  | [optional] 
**backorders** | **double** |  | 
**shippingPrice** | **string** |  | [optional] 
**status** | **double** |  | 
**reservedQty** | **string** |  | [optional] 
**availState** | **string** |  | [optional] 
**availDate** | [**\DateTime**](Date.md) |  | [optional] 
**relationshipStatus** | **string** |  | [optional] 
**udropshipVendor** | **object** |  | [optional] 
**catalogProduct** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


