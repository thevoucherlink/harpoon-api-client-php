# LoginExternal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **string** |  | 
**version** | **string** |  | [optional] 
**requestUrl** | **string** |  | 
**authorizeUrl** | **string** |  | 
**accessUrl** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


