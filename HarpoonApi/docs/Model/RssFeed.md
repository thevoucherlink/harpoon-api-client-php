# RssFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **string** |  | 
**subCategory** | **string** |  | [optional] 
**url** | **string** |  | 
**name** | **string** |  | [optional] 
**sortOrder** | **double** |  | [optional] [default to 0.0]
**styleCss** | **string** |  | [optional] 
**noAds** | **bool** |  | [optional] [default to false]
**adMRectVisible** | **bool** |  | [optional] [default to false]
**adMRectPosition** | **double** |  | [optional] [default to 0.0]
**id** | **double** |  | [optional] 
**appId** | **string** |  | [optional] 
**rssFeedGroupId** | **double** |  | [optional] 
**rssFeedGroup** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


