# PlaylistItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | Stream title | [optional] 
**shortDescription** | **string** | Stream short description (on SDKs is under desc) | [optional] 
**image** | **string** | Playlist image | [optional] 
**file** | **string** | Stream file | [optional] 
**type** | **string** | Stream type (used only in JS SDK) | [optional] 
**mediaType** | **string** | Stream media type (e.g. video , audio , radioStream) | [optional] 
**mediaId** | **double** | Ad media id (on SDKs is under mediaid) | [optional] 
**order** | **double** | Sort order index | [optional] [default to 0.0]
**id** | **double** |  | [optional] 
**playlistId** | **double** |  | [optional] 
**playlist** | **object** |  | [optional] 
**playerSources** | **object[]** |  | [optional] 
**playerTracks** | **object[]** |  | [optional] 
**radioShows** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


