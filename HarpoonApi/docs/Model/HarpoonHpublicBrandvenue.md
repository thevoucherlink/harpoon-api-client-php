# HarpoonHpublicBrandvenue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**brandId** | **double** |  | 
**name** | **string** |  | 
**address** | **string** |  | [optional] 
**latitude** | **double** |  | 
**longitude** | **double** |  | 
**createdAt** | [**\DateTime**](Date.md) |  | 
**updatedAt** | [**\DateTime**](Date.md) |  | 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**country** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


