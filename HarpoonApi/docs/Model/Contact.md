# Contact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Email address like hello@harpoonconnect.com | [optional] 
**website** | **string** | URL like http://harpoonconnect.com or https://www.harpoonconnect.com | [optional] 
**phone** | **string** | Telephone number like 08123456789 | [optional] 
**address** | [**\Swagger\Client\Model\Address**](Address.md) |  | [optional] 
**twitter** | **string** | URL starting with https://www.twitter.com/ followed by your username or id | [optional] 
**facebook** | **string** | URL starting with https://www.facebook.com/ followed by your username or id | [optional] 
**whatsapp** | **string** | Telephone number including country code like: +35381234567890 | [optional] 
**snapchat** | **string** | SnapChat username | [optional] 
**googlePlus** | **string** | URL starting with https://plus.google.com/+ followed by your username or id | [optional] 
**linkedIn** | **string** | LinkedIn profile URL | [optional] 
**hashtag** | **string** | A string starting with # and followed by alphanumeric characters (both lower and upper case) | [optional] 
**text** | **string** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


