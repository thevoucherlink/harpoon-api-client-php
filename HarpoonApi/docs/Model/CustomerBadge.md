# CustomerBadge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer** | **double** |  | [optional] [default to 0.0]
**event** | **double** |  | [optional] [default to 0.0]
**dealAll** | **double** |  | [optional] [default to 0.0]
**dealCoupon** | **double** |  | [optional] [default to 0.0]
**dealSimple** | **double** |  | [optional] [default to 0.0]
**dealGroup** | **double** |  | [optional] [default to 0.0]
**competition** | **double** |  | [optional] [default to 0.0]
**walletOfferAvailable** | **double** |  | [optional] [default to 0.0]
**walletOfferNew** | **double** |  | [optional] [default to 0.0]
**walletOfferExpiring** | **double** |  | [optional] [default to 0.0]
**walletOfferExpired** | **double** |  | [optional] [default to 0.0]
**notificationUnread** | **double** |  | [optional] [default to 0.0]
**brandFeed** | **double** |  | [optional] [default to 0.0]
**brand** | **double** |  | [optional] [default to 0.0]
**brandActivity** | **double** |  | [optional] [default to 0.0]
**all** | **double** |  | [optional] [default to 0.0]
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


