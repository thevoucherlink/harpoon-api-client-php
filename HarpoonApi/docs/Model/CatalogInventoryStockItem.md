# CatalogInventoryStockItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**backorders** | **double** |  | 
**enableQtyIncrements** | **double** |  | 
**isDecimalDivided** | **double** |  | 
**isQtyDecimal** | **double** |  | 
**isInStock** | **double** |  | 
**id** | **double** |  | 
**lowStockDate** | [**\DateTime**](Date.md) |  | [optional] 
**manageStock** | **double** |  | 
**minQty** | **string** |  | 
**maxSaleQty** | **string** |  | 
**minSaleQty** | **string** |  | 
**notifyStockQty** | **string** |  | [optional] 
**productId** | **double** |  | 
**qty** | **string** |  | 
**stockId** | **double** |  | 
**useConfigBackorders** | **double** |  | 
**qtyIncrements** | **string** |  | 
**stockStatusChangedAuto** | **double** |  | 
**useConfigEnableQtyInc** | **double** |  | 
**useConfigMaxSaleQty** | **double** |  | 
**useConfigManageStock** | **double** |  | 
**useConfigMinQty** | **double** |  | 
**useConfigNotifyStockQty** | **double** |  | 
**useConfigMinSaleQty** | **double** |  | 
**useConfigQtyIncrements** | **double** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


