# Coupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**price** | **double** |  | [optional] [default to 0.0]
**hasClaimed** | **bool** |  | [optional] [default to false]
**qtyLeft** | **double** |  | [optional] [default to 0.0]
**qtyTotal** | **double** |  | [optional] [default to 0.0]
**qtyClaimed** | **double** |  | [optional] [default to 0.0]
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**cover** | **string** |  | [optional] 
**campaignType** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**category** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**topic** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**alias** | **string** |  | [optional] 
**from** | [**\DateTime**](Date.md) |  | [optional] 
**to** | [**\DateTime**](Date.md) |  | [optional] 
**baseCurrency** | **string** |  | [optional] [default to 'EUR']
**priceText** | **string** |  | [optional] 
**bannerText** | **string** |  | [optional] 
**checkoutLink** | **string** |  | [optional] 
**nearestVenue** | [**\Swagger\Client\Model\Venue**](Venue.md) |  | [optional] 
**actionText** | **string** |  | [optional] [default to 'Sold Out']
**status** | **string** |  | [optional] [default to 'soldOut']
**collectionNotes** | **string** |  | [optional] 
**termsConditions** | **string** |  | [optional] 
**locationLink** | **string** |  | [optional] 
**altLink** | **string** |  | [optional] 
**redemptionType** | **string** |  | [optional] 
**brand** | [**\Swagger\Client\Model\Brand**](Brand.md) |  | [optional] 
**closestPurchase** | [**\Swagger\Client\Model\OfferClosestPurchase**](OfferClosestPurchase.md) |  | [optional] 
**isFeatured** | **bool** |  | [optional] [default to false]
**qtyPerOrder** | **double** |  | [optional] [default to 1.0]
**shareLink** | **string** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


