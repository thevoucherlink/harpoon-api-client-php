# RssTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imgTumbnail** | **string** |  | [optional] [default to '<media>']
**imgMain** | **string** |  | [optional] [default to 'media']
**link** | **string** |  | [optional] [default to 'link']
**postDateTime** | **string** |  | [optional] [default to 'pubDate']
**description** | **string** |  | [optional] [default to 'content:encoded']
**category** | **string** |  | [optional] [default to 'category']
**title** | **string** |  | [optional] [default to 'title']
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


