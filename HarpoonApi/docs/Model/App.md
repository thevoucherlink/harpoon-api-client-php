# App

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**internalId** | **double** | Id used internally in Harpoon | [optional] 
**vendorId** | **double** | Vendor App owner | [optional] 
**name** | **string** | Name of the application | 
**category** | **string** |  | [optional] [default to 'Lifestyle']
**description** | **string** |  | [optional] 
**keywords** | **string** |  | [optional] [default to 'buy,claim,offer,events,competitions']
**copyright** | **string** |  | [optional] [default to '© 2016 The Voucher Link Ltd.']
**realm** | **string** |  | [optional] 
**bundle** | **string** | Bundle Identifier of the application, com.{company name}.{project name} | [optional] [default to '']
**urlScheme** | **string** | URL Scheme used for deeplinking | [optional] [default to '']
**brandFollowOffer** | **bool** | Customer need to follow the Brand to display the Offers outside of the Brand profile page | [optional] [default to false]
**privacyUrl** | **string** | URL linking to the Privacy page for the app | [optional] [default to 'http://harpoonconnect.com/privacy/']
**termsOfServiceUrl** | **string** | URL linking to the Terms of Service page for the app | [optional] [default to 'http://harpoonconnect.com/terms/']
**restrictionAge** | **double** | Restrict the use of the app to just the customer with at least the value specified, 0 &#x3D; no limit | [optional] [default to 0.0]
**storeAndroidUrl** | **string** | URL linking to the Google Play page for the app | [optional] [default to '']
**storeIosUrl** | **string** | URL linking to the iTunes App Store page for the app | [optional] [default to '']
**typeNewsFeed** | **string** | News Feed source, nativeFeed/rss | [optional] [default to 'nativeFeed']
**clientSecret** | **string** |  | [optional] 
**grantTypes** | **string[]** |  | [optional] 
**scopes** | **string[]** |  | [optional] 
**status** | **string** | Status of the application, production/sandbox/disabled | [optional] [default to 'sandbox']
**created** | [**\DateTime**](Date.md) |  | [optional] 
**modified** | [**\DateTime**](Date.md) |  | [optional] 
**styleOfferList** | **string** | Size of the Offer List Item, big/small | [optional] [default to 'big']
**appConfig** | [**\Swagger\Client\Model\AppConfig**](AppConfig.md) |  | [optional] 
**clientToken** | **string** |  | [optional] 
**multiConfigTitle** | **string** |  | [optional] [default to 'Choose your App:']
**multiConfig** | [**\Swagger\Client\Model\AppConfig[]**](AppConfig.md) |  | [optional] 
**appId** | **string** |  | [optional] 
**parentId** | **string** |  | [optional] 
**rssFeeds** | **object[]** |  | [optional] 
**rssFeedGroups** | **object[]** |  | [optional] 
**radioStreams** | **object[]** |  | [optional] 
**customers** | **object[]** |  | [optional] 
**appConfigs** | **object[]** |  | [optional] 
**playlists** | **object[]** |  | [optional] 
**apps** | **object[]** |  | [optional] 
**parent** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


