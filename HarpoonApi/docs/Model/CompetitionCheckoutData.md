# CompetitionCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chances** | [**\Swagger\Client\Model\CompetitionCheckoutChanceData[]**](CompetitionCheckoutChanceData.md) | Chances to checkout | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


