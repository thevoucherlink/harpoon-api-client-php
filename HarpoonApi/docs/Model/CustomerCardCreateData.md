# CustomerCardCreateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**number** | **string** |  | 
**cvc** | **string** |  | 
**exMonth** | **string** |  | 
**exYear** | **string** |  | 
**cardholderName** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


