# StripePlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **string** |  | [optional] 
**currency** | **string** |  | [optional] 
**interval** | **string** |  | [optional] 
**intervalCount** | **double** |  | [optional] 
**metadata** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**statementDescriptor** | **string** |  | [optional] 
**trialPeriodDays** | **double** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


