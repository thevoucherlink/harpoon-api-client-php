# RadioPlaySession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionTime** | **double** |  | [optional] 
**playUpdateTime** | [**\DateTime**](Date.md) |  | [optional] 
**radioStreamId** | **double** |  | [optional] 
**radioShowId** | **double** |  | [optional] 
**playEndTime** | [**\DateTime**](Date.md) |  | [optional] 
**playStartTime** | [**\DateTime**](Date.md) |  | [optional] 
**customerId** | **double** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


