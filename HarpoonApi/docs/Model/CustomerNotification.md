# CustomerNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** |  | [optional] 
**cover** | **string** |  | [optional] 
**coverUpload** | [**\Swagger\Client\Model\MagentoImageUpload**](MagentoImageUpload.md) |  | [optional] 
**from** | [**\Swagger\Client\Model\NotificationFrom**](NotificationFrom.md) |  | [optional] 
**related** | [**\Swagger\Client\Model\NotificationRelated**](NotificationRelated.md) |  | [optional] 
**link** | **string** |  | [optional] 
**actionCode** | **string** |  | [optional] [default to 'post']
**status** | **string** |  | [optional] [default to 'unread']
**sentAt** | [**\DateTime**](Date.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


