# CompetitionCheckoutChanceData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** | Chance to checkout | 
**qty** | **double** | Quantity to checkout | [optional] [default to 0.0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


