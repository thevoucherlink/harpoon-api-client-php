# Competition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basePrice** | **double** |  | [optional] [default to 0.0]
**attendees** | [**\Swagger\Client\Model\CompetitionAttendee[]**](CompetitionAttendee.md) |  | [optional] 
**attendeeCount** | **double** |  | [optional] [default to 0.0]
**hasJoined** | **bool** |  | [optional] [default to false]
**chanceCount** | **double** |  | [optional] [default to 0.0]
**isWinner** | **bool** |  | [optional] [default to false]
**facebook** | [**\Swagger\Client\Model\FacebookEvent**](FacebookEvent.md) |  | [optional] 
**chances** | [**\Swagger\Client\Model\CompetitionChance[]**](CompetitionChance.md) |  | [optional] 
**earnMoreChancesURL** | **string** |  | [optional] 
**type** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**earnMoreChances** | **bool** |  | [optional] 
**competitionQuestion** | **string** |  | [optional] [default to '']
**competitionAnswer** | **string** |  | [optional] [default to '']
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**cover** | **string** |  | [optional] 
**campaignType** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**category** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**topic** | [**\Swagger\Client\Model\Category**](Category.md) |  | [optional] 
**alias** | **string** |  | [optional] 
**from** | [**\DateTime**](Date.md) |  | [optional] 
**to** | [**\DateTime**](Date.md) |  | [optional] 
**baseCurrency** | **string** |  | [optional] [default to 'EUR']
**priceText** | **string** |  | [optional] 
**bannerText** | **string** |  | [optional] 
**checkoutLink** | **string** |  | [optional] 
**nearestVenue** | [**\Swagger\Client\Model\Venue**](Venue.md) |  | [optional] 
**actionText** | **string** |  | [optional] [default to 'Sold Out']
**status** | **string** |  | [optional] [default to 'soldOut']
**collectionNotes** | **string** |  | [optional] 
**termsConditions** | **string** |  | [optional] 
**locationLink** | **string** |  | [optional] 
**altLink** | **string** |  | [optional] 
**redemptionType** | **string** |  | [optional] 
**brand** | [**\Swagger\Client\Model\Brand**](Brand.md) |  | [optional] 
**closestPurchase** | [**\Swagger\Client\Model\OfferClosestPurchase**](OfferClosestPurchase.md) |  | [optional] 
**isFeatured** | **bool** |  | [optional] [default to false]
**qtyPerOrder** | **double** |  | [optional] [default to 1.0]
**shareLink** | **string** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


