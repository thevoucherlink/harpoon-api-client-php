# Venue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**coordinates** | [**\Swagger\Client\Model\GeoLocation**](GeoLocation.md) |  | [optional] 
**phone** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**brand** | [**\Swagger\Client\Model\Brand**](Brand.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


