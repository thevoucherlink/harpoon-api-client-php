# Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | 
**type** | **string** |  | [optional] 
**funding** | **string** |  | [optional] 
**lastDigits** | **string** |  | [optional] 
**exMonth** | **string** |  | 
**exYear** | **string** |  | 
**cardholderName** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


