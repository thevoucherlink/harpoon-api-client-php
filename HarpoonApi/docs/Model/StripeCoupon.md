# StripeCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **string** |  | [optional] 
**amountOff** | **double** |  | [optional] 
**created** | **double** |  | [optional] 
**currency** | **string** |  | [optional] 
**durationInMonths** | **double** |  | [optional] 
**livemode** | **bool** |  | [optional] 
**maxRedemptions** | **double** |  | [optional] 
**metadata** | **string** |  | [optional] 
**percentOff** | **double** |  | [optional] 
**redeemBy** | **double** |  | [optional] 
**timesRedeemed** | **double** |  | [optional] 
**valid** | **bool** |  | [optional] 
**duration** | **string** |  | 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


