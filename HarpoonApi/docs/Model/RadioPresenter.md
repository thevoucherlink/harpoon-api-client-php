# RadioPresenter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**image** | **string** |  | [optional] 
**contact** | [**\Swagger\Client\Model\Contact**](Contact.md) | Contacts for this presenter | [optional] 
**id** | **double** |  | [optional] 
**radioShows** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


