# StripeSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicationFeePercent** | **double** |  | [optional] 
**cancelAtPeriodEnd** | **bool** |  | [optional] 
**canceledAt** | **double** |  | [optional] 
**currentPeriodEnd** | **double** |  | [optional] 
**currentPeriodStart** | **double** |  | [optional] 
**metadata** | **string** |  | [optional] 
**quantity** | **double** |  | [optional] 
**start** | **double** |  | [optional] 
**status** | **string** |  | [optional] 
**taxPercent** | **double** |  | [optional] 
**trialEnd** | **double** |  | [optional] 
**trialStart** | **double** |  | [optional] 
**customer** | **string** |  | 
**discount** | [**\Swagger\Client\Model\StripeDiscount**](StripeDiscount.md) |  | [optional] 
**plan** | [**\Swagger\Client\Model\StripePlan**](StripePlan.md) |  | 
**object** | **string** |  | [optional] 
**endedAt** | **double** |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


