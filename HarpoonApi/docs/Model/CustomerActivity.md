# CustomerActivity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**\Swagger\Client\Model\Customer**](Customer.md) |  | [optional] 
**message** | **string** |  | [optional] 
**cover** | **string** |  | [optional] 
**related** | [**\Swagger\Client\Model\AnonymousModel8**](AnonymousModel8.md) |  | [optional] 
**link** | **string** |  | [optional] 
**actionCode** | **string** |  | [optional] 
**privacyCode** | **string** |  | [optional] 
**postedAt** | [**\DateTime**](Date.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


