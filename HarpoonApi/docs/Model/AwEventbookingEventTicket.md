# AwEventbookingEventTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **double** |  | 
**eventId** | **double** |  | 
**price** | **string** |  | [optional] 
**priceType** | **string** |  | [optional] 
**sku** | **string** |  | [optional] 
**qty** | **double** |  | [optional] 
**codeprefix** | **string** |  | [optional] 
**fee** | **string** |  | 
**isPassOnFee** | **double** |  | 
**chance** | **double** |  | 
**attributes** | **object[]** |  | [optional] 
**tickets** | **object[]** |  | [optional] 
**awEventbookingTicket** | **object[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


