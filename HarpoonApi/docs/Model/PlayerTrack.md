# PlayerTrack

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **string** | Source file | [optional] 
**type** | **string** | Source type (used only in JS SDK) | [optional] 
**label** | **string** | Source label | [optional] 
**default** | **bool** | If Source is the default Source | [optional] [default to false]
**order** | **double** | Sort order index | [optional] [default to 0.0]
**id** | **double** |  | [optional] 
**playlistItemId** | **double** |  | [optional] 
**playlistItem** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


