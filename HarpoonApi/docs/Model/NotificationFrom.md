# NotificationFrom

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**\Swagger\Client\Model\Customer**](Customer.md) |  | [optional] 
**brand** | [**\Swagger\Client\Model\Brand**](Brand.md) |  | [optional] 
**id** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


