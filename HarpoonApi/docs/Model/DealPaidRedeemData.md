# DealPaidRedeemData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseId** | **double** | Paid Deal purchase id | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


