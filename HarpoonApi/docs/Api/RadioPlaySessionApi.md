# Harpoon\Api\RadioPlaySessionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPlaySessionCount**](RadioPlaySessionApi.md#radioPlaySessionCount) | **GET** /RadioPlaySessions/count | Count instances of the model matched by where from the data source.
[**radioPlaySessionCreate**](RadioPlaySessionApi.md#radioPlaySessionCreate) | **POST** /RadioPlaySessions | Create a new instance of the model and persist it into the data source.
[**radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**](RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream) | **GET** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**](RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream) | **POST** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionDeleteById**](RadioPlaySessionApi.md#radioPlaySessionDeleteById) | **DELETE** /RadioPlaySessions/{id} | Delete a model instance by {{id}} from the data source.
[**radioPlaySessionExistsGetRadioPlaySessionsidExists**](RadioPlaySessionApi.md#radioPlaySessionExistsGetRadioPlaySessionsidExists) | **GET** /RadioPlaySessions/{id}/exists | Check whether a model instance exists in the data source.
[**radioPlaySessionExistsHeadRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionExistsHeadRadioPlaySessionsid) | **HEAD** /RadioPlaySessions/{id} | Check whether a model instance exists in the data source.
[**radioPlaySessionFind**](RadioPlaySessionApi.md#radioPlaySessionFind) | **GET** /RadioPlaySessions | Find all instances of the model matched by filter from the data source.
[**radioPlaySessionFindById**](RadioPlaySessionApi.md#radioPlaySessionFindById) | **GET** /RadioPlaySessions/{id} | Find a model instance by {{id}} from the data source.
[**radioPlaySessionFindOne**](RadioPlaySessionApi.md#radioPlaySessionFindOne) | **GET** /RadioPlaySessions/findOne | Find first instance of the model matched by filter from the data source.
[**radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid) | **PATCH** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid) | **PUT** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceById**](RadioPlaySessionApi.md#radioPlaySessionReplaceById) | **POST** /RadioPlaySessions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceOrCreate**](RadioPlaySessionApi.md#radioPlaySessionReplaceOrCreate) | **POST** /RadioPlaySessions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpdateAll**](RadioPlaySessionApi.md#radioPlaySessionUpdateAll) | **POST** /RadioPlaySessions/update | Update instances of the model matched by {{where}} from the data source.
[**radioPlaySessionUpsertPatchRadioPlaySessions**](RadioPlaySessionApi.md#radioPlaySessionUpsertPatchRadioPlaySessions) | **PATCH** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertPutRadioPlaySessions**](RadioPlaySessionApi.md#radioPlaySessionUpsertPutRadioPlaySessions) | **PUT** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertWithWhere**](RadioPlaySessionApi.md#radioPlaySessionUpsertWithWhere) | **POST** /RadioPlaySessions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioPlaySessionCount**
> \Swagger\Client\Model\InlineResponse2001 radioPlaySessionCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioPlaySessionCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionCreate**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | Model instance data

try {
    $result = $api_instance->radioPlaySessionCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**
> \SplFileObject radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**
> \SplFileObject radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionDeleteById**
> object radioPlaySessionDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPlaySessionDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionExistsGetRadioPlaySessionsidExists**
> \Swagger\Client\Model\InlineResponse2003 radioPlaySessionExistsGetRadioPlaySessionsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPlaySessionExistsGetRadioPlaySessionsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionExistsGetRadioPlaySessionsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionExistsHeadRadioPlaySessionsid**
> \Swagger\Client\Model\InlineResponse2003 radioPlaySessionExistsHeadRadioPlaySessionsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPlaySessionExistsHeadRadioPlaySessionsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionExistsHeadRadioPlaySessionsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionFind**
> \Swagger\Client\Model\RadioPlaySession[] radioPlaySessionFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioPlaySessionFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession[]**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionFindById**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->radioPlaySessionFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionFindOne**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioPlaySessionFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$id = "id_example"; // string | RadioPlaySession id
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | An object of model property name/value pairs

try {
    $result = $api_instance->radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPlaySession id |
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$id = "id_example"; // string | RadioPlaySession id
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | An object of model property name/value pairs

try {
    $result = $api_instance->radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPlaySession id |
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionReplaceById**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | Model instance data

try {
    $result = $api_instance->radioPlaySessionReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionReplaceOrCreate**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | Model instance data

try {
    $result = $api_instance->radioPlaySessionReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 radioPlaySessionUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | An object of model property name/value pairs

try {
    $result = $api_instance->radioPlaySessionUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionUpsertPatchRadioPlaySessions**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionUpsertPatchRadioPlaySessions($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | Model instance data

try {
    $result = $api_instance->radioPlaySessionUpsertPatchRadioPlaySessions($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionUpsertPatchRadioPlaySessions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionUpsertPutRadioPlaySessions**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionUpsertPutRadioPlaySessions($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | Model instance data

try {
    $result = $api_instance->radioPlaySessionUpsertPutRadioPlaySessions($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionUpsertPutRadioPlaySessions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPlaySessionUpsertWithWhere**
> \Swagger\Client\Model\RadioPlaySession radioPlaySessionUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPlaySessionApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioPlaySession(); // \Swagger\Client\Model\RadioPlaySession | An object of model property name/value pairs

try {
    $result = $api_instance->radioPlaySessionUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPlaySessionApi->radioPlaySessionUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioPlaySession**](../Model/\Swagger\Client\Model\RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPlaySession**](../Model/RadioPlaySession.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

