# Harpoon\Api\HarpoonHpublicApplicationpartnerApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**harpoonHpublicApplicationpartnerCount**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCount) | **GET** /HarpoonHpublicApplicationpartners/count | Count instances of the model matched by where from the data source.
[**harpoonHpublicApplicationpartnerCreate**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreate) | **POST** /HarpoonHpublicApplicationpartners | Create a new instance of the model and persist it into the data source.
[**harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream) | **GET** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream) | **POST** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerDeleteById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerDeleteById) | **DELETE** /HarpoonHpublicApplicationpartners/{id} | Delete a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists) | **GET** /HarpoonHpublicApplicationpartners/{id}/exists | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid) | **HEAD** /HarpoonHpublicApplicationpartners/{id} | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerFind**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFind) | **GET** /HarpoonHpublicApplicationpartners | Find all instances of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerFindById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindById) | **GET** /HarpoonHpublicApplicationpartners/{id} | Find a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerFindOne**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindOne) | **GET** /HarpoonHpublicApplicationpartners/findOne | Find first instance of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor) | **GET** /HarpoonHpublicApplicationpartners/{id}/udropshipVendor | Fetches belongsTo relation udropshipVendor.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid) | **PATCH** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid) | **PUT** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceById) | **POST** /HarpoonHpublicApplicationpartners/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceOrCreate**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceOrCreate) | **POST** /HarpoonHpublicApplicationpartners/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpdateAll**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpdateAll) | **POST** /HarpoonHpublicApplicationpartners/update | Update instances of the model matched by {{where}} from the data source.
[**harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners) | **PATCH** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners) | **PUT** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertWithWhere**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertWithWhere) | **POST** /HarpoonHpublicApplicationpartners/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **harpoonHpublicApplicationpartnerCount**
> \Swagger\Client\Model\InlineResponse2001 harpoonHpublicApplicationpartnerCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerCreate**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | Model instance data

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**
> \SplFileObject harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**
> \SplFileObject harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerDeleteById**
> object harpoonHpublicApplicationpartnerDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**
> \Swagger\Client\Model\InlineResponse2003 harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**
> \Swagger\Client\Model\InlineResponse2003 harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerFind**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner[] harpoonHpublicApplicationpartnerFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner[]**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerFindById**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerFindOne**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**
> \Swagger\Client\Model\UdropshipVendor harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor($id, $refresh)

Fetches belongsTo relation udropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | HarpoonHpublicApplicationpartner id
$refresh = true; // bool | 

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| HarpoonHpublicApplicationpartner id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | HarpoonHpublicApplicationpartner id
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | An object of model property name/value pairs

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| HarpoonHpublicApplicationpartner id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | HarpoonHpublicApplicationpartner id
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | An object of model property name/value pairs

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| HarpoonHpublicApplicationpartner id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerReplaceById**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | Model instance data

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerReplaceOrCreate**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | Model instance data

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 harpoonHpublicApplicationpartnerUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | An object of model property name/value pairs

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | Model instance data

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | Model instance data

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **harpoonHpublicApplicationpartnerUpsertWithWhere**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\HarpoonHpublicApplicationpartnerApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | An object of model property name/value pairs

try {
    $result = $api_instance->harpoonHpublicApplicationpartnerUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

