# Harpoon\Api\RadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowCount**](RadioShowApi.md#radioShowCount) | **GET** /RadioShows/count | Count instances of the model matched by where from the data source.
[**radioShowCreate**](RadioShowApi.md#radioShowCreate) | **POST** /RadioShows | Create a new instance of the model and persist it into the data source.
[**radioShowCreateChangeStreamGetRadioShowsChangeStream**](RadioShowApi.md#radioShowCreateChangeStreamGetRadioShowsChangeStream) | **GET** /RadioShows/change-stream | Create a change stream.
[**radioShowCreateChangeStreamPostRadioShowsChangeStream**](RadioShowApi.md#radioShowCreateChangeStreamPostRadioShowsChangeStream) | **POST** /RadioShows/change-stream | Create a change stream.
[**radioShowDeleteById**](RadioShowApi.md#radioShowDeleteById) | **DELETE** /RadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowExistsGetRadioShowsidExists**](RadioShowApi.md#radioShowExistsGetRadioShowsidExists) | **GET** /RadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowExistsHeadRadioShowsid**](RadioShowApi.md#radioShowExistsHeadRadioShowsid) | **HEAD** /RadioShows/{id} | Check whether a model instance exists in the data source.
[**radioShowFind**](RadioShowApi.md#radioShowFind) | **GET** /RadioShows | Find all instances of the model matched by filter from the data source.
[**radioShowFindById**](RadioShowApi.md#radioShowFindById) | **GET** /RadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioShowFindOne**](RadioShowApi.md#radioShowFindOne) | **GET** /RadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowPrototypeCountRadioPresenters**](RadioShowApi.md#radioShowPrototypeCountRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/count | Counts radioPresenters of RadioShow.
[**radioShowPrototypeCountRadioShowTimes**](RadioShowApi.md#radioShowPrototypeCountRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/count | Counts radioShowTimes of RadioShow.
[**radioShowPrototypeCreateRadioPresenters**](RadioShowApi.md#radioShowPrototypeCreateRadioPresenters) | **POST** /RadioShows/{id}/radioPresenters | Creates a new instance in radioPresenters of this model.
[**radioShowPrototypeCreateRadioShowTimes**](RadioShowApi.md#radioShowPrototypeCreateRadioShowTimes) | **POST** /RadioShows/{id}/radioShowTimes | Creates a new instance in radioShowTimes of this model.
[**radioShowPrototypeDeleteRadioPresenters**](RadioShowApi.md#radioShowPrototypeDeleteRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters | Deletes all radioPresenters of this model.
[**radioShowPrototypeDeleteRadioShowTimes**](RadioShowApi.md#radioShowPrototypeDeleteRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes | Deletes all radioShowTimes of this model.
[**radioShowPrototypeDestroyByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeDestroyByIdRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/{fk} | Delete a related item by id for radioPresenters.
[**radioShowPrototypeDestroyByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeDestroyByIdRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes/{fk} | Delete a related item by id for radioShowTimes.
[**radioShowPrototypeExistsRadioPresenters**](RadioShowApi.md#radioShowPrototypeExistsRadioPresenters) | **HEAD** /RadioShows/{id}/radioPresenters/rel/{fk} | Check the existence of radioPresenters relation to an item by id.
[**radioShowPrototypeFindByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeFindByIdRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/{fk} | Find a related item by id for radioPresenters.
[**radioShowPrototypeFindByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeFindByIdRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/{fk} | Find a related item by id for radioShowTimes.
[**radioShowPrototypeGetPlaylistItem**](RadioShowApi.md#radioShowPrototypeGetPlaylistItem) | **GET** /RadioShows/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**radioShowPrototypeGetRadioPresenters**](RadioShowApi.md#radioShowPrototypeGetRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters | Queries radioPresenters of RadioShow.
[**radioShowPrototypeGetRadioShowTimes**](RadioShowApi.md#radioShowPrototypeGetRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes | Queries radioShowTimes of RadioShow.
[**radioShowPrototypeGetRadioStream**](RadioShowApi.md#radioShowPrototypeGetRadioStream) | **GET** /RadioShows/{id}/radioStream | Fetches belongsTo relation radioStream.
[**radioShowPrototypeLinkRadioPresenters**](RadioShowApi.md#radioShowPrototypeLinkRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/rel/{fk} | Add a related item by id for radioPresenters.
[**radioShowPrototypeUnlinkRadioPresenters**](RadioShowApi.md#radioShowPrototypeUnlinkRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/rel/{fk} | Remove the radioPresenters relation to an item by id.
[**radioShowPrototypeUpdateAttributesPatchRadioShowsid**](RadioShowApi.md#radioShowPrototypeUpdateAttributesPatchRadioShowsid) | **PATCH** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateAttributesPutRadioShowsid**](RadioShowApi.md#radioShowPrototypeUpdateAttributesPutRadioShowsid) | **PUT** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeUpdateByIdRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/{fk} | Update a related item by id for radioPresenters.
[**radioShowPrototypeUpdateByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeUpdateByIdRadioShowTimes) | **PUT** /RadioShows/{id}/radioShowTimes/{fk} | Update a related item by id for radioShowTimes.
[**radioShowReplaceById**](RadioShowApi.md#radioShowReplaceById) | **POST** /RadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowReplaceOrCreate**](RadioShowApi.md#radioShowReplaceOrCreate) | **POST** /RadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowUpdateAll**](RadioShowApi.md#radioShowUpdateAll) | **POST** /RadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowUploadFile**](RadioShowApi.md#radioShowUploadFile) | **POST** /RadioShows/{id}/file | Upload File to Radio Show
[**radioShowUpsertPatchRadioShows**](RadioShowApi.md#radioShowUpsertPatchRadioShows) | **PATCH** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertPutRadioShows**](RadioShowApi.md#radioShowUpsertPutRadioShows) | **PUT** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertWithWhere**](RadioShowApi.md#radioShowUpsertWithWhere) | **POST** /RadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioShowCount**
> \Swagger\Client\Model\InlineResponse2001 radioShowCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioShowCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowCreate**
> \Swagger\Client\Model\RadioShow radioShowCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | Model instance data

try {
    $result = $api_instance->radioShowCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowCreateChangeStreamGetRadioShowsChangeStream**
> \SplFileObject radioShowCreateChangeStreamGetRadioShowsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioShowCreateChangeStreamGetRadioShowsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowCreateChangeStreamGetRadioShowsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowCreateChangeStreamPostRadioShowsChangeStream**
> \SplFileObject radioShowCreateChangeStreamPostRadioShowsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioShowCreateChangeStreamPostRadioShowsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowCreateChangeStreamPostRadioShowsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowDeleteById**
> object radioShowDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioShowDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowExistsGetRadioShowsidExists**
> \Swagger\Client\Model\InlineResponse2003 radioShowExistsGetRadioShowsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioShowExistsGetRadioShowsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowExistsGetRadioShowsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowExistsHeadRadioShowsid**
> \Swagger\Client\Model\InlineResponse2003 radioShowExistsHeadRadioShowsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioShowExistsHeadRadioShowsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowExistsHeadRadioShowsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowFind**
> \Swagger\Client\Model\RadioShow[] radioShowFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioShowFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow[]**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowFindById**
> \Swagger\Client\Model\RadioShow radioShowFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->radioShowFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowFindOne**
> \Swagger\Client\Model\RadioShow radioShowFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioShowFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeCountRadioPresenters**
> \Swagger\Client\Model\InlineResponse2001 radioShowPrototypeCountRadioPresenters($id, $where)

Counts radioPresenters of RadioShow.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioShowPrototypeCountRadioPresenters($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeCountRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeCountRadioShowTimes**
> \Swagger\Client\Model\InlineResponse2001 radioShowPrototypeCountRadioShowTimes($id, $where)

Counts radioShowTimes of RadioShow.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioShowPrototypeCountRadioShowTimes($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeCountRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeCreateRadioPresenters**
> \Swagger\Client\Model\RadioPresenter radioShowPrototypeCreateRadioPresenters($id, $data)

Creates a new instance in radioPresenters of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | 

try {
    $result = $api_instance->radioShowPrototypeCreateRadioPresenters($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeCreateRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeCreateRadioShowTimes**
> \Swagger\Client\Model\RadioShowTime radioShowPrototypeCreateRadioShowTimes($id, $data)

Creates a new instance in radioShowTimes of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | 

try {
    $result = $api_instance->radioShowPrototypeCreateRadioShowTimes($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeCreateRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeDeleteRadioPresenters**
> radioShowPrototypeDeleteRadioPresenters($id)

Deletes all radioPresenters of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id

try {
    $api_instance->radioShowPrototypeDeleteRadioPresenters($id);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeDeleteRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeDeleteRadioShowTimes**
> radioShowPrototypeDeleteRadioShowTimes($id)

Deletes all radioShowTimes of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id

try {
    $api_instance->radioShowPrototypeDeleteRadioShowTimes($id);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeDeleteRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeDestroyByIdRadioPresenters**
> radioShowPrototypeDestroyByIdRadioPresenters($fk, $id)

Delete a related item by id for radioPresenters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioPresenters
$id = "id_example"; // string | RadioShow id

try {
    $api_instance->radioShowPrototypeDestroyByIdRadioPresenters($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeDestroyByIdRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioPresenters |
 **id** | **string**| RadioShow id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeDestroyByIdRadioShowTimes**
> radioShowPrototypeDestroyByIdRadioShowTimes($fk, $id)

Delete a related item by id for radioShowTimes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioShowTimes
$id = "id_example"; // string | RadioShow id

try {
    $api_instance->radioShowPrototypeDestroyByIdRadioShowTimes($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeDestroyByIdRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShowTimes |
 **id** | **string**| RadioShow id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeExistsRadioPresenters**
> bool radioShowPrototypeExistsRadioPresenters($fk, $id)

Check the existence of radioPresenters relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioPresenters
$id = "id_example"; // string | RadioShow id

try {
    $result = $api_instance->radioShowPrototypeExistsRadioPresenters($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeExistsRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioPresenters |
 **id** | **string**| RadioShow id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeFindByIdRadioPresenters**
> \Swagger\Client\Model\RadioPresenter radioShowPrototypeFindByIdRadioPresenters($fk, $id)

Find a related item by id for radioPresenters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioPresenters
$id = "id_example"; // string | RadioShow id

try {
    $result = $api_instance->radioShowPrototypeFindByIdRadioPresenters($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeFindByIdRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioPresenters |
 **id** | **string**| RadioShow id |

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeFindByIdRadioShowTimes**
> \Swagger\Client\Model\RadioShowTime radioShowPrototypeFindByIdRadioShowTimes($fk, $id)

Find a related item by id for radioShowTimes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioShowTimes
$id = "id_example"; // string | RadioShow id

try {
    $result = $api_instance->radioShowPrototypeFindByIdRadioShowTimes($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeFindByIdRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShowTimes |
 **id** | **string**| RadioShow id |

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeGetPlaylistItem**
> \Swagger\Client\Model\PlaylistItem radioShowPrototypeGetPlaylistItem($id, $refresh)

Fetches belongsTo relation playlistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$refresh = true; // bool | 

try {
    $result = $api_instance->radioShowPrototypeGetPlaylistItem($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeGetPlaylistItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeGetRadioPresenters**
> \Swagger\Client\Model\RadioPresenter[] radioShowPrototypeGetRadioPresenters($id, $filter)

Queries radioPresenters of RadioShow.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->radioShowPrototypeGetRadioPresenters($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeGetRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter[]**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeGetRadioShowTimes**
> \Swagger\Client\Model\RadioShowTime[] radioShowPrototypeGetRadioShowTimes($id, $filter)

Queries radioShowTimes of RadioShow.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->radioShowPrototypeGetRadioShowTimes($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeGetRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime[]**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeGetRadioStream**
> \Swagger\Client\Model\RadioStream radioShowPrototypeGetRadioStream($id, $refresh)

Fetches belongsTo relation radioStream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$refresh = true; // bool | 

try {
    $result = $api_instance->radioShowPrototypeGetRadioStream($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeGetRadioStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeLinkRadioPresenters**
> \Swagger\Client\Model\RadioPresenterRadioShow radioShowPrototypeLinkRadioPresenters($fk, $id, $data)

Add a related item by id for radioPresenters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioPresenters
$id = "id_example"; // string | RadioShow id
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | 

try {
    $result = $api_instance->radioShowPrototypeLinkRadioPresenters($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeLinkRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioPresenters |
 **id** | **string**| RadioShow id |
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeUnlinkRadioPresenters**
> radioShowPrototypeUnlinkRadioPresenters($fk, $id)

Remove the radioPresenters relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioPresenters
$id = "id_example"; // string | RadioShow id

try {
    $api_instance->radioShowPrototypeUnlinkRadioPresenters($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeUnlinkRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioPresenters |
 **id** | **string**| RadioShow id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeUpdateAttributesPatchRadioShowsid**
> \Swagger\Client\Model\RadioShow radioShowPrototypeUpdateAttributesPatchRadioShowsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowPrototypeUpdateAttributesPatchRadioShowsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeUpdateAttributesPatchRadioShowsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeUpdateAttributesPutRadioShowsid**
> \Swagger\Client\Model\RadioShow radioShowPrototypeUpdateAttributesPutRadioShowsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | RadioShow id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowPrototypeUpdateAttributesPutRadioShowsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeUpdateAttributesPutRadioShowsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShow id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeUpdateByIdRadioPresenters**
> \Swagger\Client\Model\RadioPresenter radioShowPrototypeUpdateByIdRadioPresenters($fk, $id, $data)

Update a related item by id for radioPresenters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioPresenters
$id = "id_example"; // string | RadioShow id
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | 

try {
    $result = $api_instance->radioShowPrototypeUpdateByIdRadioPresenters($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeUpdateByIdRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioPresenters |
 **id** | **string**| RadioShow id |
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowPrototypeUpdateByIdRadioShowTimes**
> \Swagger\Client\Model\RadioShowTime radioShowPrototypeUpdateByIdRadioShowTimes($fk, $id, $data)

Update a related item by id for radioShowTimes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$fk = "fk_example"; // string | Foreign key for radioShowTimes
$id = "id_example"; // string | RadioShow id
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | 

try {
    $result = $api_instance->radioShowPrototypeUpdateByIdRadioShowTimes($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowPrototypeUpdateByIdRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShowTimes |
 **id** | **string**| RadioShow id |
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowReplaceById**
> \Swagger\Client\Model\RadioShow radioShowReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | Model instance data

try {
    $result = $api_instance->radioShowReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowReplaceOrCreate**
> \Swagger\Client\Model\RadioShow radioShowReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | Model instance data

try {
    $result = $api_instance->radioShowReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 radioShowUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowUploadFile**
> \Swagger\Client\Model\MagentoFileUpload radioShowUploadFile($id, $data)

Upload File to Radio Show

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$id = "id_example"; // string | Radio Show Id
$data = new \Swagger\Client\Model\MagentoFileUpload(); // \Swagger\Client\Model\MagentoFileUpload | Corresponds to the file you're uploading formatted as an object

try {
    $result = $api_instance->radioShowUploadFile($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowUploadFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Radio Show Id |
 **data** | [**\Swagger\Client\Model\MagentoFileUpload**](../Model/\Swagger\Client\Model\MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object | [optional]

### Return type

[**\Swagger\Client\Model\MagentoFileUpload**](../Model/MagentoFileUpload.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowUpsertPatchRadioShows**
> \Swagger\Client\Model\RadioShow radioShowUpsertPatchRadioShows($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | Model instance data

try {
    $result = $api_instance->radioShowUpsertPatchRadioShows($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowUpsertPatchRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowUpsertPutRadioShows**
> \Swagger\Client\Model\RadioShow radioShowUpsertPutRadioShows($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | Model instance data

try {
    $result = $api_instance->radioShowUpsertPutRadioShows($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowUpsertPutRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowUpsertWithWhere**
> \Swagger\Client\Model\RadioShow radioShowUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowApi->radioShowUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

