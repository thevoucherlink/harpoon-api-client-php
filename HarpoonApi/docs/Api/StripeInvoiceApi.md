# Harpoon\Api\StripeInvoiceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceCount**](StripeInvoiceApi.md#stripeInvoiceCount) | **GET** /StripeInvoices/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceCreate**](StripeInvoiceApi.md#stripeInvoiceCreate) | **POST** /StripeInvoices | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**](StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream) | **GET** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**](StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream) | **POST** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceDeleteById**](StripeInvoiceApi.md#stripeInvoiceDeleteById) | **DELETE** /StripeInvoices/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceExistsGetStripeInvoicesidExists**](StripeInvoiceApi.md#stripeInvoiceExistsGetStripeInvoicesidExists) | **GET** /StripeInvoices/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceExistsHeadStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoiceExistsHeadStripeInvoicesid) | **HEAD** /StripeInvoices/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceFind**](StripeInvoiceApi.md#stripeInvoiceFind) | **GET** /StripeInvoices | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceFindById**](StripeInvoiceApi.md#stripeInvoiceFindById) | **GET** /StripeInvoices/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceFindOne**](StripeInvoiceApi.md#stripeInvoiceFindOne) | **GET** /StripeInvoices/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid) | **PATCH** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid) | **PUT** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceById**](StripeInvoiceApi.md#stripeInvoiceReplaceById) | **POST** /StripeInvoices/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceOrCreate**](StripeInvoiceApi.md#stripeInvoiceReplaceOrCreate) | **POST** /StripeInvoices/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpdateAll**](StripeInvoiceApi.md#stripeInvoiceUpdateAll) | **POST** /StripeInvoices/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceUpsertPatchStripeInvoices**](StripeInvoiceApi.md#stripeInvoiceUpsertPatchStripeInvoices) | **PATCH** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertPutStripeInvoices**](StripeInvoiceApi.md#stripeInvoiceUpsertPutStripeInvoices) | **PUT** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertWithWhere**](StripeInvoiceApi.md#stripeInvoiceUpsertWithWhere) | **POST** /StripeInvoices/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeInvoiceCount**
> \Swagger\Client\Model\InlineResponse2001 stripeInvoiceCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->stripeInvoiceCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceCreate**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | Model instance data

try {
    $result = $api_instance->stripeInvoiceCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**
> \SplFileObject stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**
> \SplFileObject stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceDeleteById**
> object stripeInvoiceDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeInvoiceDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceExistsGetStripeInvoicesidExists**
> \Swagger\Client\Model\InlineResponse2003 stripeInvoiceExistsGetStripeInvoicesidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeInvoiceExistsGetStripeInvoicesidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceExistsGetStripeInvoicesidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceExistsHeadStripeInvoicesid**
> \Swagger\Client\Model\InlineResponse2003 stripeInvoiceExistsHeadStripeInvoicesid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeInvoiceExistsHeadStripeInvoicesid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceExistsHeadStripeInvoicesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceFind**
> \Swagger\Client\Model\StripeInvoice[] stripeInvoiceFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeInvoiceFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice[]**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceFindById**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->stripeInvoiceFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceFindOne**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeInvoiceFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**
> \Swagger\Client\Model\StripeInvoice stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$id = "id_example"; // string | StripeInvoice id
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeInvoice id |
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**
> \Swagger\Client\Model\StripeInvoice stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$id = "id_example"; // string | StripeInvoice id
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeInvoice id |
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceReplaceById**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | Model instance data

try {
    $result = $api_instance->stripeInvoiceReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceReplaceOrCreate**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | Model instance data

try {
    $result = $api_instance->stripeInvoiceReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 stripeInvoiceUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoiceUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceUpsertPatchStripeInvoices**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceUpsertPatchStripeInvoices($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | Model instance data

try {
    $result = $api_instance->stripeInvoiceUpsertPatchStripeInvoices($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceUpsertPatchStripeInvoices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceUpsertPutStripeInvoices**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceUpsertPutStripeInvoices($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | Model instance data

try {
    $result = $api_instance->stripeInvoiceUpsertPutStripeInvoices($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceUpsertPutStripeInvoices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceUpsertWithWhere**
> \Swagger\Client\Model\StripeInvoice stripeInvoiceUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeInvoice(); // \Swagger\Client\Model\StripeInvoice | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoiceUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceApi->stripeInvoiceUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeInvoice**](../Model/\Swagger\Client\Model\StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoice**](../Model/StripeInvoice.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

