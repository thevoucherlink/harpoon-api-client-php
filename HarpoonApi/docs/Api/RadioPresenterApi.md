# Harpoon\Api\RadioPresenterApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterCount**](RadioPresenterApi.md#radioPresenterCount) | **GET** /RadioPresenters/count | Count instances of the model matched by where from the data source.
[**radioPresenterCreate**](RadioPresenterApi.md#radioPresenterCreate) | **POST** /RadioPresenters | Create a new instance of the model and persist it into the data source.
[**radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**](RadioPresenterApi.md#radioPresenterCreateChangeStreamGetRadioPresentersChangeStream) | **GET** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**](RadioPresenterApi.md#radioPresenterCreateChangeStreamPostRadioPresentersChangeStream) | **POST** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterDeleteById**](RadioPresenterApi.md#radioPresenterDeleteById) | **DELETE** /RadioPresenters/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterExistsGetRadioPresentersidExists**](RadioPresenterApi.md#radioPresenterExistsGetRadioPresentersidExists) | **GET** /RadioPresenters/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterExistsHeadRadioPresentersid**](RadioPresenterApi.md#radioPresenterExistsHeadRadioPresentersid) | **HEAD** /RadioPresenters/{id} | Check whether a model instance exists in the data source.
[**radioPresenterFind**](RadioPresenterApi.md#radioPresenterFind) | **GET** /RadioPresenters | Find all instances of the model matched by filter from the data source.
[**radioPresenterFindById**](RadioPresenterApi.md#radioPresenterFindById) | **GET** /RadioPresenters/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterFindOne**](RadioPresenterApi.md#radioPresenterFindOne) | **GET** /RadioPresenters/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterPrototypeCountRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeCountRadioShows) | **GET** /RadioPresenters/{id}/radioShows/count | Counts radioShows of RadioPresenter.
[**radioPresenterPrototypeCreateRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeCreateRadioShows) | **POST** /RadioPresenters/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioPresenterPrototypeDeleteRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeDeleteRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows | Deletes all radioShows of this model.
[**radioPresenterPrototypeDestroyByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeDestroyByIdRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioPresenterPrototypeExistsRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeExistsRadioShows) | **HEAD** /RadioPresenters/{id}/radioShows/rel/{fk} | Check the existence of radioShows relation to an item by id.
[**radioPresenterPrototypeFindByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeFindByIdRadioShows) | **GET** /RadioPresenters/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioPresenterPrototypeGetRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeGetRadioShows) | **GET** /RadioPresenters/{id}/radioShows | Queries radioShows of RadioPresenter.
[**radioPresenterPrototypeLinkRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeLinkRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/rel/{fk} | Add a related item by id for radioShows.
[**radioPresenterPrototypeUnlinkRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeUnlinkRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/rel/{fk} | Remove the radioShows relation to an item by id.
[**radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**](RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid) | **PATCH** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**](RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPutRadioPresentersid) | **PUT** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeUpdateByIdRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioPresenterReplaceById**](RadioPresenterApi.md#radioPresenterReplaceById) | **POST** /RadioPresenters/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterReplaceOrCreate**](RadioPresenterApi.md#radioPresenterReplaceOrCreate) | **POST** /RadioPresenters/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterUpdateAll**](RadioPresenterApi.md#radioPresenterUpdateAll) | **POST** /RadioPresenters/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterUploadImage**](RadioPresenterApi.md#radioPresenterUploadImage) | **POST** /RadioPresenters/{id}/file/image | 
[**radioPresenterUpsertPatchRadioPresenters**](RadioPresenterApi.md#radioPresenterUpsertPatchRadioPresenters) | **PATCH** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertPutRadioPresenters**](RadioPresenterApi.md#radioPresenterUpsertPutRadioPresenters) | **PUT** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertWithWhere**](RadioPresenterApi.md#radioPresenterUpsertWithWhere) | **POST** /RadioPresenters/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioPresenterCount**
> \Swagger\Client\Model\InlineResponse2001 radioPresenterCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioPresenterCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterCreate**
> \Swagger\Client\Model\RadioPresenter radioPresenterCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | Model instance data

try {
    $result = $api_instance->radioPresenterCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**
> \SplFileObject radioPresenterCreateChangeStreamGetRadioPresentersChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioPresenterCreateChangeStreamGetRadioPresentersChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterCreateChangeStreamGetRadioPresentersChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**
> \SplFileObject radioPresenterCreateChangeStreamPostRadioPresentersChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioPresenterCreateChangeStreamPostRadioPresentersChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterCreateChangeStreamPostRadioPresentersChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterDeleteById**
> object radioPresenterDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPresenterDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterExistsGetRadioPresentersidExists**
> \Swagger\Client\Model\InlineResponse2003 radioPresenterExistsGetRadioPresentersidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPresenterExistsGetRadioPresentersidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterExistsGetRadioPresentersidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterExistsHeadRadioPresentersid**
> \Swagger\Client\Model\InlineResponse2003 radioPresenterExistsHeadRadioPresentersid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPresenterExistsHeadRadioPresentersid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterExistsHeadRadioPresentersid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterFind**
> \Swagger\Client\Model\RadioPresenter[] radioPresenterFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioPresenterFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter[]**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterFindById**
> \Swagger\Client\Model\RadioPresenter radioPresenterFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->radioPresenterFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterFindOne**
> \Swagger\Client\Model\RadioPresenter radioPresenterFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioPresenterFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeCountRadioShows**
> \Swagger\Client\Model\InlineResponse2001 radioPresenterPrototypeCountRadioShows($id, $where)

Counts radioShows of RadioPresenter.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | RadioPresenter id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioPresenterPrototypeCountRadioShows($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeCountRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenter id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeCreateRadioShows**
> \Swagger\Client\Model\RadioShow radioPresenterPrototypeCreateRadioShows($id, $data)

Creates a new instance in radioShows of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | RadioPresenter id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | 

try {
    $result = $api_instance->radioPresenterPrototypeCreateRadioShows($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeCreateRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenter id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeDeleteRadioShows**
> radioPresenterPrototypeDeleteRadioShows($id)

Deletes all radioShows of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | RadioPresenter id

try {
    $api_instance->radioPresenterPrototypeDeleteRadioShows($id);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeDeleteRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenter id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeDestroyByIdRadioShows**
> radioPresenterPrototypeDestroyByIdRadioShows($fk, $id)

Delete a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioPresenter id

try {
    $api_instance->radioPresenterPrototypeDestroyByIdRadioShows($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeDestroyByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioPresenter id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeExistsRadioShows**
> bool radioPresenterPrototypeExistsRadioShows($fk, $id)

Check the existence of radioShows relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioPresenter id

try {
    $result = $api_instance->radioPresenterPrototypeExistsRadioShows($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeExistsRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioPresenter id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeFindByIdRadioShows**
> \Swagger\Client\Model\RadioShow radioPresenterPrototypeFindByIdRadioShows($fk, $id)

Find a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioPresenter id

try {
    $result = $api_instance->radioPresenterPrototypeFindByIdRadioShows($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeFindByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioPresenter id |

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeGetRadioShows**
> \Swagger\Client\Model\RadioShow[] radioPresenterPrototypeGetRadioShows($id, $filter)

Queries radioShows of RadioPresenter.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | RadioPresenter id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->radioPresenterPrototypeGetRadioShows($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeGetRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenter id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow[]**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeLinkRadioShows**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterPrototypeLinkRadioShows($fk, $id, $data)

Add a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioPresenter id
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | 

try {
    $result = $api_instance->radioPresenterPrototypeLinkRadioShows($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeLinkRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioPresenter id |
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeUnlinkRadioShows**
> radioPresenterPrototypeUnlinkRadioShows($fk, $id)

Remove the radioShows relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioPresenter id

try {
    $api_instance->radioPresenterPrototypeUnlinkRadioShows($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeUnlinkRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioPresenter id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**
> \Swagger\Client\Model\RadioPresenter radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | RadioPresenter id
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenter id |
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**
> \Swagger\Client\Model\RadioPresenter radioPresenterPrototypeUpdateAttributesPutRadioPresentersid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | RadioPresenter id
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterPrototypeUpdateAttributesPutRadioPresentersid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeUpdateAttributesPutRadioPresentersid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenter id |
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterPrototypeUpdateByIdRadioShows**
> \Swagger\Client\Model\RadioShow radioPresenterPrototypeUpdateByIdRadioShows($fk, $id, $data)

Update a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioPresenter id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | 

try {
    $result = $api_instance->radioPresenterPrototypeUpdateByIdRadioShows($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterPrototypeUpdateByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioPresenter id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterReplaceById**
> \Swagger\Client\Model\RadioPresenter radioPresenterReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | Model instance data

try {
    $result = $api_instance->radioPresenterReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterReplaceOrCreate**
> \Swagger\Client\Model\RadioPresenter radioPresenterReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | Model instance data

try {
    $result = $api_instance->radioPresenterReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 radioPresenterUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterUploadImage**
> \Swagger\Client\Model\MagentoFileUpload radioPresenterUploadImage($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$id = "id_example"; // string | Radio Presenter Id
$data = new \Swagger\Client\Model\MagentoFileUpload(); // \Swagger\Client\Model\MagentoFileUpload | Corresponds to the file you're uploading formatted as an object.

try {
    $result = $api_instance->radioPresenterUploadImage($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterUploadImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Radio Presenter Id |
 **data** | [**\Swagger\Client\Model\MagentoFileUpload**](../Model/\Swagger\Client\Model\MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional]

### Return type

[**\Swagger\Client\Model\MagentoFileUpload**](../Model/MagentoFileUpload.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterUpsertPatchRadioPresenters**
> \Swagger\Client\Model\RadioPresenter radioPresenterUpsertPatchRadioPresenters($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | Model instance data

try {
    $result = $api_instance->radioPresenterUpsertPatchRadioPresenters($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterUpsertPatchRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterUpsertPutRadioPresenters**
> \Swagger\Client\Model\RadioPresenter radioPresenterUpsertPutRadioPresenters($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | Model instance data

try {
    $result = $api_instance->radioPresenterUpsertPutRadioPresenters($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterUpsertPutRadioPresenters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterUpsertWithWhere**
> \Swagger\Client\Model\RadioPresenter radioPresenterUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioPresenter(); // \Swagger\Client\Model\RadioPresenter | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterApi->radioPresenterUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioPresenter**](../Model/\Swagger\Client\Model\RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

