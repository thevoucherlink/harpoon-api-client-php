# Harpoon\Api\DealGroupApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealGroupCheckout**](DealGroupApi.md#dealGroupCheckout) | **POST** /DealGroups/{id}/checkout | 
[**dealGroupFind**](DealGroupApi.md#dealGroupFind) | **GET** /DealGroups | Find all instances of the model matched by filter from the data source.
[**dealGroupFindById**](DealGroupApi.md#dealGroupFindById) | **GET** /DealGroups/{id} | Find a model instance by {{id}} from the data source.
[**dealGroupFindOne**](DealGroupApi.md#dealGroupFindOne) | **GET** /DealGroups/findOne | Find first instance of the model matched by filter from the data source.
[**dealGroupRedeem**](DealGroupApi.md#dealGroupRedeem) | **POST** /DealGroups/{id}/redeem | 
[**dealGroupReplaceById**](DealGroupApi.md#dealGroupReplaceById) | **POST** /DealGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealGroupReplaceOrCreate**](DealGroupApi.md#dealGroupReplaceOrCreate) | **POST** /DealGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealGroupUpsertWithWhere**](DealGroupApi.md#dealGroupUpsertWithWhere) | **POST** /DealGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**dealGroupVenues**](DealGroupApi.md#dealGroupVenues) | **GET** /DealGroups/{id}/venues | 


# **dealGroupCheckout**
> \Swagger\Client\Model\DealGroupCheckout dealGroupCheckout($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\DealGroupCheckoutData(); // \Swagger\Client\Model\DealGroupCheckoutData | 

try {
    $result = $api_instance->dealGroupCheckout($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupCheckout: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\DealGroupCheckoutData**](../Model/\Swagger\Client\Model\DealGroupCheckoutData.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\DealGroupCheckout**](../Model/DealGroupCheckout.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupFind**
> \Swagger\Client\Model\DealGroup[] dealGroupFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->dealGroupFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\DealGroup[]**](../Model/DealGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupFindById**
> \Swagger\Client\Model\DealGroup dealGroupFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->dealGroupFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\DealGroup**](../Model/DealGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupFindOne**
> \Swagger\Client\Model\DealGroup dealGroupFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->dealGroupFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\DealGroup**](../Model/DealGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupRedeem**
> \Swagger\Client\Model\DealGroupPurchase dealGroupRedeem($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\DealGroupRedeemData(); // \Swagger\Client\Model\DealGroupRedeemData | 

try {
    $result = $api_instance->dealGroupRedeem($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupRedeem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\DealGroupRedeemData**](../Model/\Swagger\Client\Model\DealGroupRedeemData.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\DealGroupPurchase**](../Model/DealGroupPurchase.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupReplaceById**
> \Swagger\Client\Model\DealGroup dealGroupReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\DealGroup(); // \Swagger\Client\Model\DealGroup | Model instance data

try {
    $result = $api_instance->dealGroupReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\DealGroup**](../Model/\Swagger\Client\Model\DealGroup.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\DealGroup**](../Model/DealGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupReplaceOrCreate**
> \Swagger\Client\Model\DealGroup dealGroupReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$data = new \Swagger\Client\Model\DealGroup(); // \Swagger\Client\Model\DealGroup | Model instance data

try {
    $result = $api_instance->dealGroupReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\DealGroup**](../Model/\Swagger\Client\Model\DealGroup.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\DealGroup**](../Model/DealGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupUpsertWithWhere**
> \Swagger\Client\Model\DealGroup dealGroupUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\DealGroup(); // \Swagger\Client\Model\DealGroup | An object of model property name/value pairs

try {
    $result = $api_instance->dealGroupUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\DealGroup**](../Model/\Swagger\Client\Model\DealGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\DealGroup**](../Model/DealGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealGroupVenues**
> \Swagger\Client\Model\Venue[] dealGroupVenues($id, $filter)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealGroupApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->dealGroupVenues($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealGroupApi->dealGroupVenues: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\Venue[]**](../Model/Venue.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

