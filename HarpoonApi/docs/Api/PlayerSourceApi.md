# Harpoon\Api\PlayerSourceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerSourceCount**](PlayerSourceApi.md#playerSourceCount) | **GET** /PlayerSources/count | Count instances of the model matched by where from the data source.
[**playerSourceCreate**](PlayerSourceApi.md#playerSourceCreate) | **POST** /PlayerSources | Create a new instance of the model and persist it into the data source.
[**playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**](PlayerSourceApi.md#playerSourceCreateChangeStreamGetPlayerSourcesChangeStream) | **GET** /PlayerSources/change-stream | Create a change stream.
[**playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**](PlayerSourceApi.md#playerSourceCreateChangeStreamPostPlayerSourcesChangeStream) | **POST** /PlayerSources/change-stream | Create a change stream.
[**playerSourceDeleteById**](PlayerSourceApi.md#playerSourceDeleteById) | **DELETE** /PlayerSources/{id} | Delete a model instance by {{id}} from the data source.
[**playerSourceExistsGetPlayerSourcesidExists**](PlayerSourceApi.md#playerSourceExistsGetPlayerSourcesidExists) | **GET** /PlayerSources/{id}/exists | Check whether a model instance exists in the data source.
[**playerSourceExistsHeadPlayerSourcesid**](PlayerSourceApi.md#playerSourceExistsHeadPlayerSourcesid) | **HEAD** /PlayerSources/{id} | Check whether a model instance exists in the data source.
[**playerSourceFind**](PlayerSourceApi.md#playerSourceFind) | **GET** /PlayerSources | Find all instances of the model matched by filter from the data source.
[**playerSourceFindById**](PlayerSourceApi.md#playerSourceFindById) | **GET** /PlayerSources/{id} | Find a model instance by {{id}} from the data source.
[**playerSourceFindOne**](PlayerSourceApi.md#playerSourceFindOne) | **GET** /PlayerSources/findOne | Find first instance of the model matched by filter from the data source.
[**playerSourcePrototypeGetPlaylistItem**](PlayerSourceApi.md#playerSourcePrototypeGetPlaylistItem) | **GET** /PlayerSources/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**](PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid) | **PATCH** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**](PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPutPlayerSourcesid) | **PUT** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourceReplaceById**](PlayerSourceApi.md#playerSourceReplaceById) | **POST** /PlayerSources/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerSourceReplaceOrCreate**](PlayerSourceApi.md#playerSourceReplaceOrCreate) | **POST** /PlayerSources/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerSourceUpdateAll**](PlayerSourceApi.md#playerSourceUpdateAll) | **POST** /PlayerSources/update | Update instances of the model matched by {{where}} from the data source.
[**playerSourceUpsertPatchPlayerSources**](PlayerSourceApi.md#playerSourceUpsertPatchPlayerSources) | **PATCH** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertPutPlayerSources**](PlayerSourceApi.md#playerSourceUpsertPutPlayerSources) | **PUT** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertWithWhere**](PlayerSourceApi.md#playerSourceUpsertWithWhere) | **POST** /PlayerSources/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playerSourceCount**
> \Swagger\Client\Model\InlineResponse2001 playerSourceCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playerSourceCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceCreate**
> \Swagger\Client\Model\PlayerSource playerSourceCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | Model instance data

try {
    $result = $api_instance->playerSourceCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**
> \SplFileObject playerSourceCreateChangeStreamGetPlayerSourcesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playerSourceCreateChangeStreamGetPlayerSourcesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceCreateChangeStreamGetPlayerSourcesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**
> \SplFileObject playerSourceCreateChangeStreamPostPlayerSourcesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playerSourceCreateChangeStreamPostPlayerSourcesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceCreateChangeStreamPostPlayerSourcesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceDeleteById**
> object playerSourceDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playerSourceDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceExistsGetPlayerSourcesidExists**
> \Swagger\Client\Model\InlineResponse2003 playerSourceExistsGetPlayerSourcesidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playerSourceExistsGetPlayerSourcesidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceExistsGetPlayerSourcesidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceExistsHeadPlayerSourcesid**
> \Swagger\Client\Model\InlineResponse2003 playerSourceExistsHeadPlayerSourcesid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playerSourceExistsHeadPlayerSourcesid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceExistsHeadPlayerSourcesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceFind**
> \Swagger\Client\Model\PlayerSource[] playerSourceFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playerSourceFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource[]**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceFindById**
> \Swagger\Client\Model\PlayerSource playerSourceFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->playerSourceFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceFindOne**
> \Swagger\Client\Model\PlayerSource playerSourceFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playerSourceFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourcePrototypeGetPlaylistItem**
> \Swagger\Client\Model\PlaylistItem playerSourcePrototypeGetPlaylistItem($id, $refresh)

Fetches belongsTo relation playlistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | PlayerSource id
$refresh = true; // bool | 

try {
    $result = $api_instance->playerSourcePrototypeGetPlaylistItem($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourcePrototypeGetPlaylistItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlayerSource id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**
> \Swagger\Client\Model\PlayerSource playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | PlayerSource id
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | An object of model property name/value pairs

try {
    $result = $api_instance->playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlayerSource id |
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**
> \Swagger\Client\Model\PlayerSource playerSourcePrototypeUpdateAttributesPutPlayerSourcesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | PlayerSource id
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | An object of model property name/value pairs

try {
    $result = $api_instance->playerSourcePrototypeUpdateAttributesPutPlayerSourcesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourcePrototypeUpdateAttributesPutPlayerSourcesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlayerSource id |
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceReplaceById**
> \Swagger\Client\Model\PlayerSource playerSourceReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | Model instance data

try {
    $result = $api_instance->playerSourceReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceReplaceOrCreate**
> \Swagger\Client\Model\PlayerSource playerSourceReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | Model instance data

try {
    $result = $api_instance->playerSourceReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 playerSourceUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | An object of model property name/value pairs

try {
    $result = $api_instance->playerSourceUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceUpsertPatchPlayerSources**
> \Swagger\Client\Model\PlayerSource playerSourceUpsertPatchPlayerSources($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | Model instance data

try {
    $result = $api_instance->playerSourceUpsertPatchPlayerSources($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceUpsertPatchPlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceUpsertPutPlayerSources**
> \Swagger\Client\Model\PlayerSource playerSourceUpsertPutPlayerSources($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | Model instance data

try {
    $result = $api_instance->playerSourceUpsertPutPlayerSources($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceUpsertPutPlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerSourceUpsertWithWhere**
> \Swagger\Client\Model\PlayerSource playerSourceUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerSourceApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | An object of model property name/value pairs

try {
    $result = $api_instance->playerSourceUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerSourceApi->playerSourceUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

