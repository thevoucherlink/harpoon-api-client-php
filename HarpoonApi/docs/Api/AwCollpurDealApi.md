# Harpoon\Api\AwCollpurDealApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealCount**](AwCollpurDealApi.md#awCollpurDealCount) | **GET** /AwCollpurDeals/count | Count instances of the model matched by where from the data source.
[**awCollpurDealCreate**](AwCollpurDealApi.md#awCollpurDealCreate) | **POST** /AwCollpurDeals | Create a new instance of the model and persist it into the data source.
[**awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**](AwCollpurDealApi.md#awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream) | **GET** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**](AwCollpurDealApi.md#awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream) | **POST** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealDeleteById**](AwCollpurDealApi.md#awCollpurDealDeleteById) | **DELETE** /AwCollpurDeals/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealExistsGetAwCollpurDealsidExists**](AwCollpurDealApi.md#awCollpurDealExistsGetAwCollpurDealsidExists) | **GET** /AwCollpurDeals/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealExistsHeadAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealExistsHeadAwCollpurDealsid) | **HEAD** /AwCollpurDeals/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealFind**](AwCollpurDealApi.md#awCollpurDealFind) | **GET** /AwCollpurDeals | Find all instances of the model matched by filter from the data source.
[**awCollpurDealFindById**](AwCollpurDealApi.md#awCollpurDealFindById) | **GET** /AwCollpurDeals/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealFindOne**](AwCollpurDealApi.md#awCollpurDealFindOne) | **GET** /AwCollpurDeals/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPrototypeCountAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeCountAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/count | Counts awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeCreateAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeCreateAwCollpurDealPurchases) | **POST** /AwCollpurDeals/{id}/awCollpurDealPurchases | Creates a new instance in awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDeleteAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeDeleteAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases | Deletes all awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Delete a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeFindByIdAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Find a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeGetAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeGetAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases | Queries awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid) | **PATCH** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid) | **PUT** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases) | **PUT** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Update a related item by id for awCollpurDealPurchases.
[**awCollpurDealReplaceById**](AwCollpurDealApi.md#awCollpurDealReplaceById) | **POST** /AwCollpurDeals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealReplaceOrCreate**](AwCollpurDealApi.md#awCollpurDealReplaceOrCreate) | **POST** /AwCollpurDeals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealUpdateAll**](AwCollpurDealApi.md#awCollpurDealUpdateAll) | **POST** /AwCollpurDeals/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealUpsertPatchAwCollpurDeals**](AwCollpurDealApi.md#awCollpurDealUpsertPatchAwCollpurDeals) | **PATCH** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertPutAwCollpurDeals**](AwCollpurDealApi.md#awCollpurDealUpsertPutAwCollpurDeals) | **PUT** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertWithWhere**](AwCollpurDealApi.md#awCollpurDealUpsertWithWhere) | **POST** /AwCollpurDeals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awCollpurDealCount**
> \Swagger\Client\Model\InlineResponse2001 awCollpurDealCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awCollpurDealCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealCreate**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | Model instance data

try {
    $result = $api_instance->awCollpurDealCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**
> \SplFileObject awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**
> \SplFileObject awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealDeleteById**
> object awCollpurDealDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurDealDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealExistsGetAwCollpurDealsidExists**
> \Swagger\Client\Model\InlineResponse2003 awCollpurDealExistsGetAwCollpurDealsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurDealExistsGetAwCollpurDealsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealExistsGetAwCollpurDealsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealExistsHeadAwCollpurDealsid**
> \Swagger\Client\Model\InlineResponse2003 awCollpurDealExistsHeadAwCollpurDealsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurDealExistsHeadAwCollpurDealsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealExistsHeadAwCollpurDealsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealFind**
> \Swagger\Client\Model\AwCollpurDeal[] awCollpurDealFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awCollpurDealFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal[]**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealFindById**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->awCollpurDealFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealFindOne**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awCollpurDealFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeCountAwCollpurDealPurchases**
> \Swagger\Client\Model\InlineResponse2001 awCollpurDealPrototypeCountAwCollpurDealPurchases($id, $where)

Counts awCollpurDealPurchases of AwCollpurDeal.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | AwCollpurDeal id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awCollpurDealPrototypeCountAwCollpurDealPurchases($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeCountAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDeal id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeCreateAwCollpurDealPurchases**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPrototypeCreateAwCollpurDealPurchases($id, $data)

Creates a new instance in awCollpurDealPurchases of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | AwCollpurDeal id
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | 

try {
    $result = $api_instance->awCollpurDealPrototypeCreateAwCollpurDealPurchases($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeCreateAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDeal id |
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeDeleteAwCollpurDealPurchases**
> awCollpurDealPrototypeDeleteAwCollpurDealPurchases($id)

Deletes all awCollpurDealPurchases of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | AwCollpurDeal id

try {
    $api_instance->awCollpurDealPrototypeDeleteAwCollpurDealPurchases($id);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeDeleteAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDeal id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**
> awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases($fk, $id)

Delete a related item by id for awCollpurDealPurchases.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$fk = "fk_example"; // string | Foreign key for awCollpurDealPurchases
$id = "id_example"; // string | AwCollpurDeal id

try {
    $api_instance->awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for awCollpurDealPurchases |
 **id** | **string**| AwCollpurDeal id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPrototypeFindByIdAwCollpurDealPurchases($fk, $id)

Find a related item by id for awCollpurDealPurchases.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$fk = "fk_example"; // string | Foreign key for awCollpurDealPurchases
$id = "id_example"; // string | AwCollpurDeal id

try {
    $result = $api_instance->awCollpurDealPrototypeFindByIdAwCollpurDealPurchases($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeFindByIdAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for awCollpurDealPurchases |
 **id** | **string**| AwCollpurDeal id |

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeGetAwCollpurDealPurchases**
> \Swagger\Client\Model\AwCollpurDealPurchases[] awCollpurDealPrototypeGetAwCollpurDealPurchases($id, $filter)

Queries awCollpurDealPurchases of AwCollpurDeal.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | AwCollpurDeal id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->awCollpurDealPrototypeGetAwCollpurDealPurchases($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeGetAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDeal id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases[]**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | AwCollpurDeal id
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDeal id |
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | AwCollpurDeal id
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDeal id |
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases($fk, $id, $data)

Update a related item by id for awCollpurDealPurchases.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$fk = "fk_example"; // string | Foreign key for awCollpurDealPurchases
$id = "id_example"; // string | AwCollpurDeal id
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | 

try {
    $result = $api_instance->awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for awCollpurDealPurchases |
 **id** | **string**| AwCollpurDeal id |
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealReplaceById**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | Model instance data

try {
    $result = $api_instance->awCollpurDealReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealReplaceOrCreate**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | Model instance data

try {
    $result = $api_instance->awCollpurDealReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 awCollpurDealUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealUpsertPatchAwCollpurDeals**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealUpsertPatchAwCollpurDeals($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | Model instance data

try {
    $result = $api_instance->awCollpurDealUpsertPatchAwCollpurDeals($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealUpsertPatchAwCollpurDeals: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealUpsertPutAwCollpurDeals**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealUpsertPutAwCollpurDeals($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | Model instance data

try {
    $result = $api_instance->awCollpurDealUpsertPutAwCollpurDeals($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealUpsertPutAwCollpurDeals: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealUpsertWithWhere**
> \Swagger\Client\Model\AwCollpurDeal awCollpurDealUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealApi->awCollpurDealUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

