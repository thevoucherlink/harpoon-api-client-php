# Harpoon\Api\CompetitionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**competitionAttendees**](CompetitionApi.md#competitionAttendees) | **GET** /Competitions/{id}/attendees | 
[**competitionCheckout**](CompetitionApi.md#competitionCheckout) | **POST** /Competitions/{id}/checkout | 
[**competitionFind**](CompetitionApi.md#competitionFind) | **GET** /Competitions | Find all instances of the model matched by filter from the data source.
[**competitionFindById**](CompetitionApi.md#competitionFindById) | **GET** /Competitions/{id} | Find a model instance by {{id}} from the data source.
[**competitionFindOne**](CompetitionApi.md#competitionFindOne) | **GET** /Competitions/findOne | Find first instance of the model matched by filter from the data source.
[**competitionRedeem**](CompetitionApi.md#competitionRedeem) | **POST** /Competitions/{id}/redeem | 
[**competitionReplaceById**](CompetitionApi.md#competitionReplaceById) | **POST** /Competitions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**competitionReplaceOrCreate**](CompetitionApi.md#competitionReplaceOrCreate) | **POST** /Competitions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**competitionUpsertWithWhere**](CompetitionApi.md#competitionUpsertWithWhere) | **POST** /Competitions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**competitionValidateAnswer**](CompetitionApi.md#competitionValidateAnswer) | **POST** /Competitions/{id}/validateAnswer | 
[**competitionVenues**](CompetitionApi.md#competitionVenues) | **GET** /Competitions/{id}/venues | 


# **competitionAttendees**
> \Swagger\Client\Model\Customer[] competitionAttendees($id, $filter)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->competitionAttendees($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionAttendees: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\Customer[]**](../Model/Customer.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionCheckout**
> \Swagger\Client\Model\CompetitionCheckout competitionCheckout($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\CompetitionCheckoutData(); // \Swagger\Client\Model\CompetitionCheckoutData | 

try {
    $result = $api_instance->competitionCheckout($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionCheckout: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\CompetitionCheckoutData**](../Model/\Swagger\Client\Model\CompetitionCheckoutData.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompetitionCheckout**](../Model/CompetitionCheckout.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionFind**
> \Swagger\Client\Model\Competition[] competitionFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->competitionFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\Competition[]**](../Model/Competition.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionFindById**
> \Swagger\Client\Model\Competition competitionFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->competitionFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\Competition**](../Model/Competition.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionFindOne**
> \Swagger\Client\Model\Competition competitionFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->competitionFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\Competition**](../Model/Competition.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionRedeem**
> \Swagger\Client\Model\CompetitionPurchase competitionRedeem($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\CompetitionRedeemData(); // \Swagger\Client\Model\CompetitionRedeemData | 

try {
    $result = $api_instance->competitionRedeem($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionRedeem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\CompetitionRedeemData**](../Model/\Swagger\Client\Model\CompetitionRedeemData.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompetitionPurchase**](../Model/CompetitionPurchase.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionReplaceById**
> \Swagger\Client\Model\Competition competitionReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\Competition(); // \Swagger\Client\Model\Competition | Model instance data

try {
    $result = $api_instance->competitionReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\Competition**](../Model/\Swagger\Client\Model\Competition.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Competition**](../Model/Competition.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionReplaceOrCreate**
> \Swagger\Client\Model\Competition competitionReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$data = new \Swagger\Client\Model\Competition(); // \Swagger\Client\Model\Competition | Model instance data

try {
    $result = $api_instance->competitionReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\Competition**](../Model/\Swagger\Client\Model\Competition.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Competition**](../Model/Competition.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionUpsertWithWhere**
> \Swagger\Client\Model\Competition competitionUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\Competition(); // \Swagger\Client\Model\Competition | An object of model property name/value pairs

try {
    $result = $api_instance->competitionUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\Competition**](../Model/\Swagger\Client\Model\Competition.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\Competition**](../Model/Competition.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionValidateAnswer**
> object competitionValidateAnswer($id, $answer)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$id = "id_example"; // string | Model id
$answer = new \Swagger\Client\Model\CompetitionAnswerData(); // \Swagger\Client\Model\CompetitionAnswerData | Competition Answer

try {
    $result = $api_instance->competitionValidateAnswer($id, $answer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionValidateAnswer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **answer** | [**\Swagger\Client\Model\CompetitionAnswerData**](../Model/\Swagger\Client\Model\CompetitionAnswerData.md)| Competition Answer |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **competitionVenues**
> \Swagger\Client\Model\Venue[] competitionVenues($id, $filter)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CompetitionApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->competitionVenues($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompetitionApi->competitionVenues: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\Venue[]**](../Model/Venue.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

