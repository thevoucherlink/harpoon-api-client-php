# Harpoon\Api\CoreWebsiteApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**coreWebsiteCount**](CoreWebsiteApi.md#coreWebsiteCount) | **GET** /CoreWebsites/count | Count instances of the model matched by where from the data source.
[**coreWebsiteCreate**](CoreWebsiteApi.md#coreWebsiteCreate) | **POST** /CoreWebsites | Create a new instance of the model and persist it into the data source.
[**coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**](CoreWebsiteApi.md#coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream) | **GET** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**](CoreWebsiteApi.md#coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream) | **POST** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteDeleteById**](CoreWebsiteApi.md#coreWebsiteDeleteById) | **DELETE** /CoreWebsites/{id} | Delete a model instance by {{id}} from the data source.
[**coreWebsiteExistsGetCoreWebsitesidExists**](CoreWebsiteApi.md#coreWebsiteExistsGetCoreWebsitesidExists) | **GET** /CoreWebsites/{id}/exists | Check whether a model instance exists in the data source.
[**coreWebsiteExistsHeadCoreWebsitesid**](CoreWebsiteApi.md#coreWebsiteExistsHeadCoreWebsitesid) | **HEAD** /CoreWebsites/{id} | Check whether a model instance exists in the data source.
[**coreWebsiteFind**](CoreWebsiteApi.md#coreWebsiteFind) | **GET** /CoreWebsites | Find all instances of the model matched by filter from the data source.
[**coreWebsiteFindById**](CoreWebsiteApi.md#coreWebsiteFindById) | **GET** /CoreWebsites/{id} | Find a model instance by {{id}} from the data source.
[**coreWebsiteFindOne**](CoreWebsiteApi.md#coreWebsiteFindOne) | **GET** /CoreWebsites/findOne | Find first instance of the model matched by filter from the data source.
[**coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**](CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid) | **PATCH** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**](CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid) | **PUT** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceById**](CoreWebsiteApi.md#coreWebsiteReplaceById) | **POST** /CoreWebsites/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceOrCreate**](CoreWebsiteApi.md#coreWebsiteReplaceOrCreate) | **POST** /CoreWebsites/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**coreWebsiteUpdateAll**](CoreWebsiteApi.md#coreWebsiteUpdateAll) | **POST** /CoreWebsites/update | Update instances of the model matched by {{where}} from the data source.
[**coreWebsiteUpsertPatchCoreWebsites**](CoreWebsiteApi.md#coreWebsiteUpsertPatchCoreWebsites) | **PATCH** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertPutCoreWebsites**](CoreWebsiteApi.md#coreWebsiteUpsertPutCoreWebsites) | **PUT** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertWithWhere**](CoreWebsiteApi.md#coreWebsiteUpsertWithWhere) | **POST** /CoreWebsites/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **coreWebsiteCount**
> \Swagger\Client\Model\InlineResponse2001 coreWebsiteCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->coreWebsiteCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteCreate**
> \Swagger\Client\Model\CoreWebsite coreWebsiteCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | Model instance data

try {
    $result = $api_instance->coreWebsiteCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**
> \SplFileObject coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**
> \SplFileObject coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteDeleteById**
> object coreWebsiteDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->coreWebsiteDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteExistsGetCoreWebsitesidExists**
> \Swagger\Client\Model\InlineResponse2003 coreWebsiteExistsGetCoreWebsitesidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->coreWebsiteExistsGetCoreWebsitesidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteExistsGetCoreWebsitesidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteExistsHeadCoreWebsitesid**
> \Swagger\Client\Model\InlineResponse2003 coreWebsiteExistsHeadCoreWebsitesid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->coreWebsiteExistsHeadCoreWebsitesid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteExistsHeadCoreWebsitesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteFind**
> \Swagger\Client\Model\CoreWebsite[] coreWebsiteFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->coreWebsiteFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite[]**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteFindById**
> \Swagger\Client\Model\CoreWebsite coreWebsiteFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->coreWebsiteFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteFindOne**
> \Swagger\Client\Model\CoreWebsite coreWebsiteFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->coreWebsiteFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**
> \Swagger\Client\Model\CoreWebsite coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$id = "id_example"; // string | CoreWebsite id
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | An object of model property name/value pairs

try {
    $result = $api_instance->coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CoreWebsite id |
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**
> \Swagger\Client\Model\CoreWebsite coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$id = "id_example"; // string | CoreWebsite id
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | An object of model property name/value pairs

try {
    $result = $api_instance->coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CoreWebsite id |
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteReplaceById**
> \Swagger\Client\Model\CoreWebsite coreWebsiteReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | Model instance data

try {
    $result = $api_instance->coreWebsiteReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteReplaceOrCreate**
> \Swagger\Client\Model\CoreWebsite coreWebsiteReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | Model instance data

try {
    $result = $api_instance->coreWebsiteReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 coreWebsiteUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | An object of model property name/value pairs

try {
    $result = $api_instance->coreWebsiteUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteUpsertPatchCoreWebsites**
> \Swagger\Client\Model\CoreWebsite coreWebsiteUpsertPatchCoreWebsites($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | Model instance data

try {
    $result = $api_instance->coreWebsiteUpsertPatchCoreWebsites($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteUpsertPatchCoreWebsites: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteUpsertPutCoreWebsites**
> \Swagger\Client\Model\CoreWebsite coreWebsiteUpsertPutCoreWebsites($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | Model instance data

try {
    $result = $api_instance->coreWebsiteUpsertPutCoreWebsites($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteUpsertPutCoreWebsites: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **coreWebsiteUpsertWithWhere**
> \Swagger\Client\Model\CoreWebsite coreWebsiteUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CoreWebsiteApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\CoreWebsite(); // \Swagger\Client\Model\CoreWebsite | An object of model property name/value pairs

try {
    $result = $api_instance->coreWebsiteUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CoreWebsiteApi->coreWebsiteUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\CoreWebsite**](../Model/\Swagger\Client\Model\CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CoreWebsite**](../Model/CoreWebsite.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

