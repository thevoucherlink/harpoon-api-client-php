# Harpoon\Api\AwEventbookingTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingTicketCount**](AwEventbookingTicketApi.md#awEventbookingTicketCount) | **GET** /AwEventbookingTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingTicketCreate**](AwEventbookingTicketApi.md#awEventbookingTicketCreate) | **POST** /AwEventbookingTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**](AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream) | **GET** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**](AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream) | **POST** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketDeleteById**](AwEventbookingTicketApi.md#awEventbookingTicketDeleteById) | **DELETE** /AwEventbookingTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingTicketExistsGetAwEventbookingTicketsidExists**](AwEventbookingTicketApi.md#awEventbookingTicketExistsGetAwEventbookingTicketsidExists) | **GET** /AwEventbookingTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingTicketExistsHeadAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketExistsHeadAwEventbookingTicketsid) | **HEAD** /AwEventbookingTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingTicketFind**](AwEventbookingTicketApi.md#awEventbookingTicketFind) | **GET** /AwEventbookingTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingTicketFindById**](AwEventbookingTicketApi.md#awEventbookingTicketFindById) | **GET** /AwEventbookingTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingTicketFindOne**](AwEventbookingTicketApi.md#awEventbookingTicketFindOne) | **GET** /AwEventbookingTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid) | **PATCH** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid) | **PUT** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceById**](AwEventbookingTicketApi.md#awEventbookingTicketReplaceById) | **POST** /AwEventbookingTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceOrCreate**](AwEventbookingTicketApi.md#awEventbookingTicketReplaceOrCreate) | **POST** /AwEventbookingTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpdateAll**](AwEventbookingTicketApi.md#awEventbookingTicketUpdateAll) | **POST** /AwEventbookingTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingTicketUpsertPatchAwEventbookingTickets**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertPatchAwEventbookingTickets) | **PATCH** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertPutAwEventbookingTickets**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertPutAwEventbookingTickets) | **PUT** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertWithWhere**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertWithWhere) | **POST** /AwEventbookingTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingTicketCount**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingTicketCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingTicketCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketCreate**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | Model instance data

try {
    $result = $api_instance->awEventbookingTicketCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**
> \SplFileObject awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**
> \SplFileObject awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketDeleteById**
> object awEventbookingTicketDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingTicketDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketExistsGetAwEventbookingTicketsidExists**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingTicketExistsGetAwEventbookingTicketsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingTicketExistsGetAwEventbookingTicketsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketExistsGetAwEventbookingTicketsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketExistsHeadAwEventbookingTicketsid**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingTicketExistsHeadAwEventbookingTicketsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingTicketExistsHeadAwEventbookingTicketsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketExistsHeadAwEventbookingTicketsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketFind**
> \Swagger\Client\Model\AwEventbookingTicket[] awEventbookingTicketFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingTicketFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket[]**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketFindById**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->awEventbookingTicketFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketFindOne**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingTicketFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$id = "id_example"; // string | AwEventbookingTicket id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$id = "id_example"; // string | AwEventbookingTicket id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketReplaceById**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | Model instance data

try {
    $result = $api_instance->awEventbookingTicketReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketReplaceOrCreate**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | Model instance data

try {
    $result = $api_instance->awEventbookingTicketReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 awEventbookingTicketUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingTicketUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketUpsertPatchAwEventbookingTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketUpsertPatchAwEventbookingTickets($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | Model instance data

try {
    $result = $api_instance->awEventbookingTicketUpsertPatchAwEventbookingTickets($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketUpsertPatchAwEventbookingTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketUpsertPutAwEventbookingTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketUpsertPutAwEventbookingTickets($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | Model instance data

try {
    $result = $api_instance->awEventbookingTicketUpsertPutAwEventbookingTickets($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketUpsertPutAwEventbookingTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingTicketUpsertWithWhere**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingTicketUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingTicketApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingTicketUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingTicketApi->awEventbookingTicketUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

