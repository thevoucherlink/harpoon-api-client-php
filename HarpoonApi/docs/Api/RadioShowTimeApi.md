# Harpoon\Api\RadioShowTimeApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowTimeCount**](RadioShowTimeApi.md#radioShowTimeCount) | **GET** /RadioShowTimes/count | Count instances of the model matched by where from the data source.
[**radioShowTimeCreate**](RadioShowTimeApi.md#radioShowTimeCreate) | **POST** /RadioShowTimes | Create a new instance of the model and persist it into the data source.
[**radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**](RadioShowTimeApi.md#radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream) | **GET** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**](RadioShowTimeApi.md#radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream) | **POST** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeDeleteById**](RadioShowTimeApi.md#radioShowTimeDeleteById) | **DELETE** /RadioShowTimes/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowTimeExistsGetRadioShowTimesidExists**](RadioShowTimeApi.md#radioShowTimeExistsGetRadioShowTimesidExists) | **GET** /RadioShowTimes/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowTimeExistsHeadRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimeExistsHeadRadioShowTimesid) | **HEAD** /RadioShowTimes/{id} | Check whether a model instance exists in the data source.
[**radioShowTimeFind**](RadioShowTimeApi.md#radioShowTimeFind) | **GET** /RadioShowTimes | Find all instances of the model matched by filter from the data source.
[**radioShowTimeFindById**](RadioShowTimeApi.md#radioShowTimeFindById) | **GET** /RadioShowTimes/{id} | Find a model instance by {{id}} from the data source.
[**radioShowTimeFindOne**](RadioShowTimeApi.md#radioShowTimeFindOne) | **GET** /RadioShowTimes/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowTimePrototypeGetRadioShow**](RadioShowTimeApi.md#radioShowTimePrototypeGetRadioShow) | **GET** /RadioShowTimes/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid) | **PATCH** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid) | **PUT** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceById**](RadioShowTimeApi.md#radioShowTimeReplaceById) | **POST** /RadioShowTimes/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceOrCreate**](RadioShowTimeApi.md#radioShowTimeReplaceOrCreate) | **POST** /RadioShowTimes/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowTimeUpdateAll**](RadioShowTimeApi.md#radioShowTimeUpdateAll) | **POST** /RadioShowTimes/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowTimeUpsertPatchRadioShowTimes**](RadioShowTimeApi.md#radioShowTimeUpsertPatchRadioShowTimes) | **PATCH** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertPutRadioShowTimes**](RadioShowTimeApi.md#radioShowTimeUpsertPutRadioShowTimes) | **PUT** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertWithWhere**](RadioShowTimeApi.md#radioShowTimeUpsertWithWhere) | **POST** /RadioShowTimes/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioShowTimeCount**
> \Swagger\Client\Model\InlineResponse2001 radioShowTimeCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioShowTimeCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeCreate**
> \Swagger\Client\Model\RadioShowTime radioShowTimeCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | Model instance data

try {
    $result = $api_instance->radioShowTimeCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**
> \SplFileObject radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**
> \SplFileObject radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeDeleteById**
> object radioShowTimeDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioShowTimeDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeExistsGetRadioShowTimesidExists**
> \Swagger\Client\Model\InlineResponse2003 radioShowTimeExistsGetRadioShowTimesidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioShowTimeExistsGetRadioShowTimesidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeExistsGetRadioShowTimesidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeExistsHeadRadioShowTimesid**
> \Swagger\Client\Model\InlineResponse2003 radioShowTimeExistsHeadRadioShowTimesid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioShowTimeExistsHeadRadioShowTimesid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeExistsHeadRadioShowTimesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeFind**
> \Swagger\Client\Model\RadioShowTime[] radioShowTimeFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioShowTimeFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime[]**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeFindById**
> \Swagger\Client\Model\RadioShowTime radioShowTimeFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->radioShowTimeFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeFindOne**
> \Swagger\Client\Model\RadioShowTime radioShowTimeFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioShowTimeFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimePrototypeGetRadioShow**
> \Swagger\Client\Model\RadioShow radioShowTimePrototypeGetRadioShow($id, $refresh)

Fetches belongsTo relation radioShow.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | RadioShowTime id
$refresh = true; // bool | 

try {
    $result = $api_instance->radioShowTimePrototypeGetRadioShow($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimePrototypeGetRadioShow: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShowTime id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**
> \Swagger\Client\Model\RadioShowTime radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | RadioShowTime id
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShowTime id |
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**
> \Swagger\Client\Model\RadioShowTime radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | RadioShowTime id
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioShowTime id |
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeReplaceById**
> \Swagger\Client\Model\RadioShowTime radioShowTimeReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | Model instance data

try {
    $result = $api_instance->radioShowTimeReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeReplaceOrCreate**
> \Swagger\Client\Model\RadioShowTime radioShowTimeReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | Model instance data

try {
    $result = $api_instance->radioShowTimeReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 radioShowTimeUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowTimeUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeUpsertPatchRadioShowTimes**
> \Swagger\Client\Model\RadioShowTime radioShowTimeUpsertPatchRadioShowTimes($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | Model instance data

try {
    $result = $api_instance->radioShowTimeUpsertPatchRadioShowTimes($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeUpsertPatchRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeUpsertPutRadioShowTimes**
> \Swagger\Client\Model\RadioShowTime radioShowTimeUpsertPutRadioShowTimes($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | Model instance data

try {
    $result = $api_instance->radioShowTimeUpsertPutRadioShowTimes($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeUpsertPutRadioShowTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioShowTimeUpsertWithWhere**
> \Swagger\Client\Model\RadioShowTime radioShowTimeUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioShowTimeApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioShowTime(); // \Swagger\Client\Model\RadioShowTime | An object of model property name/value pairs

try {
    $result = $api_instance->radioShowTimeUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioShowTimeApi->radioShowTimeUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioShowTime**](../Model/\Swagger\Client\Model\RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioShowTime**](../Model/RadioShowTime.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

