# Harpoon\Api\AwEventbookingOrderHistoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingOrderHistoryCount**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCount) | **GET** /AwEventbookingOrderHistories/count | Count instances of the model matched by where from the data source.
[**awEventbookingOrderHistoryCreate**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreate) | **POST** /AwEventbookingOrderHistories | Create a new instance of the model and persist it into the data source.
[**awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream) | **GET** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream) | **POST** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryDeleteById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryDeleteById) | **DELETE** /AwEventbookingOrderHistories/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists) | **GET** /AwEventbookingOrderHistories/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid) | **HEAD** /AwEventbookingOrderHistories/{id} | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryFind**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFind) | **GET** /AwEventbookingOrderHistories | Find all instances of the model matched by filter from the data source.
[**awEventbookingOrderHistoryFindById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindById) | **GET** /AwEventbookingOrderHistories/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryFindOne**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindOne) | **GET** /AwEventbookingOrderHistories/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid) | **PATCH** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid) | **PUT** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceById) | **POST** /AwEventbookingOrderHistories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceOrCreate**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceOrCreate) | **POST** /AwEventbookingOrderHistories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpdateAll**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpdateAll) | **POST** /AwEventbookingOrderHistories/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories) | **PATCH** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories) | **PUT** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertWithWhere**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertWithWhere) | **POST** /AwEventbookingOrderHistories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingOrderHistoryCount**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingOrderHistoryCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingOrderHistoryCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryCreate**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | Model instance data

try {
    $result = $api_instance->awEventbookingOrderHistoryCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**
> \SplFileObject awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**
> \SplFileObject awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryDeleteById**
> object awEventbookingOrderHistoryDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingOrderHistoryDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryFind**
> \Swagger\Client\Model\AwEventbookingOrderHistory[] awEventbookingOrderHistoryFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingOrderHistoryFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory[]**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryFindById**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->awEventbookingOrderHistoryFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryFindOne**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingOrderHistoryFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$id = "id_example"; // string | AwEventbookingOrderHistory id
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingOrderHistory id |
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$id = "id_example"; // string | AwEventbookingOrderHistory id
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingOrderHistory id |
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryReplaceById**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | Model instance data

try {
    $result = $api_instance->awEventbookingOrderHistoryReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryReplaceOrCreate**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | Model instance data

try {
    $result = $api_instance->awEventbookingOrderHistoryReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 awEventbookingOrderHistoryUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingOrderHistoryUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | Model instance data

try {
    $result = $api_instance->awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | Model instance data

try {
    $result = $api_instance->awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingOrderHistoryUpsertWithWhere**
> \Swagger\Client\Model\AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingOrderHistoryApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingOrderHistory(); // \Swagger\Client\Model\AwEventbookingOrderHistory | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingOrderHistoryUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/\Swagger\Client\Model\AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingOrderHistory**](../Model/AwEventbookingOrderHistory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

