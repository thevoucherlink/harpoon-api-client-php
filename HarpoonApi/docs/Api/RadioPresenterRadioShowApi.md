# Harpoon\Api\RadioPresenterRadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterRadioShowCount**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCount) | **GET** /RadioPresenterRadioShows/count | Count instances of the model matched by where from the data source.
[**radioPresenterRadioShowCreate**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreate) | **POST** /RadioPresenterRadioShows | Create a new instance of the model and persist it into the data source.
[**radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream) | **GET** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream) | **POST** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowDeleteById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowDeleteById) | **DELETE** /RadioPresenterRadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists) | **GET** /RadioPresenterRadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid) | **HEAD** /RadioPresenterRadioShows/{id} | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowFind**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFind) | **GET** /RadioPresenterRadioShows | Find all instances of the model matched by filter from the data source.
[**radioPresenterRadioShowFindById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindById) | **GET** /RadioPresenterRadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterRadioShowFindOne**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindOne) | **GET** /RadioPresenterRadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterRadioShowPrototypeGetRadioPresenter**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioPresenter) | **GET** /RadioPresenterRadioShows/{id}/radioPresenter | Fetches belongsTo relation radioPresenter.
[**radioPresenterRadioShowPrototypeGetRadioShow**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioShow) | **GET** /RadioPresenterRadioShows/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid) | **PATCH** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid) | **PUT** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceById) | **POST** /RadioPresenterRadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceOrCreate**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceOrCreate) | **POST** /RadioPresenterRadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpdateAll**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpdateAll) | **POST** /RadioPresenterRadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows) | **PATCH** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPutRadioPresenterRadioShows) | **PUT** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertWithWhere**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertWithWhere) | **POST** /RadioPresenterRadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioPresenterRadioShowCount**
> \Swagger\Client\Model\InlineResponse2001 radioPresenterRadioShowCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioPresenterRadioShowCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowCreate**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | Model instance data

try {
    $result = $api_instance->radioPresenterRadioShowCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**
> \SplFileObject radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**
> \SplFileObject radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowDeleteById**
> object radioPresenterRadioShowDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPresenterRadioShowDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**
> \Swagger\Client\Model\InlineResponse2003 radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**
> \Swagger\Client\Model\InlineResponse2003 radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowFind**
> \Swagger\Client\Model\RadioPresenterRadioShow[] radioPresenterRadioShowFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioPresenterRadioShowFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow[]**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowFindById**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->radioPresenterRadioShowFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowFindOne**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioPresenterRadioShowFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowPrototypeGetRadioPresenter**
> \Swagger\Client\Model\RadioPresenter radioPresenterRadioShowPrototypeGetRadioPresenter($id, $refresh)

Fetches belongsTo relation radioPresenter.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | RadioPresenterRadioShow id
$refresh = true; // bool | 

try {
    $result = $api_instance->radioPresenterRadioShowPrototypeGetRadioPresenter($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeGetRadioPresenter: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenterRadioShow id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenter**](../Model/RadioPresenter.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowPrototypeGetRadioShow**
> \Swagger\Client\Model\RadioShow radioPresenterRadioShowPrototypeGetRadioShow($id, $refresh)

Fetches belongsTo relation radioShow.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | RadioPresenterRadioShow id
$refresh = true; // bool | 

try {
    $result = $api_instance->radioPresenterRadioShowPrototypeGetRadioShow($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeGetRadioShow: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenterRadioShow id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | RadioPresenterRadioShow id
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenterRadioShow id |
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | RadioPresenterRadioShow id
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioPresenterRadioShow id |
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowReplaceById**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | Model instance data

try {
    $result = $api_instance->radioPresenterRadioShowReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowReplaceOrCreate**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | Model instance data

try {
    $result = $api_instance->radioPresenterRadioShowReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 radioPresenterRadioShowUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterRadioShowUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | Model instance data

try {
    $result = $api_instance->radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowUpsertPutRadioPresenterRadioShows($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | Model instance data

try {
    $result = $api_instance->radioPresenterRadioShowUpsertPutRadioPresenterRadioShows($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowUpsertPutRadioPresenterRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioPresenterRadioShowUpsertWithWhere**
> \Swagger\Client\Model\RadioPresenterRadioShow radioPresenterRadioShowUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioPresenterRadioShowApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioPresenterRadioShow(); // \Swagger\Client\Model\RadioPresenterRadioShow | An object of model property name/value pairs

try {
    $result = $api_instance->radioPresenterRadioShowUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioPresenterRadioShowApi->radioPresenterRadioShowUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/\Swagger\Client\Model\RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioPresenterRadioShow**](../Model/RadioPresenterRadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

