# Harpoon\Api\AwEventbookingEventApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventCount**](AwEventbookingEventApi.md#awEventbookingEventCount) | **GET** /AwEventbookingEvents/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventCreate**](AwEventbookingEventApi.md#awEventbookingEventCreate) | **POST** /AwEventbookingEvents | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**](AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream) | **GET** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**](AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream) | **POST** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventDeleteById**](AwEventbookingEventApi.md#awEventbookingEventDeleteById) | **DELETE** /AwEventbookingEvents/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventExistsGetAwEventbookingEventsidExists**](AwEventbookingEventApi.md#awEventbookingEventExistsGetAwEventbookingEventsidExists) | **GET** /AwEventbookingEvents/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventExistsHeadAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventExistsHeadAwEventbookingEventsid) | **HEAD** /AwEventbookingEvents/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventFind**](AwEventbookingEventApi.md#awEventbookingEventFind) | **GET** /AwEventbookingEvents | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventFindById**](AwEventbookingEventApi.md#awEventbookingEventFindById) | **GET** /AwEventbookingEvents/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventFindOne**](AwEventbookingEventApi.md#awEventbookingEventFindOne) | **GET** /AwEventbookingEvents/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventPrototypeCountAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/count | Counts attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/count | Counts purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountTickets) | **GET** /AwEventbookingEvents/{id}/tickets/count | Counts tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCreateAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateAttributes) | **POST** /AwEventbookingEvents/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventPrototypeCreatePurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreatePurchasedTickets) | **POST** /AwEventbookingEvents/{id}/purchasedTickets | Creates a new instance in purchasedTickets of this model.
[**awEventbookingEventPrototypeCreateTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateTickets) | **POST** /AwEventbookingEvents/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventPrototypeDeleteAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventPrototypeDeletePurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeletePurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets | Deletes all purchasedTickets of this model.
[**awEventbookingEventPrototypeDeleteTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventPrototypeDestroyByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventPrototypeDestroyByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Delete a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeDestroyByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventPrototypeExistsPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeExistsPurchasedTickets) | **HEAD** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Check the existence of purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeFindByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventPrototypeFindByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Find a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeFindByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdTickets) | **GET** /AwEventbookingEvents/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventPrototypeGetAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetAttributes) | **GET** /AwEventbookingEvents/{id}/attributes | Queries attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets | Queries purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetTickets) | **GET** /AwEventbookingEvents/{id}/tickets | Queries tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeLinkPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeLinkPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Add a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUnlinkPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUnlinkPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Remove the purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid) | **PATCH** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid) | **PUT** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEvents/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventPrototypeUpdateByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Update a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUpdateByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEvents/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventReplaceById**](AwEventbookingEventApi.md#awEventbookingEventReplaceById) | **POST** /AwEventbookingEvents/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventReplaceOrCreate**](AwEventbookingEventApi.md#awEventbookingEventReplaceOrCreate) | **POST** /AwEventbookingEvents/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpdateAll**](AwEventbookingEventApi.md#awEventbookingEventUpdateAll) | **POST** /AwEventbookingEvents/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventUpsertPatchAwEventbookingEvents**](AwEventbookingEventApi.md#awEventbookingEventUpsertPatchAwEventbookingEvents) | **PATCH** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertPutAwEventbookingEvents**](AwEventbookingEventApi.md#awEventbookingEventUpsertPutAwEventbookingEvents) | **PUT** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertWithWhere**](AwEventbookingEventApi.md#awEventbookingEventUpsertWithWhere) | **POST** /AwEventbookingEvents/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingEventCount**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventCreate**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | Model instance data

try {
    $result = $api_instance->awEventbookingEventCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**
> \SplFileObject awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**
> \SplFileObject awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventDeleteById**
> object awEventbookingEventDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingEventDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventExistsGetAwEventbookingEventsidExists**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingEventExistsGetAwEventbookingEventsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingEventExistsGetAwEventbookingEventsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventExistsGetAwEventbookingEventsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventExistsHeadAwEventbookingEventsid**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingEventExistsHeadAwEventbookingEventsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingEventExistsHeadAwEventbookingEventsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventExistsHeadAwEventbookingEventsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventFind**
> \Swagger\Client\Model\AwEventbookingEvent[] awEventbookingEventFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingEventFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent[]**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventFindById**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->awEventbookingEventFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventFindOne**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingEventFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeCountAttributes**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventPrototypeCountAttributes($id, $where)

Counts attributes of AwEventbookingEvent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventPrototypeCountAttributes($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeCountAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeCountPurchasedTickets**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventPrototypeCountPurchasedTickets($id, $where)

Counts purchasedTickets of AwEventbookingEvent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventPrototypeCountPurchasedTickets($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeCountPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeCountTickets**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventPrototypeCountTickets($id, $where)

Counts tickets of AwEventbookingEvent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventPrototypeCountTickets($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeCountTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeCreateAttributes**
> \Swagger\Client\Model\AwEventbookingEventAttribute awEventbookingEventPrototypeCreateAttributes($id, $data)

Creates a new instance in attributes of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingEventAttribute(); // \Swagger\Client\Model\AwEventbookingEventAttribute | 

try {
    $result = $api_instance->awEventbookingEventPrototypeCreateAttributes($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeCreateAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventAttribute**](../Model/\Swagger\Client\Model\AwEventbookingEventAttribute.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventAttribute**](../Model/AwEventbookingEventAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeCreatePurchasedTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventPrototypeCreatePurchasedTickets($id, $data)

Creates a new instance in purchasedTickets of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | 

try {
    $result = $api_instance->awEventbookingEventPrototypeCreatePurchasedTickets($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeCreatePurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeCreateTickets**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventPrototypeCreateTickets($id, $data)

Creates a new instance in tickets of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | 

try {
    $result = $api_instance->awEventbookingEventPrototypeCreateTickets($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeCreateTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeDeleteAttributes**
> awEventbookingEventPrototypeDeleteAttributes($id)

Deletes all attributes of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $api_instance->awEventbookingEventPrototypeDeleteAttributes($id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeDeleteAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeDeletePurchasedTickets**
> awEventbookingEventPrototypeDeletePurchasedTickets($id)

Deletes all purchasedTickets of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $api_instance->awEventbookingEventPrototypeDeletePurchasedTickets($id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeDeletePurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeDeleteTickets**
> awEventbookingEventPrototypeDeleteTickets($id)

Deletes all tickets of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $api_instance->awEventbookingEventPrototypeDeleteTickets($id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeDeleteTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeDestroyByIdAttributes**
> awEventbookingEventPrototypeDestroyByIdAttributes($fk, $id)

Delete a related item by id for attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for attributes
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $api_instance->awEventbookingEventPrototypeDestroyByIdAttributes($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeDestroyByIdAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for attributes |
 **id** | **string**| AwEventbookingEvent id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeDestroyByIdPurchasedTickets**
> awEventbookingEventPrototypeDestroyByIdPurchasedTickets($fk, $id)

Delete a related item by id for purchasedTickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for purchasedTickets
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $api_instance->awEventbookingEventPrototypeDestroyByIdPurchasedTickets($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeDestroyByIdPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for purchasedTickets |
 **id** | **string**| AwEventbookingEvent id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeDestroyByIdTickets**
> awEventbookingEventPrototypeDestroyByIdTickets($fk, $id)

Delete a related item by id for tickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for tickets
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $api_instance->awEventbookingEventPrototypeDestroyByIdTickets($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeDestroyByIdTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for tickets |
 **id** | **string**| AwEventbookingEvent id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeExistsPurchasedTickets**
> bool awEventbookingEventPrototypeExistsPurchasedTickets($fk, $id)

Check the existence of purchasedTickets relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for purchasedTickets
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $result = $api_instance->awEventbookingEventPrototypeExistsPurchasedTickets($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeExistsPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for purchasedTickets |
 **id** | **string**| AwEventbookingEvent id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeFindByIdAttributes**
> \Swagger\Client\Model\AwEventbookingEventAttribute awEventbookingEventPrototypeFindByIdAttributes($fk, $id)

Find a related item by id for attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for attributes
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $result = $api_instance->awEventbookingEventPrototypeFindByIdAttributes($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeFindByIdAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for attributes |
 **id** | **string**| AwEventbookingEvent id |

### Return type

[**\Swagger\Client\Model\AwEventbookingEventAttribute**](../Model/AwEventbookingEventAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeFindByIdPurchasedTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventPrototypeFindByIdPurchasedTickets($fk, $id)

Find a related item by id for purchasedTickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for purchasedTickets
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $result = $api_instance->awEventbookingEventPrototypeFindByIdPurchasedTickets($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeFindByIdPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for purchasedTickets |
 **id** | **string**| AwEventbookingEvent id |

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeFindByIdTickets**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventPrototypeFindByIdTickets($fk, $id)

Find a related item by id for tickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for tickets
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $result = $api_instance->awEventbookingEventPrototypeFindByIdTickets($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeFindByIdTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for tickets |
 **id** | **string**| AwEventbookingEvent id |

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeGetAttributes**
> \Swagger\Client\Model\AwEventbookingEventAttribute[] awEventbookingEventPrototypeGetAttributes($id, $filter)

Queries attributes of AwEventbookingEvent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventPrototypeGetAttributes($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeGetAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventAttribute[]**](../Model/AwEventbookingEventAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeGetPurchasedTickets**
> \Swagger\Client\Model\AwEventbookingTicket[] awEventbookingEventPrototypeGetPurchasedTickets($id, $filter)

Queries purchasedTickets of AwEventbookingEvent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventPrototypeGetPurchasedTickets($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeGetPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket[]**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeGetTickets**
> \Swagger\Client\Model\AwEventbookingEventTicket[] awEventbookingEventPrototypeGetTickets($id, $filter)

Queries tickets of AwEventbookingEvent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventPrototypeGetTickets($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeGetTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket[]**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeLinkPurchasedTickets**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventPrototypeLinkPurchasedTickets($fk, $id, $data)

Add a related item by id for purchasedTickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for purchasedTickets
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | 

try {
    $result = $api_instance->awEventbookingEventPrototypeLinkPurchasedTickets($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeLinkPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for purchasedTickets |
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeUnlinkPurchasedTickets**
> awEventbookingEventPrototypeUnlinkPurchasedTickets($fk, $id)

Remove the purchasedTickets relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for purchasedTickets
$id = "id_example"; // string | AwEventbookingEvent id

try {
    $api_instance->awEventbookingEventPrototypeUnlinkPurchasedTickets($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeUnlinkPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for purchasedTickets |
 **id** | **string**| AwEventbookingEvent id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeUpdateByIdAttributes**
> \Swagger\Client\Model\AwEventbookingEventAttribute awEventbookingEventPrototypeUpdateByIdAttributes($fk, $id, $data)

Update a related item by id for attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for attributes
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingEventAttribute(); // \Swagger\Client\Model\AwEventbookingEventAttribute | 

try {
    $result = $api_instance->awEventbookingEventPrototypeUpdateByIdAttributes($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeUpdateByIdAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for attributes |
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventAttribute**](../Model/\Swagger\Client\Model\AwEventbookingEventAttribute.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventAttribute**](../Model/AwEventbookingEventAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeUpdateByIdPurchasedTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventPrototypeUpdateByIdPurchasedTickets($fk, $id, $data)

Update a related item by id for purchasedTickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for purchasedTickets
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | 

try {
    $result = $api_instance->awEventbookingEventPrototypeUpdateByIdPurchasedTickets($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeUpdateByIdPurchasedTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for purchasedTickets |
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventPrototypeUpdateByIdTickets**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventPrototypeUpdateByIdTickets($fk, $id, $data)

Update a related item by id for tickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$fk = "fk_example"; // string | Foreign key for tickets
$id = "id_example"; // string | AwEventbookingEvent id
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | 

try {
    $result = $api_instance->awEventbookingEventPrototypeUpdateByIdTickets($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventPrototypeUpdateByIdTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for tickets |
 **id** | **string**| AwEventbookingEvent id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventReplaceById**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | Model instance data

try {
    $result = $api_instance->awEventbookingEventReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventReplaceOrCreate**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | Model instance data

try {
    $result = $api_instance->awEventbookingEventReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 awEventbookingEventUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventUpsertPatchAwEventbookingEvents**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventUpsertPatchAwEventbookingEvents($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | Model instance data

try {
    $result = $api_instance->awEventbookingEventUpsertPatchAwEventbookingEvents($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventUpsertPatchAwEventbookingEvents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventUpsertPutAwEventbookingEvents**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventUpsertPutAwEventbookingEvents($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | Model instance data

try {
    $result = $api_instance->awEventbookingEventUpsertPutAwEventbookingEvents($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventUpsertPutAwEventbookingEvents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventUpsertWithWhere**
> \Swagger\Client\Model\AwEventbookingEvent awEventbookingEventUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventApi->awEventbookingEventUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

