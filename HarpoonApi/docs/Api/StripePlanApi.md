# Harpoon\Api\StripePlanApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripePlanCount**](StripePlanApi.md#stripePlanCount) | **GET** /StripePlans/count | Count instances of the model matched by where from the data source.
[**stripePlanCreate**](StripePlanApi.md#stripePlanCreate) | **POST** /StripePlans | Create a new instance of the model and persist it into the data source.
[**stripePlanCreateChangeStreamGetStripePlansChangeStream**](StripePlanApi.md#stripePlanCreateChangeStreamGetStripePlansChangeStream) | **GET** /StripePlans/change-stream | Create a change stream.
[**stripePlanCreateChangeStreamPostStripePlansChangeStream**](StripePlanApi.md#stripePlanCreateChangeStreamPostStripePlansChangeStream) | **POST** /StripePlans/change-stream | Create a change stream.
[**stripePlanDeleteById**](StripePlanApi.md#stripePlanDeleteById) | **DELETE** /StripePlans/{id} | Delete a model instance by {{id}} from the data source.
[**stripePlanExistsGetStripePlansidExists**](StripePlanApi.md#stripePlanExistsGetStripePlansidExists) | **GET** /StripePlans/{id}/exists | Check whether a model instance exists in the data source.
[**stripePlanExistsHeadStripePlansid**](StripePlanApi.md#stripePlanExistsHeadStripePlansid) | **HEAD** /StripePlans/{id} | Check whether a model instance exists in the data source.
[**stripePlanFind**](StripePlanApi.md#stripePlanFind) | **GET** /StripePlans | Find all instances of the model matched by filter from the data source.
[**stripePlanFindById**](StripePlanApi.md#stripePlanFindById) | **GET** /StripePlans/{id} | Find a model instance by {{id}} from the data source.
[**stripePlanFindOne**](StripePlanApi.md#stripePlanFindOne) | **GET** /StripePlans/findOne | Find first instance of the model matched by filter from the data source.
[**stripePlanPrototypeUpdateAttributesPatchStripePlansid**](StripePlanApi.md#stripePlanPrototypeUpdateAttributesPatchStripePlansid) | **PATCH** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanPrototypeUpdateAttributesPutStripePlansid**](StripePlanApi.md#stripePlanPrototypeUpdateAttributesPutStripePlansid) | **PUT** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanReplaceById**](StripePlanApi.md#stripePlanReplaceById) | **POST** /StripePlans/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripePlanReplaceOrCreate**](StripePlanApi.md#stripePlanReplaceOrCreate) | **POST** /StripePlans/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripePlanUpdateAll**](StripePlanApi.md#stripePlanUpdateAll) | **POST** /StripePlans/update | Update instances of the model matched by {{where}} from the data source.
[**stripePlanUpsertPatchStripePlans**](StripePlanApi.md#stripePlanUpsertPatchStripePlans) | **PATCH** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertPutStripePlans**](StripePlanApi.md#stripePlanUpsertPutStripePlans) | **PUT** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertWithWhere**](StripePlanApi.md#stripePlanUpsertWithWhere) | **POST** /StripePlans/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripePlanCount**
> \Swagger\Client\Model\InlineResponse2001 stripePlanCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->stripePlanCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanCreate**
> \Swagger\Client\Model\StripePlan stripePlanCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | Model instance data

try {
    $result = $api_instance->stripePlanCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanCreateChangeStreamGetStripePlansChangeStream**
> \SplFileObject stripePlanCreateChangeStreamGetStripePlansChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripePlanCreateChangeStreamGetStripePlansChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanCreateChangeStreamGetStripePlansChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanCreateChangeStreamPostStripePlansChangeStream**
> \SplFileObject stripePlanCreateChangeStreamPostStripePlansChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripePlanCreateChangeStreamPostStripePlansChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanCreateChangeStreamPostStripePlansChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanDeleteById**
> object stripePlanDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripePlanDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanExistsGetStripePlansidExists**
> \Swagger\Client\Model\InlineResponse2003 stripePlanExistsGetStripePlansidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripePlanExistsGetStripePlansidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanExistsGetStripePlansidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanExistsHeadStripePlansid**
> \Swagger\Client\Model\InlineResponse2003 stripePlanExistsHeadStripePlansid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripePlanExistsHeadStripePlansid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanExistsHeadStripePlansid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanFind**
> \Swagger\Client\Model\StripePlan[] stripePlanFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripePlanFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan[]**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanFindById**
> \Swagger\Client\Model\StripePlan stripePlanFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->stripePlanFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanFindOne**
> \Swagger\Client\Model\StripePlan stripePlanFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripePlanFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanPrototypeUpdateAttributesPatchStripePlansid**
> \Swagger\Client\Model\StripePlan stripePlanPrototypeUpdateAttributesPatchStripePlansid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$id = "id_example"; // string | StripePlan id
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | An object of model property name/value pairs

try {
    $result = $api_instance->stripePlanPrototypeUpdateAttributesPatchStripePlansid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanPrototypeUpdateAttributesPatchStripePlansid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripePlan id |
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanPrototypeUpdateAttributesPutStripePlansid**
> \Swagger\Client\Model\StripePlan stripePlanPrototypeUpdateAttributesPutStripePlansid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$id = "id_example"; // string | StripePlan id
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | An object of model property name/value pairs

try {
    $result = $api_instance->stripePlanPrototypeUpdateAttributesPutStripePlansid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanPrototypeUpdateAttributesPutStripePlansid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripePlan id |
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanReplaceById**
> \Swagger\Client\Model\StripePlan stripePlanReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | Model instance data

try {
    $result = $api_instance->stripePlanReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanReplaceOrCreate**
> \Swagger\Client\Model\StripePlan stripePlanReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | Model instance data

try {
    $result = $api_instance->stripePlanReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 stripePlanUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | An object of model property name/value pairs

try {
    $result = $api_instance->stripePlanUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanUpsertPatchStripePlans**
> \Swagger\Client\Model\StripePlan stripePlanUpsertPatchStripePlans($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | Model instance data

try {
    $result = $api_instance->stripePlanUpsertPatchStripePlans($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanUpsertPatchStripePlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanUpsertPutStripePlans**
> \Swagger\Client\Model\StripePlan stripePlanUpsertPutStripePlans($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | Model instance data

try {
    $result = $api_instance->stripePlanUpsertPutStripePlans($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanUpsertPutStripePlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripePlanUpsertWithWhere**
> \Swagger\Client\Model\StripePlan stripePlanUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripePlanApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripePlan(); // \Swagger\Client\Model\StripePlan | An object of model property name/value pairs

try {
    $result = $api_instance->stripePlanUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripePlanApi->stripePlanUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripePlan**](../Model/\Swagger\Client\Model\StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripePlan**](../Model/StripePlan.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

