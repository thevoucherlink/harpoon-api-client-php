# Harpoon\Api\CatalogProductApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogProductCount**](CatalogProductApi.md#catalogProductCount) | **GET** /CatalogProducts/count | Count instances of the model matched by where from the data source.
[**catalogProductCreate**](CatalogProductApi.md#catalogProductCreate) | **POST** /CatalogProducts | Create a new instance of the model and persist it into the data source.
[**catalogProductCreateChangeStreamGetCatalogProductsChangeStream**](CatalogProductApi.md#catalogProductCreateChangeStreamGetCatalogProductsChangeStream) | **GET** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductCreateChangeStreamPostCatalogProductsChangeStream**](CatalogProductApi.md#catalogProductCreateChangeStreamPostCatalogProductsChangeStream) | **POST** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductDeleteById**](CatalogProductApi.md#catalogProductDeleteById) | **DELETE** /CatalogProducts/{id} | Delete a model instance by {{id}} from the data source.
[**catalogProductExistsGetCatalogProductsidExists**](CatalogProductApi.md#catalogProductExistsGetCatalogProductsidExists) | **GET** /CatalogProducts/{id}/exists | Check whether a model instance exists in the data source.
[**catalogProductExistsHeadCatalogProductsid**](CatalogProductApi.md#catalogProductExistsHeadCatalogProductsid) | **HEAD** /CatalogProducts/{id} | Check whether a model instance exists in the data source.
[**catalogProductFind**](CatalogProductApi.md#catalogProductFind) | **GET** /CatalogProducts | Find all instances of the model matched by filter from the data source.
[**catalogProductFindById**](CatalogProductApi.md#catalogProductFindById) | **GET** /CatalogProducts/{id} | Find a model instance by {{id}} from the data source.
[**catalogProductFindOne**](CatalogProductApi.md#catalogProductFindOne) | **GET** /CatalogProducts/findOne | Find first instance of the model matched by filter from the data source.
[**catalogProductPrototypeCountCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeCountCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/count | Counts catalogCategories of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/count | Counts udropshipVendors of CatalogProduct.
[**catalogProductPrototypeCreateAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeCreateAwCollpurDeal) | **POST** /CatalogProducts/{id}/awCollpurDeal | Creates a new instance in awCollpurDeal of this model.
[**catalogProductPrototypeCreateCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeCreateCatalogCategories) | **POST** /CatalogProducts/{id}/catalogCategories | Creates a new instance in catalogCategories of this model.
[**catalogProductPrototypeCreateInventory**](CatalogProductApi.md#catalogProductPrototypeCreateInventory) | **POST** /CatalogProducts/{id}/inventory | Creates a new instance in inventory of this model.
[**catalogProductPrototypeCreateProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeCreateProductEventCompetition) | **POST** /CatalogProducts/{id}/productEventCompetition | Creates a new instance in productEventCompetition of this model.
[**catalogProductPrototypeCreateUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendorProducts) | **POST** /CatalogProducts/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**catalogProductPrototypeCreateUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendors) | **POST** /CatalogProducts/{id}/udropshipVendors | Creates a new instance in udropshipVendors of this model.
[**catalogProductPrototypeDeleteCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeDeleteCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories | Deletes all catalogCategories of this model.
[**catalogProductPrototypeDeleteUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**catalogProductPrototypeDeleteUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors | Deletes all udropshipVendors of this model.
[**catalogProductPrototypeDestroyAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeDestroyAwCollpurDeal) | **DELETE** /CatalogProducts/{id}/awCollpurDeal | Deletes awCollpurDeal of this model.
[**catalogProductPrototypeDestroyByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/{fk} | Delete a related item by id for catalogCategories.
[**catalogProductPrototypeDestroyByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeDestroyByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/{fk} | Delete a related item by id for udropshipVendors.
[**catalogProductPrototypeDestroyInventory**](CatalogProductApi.md#catalogProductPrototypeDestroyInventory) | **DELETE** /CatalogProducts/{id}/inventory | Deletes inventory of this model.
[**catalogProductPrototypeDestroyProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeDestroyProductEventCompetition) | **DELETE** /CatalogProducts/{id}/productEventCompetition | Deletes productEventCompetition of this model.
[**catalogProductPrototypeExistsCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeExistsCatalogCategories) | **HEAD** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Check the existence of catalogCategories relation to an item by id.
[**catalogProductPrototypeExistsUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeExistsUdropshipVendors) | **HEAD** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Check the existence of udropshipVendors relation to an item by id.
[**catalogProductPrototypeFindByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeFindByIdCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/{fk} | Find a related item by id for catalogCategories.
[**catalogProductPrototypeFindByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeFindByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/{fk} | Find a related item by id for udropshipVendors.
[**catalogProductPrototypeGetAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeGetAwCollpurDeal) | **GET** /CatalogProducts/{id}/awCollpurDeal | Fetches hasOne relation awCollpurDeal.
[**catalogProductPrototypeGetCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeGetCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories | Queries catalogCategories of CatalogProduct.
[**catalogProductPrototypeGetCheckoutAgreement**](CatalogProductApi.md#catalogProductPrototypeGetCheckoutAgreement) | **GET** /CatalogProducts/{id}/checkoutAgreement | Fetches belongsTo relation checkoutAgreement.
[**catalogProductPrototypeGetCreator**](CatalogProductApi.md#catalogProductPrototypeGetCreator) | **GET** /CatalogProducts/{id}/creator | Fetches belongsTo relation creator.
[**catalogProductPrototypeGetInventory**](CatalogProductApi.md#catalogProductPrototypeGetInventory) | **GET** /CatalogProducts/{id}/inventory | Fetches hasOne relation inventory.
[**catalogProductPrototypeGetProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeGetProductEventCompetition) | **GET** /CatalogProducts/{id}/productEventCompetition | Fetches hasOne relation productEventCompetition.
[**catalogProductPrototypeGetUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeGetUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors | Queries udropshipVendors of CatalogProduct.
[**catalogProductPrototypeLinkCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeLinkCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Add a related item by id for catalogCategories.
[**catalogProductPrototypeLinkUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeLinkUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Add a related item by id for udropshipVendors.
[**catalogProductPrototypeUnlinkCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeUnlinkCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Remove the catalogCategories relation to an item by id.
[**catalogProductPrototypeUnlinkUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeUnlinkUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Remove the udropshipVendors relation to an item by id.
[**catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**](CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPatchCatalogProductsid) | **PATCH** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAttributesPutCatalogProductsid**](CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPutCatalogProductsid) | **PUT** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeUpdateAwCollpurDeal) | **PUT** /CatalogProducts/{id}/awCollpurDeal | Update awCollpurDeal of this model.
[**catalogProductPrototypeUpdateByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/{fk} | Update a related item by id for catalogCategories.
[**catalogProductPrototypeUpdateByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeUpdateByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/{fk} | Update a related item by id for udropshipVendors.
[**catalogProductPrototypeUpdateInventory**](CatalogProductApi.md#catalogProductPrototypeUpdateInventory) | **PUT** /CatalogProducts/{id}/inventory | Update inventory of this model.
[**catalogProductPrototypeUpdateProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeUpdateProductEventCompetition) | **PUT** /CatalogProducts/{id}/productEventCompetition | Update productEventCompetition of this model.
[**catalogProductReplaceById**](CatalogProductApi.md#catalogProductReplaceById) | **POST** /CatalogProducts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogProductReplaceOrCreate**](CatalogProductApi.md#catalogProductReplaceOrCreate) | **POST** /CatalogProducts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogProductUpdateAll**](CatalogProductApi.md#catalogProductUpdateAll) | **POST** /CatalogProducts/update | Update instances of the model matched by {{where}} from the data source.
[**catalogProductUpsertPatchCatalogProducts**](CatalogProductApi.md#catalogProductUpsertPatchCatalogProducts) | **PATCH** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertPutCatalogProducts**](CatalogProductApi.md#catalogProductUpsertPutCatalogProducts) | **PUT** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertWithWhere**](CatalogProductApi.md#catalogProductUpsertWithWhere) | **POST** /CatalogProducts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **catalogProductCount**
> \Swagger\Client\Model\InlineResponse2001 catalogProductCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->catalogProductCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductCreate**
> \Swagger\Client\Model\CatalogProduct catalogProductCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | Model instance data

try {
    $result = $api_instance->catalogProductCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductCreateChangeStreamGetCatalogProductsChangeStream**
> \SplFileObject catalogProductCreateChangeStreamGetCatalogProductsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->catalogProductCreateChangeStreamGetCatalogProductsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductCreateChangeStreamGetCatalogProductsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductCreateChangeStreamPostCatalogProductsChangeStream**
> \SplFileObject catalogProductCreateChangeStreamPostCatalogProductsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->catalogProductCreateChangeStreamPostCatalogProductsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductCreateChangeStreamPostCatalogProductsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductDeleteById**
> object catalogProductDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->catalogProductDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductExistsGetCatalogProductsidExists**
> \Swagger\Client\Model\InlineResponse2003 catalogProductExistsGetCatalogProductsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->catalogProductExistsGetCatalogProductsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductExistsGetCatalogProductsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductExistsHeadCatalogProductsid**
> \Swagger\Client\Model\InlineResponse2003 catalogProductExistsHeadCatalogProductsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->catalogProductExistsHeadCatalogProductsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductExistsHeadCatalogProductsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductFind**
> \Swagger\Client\Model\CatalogProduct[] catalogProductFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->catalogProductFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct[]**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductFindById**
> \Swagger\Client\Model\CatalogProduct catalogProductFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->catalogProductFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductFindOne**
> \Swagger\Client\Model\CatalogProduct catalogProductFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->catalogProductFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCountCatalogCategories**
> \Swagger\Client\Model\InlineResponse2001 catalogProductPrototypeCountCatalogCategories($id, $where)

Counts catalogCategories of CatalogProduct.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->catalogProductPrototypeCountCatalogCategories($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCountCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCountUdropshipVendorProducts**
> \Swagger\Client\Model\InlineResponse2001 catalogProductPrototypeCountUdropshipVendorProducts($id, $where)

Counts udropshipVendorProducts of CatalogProduct.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->catalogProductPrototypeCountUdropshipVendorProducts($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCountUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCountUdropshipVendors**
> \Swagger\Client\Model\InlineResponse2001 catalogProductPrototypeCountUdropshipVendors($id, $where)

Counts udropshipVendors of CatalogProduct.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->catalogProductPrototypeCountUdropshipVendors($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCountUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCreateAwCollpurDeal**
> \Swagger\Client\Model\AwCollpurDeal catalogProductPrototypeCreateAwCollpurDeal($id, $data)

Creates a new instance in awCollpurDeal of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | 

try {
    $result = $api_instance->catalogProductPrototypeCreateAwCollpurDeal($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCreateAwCollpurDeal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCreateCatalogCategories**
> \Swagger\Client\Model\CatalogCategory catalogProductPrototypeCreateCatalogCategories($id, $data)

Creates a new instance in catalogCategories of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | 

try {
    $result = $api_instance->catalogProductPrototypeCreateCatalogCategories($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCreateCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCreateInventory**
> \Swagger\Client\Model\CatalogInventoryStockItem catalogProductPrototypeCreateInventory($id, $data)

Creates a new instance in inventory of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\CatalogInventoryStockItem(); // \Swagger\Client\Model\CatalogInventoryStockItem | 

try {
    $result = $api_instance->catalogProductPrototypeCreateInventory($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCreateInventory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\CatalogInventoryStockItem**](../Model/\Swagger\Client\Model\CatalogInventoryStockItem.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogInventoryStockItem**](../Model/CatalogInventoryStockItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCreateProductEventCompetition**
> \Swagger\Client\Model\AwEventbookingEvent catalogProductPrototypeCreateProductEventCompetition($id, $data)

Creates a new instance in productEventCompetition of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | 

try {
    $result = $api_instance->catalogProductPrototypeCreateProductEventCompetition($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCreateProductEventCompetition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCreateUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct catalogProductPrototypeCreateUdropshipVendorProducts($id, $data)

Creates a new instance in udropshipVendorProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\UdropshipVendorProduct(); // \Swagger\Client\Model\UdropshipVendorProduct | 

try {
    $result = $api_instance->catalogProductPrototypeCreateUdropshipVendorProducts($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCreateUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/\Swagger\Client\Model\UdropshipVendorProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeCreateUdropshipVendors**
> \Swagger\Client\Model\UdropshipVendor catalogProductPrototypeCreateUdropshipVendors($id, $data)

Creates a new instance in udropshipVendors of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | 

try {
    $result = $api_instance->catalogProductPrototypeCreateUdropshipVendors($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeCreateUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDeleteCatalogCategories**
> catalogProductPrototypeDeleteCatalogCategories($id)

Deletes all catalogCategories of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDeleteCatalogCategories($id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDeleteCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDeleteUdropshipVendorProducts**
> catalogProductPrototypeDeleteUdropshipVendorProducts($id)

Deletes all udropshipVendorProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDeleteUdropshipVendorProducts($id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDeleteUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDeleteUdropshipVendors**
> catalogProductPrototypeDeleteUdropshipVendors($id)

Deletes all udropshipVendors of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDeleteUdropshipVendors($id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDeleteUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDestroyAwCollpurDeal**
> catalogProductPrototypeDestroyAwCollpurDeal($id)

Deletes awCollpurDeal of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDestroyAwCollpurDeal($id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDestroyAwCollpurDeal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDestroyByIdCatalogCategories**
> catalogProductPrototypeDestroyByIdCatalogCategories($fk, $id)

Delete a related item by id for catalogCategories.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for catalogCategories
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDestroyByIdCatalogCategories($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDestroyByIdCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogCategories |
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDestroyByIdUdropshipVendorProducts**
> catalogProductPrototypeDestroyByIdUdropshipVendorProducts($fk, $id)

Delete a related item by id for udropshipVendorProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendorProducts
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDestroyByIdUdropshipVendorProducts($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDestroyByIdUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendorProducts |
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDestroyByIdUdropshipVendors**
> catalogProductPrototypeDestroyByIdUdropshipVendors($fk, $id)

Delete a related item by id for udropshipVendors.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendors
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDestroyByIdUdropshipVendors($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDestroyByIdUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendors |
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDestroyInventory**
> catalogProductPrototypeDestroyInventory($id)

Deletes inventory of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDestroyInventory($id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDestroyInventory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeDestroyProductEventCompetition**
> catalogProductPrototypeDestroyProductEventCompetition($id)

Deletes productEventCompetition of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeDestroyProductEventCompetition($id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeDestroyProductEventCompetition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeExistsCatalogCategories**
> bool catalogProductPrototypeExistsCatalogCategories($fk, $id)

Check the existence of catalogCategories relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for catalogCategories
$id = "id_example"; // string | CatalogProduct id

try {
    $result = $api_instance->catalogProductPrototypeExistsCatalogCategories($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeExistsCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogCategories |
 **id** | **string**| CatalogProduct id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeExistsUdropshipVendors**
> bool catalogProductPrototypeExistsUdropshipVendors($fk, $id)

Check the existence of udropshipVendors relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendors
$id = "id_example"; // string | CatalogProduct id

try {
    $result = $api_instance->catalogProductPrototypeExistsUdropshipVendors($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeExistsUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendors |
 **id** | **string**| CatalogProduct id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeFindByIdCatalogCategories**
> \Swagger\Client\Model\CatalogCategory catalogProductPrototypeFindByIdCatalogCategories($fk, $id)

Find a related item by id for catalogCategories.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for catalogCategories
$id = "id_example"; // string | CatalogProduct id

try {
    $result = $api_instance->catalogProductPrototypeFindByIdCatalogCategories($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeFindByIdCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogCategories |
 **id** | **string**| CatalogProduct id |

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeFindByIdUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct catalogProductPrototypeFindByIdUdropshipVendorProducts($fk, $id)

Find a related item by id for udropshipVendorProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendorProducts
$id = "id_example"; // string | CatalogProduct id

try {
    $result = $api_instance->catalogProductPrototypeFindByIdUdropshipVendorProducts($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeFindByIdUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendorProducts |
 **id** | **string**| CatalogProduct id |

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeFindByIdUdropshipVendors**
> \Swagger\Client\Model\UdropshipVendor catalogProductPrototypeFindByIdUdropshipVendors($fk, $id)

Find a related item by id for udropshipVendors.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendors
$id = "id_example"; // string | CatalogProduct id

try {
    $result = $api_instance->catalogProductPrototypeFindByIdUdropshipVendors($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeFindByIdUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendors |
 **id** | **string**| CatalogProduct id |

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetAwCollpurDeal**
> \Swagger\Client\Model\AwCollpurDeal catalogProductPrototypeGetAwCollpurDeal($id, $refresh)

Fetches hasOne relation awCollpurDeal.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$refresh = true; // bool | 

try {
    $result = $api_instance->catalogProductPrototypeGetAwCollpurDeal($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetAwCollpurDeal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetCatalogCategories**
> \Swagger\Client\Model\CatalogCategory[] catalogProductPrototypeGetCatalogCategories($id, $filter)

Queries catalogCategories of CatalogProduct.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->catalogProductPrototypeGetCatalogCategories($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory[]**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetCheckoutAgreement**
> \Swagger\Client\Model\CheckoutAgreement catalogProductPrototypeGetCheckoutAgreement($id, $refresh)

Fetches belongsTo relation checkoutAgreement.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$refresh = true; // bool | 

try {
    $result = $api_instance->catalogProductPrototypeGetCheckoutAgreement($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetCheckoutAgreement: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\CheckoutAgreement**](../Model/CheckoutAgreement.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetCreator**
> \Swagger\Client\Model\UdropshipVendor catalogProductPrototypeGetCreator($id, $refresh)

Fetches belongsTo relation creator.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$refresh = true; // bool | 

try {
    $result = $api_instance->catalogProductPrototypeGetCreator($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetCreator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetInventory**
> \Swagger\Client\Model\CatalogInventoryStockItem catalogProductPrototypeGetInventory($id, $refresh)

Fetches hasOne relation inventory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$refresh = true; // bool | 

try {
    $result = $api_instance->catalogProductPrototypeGetInventory($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetInventory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogInventoryStockItem**](../Model/CatalogInventoryStockItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetProductEventCompetition**
> \Swagger\Client\Model\AwEventbookingEvent catalogProductPrototypeGetProductEventCompetition($id, $refresh)

Fetches hasOne relation productEventCompetition.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$refresh = true; // bool | 

try {
    $result = $api_instance->catalogProductPrototypeGetProductEventCompetition($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetProductEventCompetition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct[] catalogProductPrototypeGetUdropshipVendorProducts($id, $filter)

Queries udropshipVendorProducts of CatalogProduct.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->catalogProductPrototypeGetUdropshipVendorProducts($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct[]**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeGetUdropshipVendors**
> \Swagger\Client\Model\UdropshipVendor[] catalogProductPrototypeGetUdropshipVendors($id, $filter)

Queries udropshipVendors of CatalogProduct.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->catalogProductPrototypeGetUdropshipVendors($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeGetUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor[]**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeLinkCatalogCategories**
> \Swagger\Client\Model\CatalogCategoryProduct catalogProductPrototypeLinkCatalogCategories($fk, $id, $data)

Add a related item by id for catalogCategories.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for catalogCategories
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\CatalogCategoryProduct(); // \Swagger\Client\Model\CatalogCategoryProduct | 

try {
    $result = $api_instance->catalogProductPrototypeLinkCatalogCategories($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeLinkCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogCategories |
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\CatalogCategoryProduct**](../Model/\Swagger\Client\Model\CatalogCategoryProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategoryProduct**](../Model/CatalogCategoryProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeLinkUdropshipVendors**
> \Swagger\Client\Model\UdropshipVendorProductAssoc catalogProductPrototypeLinkUdropshipVendors($fk, $id, $data)

Add a related item by id for udropshipVendors.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendors
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\UdropshipVendorProductAssoc(); // \Swagger\Client\Model\UdropshipVendorProductAssoc | 

try {
    $result = $api_instance->catalogProductPrototypeLinkUdropshipVendors($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeLinkUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendors |
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\UdropshipVendorProductAssoc**](../Model/\Swagger\Client\Model\UdropshipVendorProductAssoc.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProductAssoc**](../Model/UdropshipVendorProductAssoc.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUnlinkCatalogCategories**
> catalogProductPrototypeUnlinkCatalogCategories($fk, $id)

Remove the catalogCategories relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for catalogCategories
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeUnlinkCatalogCategories($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUnlinkCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogCategories |
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUnlinkUdropshipVendors**
> catalogProductPrototypeUnlinkUdropshipVendors($fk, $id)

Remove the udropshipVendors relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendors
$id = "id_example"; // string | CatalogProduct id

try {
    $api_instance->catalogProductPrototypeUnlinkUdropshipVendors($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUnlinkUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendors |
 **id** | **string**| CatalogProduct id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**
> \Swagger\Client\Model\CatalogProduct catalogProductPrototypeUpdateAttributesPatchCatalogProductsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | An object of model property name/value pairs

try {
    $result = $api_instance->catalogProductPrototypeUpdateAttributesPatchCatalogProductsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateAttributesPatchCatalogProductsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateAttributesPutCatalogProductsid**
> \Swagger\Client\Model\CatalogProduct catalogProductPrototypeUpdateAttributesPutCatalogProductsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | An object of model property name/value pairs

try {
    $result = $api_instance->catalogProductPrototypeUpdateAttributesPutCatalogProductsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateAttributesPutCatalogProductsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateAwCollpurDeal**
> \Swagger\Client\Model\AwCollpurDeal catalogProductPrototypeUpdateAwCollpurDeal($id, $data)

Update awCollpurDeal of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\AwCollpurDeal(); // \Swagger\Client\Model\AwCollpurDeal | 

try {
    $result = $api_instance->catalogProductPrototypeUpdateAwCollpurDeal($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateAwCollpurDeal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\AwCollpurDeal**](../Model/\Swagger\Client\Model\AwCollpurDeal.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDeal**](../Model/AwCollpurDeal.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateByIdCatalogCategories**
> \Swagger\Client\Model\CatalogCategory catalogProductPrototypeUpdateByIdCatalogCategories($fk, $id, $data)

Update a related item by id for catalogCategories.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for catalogCategories
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | 

try {
    $result = $api_instance->catalogProductPrototypeUpdateByIdCatalogCategories($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateByIdCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogCategories |
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateByIdUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct catalogProductPrototypeUpdateByIdUdropshipVendorProducts($fk, $id, $data)

Update a related item by id for udropshipVendorProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendorProducts
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\UdropshipVendorProduct(); // \Swagger\Client\Model\UdropshipVendorProduct | 

try {
    $result = $api_instance->catalogProductPrototypeUpdateByIdUdropshipVendorProducts($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateByIdUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendorProducts |
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/\Swagger\Client\Model\UdropshipVendorProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateByIdUdropshipVendors**
> \Swagger\Client\Model\UdropshipVendor catalogProductPrototypeUpdateByIdUdropshipVendors($fk, $id, $data)

Update a related item by id for udropshipVendors.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendors
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | 

try {
    $result = $api_instance->catalogProductPrototypeUpdateByIdUdropshipVendors($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateByIdUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendors |
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateInventory**
> \Swagger\Client\Model\CatalogInventoryStockItem catalogProductPrototypeUpdateInventory($id, $data)

Update inventory of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\CatalogInventoryStockItem(); // \Swagger\Client\Model\CatalogInventoryStockItem | 

try {
    $result = $api_instance->catalogProductPrototypeUpdateInventory($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateInventory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\CatalogInventoryStockItem**](../Model/\Swagger\Client\Model\CatalogInventoryStockItem.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogInventoryStockItem**](../Model/CatalogInventoryStockItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductPrototypeUpdateProductEventCompetition**
> \Swagger\Client\Model\AwEventbookingEvent catalogProductPrototypeUpdateProductEventCompetition($id, $data)

Update productEventCompetition of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | CatalogProduct id
$data = new \Swagger\Client\Model\AwEventbookingEvent(); // \Swagger\Client\Model\AwEventbookingEvent | 

try {
    $result = $api_instance->catalogProductPrototypeUpdateProductEventCompetition($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductPrototypeUpdateProductEventCompetition: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogProduct id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEvent**](../Model/\Swagger\Client\Model\AwEventbookingEvent.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEvent**](../Model/AwEventbookingEvent.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductReplaceById**
> \Swagger\Client\Model\CatalogProduct catalogProductReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | Model instance data

try {
    $result = $api_instance->catalogProductReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductReplaceOrCreate**
> \Swagger\Client\Model\CatalogProduct catalogProductReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | Model instance data

try {
    $result = $api_instance->catalogProductReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 catalogProductUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | An object of model property name/value pairs

try {
    $result = $api_instance->catalogProductUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductUpsertPatchCatalogProducts**
> \Swagger\Client\Model\CatalogProduct catalogProductUpsertPatchCatalogProducts($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | Model instance data

try {
    $result = $api_instance->catalogProductUpsertPatchCatalogProducts($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductUpsertPatchCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductUpsertPutCatalogProducts**
> \Swagger\Client\Model\CatalogProduct catalogProductUpsertPutCatalogProducts($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | Model instance data

try {
    $result = $api_instance->catalogProductUpsertPutCatalogProducts($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductUpsertPutCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogProductUpsertWithWhere**
> \Swagger\Client\Model\CatalogProduct catalogProductUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogProductApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | An object of model property name/value pairs

try {
    $result = $api_instance->catalogProductUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogProductApi->catalogProductUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

