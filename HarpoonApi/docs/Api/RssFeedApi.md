# Harpoon\Api\RssFeedApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedCount**](RssFeedApi.md#rssFeedCount) | **GET** /RssFeeds/count | Count instances of the model matched by where from the data source.
[**rssFeedCreate**](RssFeedApi.md#rssFeedCreate) | **POST** /RssFeeds | Create a new instance of the model and persist it into the data source.
[**rssFeedCreateChangeStreamGetRssFeedsChangeStream**](RssFeedApi.md#rssFeedCreateChangeStreamGetRssFeedsChangeStream) | **GET** /RssFeeds/change-stream | Create a change stream.
[**rssFeedCreateChangeStreamPostRssFeedsChangeStream**](RssFeedApi.md#rssFeedCreateChangeStreamPostRssFeedsChangeStream) | **POST** /RssFeeds/change-stream | Create a change stream.
[**rssFeedDeleteById**](RssFeedApi.md#rssFeedDeleteById) | **DELETE** /RssFeeds/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedExistsGetRssFeedsidExists**](RssFeedApi.md#rssFeedExistsGetRssFeedsidExists) | **GET** /RssFeeds/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedExistsHeadRssFeedsid**](RssFeedApi.md#rssFeedExistsHeadRssFeedsid) | **HEAD** /RssFeeds/{id} | Check whether a model instance exists in the data source.
[**rssFeedFind**](RssFeedApi.md#rssFeedFind) | **GET** /RssFeeds | Find all instances of the model matched by filter from the data source.
[**rssFeedFindById**](RssFeedApi.md#rssFeedFindById) | **GET** /RssFeeds/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedFindOne**](RssFeedApi.md#rssFeedFindOne) | **GET** /RssFeeds/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedPrototypeGetRssFeedGroup**](RssFeedApi.md#rssFeedPrototypeGetRssFeedGroup) | **GET** /RssFeeds/{id}/rssFeedGroup | Fetches belongsTo relation rssFeedGroup.
[**rssFeedPrototypeUpdateAttributesPatchRssFeedsid**](RssFeedApi.md#rssFeedPrototypeUpdateAttributesPatchRssFeedsid) | **PATCH** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedPrototypeUpdateAttributesPutRssFeedsid**](RssFeedApi.md#rssFeedPrototypeUpdateAttributesPutRssFeedsid) | **PUT** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedReplaceById**](RssFeedApi.md#rssFeedReplaceById) | **POST** /RssFeeds/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedReplaceOrCreate**](RssFeedApi.md#rssFeedReplaceOrCreate) | **POST** /RssFeeds/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedUpdateAll**](RssFeedApi.md#rssFeedUpdateAll) | **POST** /RssFeeds/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedUpsertPatchRssFeeds**](RssFeedApi.md#rssFeedUpsertPatchRssFeeds) | **PATCH** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertPutRssFeeds**](RssFeedApi.md#rssFeedUpsertPutRssFeeds) | **PUT** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertWithWhere**](RssFeedApi.md#rssFeedUpsertWithWhere) | **POST** /RssFeeds/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **rssFeedCount**
> \Swagger\Client\Model\InlineResponse2001 rssFeedCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->rssFeedCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedCreate**
> \Swagger\Client\Model\RssFeed rssFeedCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | Model instance data

try {
    $result = $api_instance->rssFeedCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedCreateChangeStreamGetRssFeedsChangeStream**
> \SplFileObject rssFeedCreateChangeStreamGetRssFeedsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->rssFeedCreateChangeStreamGetRssFeedsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedCreateChangeStreamGetRssFeedsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedCreateChangeStreamPostRssFeedsChangeStream**
> \SplFileObject rssFeedCreateChangeStreamPostRssFeedsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->rssFeedCreateChangeStreamPostRssFeedsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedCreateChangeStreamPostRssFeedsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedDeleteById**
> object rssFeedDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->rssFeedDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedExistsGetRssFeedsidExists**
> \Swagger\Client\Model\InlineResponse2003 rssFeedExistsGetRssFeedsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->rssFeedExistsGetRssFeedsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedExistsGetRssFeedsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedExistsHeadRssFeedsid**
> \Swagger\Client\Model\InlineResponse2003 rssFeedExistsHeadRssFeedsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->rssFeedExistsHeadRssFeedsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedExistsHeadRssFeedsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedFind**
> \Swagger\Client\Model\RssFeed[] rssFeedFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->rssFeedFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed[]**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedFindById**
> \Swagger\Client\Model\RssFeed rssFeedFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->rssFeedFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedFindOne**
> \Swagger\Client\Model\RssFeed rssFeedFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->rssFeedFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedPrototypeGetRssFeedGroup**
> \Swagger\Client\Model\RssFeedGroup rssFeedPrototypeGetRssFeedGroup($id, $refresh)

Fetches belongsTo relation rssFeedGroup.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | RssFeed id
$refresh = true; // bool | 

try {
    $result = $api_instance->rssFeedPrototypeGetRssFeedGroup($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedPrototypeGetRssFeedGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeed id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedPrototypeUpdateAttributesPatchRssFeedsid**
> \Swagger\Client\Model\RssFeed rssFeedPrototypeUpdateAttributesPatchRssFeedsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | RssFeed id
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedPrototypeUpdateAttributesPatchRssFeedsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedPrototypeUpdateAttributesPatchRssFeedsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeed id |
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedPrototypeUpdateAttributesPutRssFeedsid**
> \Swagger\Client\Model\RssFeed rssFeedPrototypeUpdateAttributesPutRssFeedsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | RssFeed id
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedPrototypeUpdateAttributesPutRssFeedsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedPrototypeUpdateAttributesPutRssFeedsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeed id |
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedReplaceById**
> \Swagger\Client\Model\RssFeed rssFeedReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | Model instance data

try {
    $result = $api_instance->rssFeedReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedReplaceOrCreate**
> \Swagger\Client\Model\RssFeed rssFeedReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | Model instance data

try {
    $result = $api_instance->rssFeedReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 rssFeedUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedUpsertPatchRssFeeds**
> \Swagger\Client\Model\RssFeed rssFeedUpsertPatchRssFeeds($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | Model instance data

try {
    $result = $api_instance->rssFeedUpsertPatchRssFeeds($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedUpsertPatchRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedUpsertPutRssFeeds**
> \Swagger\Client\Model\RssFeed rssFeedUpsertPutRssFeeds($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | Model instance data

try {
    $result = $api_instance->rssFeedUpsertPutRssFeeds($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedUpsertPutRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedUpsertWithWhere**
> \Swagger\Client\Model\RssFeed rssFeedUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedApi->rssFeedUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

