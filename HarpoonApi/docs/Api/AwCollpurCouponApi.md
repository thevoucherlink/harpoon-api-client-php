# Harpoon\Api\AwCollpurCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurCouponCount**](AwCollpurCouponApi.md#awCollpurCouponCount) | **GET** /AwCollpurCoupons/count | Count instances of the model matched by where from the data source.
[**awCollpurCouponCreate**](AwCollpurCouponApi.md#awCollpurCouponCreate) | **POST** /AwCollpurCoupons | Create a new instance of the model and persist it into the data source.
[**awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**](AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream) | **GET** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**](AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream) | **POST** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponDeleteById**](AwCollpurCouponApi.md#awCollpurCouponDeleteById) | **DELETE** /AwCollpurCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurCouponExistsGetAwCollpurCouponsidExists**](AwCollpurCouponApi.md#awCollpurCouponExistsGetAwCollpurCouponsidExists) | **GET** /AwCollpurCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurCouponExistsHeadAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponExistsHeadAwCollpurCouponsid) | **HEAD** /AwCollpurCoupons/{id} | Check whether a model instance exists in the data source.
[**awCollpurCouponFind**](AwCollpurCouponApi.md#awCollpurCouponFind) | **GET** /AwCollpurCoupons | Find all instances of the model matched by filter from the data source.
[**awCollpurCouponFindById**](AwCollpurCouponApi.md#awCollpurCouponFindById) | **GET** /AwCollpurCoupons/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurCouponFindOne**](AwCollpurCouponApi.md#awCollpurCouponFindOne) | **GET** /AwCollpurCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid) | **PATCH** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid) | **PUT** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceById**](AwCollpurCouponApi.md#awCollpurCouponReplaceById) | **POST** /AwCollpurCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceOrCreate**](AwCollpurCouponApi.md#awCollpurCouponReplaceOrCreate) | **POST** /AwCollpurCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpdateAll**](AwCollpurCouponApi.md#awCollpurCouponUpdateAll) | **POST** /AwCollpurCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurCouponUpsertPatchAwCollpurCoupons**](AwCollpurCouponApi.md#awCollpurCouponUpsertPatchAwCollpurCoupons) | **PATCH** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertPutAwCollpurCoupons**](AwCollpurCouponApi.md#awCollpurCouponUpsertPutAwCollpurCoupons) | **PUT** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertWithWhere**](AwCollpurCouponApi.md#awCollpurCouponUpsertWithWhere) | **POST** /AwCollpurCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awCollpurCouponCount**
> \Swagger\Client\Model\InlineResponse2001 awCollpurCouponCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awCollpurCouponCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponCreate**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | Model instance data

try {
    $result = $api_instance->awCollpurCouponCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**
> \SplFileObject awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**
> \SplFileObject awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponDeleteById**
> object awCollpurCouponDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurCouponDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponExistsGetAwCollpurCouponsidExists**
> \Swagger\Client\Model\InlineResponse2003 awCollpurCouponExistsGetAwCollpurCouponsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurCouponExistsGetAwCollpurCouponsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponExistsGetAwCollpurCouponsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponExistsHeadAwCollpurCouponsid**
> \Swagger\Client\Model\InlineResponse2003 awCollpurCouponExistsHeadAwCollpurCouponsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurCouponExistsHeadAwCollpurCouponsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponExistsHeadAwCollpurCouponsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponFind**
> \Swagger\Client\Model\AwCollpurCoupon[] awCollpurCouponFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awCollpurCouponFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon[]**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponFindById**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->awCollpurCouponFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponFindOne**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awCollpurCouponFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$id = "id_example"; // string | AwCollpurCoupon id
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurCoupon id |
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$id = "id_example"; // string | AwCollpurCoupon id
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurCoupon id |
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponReplaceById**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | Model instance data

try {
    $result = $api_instance->awCollpurCouponReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponReplaceOrCreate**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | Model instance data

try {
    $result = $api_instance->awCollpurCouponReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 awCollpurCouponUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurCouponUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponUpsertPatchAwCollpurCoupons**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponUpsertPatchAwCollpurCoupons($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | Model instance data

try {
    $result = $api_instance->awCollpurCouponUpsertPatchAwCollpurCoupons($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponUpsertPatchAwCollpurCoupons: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponUpsertPutAwCollpurCoupons**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponUpsertPutAwCollpurCoupons($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | Model instance data

try {
    $result = $api_instance->awCollpurCouponUpsertPutAwCollpurCoupons($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponUpsertPutAwCollpurCoupons: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurCouponUpsertWithWhere**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurCouponUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurCouponApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurCouponUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurCouponApi->awCollpurCouponUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

