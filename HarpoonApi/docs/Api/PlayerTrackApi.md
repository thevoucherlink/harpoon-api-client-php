# Harpoon\Api\PlayerTrackApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerTrackCount**](PlayerTrackApi.md#playerTrackCount) | **GET** /PlayerTracks/count | Count instances of the model matched by where from the data source.
[**playerTrackCreate**](PlayerTrackApi.md#playerTrackCreate) | **POST** /PlayerTracks | Create a new instance of the model and persist it into the data source.
[**playerTrackCreateChangeStreamGetPlayerTracksChangeStream**](PlayerTrackApi.md#playerTrackCreateChangeStreamGetPlayerTracksChangeStream) | **GET** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackCreateChangeStreamPostPlayerTracksChangeStream**](PlayerTrackApi.md#playerTrackCreateChangeStreamPostPlayerTracksChangeStream) | **POST** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackDeleteById**](PlayerTrackApi.md#playerTrackDeleteById) | **DELETE** /PlayerTracks/{id} | Delete a model instance by {{id}} from the data source.
[**playerTrackExistsGetPlayerTracksidExists**](PlayerTrackApi.md#playerTrackExistsGetPlayerTracksidExists) | **GET** /PlayerTracks/{id}/exists | Check whether a model instance exists in the data source.
[**playerTrackExistsHeadPlayerTracksid**](PlayerTrackApi.md#playerTrackExistsHeadPlayerTracksid) | **HEAD** /PlayerTracks/{id} | Check whether a model instance exists in the data source.
[**playerTrackFind**](PlayerTrackApi.md#playerTrackFind) | **GET** /PlayerTracks | Find all instances of the model matched by filter from the data source.
[**playerTrackFindById**](PlayerTrackApi.md#playerTrackFindById) | **GET** /PlayerTracks/{id} | Find a model instance by {{id}} from the data source.
[**playerTrackFindOne**](PlayerTrackApi.md#playerTrackFindOne) | **GET** /PlayerTracks/findOne | Find first instance of the model matched by filter from the data source.
[**playerTrackPrototypeGetPlaylistItem**](PlayerTrackApi.md#playerTrackPrototypeGetPlaylistItem) | **GET** /PlayerTracks/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**](PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPatchPlayerTracksid) | **PATCH** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackPrototypeUpdateAttributesPutPlayerTracksid**](PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPutPlayerTracksid) | **PUT** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackReplaceById**](PlayerTrackApi.md#playerTrackReplaceById) | **POST** /PlayerTracks/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerTrackReplaceOrCreate**](PlayerTrackApi.md#playerTrackReplaceOrCreate) | **POST** /PlayerTracks/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerTrackUpdateAll**](PlayerTrackApi.md#playerTrackUpdateAll) | **POST** /PlayerTracks/update | Update instances of the model matched by {{where}} from the data source.
[**playerTrackUpsertPatchPlayerTracks**](PlayerTrackApi.md#playerTrackUpsertPatchPlayerTracks) | **PATCH** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertPutPlayerTracks**](PlayerTrackApi.md#playerTrackUpsertPutPlayerTracks) | **PUT** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertWithWhere**](PlayerTrackApi.md#playerTrackUpsertWithWhere) | **POST** /PlayerTracks/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playerTrackCount**
> \Swagger\Client\Model\InlineResponse2001 playerTrackCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playerTrackCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackCreate**
> \Swagger\Client\Model\PlayerTrack playerTrackCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | Model instance data

try {
    $result = $api_instance->playerTrackCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackCreateChangeStreamGetPlayerTracksChangeStream**
> \SplFileObject playerTrackCreateChangeStreamGetPlayerTracksChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playerTrackCreateChangeStreamGetPlayerTracksChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackCreateChangeStreamGetPlayerTracksChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackCreateChangeStreamPostPlayerTracksChangeStream**
> \SplFileObject playerTrackCreateChangeStreamPostPlayerTracksChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playerTrackCreateChangeStreamPostPlayerTracksChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackCreateChangeStreamPostPlayerTracksChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackDeleteById**
> object playerTrackDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playerTrackDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackExistsGetPlayerTracksidExists**
> \Swagger\Client\Model\InlineResponse2003 playerTrackExistsGetPlayerTracksidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playerTrackExistsGetPlayerTracksidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackExistsGetPlayerTracksidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackExistsHeadPlayerTracksid**
> \Swagger\Client\Model\InlineResponse2003 playerTrackExistsHeadPlayerTracksid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playerTrackExistsHeadPlayerTracksid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackExistsHeadPlayerTracksid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackFind**
> \Swagger\Client\Model\PlayerTrack[] playerTrackFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playerTrackFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack[]**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackFindById**
> \Swagger\Client\Model\PlayerTrack playerTrackFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->playerTrackFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackFindOne**
> \Swagger\Client\Model\PlayerTrack playerTrackFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playerTrackFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackPrototypeGetPlaylistItem**
> \Swagger\Client\Model\PlaylistItem playerTrackPrototypeGetPlaylistItem($id, $refresh)

Fetches belongsTo relation playlistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | PlayerTrack id
$refresh = true; // bool | 

try {
    $result = $api_instance->playerTrackPrototypeGetPlaylistItem($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackPrototypeGetPlaylistItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlayerTrack id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**
> \Swagger\Client\Model\PlayerTrack playerTrackPrototypeUpdateAttributesPatchPlayerTracksid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | PlayerTrack id
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | An object of model property name/value pairs

try {
    $result = $api_instance->playerTrackPrototypeUpdateAttributesPatchPlayerTracksid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackPrototypeUpdateAttributesPatchPlayerTracksid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlayerTrack id |
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackPrototypeUpdateAttributesPutPlayerTracksid**
> \Swagger\Client\Model\PlayerTrack playerTrackPrototypeUpdateAttributesPutPlayerTracksid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | PlayerTrack id
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | An object of model property name/value pairs

try {
    $result = $api_instance->playerTrackPrototypeUpdateAttributesPutPlayerTracksid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackPrototypeUpdateAttributesPutPlayerTracksid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlayerTrack id |
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackReplaceById**
> \Swagger\Client\Model\PlayerTrack playerTrackReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | Model instance data

try {
    $result = $api_instance->playerTrackReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackReplaceOrCreate**
> \Swagger\Client\Model\PlayerTrack playerTrackReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | Model instance data

try {
    $result = $api_instance->playerTrackReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 playerTrackUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | An object of model property name/value pairs

try {
    $result = $api_instance->playerTrackUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackUpsertPatchPlayerTracks**
> \Swagger\Client\Model\PlayerTrack playerTrackUpsertPatchPlayerTracks($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | Model instance data

try {
    $result = $api_instance->playerTrackUpsertPatchPlayerTracks($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackUpsertPatchPlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackUpsertPutPlayerTracks**
> \Swagger\Client\Model\PlayerTrack playerTrackUpsertPutPlayerTracks($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | Model instance data

try {
    $result = $api_instance->playerTrackUpsertPutPlayerTracks($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackUpsertPutPlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerTrackUpsertWithWhere**
> \Swagger\Client\Model\PlayerTrack playerTrackUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlayerTrackApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | An object of model property name/value pairs

try {
    $result = $api_instance->playerTrackUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlayerTrackApi->playerTrackUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

