# Harpoon\Api\AppApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**appAnalyticTrack**](AppApi.md#appAnalyticTrack) | **POST** /Apps/{id}/analytics/track | 
[**appCount**](AppApi.md#appCount) | **GET** /Apps/count | Count instances of the model matched by where from the data source.
[**appCreateChangeStreamGetAppsChangeStream**](AppApi.md#appCreateChangeStreamGetAppsChangeStream) | **GET** /Apps/change-stream | Create a change stream.
[**appCreateChangeStreamPostAppsChangeStream**](AppApi.md#appCreateChangeStreamPostAppsChangeStream) | **POST** /Apps/change-stream | Create a change stream.
[**appCreateWithAuthentication**](AppApi.md#appCreateWithAuthentication) | **POST** /Apps | Create a new instance of the model and persist it into the data source.
[**appDeleteById**](AppApi.md#appDeleteById) | **DELETE** /Apps/{id} | Delete a model instance by {{id}} from the data source.
[**appExistsGetAppsidExists**](AppApi.md#appExistsGetAppsidExists) | **GET** /Apps/{id}/exists | Check whether a model instance exists in the data source.
[**appExistsHeadAppsid**](AppApi.md#appExistsHeadAppsid) | **HEAD** /Apps/{id} | Check whether a model instance exists in the data source.
[**appFind**](AppApi.md#appFind) | **GET** /Apps | Find all instances of the model matched by filter from the data source.
[**appFindById**](AppApi.md#appFindById) | **GET** /Apps/{id} | Find a model instance by {{id}} from the data source.
[**appFindByIdForVersion**](AppApi.md#appFindByIdForVersion) | **GET** /Apps/{id}/{appOs}/{appVersion} | 
[**appFindOne**](AppApi.md#appFindOne) | **GET** /Apps/findOne | Find first instance of the model matched by filter from the data source.
[**appPrototypeCountAppConfigs**](AppApi.md#appPrototypeCountAppConfigs) | **GET** /Apps/{id}/appConfigs/count | Counts appConfigs of App.
[**appPrototypeCountApps**](AppApi.md#appPrototypeCountApps) | **GET** /Apps/{id}/apps/count | Counts apps of App.
[**appPrototypeCountPlaylists**](AppApi.md#appPrototypeCountPlaylists) | **GET** /Apps/{id}/playlists/count | Counts playlists of App.
[**appPrototypeCountRadioStreams**](AppApi.md#appPrototypeCountRadioStreams) | **GET** /Apps/{id}/radioStreams/count | Counts radioStreams of App.
[**appPrototypeCountRssFeedGroups**](AppApi.md#appPrototypeCountRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/count | Counts rssFeedGroups of App.
[**appPrototypeCountRssFeeds**](AppApi.md#appPrototypeCountRssFeeds) | **GET** /Apps/{id}/rssFeeds/count | Counts rssFeeds of App.
[**appPrototypeCreateAppConfigs**](AppApi.md#appPrototypeCreateAppConfigs) | **POST** /Apps/{id}/appConfigs | Creates a new instance in appConfigs of this model.
[**appPrototypeCreateApps**](AppApi.md#appPrototypeCreateApps) | **POST** /Apps/{id}/apps | Creates a new instance in apps of this model.
[**appPrototypeCreateCustomers**](AppApi.md#appPrototypeCreateCustomers) | **POST** /Apps/{id}/customers | Creates a new instance in customers of this model.
[**appPrototypeCreatePlaylists**](AppApi.md#appPrototypeCreatePlaylists) | **POST** /Apps/{id}/playlists | Creates a new instance in playlists of this model.
[**appPrototypeCreateRadioStreams**](AppApi.md#appPrototypeCreateRadioStreams) | **POST** /Apps/{id}/radioStreams | Creates a new instance in radioStreams of this model.
[**appPrototypeCreateRssFeedGroups**](AppApi.md#appPrototypeCreateRssFeedGroups) | **POST** /Apps/{id}/rssFeedGroups | Creates a new instance in rssFeedGroups of this model.
[**appPrototypeCreateRssFeeds**](AppApi.md#appPrototypeCreateRssFeeds) | **POST** /Apps/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**appPrototypeDeleteAppConfigs**](AppApi.md#appPrototypeDeleteAppConfigs) | **DELETE** /Apps/{id}/appConfigs | Deletes all appConfigs of this model.
[**appPrototypeDeleteApps**](AppApi.md#appPrototypeDeleteApps) | **DELETE** /Apps/{id}/apps | Deletes all apps of this model.
[**appPrototypeDeletePlaylists**](AppApi.md#appPrototypeDeletePlaylists) | **DELETE** /Apps/{id}/playlists | Deletes all playlists of this model.
[**appPrototypeDeleteRadioStreams**](AppApi.md#appPrototypeDeleteRadioStreams) | **DELETE** /Apps/{id}/radioStreams | Deletes all radioStreams of this model.
[**appPrototypeDeleteRssFeedGroups**](AppApi.md#appPrototypeDeleteRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups | Deletes all rssFeedGroups of this model.
[**appPrototypeDeleteRssFeeds**](AppApi.md#appPrototypeDeleteRssFeeds) | **DELETE** /Apps/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**appPrototypeDestroyByIdAppConfigs**](AppApi.md#appPrototypeDestroyByIdAppConfigs) | **DELETE** /Apps/{id}/appConfigs/{fk} | Delete a related item by id for appConfigs.
[**appPrototypeDestroyByIdApps**](AppApi.md#appPrototypeDestroyByIdApps) | **DELETE** /Apps/{id}/apps/{fk} | Delete a related item by id for apps.
[**appPrototypeDestroyByIdPlaylists**](AppApi.md#appPrototypeDestroyByIdPlaylists) | **DELETE** /Apps/{id}/playlists/{fk} | Delete a related item by id for playlists.
[**appPrototypeDestroyByIdRadioStreams**](AppApi.md#appPrototypeDestroyByIdRadioStreams) | **DELETE** /Apps/{id}/radioStreams/{fk} | Delete a related item by id for radioStreams.
[**appPrototypeDestroyByIdRssFeedGroups**](AppApi.md#appPrototypeDestroyByIdRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups/{fk} | Delete a related item by id for rssFeedGroups.
[**appPrototypeDestroyByIdRssFeeds**](AppApi.md#appPrototypeDestroyByIdRssFeeds) | **DELETE** /Apps/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**appPrototypeFindByIdAppConfigs**](AppApi.md#appPrototypeFindByIdAppConfigs) | **GET** /Apps/{id}/appConfigs/{fk} | Find a related item by id for appConfigs.
[**appPrototypeFindByIdApps**](AppApi.md#appPrototypeFindByIdApps) | **GET** /Apps/{id}/apps/{fk} | Find a related item by id for apps.
[**appPrototypeFindByIdCustomers**](AppApi.md#appPrototypeFindByIdCustomers) | **GET** /Apps/{id}/customers/{fk} | Find a related item by id for customers.
[**appPrototypeFindByIdPlaylists**](AppApi.md#appPrototypeFindByIdPlaylists) | **GET** /Apps/{id}/playlists/{fk} | Find a related item by id for playlists.
[**appPrototypeFindByIdRadioStreams**](AppApi.md#appPrototypeFindByIdRadioStreams) | **GET** /Apps/{id}/radioStreams/{fk} | Find a related item by id for radioStreams.
[**appPrototypeFindByIdRssFeedGroups**](AppApi.md#appPrototypeFindByIdRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/{fk} | Find a related item by id for rssFeedGroups.
[**appPrototypeFindByIdRssFeeds**](AppApi.md#appPrototypeFindByIdRssFeeds) | **GET** /Apps/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**appPrototypeGetAppConfigs**](AppApi.md#appPrototypeGetAppConfigs) | **GET** /Apps/{id}/appConfigs | Queries appConfigs of App.
[**appPrototypeGetApps**](AppApi.md#appPrototypeGetApps) | **GET** /Apps/{id}/apps | Queries apps of App.
[**appPrototypeGetCustomers**](AppApi.md#appPrototypeGetCustomers) | **GET** /Apps/{id}/customers | Queries customers of App.
[**appPrototypeGetParent**](AppApi.md#appPrototypeGetParent) | **GET** /Apps/{id}/parent | Fetches belongsTo relation parent.
[**appPrototypeGetPlaylists**](AppApi.md#appPrototypeGetPlaylists) | **GET** /Apps/{id}/playlists | Queries playlists of App.
[**appPrototypeGetRadioStreams**](AppApi.md#appPrototypeGetRadioStreams) | **GET** /Apps/{id}/radioStreams | Queries radioStreams of App.
[**appPrototypeGetRssFeedGroups**](AppApi.md#appPrototypeGetRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups | Queries rssFeedGroups of App.
[**appPrototypeGetRssFeeds**](AppApi.md#appPrototypeGetRssFeeds) | **GET** /Apps/{id}/rssFeeds | Queries rssFeeds of App.
[**appPrototypeUpdateAttributesPatchAppsid**](AppApi.md#appPrototypeUpdateAttributesPatchAppsid) | **PATCH** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateAttributesPutAppsid**](AppApi.md#appPrototypeUpdateAttributesPutAppsid) | **PUT** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateByIdAppConfigs**](AppApi.md#appPrototypeUpdateByIdAppConfigs) | **PUT** /Apps/{id}/appConfigs/{fk} | Update a related item by id for appConfigs.
[**appPrototypeUpdateByIdApps**](AppApi.md#appPrototypeUpdateByIdApps) | **PUT** /Apps/{id}/apps/{fk} | Update a related item by id for apps.
[**appPrototypeUpdateByIdCustomers**](AppApi.md#appPrototypeUpdateByIdCustomers) | **PUT** /Apps/{id}/customers/{fk} | Update a related item by id for customers.
[**appPrototypeUpdateByIdPlaylists**](AppApi.md#appPrototypeUpdateByIdPlaylists) | **PUT** /Apps/{id}/playlists/{fk} | Update a related item by id for playlists.
[**appPrototypeUpdateByIdRadioStreams**](AppApi.md#appPrototypeUpdateByIdRadioStreams) | **PUT** /Apps/{id}/radioStreams/{fk} | Update a related item by id for radioStreams.
[**appPrototypeUpdateByIdRssFeedGroups**](AppApi.md#appPrototypeUpdateByIdRssFeedGroups) | **PUT** /Apps/{id}/rssFeedGroups/{fk} | Update a related item by id for rssFeedGroups.
[**appPrototypeUpdateByIdRssFeeds**](AppApi.md#appPrototypeUpdateByIdRssFeeds) | **PUT** /Apps/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**appReplaceById**](AppApi.md#appReplaceById) | **POST** /Apps/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**appReplaceOrCreate**](AppApi.md#appReplaceOrCreate) | **POST** /Apps/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**appUpdateAll**](AppApi.md#appUpdateAll) | **POST** /Apps/update | Update instances of the model matched by {{where}} from the data source.
[**appUploadFile**](AppApi.md#appUploadFile) | **POST** /Apps/{id}/file/{type} | 
[**appUpsertPatchApps**](AppApi.md#appUpsertPatchApps) | **PATCH** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertPutApps**](AppApi.md#appUpsertPutApps) | **PUT** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertWithWhere**](AppApi.md#appUpsertWithWhere) | **POST** /Apps/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**appVerify**](AppApi.md#appVerify) | **GET** /Apps/verify | 


# **appAnalyticTrack**
> object appAnalyticTrack($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | 
$data = new \Swagger\Client\Model\AnalyticRequest(); // \Swagger\Client\Model\AnalyticRequest | 

try {
    $result = $api_instance->appAnalyticTrack($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appAnalyticTrack: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **data** | [**\Swagger\Client\Model\AnalyticRequest**](../Model/\Swagger\Client\Model\AnalyticRequest.md)|  | [optional]

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appCount**
> \Swagger\Client\Model\InlineResponse2001 appCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->appCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appCreateChangeStreamGetAppsChangeStream**
> \SplFileObject appCreateChangeStreamGetAppsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->appCreateChangeStreamGetAppsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appCreateChangeStreamGetAppsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appCreateChangeStreamPostAppsChangeStream**
> \SplFileObject appCreateChangeStreamPostAppsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->appCreateChangeStreamPostAppsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appCreateChangeStreamPostAppsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appCreateWithAuthentication**
> \Swagger\Client\Model\App appCreateWithAuthentication($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | Model instance data

try {
    $result = $api_instance->appCreateWithAuthentication($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appCreateWithAuthentication: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appDeleteById**
> object appDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->appDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appExistsGetAppsidExists**
> \Swagger\Client\Model\InlineResponse2003 appExistsGetAppsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->appExistsGetAppsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appExistsGetAppsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appExistsHeadAppsid**
> \Swagger\Client\Model\InlineResponse2003 appExistsHeadAppsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->appExistsHeadAppsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appExistsHeadAppsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appFind**
> \Swagger\Client\Model\App[] appFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->appFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\App[]**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appFindById**
> \Swagger\Client\Model\App appFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->appFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appFindByIdForVersion**
> \Swagger\Client\Model\App appFindByIdForVersion($id, $appOs, $appVersion)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | 
$appOs = "appOs_example"; // string | Either \"ios\" or \"android\"
$appVersion = "appVersion_example"; // string | e.g. 1.2.3

try {
    $result = $api_instance->appFindByIdForVersion($id, $appOs, $appVersion);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appFindByIdForVersion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **appOs** | **string**| Either \&quot;ios\&quot; or \&quot;android\&quot; |
 **appVersion** | **string**| e.g. 1.2.3 |

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appFindOne**
> \Swagger\Client\Model\App appFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->appFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCountAppConfigs**
> \Swagger\Client\Model\InlineResponse2001 appPrototypeCountAppConfigs($id, $where)

Counts appConfigs of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->appPrototypeCountAppConfigs($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCountAppConfigs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCountApps**
> \Swagger\Client\Model\InlineResponse2001 appPrototypeCountApps($id, $where)

Counts apps of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->appPrototypeCountApps($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCountApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCountPlaylists**
> \Swagger\Client\Model\InlineResponse2001 appPrototypeCountPlaylists($id, $where)

Counts playlists of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->appPrototypeCountPlaylists($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCountPlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCountRadioStreams**
> \Swagger\Client\Model\InlineResponse2001 appPrototypeCountRadioStreams($id, $where)

Counts radioStreams of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->appPrototypeCountRadioStreams($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCountRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCountRssFeedGroups**
> \Swagger\Client\Model\InlineResponse2001 appPrototypeCountRssFeedGroups($id, $where)

Counts rssFeedGroups of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->appPrototypeCountRssFeedGroups($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCountRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCountRssFeeds**
> \Swagger\Client\Model\InlineResponse2001 appPrototypeCountRssFeeds($id, $where)

Counts rssFeeds of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->appPrototypeCountRssFeeds($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCountRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCreateAppConfigs**
> \Swagger\Client\Model\AppConfig appPrototypeCreateAppConfigs($id, $data)

Creates a new instance in appConfigs of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\AppConfig(); // \Swagger\Client\Model\AppConfig | 

try {
    $result = $api_instance->appPrototypeCreateAppConfigs($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCreateAppConfigs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\AppConfig**](../Model/\Swagger\Client\Model\AppConfig.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AppConfig**](../Model/AppConfig.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCreateApps**
> \Swagger\Client\Model\App appPrototypeCreateApps($id, $data)

Creates a new instance in apps of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | 

try {
    $result = $api_instance->appPrototypeCreateApps($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCreateApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCreateCustomers**
> \Swagger\Client\Model\Customer appPrototypeCreateCustomers($id, $data)

Creates a new instance in customers of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\Customer(); // \Swagger\Client\Model\Customer | 

try {
    $result = $api_instance->appPrototypeCreateCustomers($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCreateCustomers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\Customer**](../Model/\Swagger\Client\Model\Customer.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Customer**](../Model/Customer.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCreatePlaylists**
> \Swagger\Client\Model\Playlist appPrototypeCreatePlaylists($id, $data)

Creates a new instance in playlists of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | 

try {
    $result = $api_instance->appPrototypeCreatePlaylists($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCreatePlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCreateRadioStreams**
> \Swagger\Client\Model\RadioStream appPrototypeCreateRadioStreams($id, $data)

Creates a new instance in radioStreams of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | 

try {
    $result = $api_instance->appPrototypeCreateRadioStreams($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCreateRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCreateRssFeedGroups**
> \Swagger\Client\Model\RssFeedGroup appPrototypeCreateRssFeedGroups($id, $data)

Creates a new instance in rssFeedGroups of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | 

try {
    $result = $api_instance->appPrototypeCreateRssFeedGroups($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCreateRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeCreateRssFeeds**
> \Swagger\Client\Model\RssFeed appPrototypeCreateRssFeeds($id, $data)

Creates a new instance in rssFeeds of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | 

try {
    $result = $api_instance->appPrototypeCreateRssFeeds($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeCreateRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDeleteAppConfigs**
> appPrototypeDeleteAppConfigs($id)

Deletes all appConfigs of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDeleteAppConfigs($id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDeleteAppConfigs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDeleteApps**
> appPrototypeDeleteApps($id)

Deletes all apps of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDeleteApps($id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDeleteApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDeletePlaylists**
> appPrototypeDeletePlaylists($id)

Deletes all playlists of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDeletePlaylists($id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDeletePlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDeleteRadioStreams**
> appPrototypeDeleteRadioStreams($id)

Deletes all radioStreams of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDeleteRadioStreams($id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDeleteRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDeleteRssFeedGroups**
> appPrototypeDeleteRssFeedGroups($id)

Deletes all rssFeedGroups of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDeleteRssFeedGroups($id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDeleteRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDeleteRssFeeds**
> appPrototypeDeleteRssFeeds($id)

Deletes all rssFeeds of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDeleteRssFeeds($id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDeleteRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDestroyByIdAppConfigs**
> appPrototypeDestroyByIdAppConfigs($fk, $id)

Delete a related item by id for appConfigs.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for appConfigs
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDestroyByIdAppConfigs($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDestroyByIdAppConfigs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for appConfigs |
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDestroyByIdApps**
> appPrototypeDestroyByIdApps($fk, $id)

Delete a related item by id for apps.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for apps
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDestroyByIdApps($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDestroyByIdApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for apps |
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDestroyByIdPlaylists**
> appPrototypeDestroyByIdPlaylists($fk, $id)

Delete a related item by id for playlists.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for playlists
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDestroyByIdPlaylists($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDestroyByIdPlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playlists |
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDestroyByIdRadioStreams**
> appPrototypeDestroyByIdRadioStreams($fk, $id)

Delete a related item by id for radioStreams.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for radioStreams
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDestroyByIdRadioStreams($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDestroyByIdRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioStreams |
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDestroyByIdRssFeedGroups**
> appPrototypeDestroyByIdRssFeedGroups($fk, $id)

Delete a related item by id for rssFeedGroups.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for rssFeedGroups
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDestroyByIdRssFeedGroups($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDestroyByIdRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeedGroups |
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeDestroyByIdRssFeeds**
> appPrototypeDestroyByIdRssFeeds($fk, $id)

Delete a related item by id for rssFeeds.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for rssFeeds
$id = "id_example"; // string | App id

try {
    $api_instance->appPrototypeDestroyByIdRssFeeds($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeDestroyByIdRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeeds |
 **id** | **string**| App id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeFindByIdAppConfigs**
> \Swagger\Client\Model\AppConfig appPrototypeFindByIdAppConfigs($fk, $id)

Find a related item by id for appConfigs.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for appConfigs
$id = "id_example"; // string | App id

try {
    $result = $api_instance->appPrototypeFindByIdAppConfigs($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeFindByIdAppConfigs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for appConfigs |
 **id** | **string**| App id |

### Return type

[**\Swagger\Client\Model\AppConfig**](../Model/AppConfig.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeFindByIdApps**
> \Swagger\Client\Model\App appPrototypeFindByIdApps($fk, $id)

Find a related item by id for apps.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for apps
$id = "id_example"; // string | App id

try {
    $result = $api_instance->appPrototypeFindByIdApps($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeFindByIdApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for apps |
 **id** | **string**| App id |

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeFindByIdCustomers**
> \Swagger\Client\Model\Customer appPrototypeFindByIdCustomers($fk, $id)

Find a related item by id for customers.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for customers
$id = "id_example"; // string | App id

try {
    $result = $api_instance->appPrototypeFindByIdCustomers($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeFindByIdCustomers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for customers |
 **id** | **string**| App id |

### Return type

[**\Swagger\Client\Model\Customer**](../Model/Customer.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeFindByIdPlaylists**
> \Swagger\Client\Model\Playlist appPrototypeFindByIdPlaylists($fk, $id)

Find a related item by id for playlists.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for playlists
$id = "id_example"; // string | App id

try {
    $result = $api_instance->appPrototypeFindByIdPlaylists($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeFindByIdPlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playlists |
 **id** | **string**| App id |

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeFindByIdRadioStreams**
> \Swagger\Client\Model\RadioStream appPrototypeFindByIdRadioStreams($fk, $id)

Find a related item by id for radioStreams.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for radioStreams
$id = "id_example"; // string | App id

try {
    $result = $api_instance->appPrototypeFindByIdRadioStreams($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeFindByIdRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioStreams |
 **id** | **string**| App id |

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeFindByIdRssFeedGroups**
> \Swagger\Client\Model\RssFeedGroup appPrototypeFindByIdRssFeedGroups($fk, $id)

Find a related item by id for rssFeedGroups.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for rssFeedGroups
$id = "id_example"; // string | App id

try {
    $result = $api_instance->appPrototypeFindByIdRssFeedGroups($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeFindByIdRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeedGroups |
 **id** | **string**| App id |

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeFindByIdRssFeeds**
> \Swagger\Client\Model\RssFeed appPrototypeFindByIdRssFeeds($fk, $id)

Find a related item by id for rssFeeds.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for rssFeeds
$id = "id_example"; // string | App id

try {
    $result = $api_instance->appPrototypeFindByIdRssFeeds($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeFindByIdRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeeds |
 **id** | **string**| App id |

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetAppConfigs**
> \Swagger\Client\Model\AppConfig[] appPrototypeGetAppConfigs($id, $filter)

Queries appConfigs of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->appPrototypeGetAppConfigs($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetAppConfigs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AppConfig[]**](../Model/AppConfig.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetApps**
> \Swagger\Client\Model\App[] appPrototypeGetApps($id, $filter)

Queries apps of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->appPrototypeGetApps($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\App[]**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetCustomers**
> \Swagger\Client\Model\Customer[] appPrototypeGetCustomers($id, $filter)

Queries customers of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->appPrototypeGetCustomers($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetCustomers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\Customer[]**](../Model/Customer.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetParent**
> \Swagger\Client\Model\App appPrototypeGetParent($id, $refresh)

Fetches belongsTo relation parent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$refresh = true; // bool | 

try {
    $result = $api_instance->appPrototypeGetParent($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetParent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetPlaylists**
> \Swagger\Client\Model\Playlist[] appPrototypeGetPlaylists($id, $filter)

Queries playlists of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->appPrototypeGetPlaylists($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetPlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\Playlist[]**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetRadioStreams**
> \Swagger\Client\Model\RadioStream[] appPrototypeGetRadioStreams($id, $filter)

Queries radioStreams of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->appPrototypeGetRadioStreams($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream[]**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetRssFeedGroups**
> \Swagger\Client\Model\RssFeedGroup[] appPrototypeGetRssFeedGroups($id, $filter)

Queries rssFeedGroups of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->appPrototypeGetRssFeedGroups($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup[]**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeGetRssFeeds**
> \Swagger\Client\Model\RssFeed[] appPrototypeGetRssFeeds($id, $filter)

Queries rssFeeds of App.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->appPrototypeGetRssFeeds($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeGetRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed[]**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateAttributesPatchAppsid**
> \Swagger\Client\Model\App appPrototypeUpdateAttributesPatchAppsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | An object of model property name/value pairs

try {
    $result = $api_instance->appPrototypeUpdateAttributesPatchAppsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateAttributesPatchAppsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateAttributesPutAppsid**
> \Swagger\Client\Model\App appPrototypeUpdateAttributesPutAppsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | An object of model property name/value pairs

try {
    $result = $api_instance->appPrototypeUpdateAttributesPutAppsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateAttributesPutAppsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateByIdAppConfigs**
> \Swagger\Client\Model\AppConfig appPrototypeUpdateByIdAppConfigs($fk, $id, $data)

Update a related item by id for appConfigs.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for appConfigs
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\AppConfig(); // \Swagger\Client\Model\AppConfig | 

try {
    $result = $api_instance->appPrototypeUpdateByIdAppConfigs($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateByIdAppConfigs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for appConfigs |
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\AppConfig**](../Model/\Swagger\Client\Model\AppConfig.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AppConfig**](../Model/AppConfig.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateByIdApps**
> \Swagger\Client\Model\App appPrototypeUpdateByIdApps($fk, $id, $data)

Update a related item by id for apps.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for apps
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | 

try {
    $result = $api_instance->appPrototypeUpdateByIdApps($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateByIdApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for apps |
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateByIdCustomers**
> \Swagger\Client\Model\Customer appPrototypeUpdateByIdCustomers($fk, $id, $data)

Update a related item by id for customers.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for customers
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\Customer(); // \Swagger\Client\Model\Customer | 

try {
    $result = $api_instance->appPrototypeUpdateByIdCustomers($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateByIdCustomers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for customers |
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\Customer**](../Model/\Swagger\Client\Model\Customer.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Customer**](../Model/Customer.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateByIdPlaylists**
> \Swagger\Client\Model\Playlist appPrototypeUpdateByIdPlaylists($fk, $id, $data)

Update a related item by id for playlists.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for playlists
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | 

try {
    $result = $api_instance->appPrototypeUpdateByIdPlaylists($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateByIdPlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playlists |
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateByIdRadioStreams**
> \Swagger\Client\Model\RadioStream appPrototypeUpdateByIdRadioStreams($fk, $id, $data)

Update a related item by id for radioStreams.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for radioStreams
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | 

try {
    $result = $api_instance->appPrototypeUpdateByIdRadioStreams($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateByIdRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioStreams |
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateByIdRssFeedGroups**
> \Swagger\Client\Model\RssFeedGroup appPrototypeUpdateByIdRssFeedGroups($fk, $id, $data)

Update a related item by id for rssFeedGroups.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for rssFeedGroups
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | 

try {
    $result = $api_instance->appPrototypeUpdateByIdRssFeedGroups($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateByIdRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeedGroups |
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appPrototypeUpdateByIdRssFeeds**
> \Swagger\Client\Model\RssFeed appPrototypeUpdateByIdRssFeeds($fk, $id, $data)

Update a related item by id for rssFeeds.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$fk = "fk_example"; // string | Foreign key for rssFeeds
$id = "id_example"; // string | App id
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | 

try {
    $result = $api_instance->appPrototypeUpdateByIdRssFeeds($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appPrototypeUpdateByIdRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeeds |
 **id** | **string**| App id |
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appReplaceById**
> \Swagger\Client\Model\App appReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | Model instance data

try {
    $result = $api_instance->appReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appReplaceOrCreate**
> \Swagger\Client\Model\App appReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | Model instance data

try {
    $result = $api_instance->appReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 appUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | An object of model property name/value pairs

try {
    $result = $api_instance->appUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appUploadFile**
> \Swagger\Client\Model\MagentoFileUpload appUploadFile($id, $type, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$id = "id_example"; // string | Application Id
$type = "type_example"; // string | Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter).
$data = new \Swagger\Client\Model\MagentoFileUpload(); // \Swagger\Client\Model\MagentoFileUpload | Corresponds to the file you're uploading formatted as an object.

try {
    $result = $api_instance->appUploadFile($id, $type, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appUploadFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Application Id |
 **type** | **string**| Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter). |
 **data** | [**\Swagger\Client\Model\MagentoFileUpload**](../Model/\Swagger\Client\Model\MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional]

### Return type

[**\Swagger\Client\Model\MagentoFileUpload**](../Model/MagentoFileUpload.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appUpsertPatchApps**
> \Swagger\Client\Model\App appUpsertPatchApps($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | Model instance data

try {
    $result = $api_instance->appUpsertPatchApps($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appUpsertPatchApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appUpsertPutApps**
> \Swagger\Client\Model\App appUpsertPutApps($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | Model instance data

try {
    $result = $api_instance->appUpsertPutApps($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appUpsertPutApps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appUpsertWithWhere**
> \Swagger\Client\Model\App appUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\App(); // \Swagger\Client\Model\App | An object of model property name/value pairs

try {
    $result = $api_instance->appUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\App**](../Model/\Swagger\Client\Model\App.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\App**](../Model/App.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **appVerify**
> object appVerify()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AppApi();

try {
    $result = $api_instance->appVerify();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppApi->appVerify: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

