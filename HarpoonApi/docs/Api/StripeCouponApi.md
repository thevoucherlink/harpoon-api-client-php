# Harpoon\Api\StripeCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeCouponCount**](StripeCouponApi.md#stripeCouponCount) | **GET** /StripeCoupons/count | Count instances of the model matched by where from the data source.
[**stripeCouponCreate**](StripeCouponApi.md#stripeCouponCreate) | **POST** /StripeCoupons | Create a new instance of the model and persist it into the data source.
[**stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**](StripeCouponApi.md#stripeCouponCreateChangeStreamGetStripeCouponsChangeStream) | **GET** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**](StripeCouponApi.md#stripeCouponCreateChangeStreamPostStripeCouponsChangeStream) | **POST** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponDeleteById**](StripeCouponApi.md#stripeCouponDeleteById) | **DELETE** /StripeCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**stripeCouponExistsGetStripeCouponsidExists**](StripeCouponApi.md#stripeCouponExistsGetStripeCouponsidExists) | **GET** /StripeCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**stripeCouponExistsHeadStripeCouponsid**](StripeCouponApi.md#stripeCouponExistsHeadStripeCouponsid) | **HEAD** /StripeCoupons/{id} | Check whether a model instance exists in the data source.
[**stripeCouponFind**](StripeCouponApi.md#stripeCouponFind) | **GET** /StripeCoupons | Find all instances of the model matched by filter from the data source.
[**stripeCouponFindById**](StripeCouponApi.md#stripeCouponFindById) | **GET** /StripeCoupons/{id} | Find a model instance by {{id}} from the data source.
[**stripeCouponFindOne**](StripeCouponApi.md#stripeCouponFindOne) | **GET** /StripeCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**](StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid) | **PATCH** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**](StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPutStripeCouponsid) | **PUT** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceById**](StripeCouponApi.md#stripeCouponReplaceById) | **POST** /StripeCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceOrCreate**](StripeCouponApi.md#stripeCouponReplaceOrCreate) | **POST** /StripeCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeCouponUpdateAll**](StripeCouponApi.md#stripeCouponUpdateAll) | **POST** /StripeCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**stripeCouponUpsertPatchStripeCoupons**](StripeCouponApi.md#stripeCouponUpsertPatchStripeCoupons) | **PATCH** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertPutStripeCoupons**](StripeCouponApi.md#stripeCouponUpsertPutStripeCoupons) | **PUT** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertWithWhere**](StripeCouponApi.md#stripeCouponUpsertWithWhere) | **POST** /StripeCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeCouponCount**
> \Swagger\Client\Model\InlineResponse2001 stripeCouponCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->stripeCouponCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponCreate**
> \Swagger\Client\Model\StripeCoupon stripeCouponCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | Model instance data

try {
    $result = $api_instance->stripeCouponCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**
> \SplFileObject stripeCouponCreateChangeStreamGetStripeCouponsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeCouponCreateChangeStreamGetStripeCouponsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponCreateChangeStreamGetStripeCouponsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**
> \SplFileObject stripeCouponCreateChangeStreamPostStripeCouponsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeCouponCreateChangeStreamPostStripeCouponsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponCreateChangeStreamPostStripeCouponsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponDeleteById**
> object stripeCouponDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeCouponDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponExistsGetStripeCouponsidExists**
> \Swagger\Client\Model\InlineResponse2003 stripeCouponExistsGetStripeCouponsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeCouponExistsGetStripeCouponsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponExistsGetStripeCouponsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponExistsHeadStripeCouponsid**
> \Swagger\Client\Model\InlineResponse2003 stripeCouponExistsHeadStripeCouponsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeCouponExistsHeadStripeCouponsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponExistsHeadStripeCouponsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponFind**
> \Swagger\Client\Model\StripeCoupon[] stripeCouponFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeCouponFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon[]**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponFindById**
> \Swagger\Client\Model\StripeCoupon stripeCouponFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->stripeCouponFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponFindOne**
> \Swagger\Client\Model\StripeCoupon stripeCouponFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeCouponFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**
> \Swagger\Client\Model\StripeCoupon stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$id = "id_example"; // string | StripeCoupon id
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeCoupon id |
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**
> \Swagger\Client\Model\StripeCoupon stripeCouponPrototypeUpdateAttributesPutStripeCouponsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$id = "id_example"; // string | StripeCoupon id
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->stripeCouponPrototypeUpdateAttributesPutStripeCouponsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponPrototypeUpdateAttributesPutStripeCouponsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeCoupon id |
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponReplaceById**
> \Swagger\Client\Model\StripeCoupon stripeCouponReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | Model instance data

try {
    $result = $api_instance->stripeCouponReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponReplaceOrCreate**
> \Swagger\Client\Model\StripeCoupon stripeCouponReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | Model instance data

try {
    $result = $api_instance->stripeCouponReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 stripeCouponUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->stripeCouponUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponUpsertPatchStripeCoupons**
> \Swagger\Client\Model\StripeCoupon stripeCouponUpsertPatchStripeCoupons($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | Model instance data

try {
    $result = $api_instance->stripeCouponUpsertPatchStripeCoupons($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponUpsertPatchStripeCoupons: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponUpsertPutStripeCoupons**
> \Swagger\Client\Model\StripeCoupon stripeCouponUpsertPutStripeCoupons($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | Model instance data

try {
    $result = $api_instance->stripeCouponUpsertPutStripeCoupons($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponUpsertPutStripeCoupons: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeCouponUpsertWithWhere**
> \Swagger\Client\Model\StripeCoupon stripeCouponUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeCouponApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeCoupon(); // \Swagger\Client\Model\StripeCoupon | An object of model property name/value pairs

try {
    $result = $api_instance->stripeCouponUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeCouponApi->stripeCouponUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeCoupon**](../Model/\Swagger\Client\Model\StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeCoupon**](../Model/StripeCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

