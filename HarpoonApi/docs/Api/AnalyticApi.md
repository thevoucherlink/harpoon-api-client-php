# Harpoon\Api\AnalyticApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**analyticReplaceById**](AnalyticApi.md#analyticReplaceById) | **POST** /Analytics/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**analyticReplaceOrCreate**](AnalyticApi.md#analyticReplaceOrCreate) | **POST** /Analytics/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**analyticTrack**](AnalyticApi.md#analyticTrack) | **POST** /Analytics/track | 
[**analyticUpsertWithWhere**](AnalyticApi.md#analyticUpsertWithWhere) | **POST** /Analytics/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **analyticReplaceById**
> \Swagger\Client\Model\Analytic analyticReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AnalyticApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\Analytic(); // \Swagger\Client\Model\Analytic | Model instance data

try {
    $result = $api_instance->analyticReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnalyticApi->analyticReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\Analytic**](../Model/\Swagger\Client\Model\Analytic.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Analytic**](../Model/Analytic.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **analyticReplaceOrCreate**
> \Swagger\Client\Model\Analytic analyticReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AnalyticApi();
$data = new \Swagger\Client\Model\Analytic(); // \Swagger\Client\Model\Analytic | Model instance data

try {
    $result = $api_instance->analyticReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnalyticApi->analyticReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\Analytic**](../Model/\Swagger\Client\Model\Analytic.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Analytic**](../Model/Analytic.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **analyticTrack**
> \Swagger\Client\Model\InlineResponse200 analyticTrack($data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AnalyticApi();
$data = new \Swagger\Client\Model\AnalyticRequest(); // \Swagger\Client\Model\AnalyticRequest | 

try {
    $result = $api_instance->analyticTrack($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnalyticApi->analyticTrack: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AnalyticRequest**](../Model/\Swagger\Client\Model\AnalyticRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **analyticUpsertWithWhere**
> \Swagger\Client\Model\Analytic analyticUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AnalyticApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\Analytic(); // \Swagger\Client\Model\Analytic | An object of model property name/value pairs

try {
    $result = $api_instance->analyticUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnalyticApi->analyticUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\Analytic**](../Model/\Swagger\Client\Model\Analytic.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\Analytic**](../Model/Analytic.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

