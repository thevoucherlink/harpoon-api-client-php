# Harpoon\Api\RssFeedGroupApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedGroupCount**](RssFeedGroupApi.md#rssFeedGroupCount) | **GET** /RssFeedGroups/count | Count instances of the model matched by where from the data source.
[**rssFeedGroupCreate**](RssFeedGroupApi.md#rssFeedGroupCreate) | **POST** /RssFeedGroups | Create a new instance of the model and persist it into the data source.
[**rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**](RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream) | **GET** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**](RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream) | **POST** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupDeleteById**](RssFeedGroupApi.md#rssFeedGroupDeleteById) | **DELETE** /RssFeedGroups/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedGroupExistsGetRssFeedGroupsidExists**](RssFeedGroupApi.md#rssFeedGroupExistsGetRssFeedGroupsidExists) | **GET** /RssFeedGroups/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedGroupExistsHeadRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupExistsHeadRssFeedGroupsid) | **HEAD** /RssFeedGroups/{id} | Check whether a model instance exists in the data source.
[**rssFeedGroupFind**](RssFeedGroupApi.md#rssFeedGroupFind) | **GET** /RssFeedGroups | Find all instances of the model matched by filter from the data source.
[**rssFeedGroupFindById**](RssFeedGroupApi.md#rssFeedGroupFindById) | **GET** /RssFeedGroups/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedGroupFindOne**](RssFeedGroupApi.md#rssFeedGroupFindOne) | **GET** /RssFeedGroups/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedGroupPrototypeCountRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeCountRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/count | Counts rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeCreateRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeCreateRssFeeds) | **POST** /RssFeedGroups/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**rssFeedGroupPrototypeDeleteRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeDeleteRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**rssFeedGroupPrototypeDestroyByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeDestroyByIdRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**rssFeedGroupPrototypeFindByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeFindByIdRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**rssFeedGroupPrototypeGetRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeGetRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds | Queries rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid) | **PATCH** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid) | **PUT** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateByIdRssFeeds) | **PUT** /RssFeedGroups/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**rssFeedGroupReplaceById**](RssFeedGroupApi.md#rssFeedGroupReplaceById) | **POST** /RssFeedGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedGroupReplaceOrCreate**](RssFeedGroupApi.md#rssFeedGroupReplaceOrCreate) | **POST** /RssFeedGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpdateAll**](RssFeedGroupApi.md#rssFeedGroupUpdateAll) | **POST** /RssFeedGroups/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedGroupUpsertPatchRssFeedGroups**](RssFeedGroupApi.md#rssFeedGroupUpsertPatchRssFeedGroups) | **PATCH** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertPutRssFeedGroups**](RssFeedGroupApi.md#rssFeedGroupUpsertPutRssFeedGroups) | **PUT** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertWithWhere**](RssFeedGroupApi.md#rssFeedGroupUpsertWithWhere) | **POST** /RssFeedGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **rssFeedGroupCount**
> \Swagger\Client\Model\InlineResponse2001 rssFeedGroupCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->rssFeedGroupCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupCreate**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | Model instance data

try {
    $result = $api_instance->rssFeedGroupCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**
> \SplFileObject rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**
> \SplFileObject rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupDeleteById**
> object rssFeedGroupDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->rssFeedGroupDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupExistsGetRssFeedGroupsidExists**
> \Swagger\Client\Model\InlineResponse2003 rssFeedGroupExistsGetRssFeedGroupsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->rssFeedGroupExistsGetRssFeedGroupsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupExistsGetRssFeedGroupsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupExistsHeadRssFeedGroupsid**
> \Swagger\Client\Model\InlineResponse2003 rssFeedGroupExistsHeadRssFeedGroupsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->rssFeedGroupExistsHeadRssFeedGroupsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupExistsHeadRssFeedGroupsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupFind**
> \Swagger\Client\Model\RssFeedGroup[] rssFeedGroupFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->rssFeedGroupFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup[]**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupFindById**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->rssFeedGroupFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupFindOne**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->rssFeedGroupFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeCountRssFeeds**
> \Swagger\Client\Model\InlineResponse2001 rssFeedGroupPrototypeCountRssFeeds($id, $where)

Counts rssFeeds of RssFeedGroup.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | RssFeedGroup id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->rssFeedGroupPrototypeCountRssFeeds($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeCountRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeedGroup id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeCreateRssFeeds**
> \Swagger\Client\Model\RssFeed rssFeedGroupPrototypeCreateRssFeeds($id, $data)

Creates a new instance in rssFeeds of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | RssFeedGroup id
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | 

try {
    $result = $api_instance->rssFeedGroupPrototypeCreateRssFeeds($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeCreateRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeedGroup id |
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeDeleteRssFeeds**
> rssFeedGroupPrototypeDeleteRssFeeds($id)

Deletes all rssFeeds of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | RssFeedGroup id

try {
    $api_instance->rssFeedGroupPrototypeDeleteRssFeeds($id);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeDeleteRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeedGroup id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeDestroyByIdRssFeeds**
> rssFeedGroupPrototypeDestroyByIdRssFeeds($fk, $id)

Delete a related item by id for rssFeeds.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$fk = "fk_example"; // string | Foreign key for rssFeeds
$id = "id_example"; // string | RssFeedGroup id

try {
    $api_instance->rssFeedGroupPrototypeDestroyByIdRssFeeds($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeDestroyByIdRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeeds |
 **id** | **string**| RssFeedGroup id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeFindByIdRssFeeds**
> \Swagger\Client\Model\RssFeed rssFeedGroupPrototypeFindByIdRssFeeds($fk, $id)

Find a related item by id for rssFeeds.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$fk = "fk_example"; // string | Foreign key for rssFeeds
$id = "id_example"; // string | RssFeedGroup id

try {
    $result = $api_instance->rssFeedGroupPrototypeFindByIdRssFeeds($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeFindByIdRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeeds |
 **id** | **string**| RssFeedGroup id |

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeGetRssFeeds**
> \Swagger\Client\Model\RssFeed[] rssFeedGroupPrototypeGetRssFeeds($id, $filter)

Queries rssFeeds of RssFeedGroup.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | RssFeedGroup id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->rssFeedGroupPrototypeGetRssFeeds($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeGetRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeedGroup id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed[]**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | RssFeedGroup id
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeedGroup id |
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | RssFeedGroup id
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RssFeedGroup id |
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupPrototypeUpdateByIdRssFeeds**
> \Swagger\Client\Model\RssFeed rssFeedGroupPrototypeUpdateByIdRssFeeds($fk, $id, $data)

Update a related item by id for rssFeeds.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$fk = "fk_example"; // string | Foreign key for rssFeeds
$id = "id_example"; // string | RssFeedGroup id
$data = new \Swagger\Client\Model\RssFeed(); // \Swagger\Client\Model\RssFeed | 

try {
    $result = $api_instance->rssFeedGroupPrototypeUpdateByIdRssFeeds($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupPrototypeUpdateByIdRssFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for rssFeeds |
 **id** | **string**| RssFeedGroup id |
 **data** | [**\Swagger\Client\Model\RssFeed**](../Model/\Swagger\Client\Model\RssFeed.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RssFeed**](../Model/RssFeed.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupReplaceById**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | Model instance data

try {
    $result = $api_instance->rssFeedGroupReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupReplaceOrCreate**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | Model instance data

try {
    $result = $api_instance->rssFeedGroupReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 rssFeedGroupUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedGroupUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupUpsertPatchRssFeedGroups**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupUpsertPatchRssFeedGroups($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | Model instance data

try {
    $result = $api_instance->rssFeedGroupUpsertPatchRssFeedGroups($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupUpsertPatchRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupUpsertPutRssFeedGroups**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupUpsertPutRssFeedGroups($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | Model instance data

try {
    $result = $api_instance->rssFeedGroupUpsertPutRssFeedGroups($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupUpsertPutRssFeedGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rssFeedGroupUpsertWithWhere**
> \Swagger\Client\Model\RssFeedGroup rssFeedGroupUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RssFeedGroupApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RssFeedGroup(); // \Swagger\Client\Model\RssFeedGroup | An object of model property name/value pairs

try {
    $result = $api_instance->rssFeedGroupUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RssFeedGroupApi->rssFeedGroupUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RssFeedGroup**](../Model/\Swagger\Client\Model\RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RssFeedGroup**](../Model/RssFeedGroup.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

