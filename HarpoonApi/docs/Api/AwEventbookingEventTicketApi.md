# Harpoon\Api\AwEventbookingEventTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventTicketCount**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCount) | **GET** /AwEventbookingEventTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventTicketCreate**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreate) | **POST** /AwEventbookingEventTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream) | **GET** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream) | **POST** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketDeleteById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketDeleteById) | **DELETE** /AwEventbookingEventTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists) | **GET** /AwEventbookingEventTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid) | **HEAD** /AwEventbookingEventTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketFind**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFind) | **GET** /AwEventbookingEventTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventTicketFindById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindById) | **GET** /AwEventbookingEventTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventTicketFindOne**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindOne) | **GET** /AwEventbookingEventTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventTicketPrototypeCountAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/count | Counts attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/count | Counts awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/count | Counts tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCreateAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAttributes) | **POST** /AwEventbookingEventTickets/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAwEventbookingTicket) | **POST** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Creates a new instance in awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeCreateTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateTickets) | **POST** /AwEventbookingEventTickets/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventTicketPrototypeDeleteAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Deletes all awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeDeleteTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventTicketPrototypeDestroyByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Delete a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeDestroyByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventTicketPrototypeFindByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Find a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeFindByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventTicketPrototypeGetAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes | Queries attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Queries awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets | Queries tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid) | **PATCH** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid) | **PUT** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEventTickets/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket) | **PUT** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Update a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeUpdateByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEventTickets/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventTicketReplaceById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceById) | **POST** /AwEventbookingEventTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketReplaceOrCreate**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceOrCreate) | **POST** /AwEventbookingEventTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpdateAll**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpdateAll) | **POST** /AwEventbookingEventTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets) | **PATCH** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPutAwEventbookingEventTickets) | **PUT** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertWithWhere**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertWithWhere) | **POST** /AwEventbookingEventTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingEventTicketCount**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventTicketCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventTicketCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketCreate**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | Model instance data

try {
    $result = $api_instance->awEventbookingEventTicketCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**
> \SplFileObject awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**
> \SplFileObject awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketDeleteById**
> object awEventbookingEventTicketDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingEventTicketDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**
> \Swagger\Client\Model\InlineResponse2003 awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketFind**
> \Swagger\Client\Model\AwEventbookingEventTicket[] awEventbookingEventTicketFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingEventTicketFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket[]**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketFindById**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->awEventbookingEventTicketFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketFindOne**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awEventbookingEventTicketFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeCountAttributes**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventTicketPrototypeCountAttributes($id, $where)

Counts attributes of AwEventbookingEventTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeCountAttributes($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCountAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeCountAwEventbookingTicket**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventTicketPrototypeCountAwEventbookingTicket($id, $where)

Counts awEventbookingTicket of AwEventbookingEventTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeCountAwEventbookingTicket($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCountAwEventbookingTicket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeCountTickets**
> \Swagger\Client\Model\InlineResponse2001 awEventbookingEventTicketPrototypeCountTickets($id, $where)

Counts tickets of AwEventbookingEventTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeCountTickets($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCountTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeCreateAttributes**
> \Swagger\Client\Model\AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeCreateAttributes($id, $data)

Creates a new instance in attributes of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingEventTicketAttribute(); // \Swagger\Client\Model\AwEventbookingEventTicketAttribute | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeCreateAttributes($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCreateAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicketAttribute**](../Model/\Swagger\Client\Model\AwEventbookingEventTicketAttribute.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicketAttribute**](../Model/AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventTicketPrototypeCreateAwEventbookingTicket($id, $data)

Creates a new instance in awEventbookingTicket of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeCreateAwEventbookingTicket($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCreateAwEventbookingTicket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeCreateTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventTicketPrototypeCreateTickets($id, $data)

Creates a new instance in tickets of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeCreateTickets($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeCreateTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeDeleteAttributes**
> awEventbookingEventTicketPrototypeDeleteAttributes($id)

Deletes all attributes of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $api_instance->awEventbookingEventTicketPrototypeDeleteAttributes($id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDeleteAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**
> awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket($id)

Deletes all awEventbookingTicket of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $api_instance->awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket($id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeDeleteTickets**
> awEventbookingEventTicketPrototypeDeleteTickets($id)

Deletes all tickets of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $api_instance->awEventbookingEventTicketPrototypeDeleteTickets($id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDeleteTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeDestroyByIdAttributes**
> awEventbookingEventTicketPrototypeDestroyByIdAttributes($fk, $id)

Delete a related item by id for attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for attributes
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $api_instance->awEventbookingEventTicketPrototypeDestroyByIdAttributes($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDestroyByIdAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for attributes |
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**
> awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket($fk, $id)

Delete a related item by id for awEventbookingTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for awEventbookingTicket
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $api_instance->awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for awEventbookingTicket |
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeDestroyByIdTickets**
> awEventbookingEventTicketPrototypeDestroyByIdTickets($fk, $id)

Delete a related item by id for tickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for tickets
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $api_instance->awEventbookingEventTicketPrototypeDestroyByIdTickets($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeDestroyByIdTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for tickets |
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeFindByIdAttributes**
> \Swagger\Client\Model\AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeFindByIdAttributes($fk, $id)

Find a related item by id for attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for attributes
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeFindByIdAttributes($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeFindByIdAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for attributes |
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicketAttribute**](../Model/AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket($fk, $id)

Find a related item by id for awEventbookingTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for awEventbookingTicket
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for awEventbookingTicket |
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeFindByIdTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventTicketPrototypeFindByIdTickets($fk, $id)

Find a related item by id for tickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for tickets
$id = "id_example"; // string | AwEventbookingEventTicket id

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeFindByIdTickets($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeFindByIdTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for tickets |
 **id** | **string**| AwEventbookingEventTicket id |

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeGetAttributes**
> \Swagger\Client\Model\AwEventbookingEventTicketAttribute[] awEventbookingEventTicketPrototypeGetAttributes($id, $filter)

Queries attributes of AwEventbookingEventTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeGetAttributes($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeGetAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicketAttribute[]**](../Model/AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeGetAwEventbookingTicket**
> \Swagger\Client\Model\AwEventbookingTicket[] awEventbookingEventTicketPrototypeGetAwEventbookingTicket($id, $filter)

Queries awEventbookingTicket of AwEventbookingEventTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeGetAwEventbookingTicket($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeGetAwEventbookingTicket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket[]**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeGetTickets**
> \Swagger\Client\Model\AwEventbookingTicket[] awEventbookingEventTicketPrototypeGetTickets($id, $filter)

Queries tickets of AwEventbookingEventTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeGetTickets($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeGetTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket[]**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeUpdateByIdAttributes**
> \Swagger\Client\Model\AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeUpdateByIdAttributes($fk, $id, $data)

Update a related item by id for attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for attributes
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingEventTicketAttribute(); // \Swagger\Client\Model\AwEventbookingEventTicketAttribute | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeUpdateByIdAttributes($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateByIdAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for attributes |
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicketAttribute**](../Model/\Swagger\Client\Model\AwEventbookingEventTicketAttribute.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicketAttribute**](../Model/AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket($fk, $id, $data)

Update a related item by id for awEventbookingTicket.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for awEventbookingTicket
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for awEventbookingTicket |
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketPrototypeUpdateByIdTickets**
> \Swagger\Client\Model\AwEventbookingTicket awEventbookingEventTicketPrototypeUpdateByIdTickets($fk, $id, $data)

Update a related item by id for tickets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$fk = "fk_example"; // string | Foreign key for tickets
$id = "id_example"; // string | AwEventbookingEventTicket id
$data = new \Swagger\Client\Model\AwEventbookingTicket(); // \Swagger\Client\Model\AwEventbookingTicket | 

try {
    $result = $api_instance->awEventbookingEventTicketPrototypeUpdateByIdTickets($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketPrototypeUpdateByIdTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for tickets |
 **id** | **string**| AwEventbookingEventTicket id |
 **data** | [**\Swagger\Client\Model\AwEventbookingTicket**](../Model/\Swagger\Client\Model\AwEventbookingTicket.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingTicket**](../Model/AwEventbookingTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketReplaceById**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | Model instance data

try {
    $result = $api_instance->awEventbookingEventTicketReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketReplaceOrCreate**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | Model instance data

try {
    $result = $api_instance->awEventbookingEventTicketReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 awEventbookingEventTicketUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventTicketUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | Model instance data

try {
    $result = $api_instance->awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketUpsertPutAwEventbookingEventTickets($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | Model instance data

try {
    $result = $api_instance->awEventbookingEventTicketUpsertPutAwEventbookingEventTickets($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketUpsertPutAwEventbookingEventTickets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awEventbookingEventTicketUpsertWithWhere**
> \Swagger\Client\Model\AwEventbookingEventTicket awEventbookingEventTicketUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwEventbookingEventTicketApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwEventbookingEventTicket(); // \Swagger\Client\Model\AwEventbookingEventTicket | An object of model property name/value pairs

try {
    $result = $api_instance->awEventbookingEventTicketUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwEventbookingEventTicketApi->awEventbookingEventTicketUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/\Swagger\Client\Model\AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwEventbookingEventTicket**](../Model/AwEventbookingEventTicket.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

