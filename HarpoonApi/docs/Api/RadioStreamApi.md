# Harpoon\Api\RadioStreamApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioStreamCount**](RadioStreamApi.md#radioStreamCount) | **GET** /RadioStreams/count | Count instances of the model matched by where from the data source.
[**radioStreamCreate**](RadioStreamApi.md#radioStreamCreate) | **POST** /RadioStreams | Create a new instance of the model and persist it into the data source.
[**radioStreamCreateChangeStreamGetRadioStreamsChangeStream**](RadioStreamApi.md#radioStreamCreateChangeStreamGetRadioStreamsChangeStream) | **GET** /RadioStreams/change-stream | Create a change stream.
[**radioStreamCreateChangeStreamPostRadioStreamsChangeStream**](RadioStreamApi.md#radioStreamCreateChangeStreamPostRadioStreamsChangeStream) | **POST** /RadioStreams/change-stream | Create a change stream.
[**radioStreamDeleteById**](RadioStreamApi.md#radioStreamDeleteById) | **DELETE** /RadioStreams/{id} | Delete a model instance by {{id}} from the data source.
[**radioStreamExistsGetRadioStreamsidExists**](RadioStreamApi.md#radioStreamExistsGetRadioStreamsidExists) | **GET** /RadioStreams/{id}/exists | Check whether a model instance exists in the data source.
[**radioStreamExistsHeadRadioStreamsid**](RadioStreamApi.md#radioStreamExistsHeadRadioStreamsid) | **HEAD** /RadioStreams/{id} | Check whether a model instance exists in the data source.
[**radioStreamFind**](RadioStreamApi.md#radioStreamFind) | **GET** /RadioStreams | Find all instances of the model matched by filter from the data source.
[**radioStreamFindById**](RadioStreamApi.md#radioStreamFindById) | **GET** /RadioStreams/{id} | Find a model instance by {{id}} from the data source.
[**radioStreamFindOne**](RadioStreamApi.md#radioStreamFindOne) | **GET** /RadioStreams/findOne | Find first instance of the model matched by filter from the data source.
[**radioStreamPrototypeCountRadioShows**](RadioStreamApi.md#radioStreamPrototypeCountRadioShows) | **GET** /RadioStreams/{id}/radioShows/count | Counts radioShows of RadioStream.
[**radioStreamPrototypeCreateRadioShows**](RadioStreamApi.md#radioStreamPrototypeCreateRadioShows) | **POST** /RadioStreams/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioStreamPrototypeDeleteRadioShows**](RadioStreamApi.md#radioStreamPrototypeDeleteRadioShows) | **DELETE** /RadioStreams/{id}/radioShows | Deletes all radioShows of this model.
[**radioStreamPrototypeDestroyByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeDestroyByIdRadioShows) | **DELETE** /RadioStreams/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioStreamPrototypeFindByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeFindByIdRadioShows) | **GET** /RadioStreams/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioStreamPrototypeGetRadioShows**](RadioStreamApi.md#radioStreamPrototypeGetRadioShows) | **GET** /RadioStreams/{id}/radioShows | Queries radioShows of RadioStream.
[**radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**](RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPatchRadioStreamsid) | **PATCH** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateAttributesPutRadioStreamsid**](RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPutRadioStreamsid) | **PUT** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeUpdateByIdRadioShows) | **PUT** /RadioStreams/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioStreamRadioShowCurrent**](RadioStreamApi.md#radioStreamRadioShowCurrent) | **GET** /RadioStreams/{id}/radioShows/current | 
[**radioStreamRadioShowSchedule**](RadioStreamApi.md#radioStreamRadioShowSchedule) | **GET** /RadioStreams/{id}/radioShows/schedule | 
[**radioStreamReplaceById**](RadioStreamApi.md#radioStreamReplaceById) | **POST** /RadioStreams/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioStreamReplaceOrCreate**](RadioStreamApi.md#radioStreamReplaceOrCreate) | **POST** /RadioStreams/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioStreamUpdateAll**](RadioStreamApi.md#radioStreamUpdateAll) | **POST** /RadioStreams/update | Update instances of the model matched by {{where}} from the data source.
[**radioStreamUpsertPatchRadioStreams**](RadioStreamApi.md#radioStreamUpsertPatchRadioStreams) | **PATCH** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertPutRadioStreams**](RadioStreamApi.md#radioStreamUpsertPutRadioStreams) | **PUT** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertWithWhere**](RadioStreamApi.md#radioStreamUpsertWithWhere) | **POST** /RadioStreams/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioStreamCount**
> \Swagger\Client\Model\InlineResponse2001 radioStreamCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioStreamCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamCreate**
> \Swagger\Client\Model\RadioStream radioStreamCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | Model instance data

try {
    $result = $api_instance->radioStreamCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamCreateChangeStreamGetRadioStreamsChangeStream**
> \SplFileObject radioStreamCreateChangeStreamGetRadioStreamsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioStreamCreateChangeStreamGetRadioStreamsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamCreateChangeStreamGetRadioStreamsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamCreateChangeStreamPostRadioStreamsChangeStream**
> \SplFileObject radioStreamCreateChangeStreamPostRadioStreamsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->radioStreamCreateChangeStreamPostRadioStreamsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamCreateChangeStreamPostRadioStreamsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamDeleteById**
> object radioStreamDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioStreamDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamExistsGetRadioStreamsidExists**
> \Swagger\Client\Model\InlineResponse2003 radioStreamExistsGetRadioStreamsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioStreamExistsGetRadioStreamsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamExistsGetRadioStreamsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamExistsHeadRadioStreamsid**
> \Swagger\Client\Model\InlineResponse2003 radioStreamExistsHeadRadioStreamsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->radioStreamExistsHeadRadioStreamsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamExistsHeadRadioStreamsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamFind**
> \Swagger\Client\Model\RadioStream[] radioStreamFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioStreamFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream[]**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamFindById**
> \Swagger\Client\Model\RadioStream radioStreamFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->radioStreamFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamFindOne**
> \Swagger\Client\Model\RadioStream radioStreamFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->radioStreamFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeCountRadioShows**
> \Swagger\Client\Model\InlineResponse2001 radioStreamPrototypeCountRadioShows($id, $where)

Counts radioShows of RadioStream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | RadioStream id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->radioStreamPrototypeCountRadioShows($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeCountRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioStream id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeCreateRadioShows**
> \Swagger\Client\Model\RadioShow radioStreamPrototypeCreateRadioShows($id, $data)

Creates a new instance in radioShows of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | RadioStream id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | 

try {
    $result = $api_instance->radioStreamPrototypeCreateRadioShows($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeCreateRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioStream id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeDeleteRadioShows**
> radioStreamPrototypeDeleteRadioShows($id)

Deletes all radioShows of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | RadioStream id

try {
    $api_instance->radioStreamPrototypeDeleteRadioShows($id);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeDeleteRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioStream id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeDestroyByIdRadioShows**
> radioStreamPrototypeDestroyByIdRadioShows($fk, $id)

Delete a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioStream id

try {
    $api_instance->radioStreamPrototypeDestroyByIdRadioShows($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeDestroyByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioStream id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeFindByIdRadioShows**
> \Swagger\Client\Model\RadioShow radioStreamPrototypeFindByIdRadioShows($fk, $id)

Find a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioStream id

try {
    $result = $api_instance->radioStreamPrototypeFindByIdRadioShows($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeFindByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioStream id |

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeGetRadioShows**
> \Swagger\Client\Model\RadioShow[] radioStreamPrototypeGetRadioShows($id, $filter)

Queries radioShows of RadioStream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | RadioStream id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->radioStreamPrototypeGetRadioShows($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeGetRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioStream id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow[]**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**
> \Swagger\Client\Model\RadioStream radioStreamPrototypeUpdateAttributesPatchRadioStreamsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | RadioStream id
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | An object of model property name/value pairs

try {
    $result = $api_instance->radioStreamPrototypeUpdateAttributesPatchRadioStreamsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeUpdateAttributesPatchRadioStreamsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioStream id |
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeUpdateAttributesPutRadioStreamsid**
> \Swagger\Client\Model\RadioStream radioStreamPrototypeUpdateAttributesPutRadioStreamsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | RadioStream id
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | An object of model property name/value pairs

try {
    $result = $api_instance->radioStreamPrototypeUpdateAttributesPutRadioStreamsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeUpdateAttributesPutRadioStreamsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| RadioStream id |
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamPrototypeUpdateByIdRadioShows**
> \Swagger\Client\Model\RadioShow radioStreamPrototypeUpdateByIdRadioShows($fk, $id, $data)

Update a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | RadioStream id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | 

try {
    $result = $api_instance->radioStreamPrototypeUpdateByIdRadioShows($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamPrototypeUpdateByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| RadioStream id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamRadioShowCurrent**
> \Swagger\Client\Model\RadioShow radioStreamRadioShowCurrent($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | Model Id

try {
    $result = $api_instance->radioStreamRadioShowCurrent($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamRadioShowCurrent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model Id |

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamRadioShowSchedule**
> \Swagger\Client\Model\RadioShow[] radioStreamRadioShowSchedule($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | Model Id

try {
    $result = $api_instance->radioStreamRadioShowSchedule($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamRadioShowSchedule: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model Id |

### Return type

[**\Swagger\Client\Model\RadioShow[]**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamReplaceById**
> \Swagger\Client\Model\RadioStream radioStreamReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | Model instance data

try {
    $result = $api_instance->radioStreamReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamReplaceOrCreate**
> \Swagger\Client\Model\RadioStream radioStreamReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | Model instance data

try {
    $result = $api_instance->radioStreamReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 radioStreamUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | An object of model property name/value pairs

try {
    $result = $api_instance->radioStreamUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamUpsertPatchRadioStreams**
> \Swagger\Client\Model\RadioStream radioStreamUpsertPatchRadioStreams($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | Model instance data

try {
    $result = $api_instance->radioStreamUpsertPatchRadioStreams($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamUpsertPatchRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamUpsertPutRadioStreams**
> \Swagger\Client\Model\RadioStream radioStreamUpsertPutRadioStreams($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | Model instance data

try {
    $result = $api_instance->radioStreamUpsertPutRadioStreams($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamUpsertPutRadioStreams: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **radioStreamUpsertWithWhere**
> \Swagger\Client\Model\RadioStream radioStreamUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\RadioStreamApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\RadioStream(); // \Swagger\Client\Model\RadioStream | An object of model property name/value pairs

try {
    $result = $api_instance->radioStreamUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RadioStreamApi->radioStreamUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\RadioStream**](../Model/\Swagger\Client\Model\RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\RadioStream**](../Model/RadioStream.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

