# Harpoon\Api\PlaylistItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistItemCount**](PlaylistItemApi.md#playlistItemCount) | **GET** /PlaylistItems/count | Count instances of the model matched by where from the data source.
[**playlistItemCreate**](PlaylistItemApi.md#playlistItemCreate) | **POST** /PlaylistItems | Create a new instance of the model and persist it into the data source.
[**playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**](PlaylistItemApi.md#playlistItemCreateChangeStreamGetPlaylistItemsChangeStream) | **GET** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**](PlaylistItemApi.md#playlistItemCreateChangeStreamPostPlaylistItemsChangeStream) | **POST** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemDeleteById**](PlaylistItemApi.md#playlistItemDeleteById) | **DELETE** /PlaylistItems/{id} | Delete a model instance by {{id}} from the data source.
[**playlistItemExistsGetPlaylistItemsidExists**](PlaylistItemApi.md#playlistItemExistsGetPlaylistItemsidExists) | **GET** /PlaylistItems/{id}/exists | Check whether a model instance exists in the data source.
[**playlistItemExistsHeadPlaylistItemsid**](PlaylistItemApi.md#playlistItemExistsHeadPlaylistItemsid) | **HEAD** /PlaylistItems/{id} | Check whether a model instance exists in the data source.
[**playlistItemFind**](PlaylistItemApi.md#playlistItemFind) | **GET** /PlaylistItems | Find all instances of the model matched by filter from the data source.
[**playlistItemFindById**](PlaylistItemApi.md#playlistItemFindById) | **GET** /PlaylistItems/{id} | Find a model instance by {{id}} from the data source.
[**playlistItemFindOne**](PlaylistItemApi.md#playlistItemFindOne) | **GET** /PlaylistItems/findOne | Find first instance of the model matched by filter from the data source.
[**playlistItemPrototypeCountPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeCountPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/count | Counts playerSources of PlaylistItem.
[**playlistItemPrototypeCountPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeCountPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/count | Counts playerTracks of PlaylistItem.
[**playlistItemPrototypeCountRadioShows**](PlaylistItemApi.md#playlistItemPrototypeCountRadioShows) | **GET** /PlaylistItems/{id}/radioShows/count | Counts radioShows of PlaylistItem.
[**playlistItemPrototypeCreatePlayerSources**](PlaylistItemApi.md#playlistItemPrototypeCreatePlayerSources) | **POST** /PlaylistItems/{id}/playerSources | Creates a new instance in playerSources of this model.
[**playlistItemPrototypeCreatePlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeCreatePlayerTracks) | **POST** /PlaylistItems/{id}/playerTracks | Creates a new instance in playerTracks of this model.
[**playlistItemPrototypeCreateRadioShows**](PlaylistItemApi.md#playlistItemPrototypeCreateRadioShows) | **POST** /PlaylistItems/{id}/radioShows | Creates a new instance in radioShows of this model.
[**playlistItemPrototypeDeletePlayerSources**](PlaylistItemApi.md#playlistItemPrototypeDeletePlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources | Deletes all playerSources of this model.
[**playlistItemPrototypeDeletePlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeDeletePlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks | Deletes all playerTracks of this model.
[**playlistItemPrototypeDeleteRadioShows**](PlaylistItemApi.md#playlistItemPrototypeDeleteRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows | Deletes all radioShows of this model.
[**playlistItemPrototypeDestroyByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources/{fk} | Delete a related item by id for playerSources.
[**playlistItemPrototypeDestroyByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks/{fk} | Delete a related item by id for playerTracks.
[**playlistItemPrototypeDestroyByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**playlistItemPrototypeFindByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/{fk} | Find a related item by id for playerSources.
[**playlistItemPrototypeFindByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/{fk} | Find a related item by id for playerTracks.
[**playlistItemPrototypeFindByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeFindByIdRadioShows) | **GET** /PlaylistItems/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**playlistItemPrototypeGetPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeGetPlayerSources) | **GET** /PlaylistItems/{id}/playerSources | Queries playerSources of PlaylistItem.
[**playlistItemPrototypeGetPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeGetPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks | Queries playerTracks of PlaylistItem.
[**playlistItemPrototypeGetPlaylist**](PlaylistItemApi.md#playlistItemPrototypeGetPlaylist) | **GET** /PlaylistItems/{id}/playlist | Fetches belongsTo relation playlist.
[**playlistItemPrototypeGetRadioShows**](PlaylistItemApi.md#playlistItemPrototypeGetRadioShows) | **GET** /PlaylistItems/{id}/radioShows | Queries radioShows of PlaylistItem.
[**playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**](PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid) | **PATCH** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**](PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPutPlaylistItemsid) | **PUT** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerSources) | **PUT** /PlaylistItems/{id}/playerSources/{fk} | Update a related item by id for playerSources.
[**playlistItemPrototypeUpdateByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerTracks) | **PUT** /PlaylistItems/{id}/playerTracks/{fk} | Update a related item by id for playerTracks.
[**playlistItemPrototypeUpdateByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdRadioShows) | **PUT** /PlaylistItems/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**playlistItemReplaceById**](PlaylistItemApi.md#playlistItemReplaceById) | **POST** /PlaylistItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistItemReplaceOrCreate**](PlaylistItemApi.md#playlistItemReplaceOrCreate) | **POST** /PlaylistItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistItemUpdateAll**](PlaylistItemApi.md#playlistItemUpdateAll) | **POST** /PlaylistItems/update | Update instances of the model matched by {{where}} from the data source.
[**playlistItemUpsertPatchPlaylistItems**](PlaylistItemApi.md#playlistItemUpsertPatchPlaylistItems) | **PATCH** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertPutPlaylistItems**](PlaylistItemApi.md#playlistItemUpsertPutPlaylistItems) | **PUT** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertWithWhere**](PlaylistItemApi.md#playlistItemUpsertWithWhere) | **POST** /PlaylistItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playlistItemCount**
> \Swagger\Client\Model\InlineResponse2001 playlistItemCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playlistItemCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemCreate**
> \Swagger\Client\Model\PlaylistItem playlistItemCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | Model instance data

try {
    $result = $api_instance->playlistItemCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**
> \SplFileObject playlistItemCreateChangeStreamGetPlaylistItemsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playlistItemCreateChangeStreamGetPlaylistItemsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemCreateChangeStreamGetPlaylistItemsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**
> \SplFileObject playlistItemCreateChangeStreamPostPlaylistItemsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playlistItemCreateChangeStreamPostPlaylistItemsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemCreateChangeStreamPostPlaylistItemsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemDeleteById**
> object playlistItemDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playlistItemDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemExistsGetPlaylistItemsidExists**
> \Swagger\Client\Model\InlineResponse2003 playlistItemExistsGetPlaylistItemsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playlistItemExistsGetPlaylistItemsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemExistsGetPlaylistItemsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemExistsHeadPlaylistItemsid**
> \Swagger\Client\Model\InlineResponse2003 playlistItemExistsHeadPlaylistItemsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playlistItemExistsHeadPlaylistItemsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemExistsHeadPlaylistItemsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemFind**
> \Swagger\Client\Model\PlaylistItem[] playlistItemFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playlistItemFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem[]**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemFindById**
> \Swagger\Client\Model\PlaylistItem playlistItemFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->playlistItemFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemFindOne**
> \Swagger\Client\Model\PlaylistItem playlistItemFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playlistItemFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeCountPlayerSources**
> \Swagger\Client\Model\InlineResponse2001 playlistItemPrototypeCountPlayerSources($id, $where)

Counts playerSources of PlaylistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playlistItemPrototypeCountPlayerSources($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeCountPlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeCountPlayerTracks**
> \Swagger\Client\Model\InlineResponse2001 playlistItemPrototypeCountPlayerTracks($id, $where)

Counts playerTracks of PlaylistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playlistItemPrototypeCountPlayerTracks($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeCountPlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeCountRadioShows**
> \Swagger\Client\Model\InlineResponse2001 playlistItemPrototypeCountRadioShows($id, $where)

Counts radioShows of PlaylistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playlistItemPrototypeCountRadioShows($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeCountRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeCreatePlayerSources**
> \Swagger\Client\Model\PlayerSource playlistItemPrototypeCreatePlayerSources($id, $data)

Creates a new instance in playerSources of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | 

try {
    $result = $api_instance->playlistItemPrototypeCreatePlayerSources($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeCreatePlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeCreatePlayerTracks**
> \Swagger\Client\Model\PlayerTrack playlistItemPrototypeCreatePlayerTracks($id, $data)

Creates a new instance in playerTracks of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | 

try {
    $result = $api_instance->playlistItemPrototypeCreatePlayerTracks($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeCreatePlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeCreateRadioShows**
> \Swagger\Client\Model\RadioShow playlistItemPrototypeCreateRadioShows($id, $data)

Creates a new instance in radioShows of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | 

try {
    $result = $api_instance->playlistItemPrototypeCreateRadioShows($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeCreateRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeDeletePlayerSources**
> playlistItemPrototypeDeletePlayerSources($id)

Deletes all playerSources of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id

try {
    $api_instance->playlistItemPrototypeDeletePlayerSources($id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeDeletePlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeDeletePlayerTracks**
> playlistItemPrototypeDeletePlayerTracks($id)

Deletes all playerTracks of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id

try {
    $api_instance->playlistItemPrototypeDeletePlayerTracks($id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeDeletePlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeDeleteRadioShows**
> playlistItemPrototypeDeleteRadioShows($id)

Deletes all radioShows of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id

try {
    $api_instance->playlistItemPrototypeDeleteRadioShows($id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeDeleteRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeDestroyByIdPlayerSources**
> playlistItemPrototypeDestroyByIdPlayerSources($fk, $id)

Delete a related item by id for playerSources.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for playerSources
$id = "id_example"; // string | PlaylistItem id

try {
    $api_instance->playlistItemPrototypeDestroyByIdPlayerSources($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeDestroyByIdPlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playerSources |
 **id** | **string**| PlaylistItem id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeDestroyByIdPlayerTracks**
> playlistItemPrototypeDestroyByIdPlayerTracks($fk, $id)

Delete a related item by id for playerTracks.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for playerTracks
$id = "id_example"; // string | PlaylistItem id

try {
    $api_instance->playlistItemPrototypeDestroyByIdPlayerTracks($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeDestroyByIdPlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playerTracks |
 **id** | **string**| PlaylistItem id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeDestroyByIdRadioShows**
> playlistItemPrototypeDestroyByIdRadioShows($fk, $id)

Delete a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | PlaylistItem id

try {
    $api_instance->playlistItemPrototypeDestroyByIdRadioShows($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeDestroyByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| PlaylistItem id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeFindByIdPlayerSources**
> \Swagger\Client\Model\PlayerSource playlistItemPrototypeFindByIdPlayerSources($fk, $id)

Find a related item by id for playerSources.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for playerSources
$id = "id_example"; // string | PlaylistItem id

try {
    $result = $api_instance->playlistItemPrototypeFindByIdPlayerSources($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeFindByIdPlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playerSources |
 **id** | **string**| PlaylistItem id |

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeFindByIdPlayerTracks**
> \Swagger\Client\Model\PlayerTrack playlistItemPrototypeFindByIdPlayerTracks($fk, $id)

Find a related item by id for playerTracks.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for playerTracks
$id = "id_example"; // string | PlaylistItem id

try {
    $result = $api_instance->playlistItemPrototypeFindByIdPlayerTracks($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeFindByIdPlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playerTracks |
 **id** | **string**| PlaylistItem id |

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeFindByIdRadioShows**
> \Swagger\Client\Model\RadioShow playlistItemPrototypeFindByIdRadioShows($fk, $id)

Find a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | PlaylistItem id

try {
    $result = $api_instance->playlistItemPrototypeFindByIdRadioShows($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeFindByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| PlaylistItem id |

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeGetPlayerSources**
> \Swagger\Client\Model\PlayerSource[] playlistItemPrototypeGetPlayerSources($id, $filter)

Queries playerSources of PlaylistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->playlistItemPrototypeGetPlayerSources($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeGetPlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource[]**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeGetPlayerTracks**
> \Swagger\Client\Model\PlayerTrack[] playlistItemPrototypeGetPlayerTracks($id, $filter)

Queries playerTracks of PlaylistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->playlistItemPrototypeGetPlayerTracks($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeGetPlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack[]**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeGetPlaylist**
> \Swagger\Client\Model\Playlist playlistItemPrototypeGetPlaylist($id, $refresh)

Fetches belongsTo relation playlist.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$refresh = true; // bool | 

try {
    $result = $api_instance->playlistItemPrototypeGetPlaylist($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeGetPlaylist: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeGetRadioShows**
> \Swagger\Client\Model\RadioShow[] playlistItemPrototypeGetRadioShows($id, $filter)

Queries radioShows of PlaylistItem.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->playlistItemPrototypeGetRadioShows($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeGetRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow[]**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**
> \Swagger\Client\Model\PlaylistItem playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | An object of model property name/value pairs

try {
    $result = $api_instance->playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**
> \Swagger\Client\Model\PlaylistItem playlistItemPrototypeUpdateAttributesPutPlaylistItemsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | An object of model property name/value pairs

try {
    $result = $api_instance->playlistItemPrototypeUpdateAttributesPutPlaylistItemsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeUpdateAttributesPutPlaylistItemsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeUpdateByIdPlayerSources**
> \Swagger\Client\Model\PlayerSource playlistItemPrototypeUpdateByIdPlayerSources($fk, $id, $data)

Update a related item by id for playerSources.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for playerSources
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\PlayerSource(); // \Swagger\Client\Model\PlayerSource | 

try {
    $result = $api_instance->playlistItemPrototypeUpdateByIdPlayerSources($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeUpdateByIdPlayerSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playerSources |
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\PlayerSource**](../Model/\Swagger\Client\Model\PlayerSource.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerSource**](../Model/PlayerSource.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeUpdateByIdPlayerTracks**
> \Swagger\Client\Model\PlayerTrack playlistItemPrototypeUpdateByIdPlayerTracks($fk, $id, $data)

Update a related item by id for playerTracks.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for playerTracks
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\PlayerTrack(); // \Swagger\Client\Model\PlayerTrack | 

try {
    $result = $api_instance->playlistItemPrototypeUpdateByIdPlayerTracks($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeUpdateByIdPlayerTracks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playerTracks |
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\PlayerTrack**](../Model/\Swagger\Client\Model\PlayerTrack.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerTrack**](../Model/PlayerTrack.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemPrototypeUpdateByIdRadioShows**
> \Swagger\Client\Model\RadioShow playlistItemPrototypeUpdateByIdRadioShows($fk, $id, $data)

Update a related item by id for radioShows.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$fk = "fk_example"; // string | Foreign key for radioShows
$id = "id_example"; // string | PlaylistItem id
$data = new \Swagger\Client\Model\RadioShow(); // \Swagger\Client\Model\RadioShow | 

try {
    $result = $api_instance->playlistItemPrototypeUpdateByIdRadioShows($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemPrototypeUpdateByIdRadioShows: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for radioShows |
 **id** | **string**| PlaylistItem id |
 **data** | [**\Swagger\Client\Model\RadioShow**](../Model/\Swagger\Client\Model\RadioShow.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\RadioShow**](../Model/RadioShow.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemReplaceById**
> \Swagger\Client\Model\PlaylistItem playlistItemReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | Model instance data

try {
    $result = $api_instance->playlistItemReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemReplaceOrCreate**
> \Swagger\Client\Model\PlaylistItem playlistItemReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | Model instance data

try {
    $result = $api_instance->playlistItemReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 playlistItemUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | An object of model property name/value pairs

try {
    $result = $api_instance->playlistItemUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemUpsertPatchPlaylistItems**
> \Swagger\Client\Model\PlaylistItem playlistItemUpsertPatchPlaylistItems($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | Model instance data

try {
    $result = $api_instance->playlistItemUpsertPatchPlaylistItems($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemUpsertPatchPlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemUpsertPutPlaylistItems**
> \Swagger\Client\Model\PlaylistItem playlistItemUpsertPutPlaylistItems($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | Model instance data

try {
    $result = $api_instance->playlistItemUpsertPutPlaylistItems($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemUpsertPutPlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistItemUpsertWithWhere**
> \Swagger\Client\Model\PlaylistItem playlistItemUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistItemApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | An object of model property name/value pairs

try {
    $result = $api_instance->playlistItemUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistItemApi->playlistItemUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

