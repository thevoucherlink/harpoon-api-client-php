# Harpoon\Api\DealPaidApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealPaidCheckout**](DealPaidApi.md#dealPaidCheckout) | **POST** /DealPaids/{id}/checkout | 
[**dealPaidFind**](DealPaidApi.md#dealPaidFind) | **GET** /DealPaids | Find all instances of the model matched by filter from the data source.
[**dealPaidFindById**](DealPaidApi.md#dealPaidFindById) | **GET** /DealPaids/{id} | Find a model instance by {{id}} from the data source.
[**dealPaidFindOne**](DealPaidApi.md#dealPaidFindOne) | **GET** /DealPaids/findOne | Find first instance of the model matched by filter from the data source.
[**dealPaidRedeem**](DealPaidApi.md#dealPaidRedeem) | **POST** /DealPaids/{id}/redeem | 
[**dealPaidReplaceById**](DealPaidApi.md#dealPaidReplaceById) | **POST** /DealPaids/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealPaidReplaceOrCreate**](DealPaidApi.md#dealPaidReplaceOrCreate) | **POST** /DealPaids/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealPaidUpsertWithWhere**](DealPaidApi.md#dealPaidUpsertWithWhere) | **POST** /DealPaids/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**dealPaidVenues**](DealPaidApi.md#dealPaidVenues) | **GET** /DealPaids/{id}/venues | 


# **dealPaidCheckout**
> \Swagger\Client\Model\DealPaidCheckout dealPaidCheckout($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\DealPaidCheckoutData(); // \Swagger\Client\Model\DealPaidCheckoutData | 

try {
    $result = $api_instance->dealPaidCheckout($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidCheckout: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\DealPaidCheckoutData**](../Model/\Swagger\Client\Model\DealPaidCheckoutData.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\DealPaidCheckout**](../Model/DealPaidCheckout.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidFind**
> \Swagger\Client\Model\DealPaid[] dealPaidFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->dealPaidFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\DealPaid[]**](../Model/DealPaid.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidFindById**
> \Swagger\Client\Model\DealPaid dealPaidFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->dealPaidFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\DealPaid**](../Model/DealPaid.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidFindOne**
> \Swagger\Client\Model\DealPaid dealPaidFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->dealPaidFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\DealPaid**](../Model/DealPaid.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidRedeem**
> \Swagger\Client\Model\DealPaidPurchase dealPaidRedeem($id, $data)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\DealPaidRedeemData(); // \Swagger\Client\Model\DealPaidRedeemData | 

try {
    $result = $api_instance->dealPaidRedeem($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidRedeem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\DealPaidRedeemData**](../Model/\Swagger\Client\Model\DealPaidRedeemData.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\DealPaidPurchase**](../Model/DealPaidPurchase.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidReplaceById**
> \Swagger\Client\Model\DealPaid dealPaidReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\DealPaid(); // \Swagger\Client\Model\DealPaid | Model instance data

try {
    $result = $api_instance->dealPaidReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\DealPaid**](../Model/\Swagger\Client\Model\DealPaid.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\DealPaid**](../Model/DealPaid.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidReplaceOrCreate**
> \Swagger\Client\Model\DealPaid dealPaidReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$data = new \Swagger\Client\Model\DealPaid(); // \Swagger\Client\Model\DealPaid | Model instance data

try {
    $result = $api_instance->dealPaidReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\DealPaid**](../Model/\Swagger\Client\Model\DealPaid.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\DealPaid**](../Model/DealPaid.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidUpsertWithWhere**
> \Swagger\Client\Model\DealPaid dealPaidUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\DealPaid(); // \Swagger\Client\Model\DealPaid | An object of model property name/value pairs

try {
    $result = $api_instance->dealPaidUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\DealPaid**](../Model/\Swagger\Client\Model\DealPaid.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\DealPaid**](../Model/DealPaid.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dealPaidVenues**
> \Swagger\Client\Model\Venue[] dealPaidVenues($id, $filter)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\DealPaidApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->dealPaidVenues($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealPaidApi->dealPaidVenues: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\Venue[]**](../Model/Venue.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

