# Harpoon\Api\CatalogCategoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogCategoryCount**](CatalogCategoryApi.md#catalogCategoryCount) | **GET** /CatalogCategories/count | Count instances of the model matched by where from the data source.
[**catalogCategoryCreate**](CatalogCategoryApi.md#catalogCategoryCreate) | **POST** /CatalogCategories | Create a new instance of the model and persist it into the data source.
[**catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**](CatalogCategoryApi.md#catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream) | **GET** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**](CatalogCategoryApi.md#catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream) | **POST** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryDeleteById**](CatalogCategoryApi.md#catalogCategoryDeleteById) | **DELETE** /CatalogCategories/{id} | Delete a model instance by {{id}} from the data source.
[**catalogCategoryExistsGetCatalogCategoriesidExists**](CatalogCategoryApi.md#catalogCategoryExistsGetCatalogCategoriesidExists) | **GET** /CatalogCategories/{id}/exists | Check whether a model instance exists in the data source.
[**catalogCategoryExistsHeadCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryExistsHeadCatalogCategoriesid) | **HEAD** /CatalogCategories/{id} | Check whether a model instance exists in the data source.
[**catalogCategoryFind**](CatalogCategoryApi.md#catalogCategoryFind) | **GET** /CatalogCategories | Find all instances of the model matched by filter from the data source.
[**catalogCategoryFindById**](CatalogCategoryApi.md#catalogCategoryFindById) | **GET** /CatalogCategories/{id} | Find a model instance by {{id}} from the data source.
[**catalogCategoryFindOne**](CatalogCategoryApi.md#catalogCategoryFindOne) | **GET** /CatalogCategories/findOne | Find first instance of the model matched by filter from the data source.
[**catalogCategoryPrototypeCountCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeCountCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/count | Counts catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeCreateCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeCreateCatalogProducts) | **POST** /CatalogCategories/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**catalogCategoryPrototypeDeleteCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeDeleteCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**catalogCategoryPrototypeDestroyByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeDestroyByIdCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**catalogCategoryPrototypeExistsCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeExistsCatalogProducts) | **HEAD** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**catalogCategoryPrototypeFindByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeFindByIdCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**catalogCategoryPrototypeGetCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeGetCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts | Queries catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeLinkCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeLinkCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**catalogCategoryPrototypeUnlinkCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeUnlinkCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid) | **PATCH** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid) | **PUT** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateByIdCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**catalogCategoryReplaceById**](CatalogCategoryApi.md#catalogCategoryReplaceById) | **POST** /CatalogCategories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogCategoryReplaceOrCreate**](CatalogCategoryApi.md#catalogCategoryReplaceOrCreate) | **POST** /CatalogCategories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogCategoryUpdateAll**](CatalogCategoryApi.md#catalogCategoryUpdateAll) | **POST** /CatalogCategories/update | Update instances of the model matched by {{where}} from the data source.
[**catalogCategoryUpsertPatchCatalogCategories**](CatalogCategoryApi.md#catalogCategoryUpsertPatchCatalogCategories) | **PATCH** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertPutCatalogCategories**](CatalogCategoryApi.md#catalogCategoryUpsertPutCatalogCategories) | **PUT** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertWithWhere**](CatalogCategoryApi.md#catalogCategoryUpsertWithWhere) | **POST** /CatalogCategories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **catalogCategoryCount**
> \Swagger\Client\Model\InlineResponse2001 catalogCategoryCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->catalogCategoryCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryCreate**
> \Swagger\Client\Model\CatalogCategory catalogCategoryCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | Model instance data

try {
    $result = $api_instance->catalogCategoryCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**
> \SplFileObject catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**
> \SplFileObject catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryDeleteById**
> object catalogCategoryDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->catalogCategoryDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryExistsGetCatalogCategoriesidExists**
> \Swagger\Client\Model\InlineResponse2003 catalogCategoryExistsGetCatalogCategoriesidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->catalogCategoryExistsGetCatalogCategoriesidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryExistsGetCatalogCategoriesidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryExistsHeadCatalogCategoriesid**
> \Swagger\Client\Model\InlineResponse2003 catalogCategoryExistsHeadCatalogCategoriesid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->catalogCategoryExistsHeadCatalogCategoriesid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryExistsHeadCatalogCategoriesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryFind**
> \Swagger\Client\Model\CatalogCategory[] catalogCategoryFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->catalogCategoryFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory[]**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryFindById**
> \Swagger\Client\Model\CatalogCategory catalogCategoryFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->catalogCategoryFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryFindOne**
> \Swagger\Client\Model\CatalogCategory catalogCategoryFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->catalogCategoryFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeCountCatalogProducts**
> \Swagger\Client\Model\InlineResponse2001 catalogCategoryPrototypeCountCatalogProducts($id, $where)

Counts catalogProducts of CatalogCategory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | CatalogCategory id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->catalogCategoryPrototypeCountCatalogProducts($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeCountCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogCategory id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeCreateCatalogProducts**
> \Swagger\Client\Model\CatalogProduct catalogCategoryPrototypeCreateCatalogProducts($id, $data)

Creates a new instance in catalogProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | CatalogCategory id
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | 

try {
    $result = $api_instance->catalogCategoryPrototypeCreateCatalogProducts($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeCreateCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogCategory id |
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeDeleteCatalogProducts**
> catalogCategoryPrototypeDeleteCatalogProducts($id)

Deletes all catalogProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | CatalogCategory id

try {
    $api_instance->catalogCategoryPrototypeDeleteCatalogProducts($id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeDeleteCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogCategory id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeDestroyByIdCatalogProducts**
> catalogCategoryPrototypeDestroyByIdCatalogProducts($fk, $id)

Delete a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | CatalogCategory id

try {
    $api_instance->catalogCategoryPrototypeDestroyByIdCatalogProducts($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeDestroyByIdCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| CatalogCategory id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeExistsCatalogProducts**
> bool catalogCategoryPrototypeExistsCatalogProducts($fk, $id)

Check the existence of catalogProducts relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | CatalogCategory id

try {
    $result = $api_instance->catalogCategoryPrototypeExistsCatalogProducts($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeExistsCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| CatalogCategory id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeFindByIdCatalogProducts**
> \Swagger\Client\Model\CatalogProduct catalogCategoryPrototypeFindByIdCatalogProducts($fk, $id)

Find a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | CatalogCategory id

try {
    $result = $api_instance->catalogCategoryPrototypeFindByIdCatalogProducts($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeFindByIdCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| CatalogCategory id |

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeGetCatalogProducts**
> \Swagger\Client\Model\CatalogProduct[] catalogCategoryPrototypeGetCatalogProducts($id, $filter)

Queries catalogProducts of CatalogCategory.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | CatalogCategory id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->catalogCategoryPrototypeGetCatalogProducts($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeGetCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogCategory id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct[]**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeLinkCatalogProducts**
> \Swagger\Client\Model\CatalogCategoryProduct catalogCategoryPrototypeLinkCatalogProducts($fk, $id, $data)

Add a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | CatalogCategory id
$data = new \Swagger\Client\Model\CatalogCategoryProduct(); // \Swagger\Client\Model\CatalogCategoryProduct | 

try {
    $result = $api_instance->catalogCategoryPrototypeLinkCatalogProducts($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeLinkCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| CatalogCategory id |
 **data** | [**\Swagger\Client\Model\CatalogCategoryProduct**](../Model/\Swagger\Client\Model\CatalogCategoryProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategoryProduct**](../Model/CatalogCategoryProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeUnlinkCatalogProducts**
> catalogCategoryPrototypeUnlinkCatalogProducts($fk, $id)

Remove the catalogProducts relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | CatalogCategory id

try {
    $api_instance->catalogCategoryPrototypeUnlinkCatalogProducts($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeUnlinkCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| CatalogCategory id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**
> \Swagger\Client\Model\CatalogCategory catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | CatalogCategory id
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | An object of model property name/value pairs

try {
    $result = $api_instance->catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogCategory id |
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**
> \Swagger\Client\Model\CatalogCategory catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | CatalogCategory id
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | An object of model property name/value pairs

try {
    $result = $api_instance->catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| CatalogCategory id |
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryPrototypeUpdateByIdCatalogProducts**
> \Swagger\Client\Model\CatalogProduct catalogCategoryPrototypeUpdateByIdCatalogProducts($fk, $id, $data)

Update a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | CatalogCategory id
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | 

try {
    $result = $api_instance->catalogCategoryPrototypeUpdateByIdCatalogProducts($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryPrototypeUpdateByIdCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| CatalogCategory id |
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryReplaceById**
> \Swagger\Client\Model\CatalogCategory catalogCategoryReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | Model instance data

try {
    $result = $api_instance->catalogCategoryReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryReplaceOrCreate**
> \Swagger\Client\Model\CatalogCategory catalogCategoryReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | Model instance data

try {
    $result = $api_instance->catalogCategoryReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 catalogCategoryUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | An object of model property name/value pairs

try {
    $result = $api_instance->catalogCategoryUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryUpsertPatchCatalogCategories**
> \Swagger\Client\Model\CatalogCategory catalogCategoryUpsertPatchCatalogCategories($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | Model instance data

try {
    $result = $api_instance->catalogCategoryUpsertPatchCatalogCategories($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryUpsertPatchCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryUpsertPutCatalogCategories**
> \Swagger\Client\Model\CatalogCategory catalogCategoryUpsertPutCatalogCategories($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | Model instance data

try {
    $result = $api_instance->catalogCategoryUpsertPutCatalogCategories($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryUpsertPutCatalogCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **catalogCategoryUpsertWithWhere**
> \Swagger\Client\Model\CatalogCategory catalogCategoryUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\CatalogCategoryApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\CatalogCategory(); // \Swagger\Client\Model\CatalogCategory | An object of model property name/value pairs

try {
    $result = $api_instance->catalogCategoryUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CatalogCategoryApi->catalogCategoryUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\CatalogCategory**](../Model/\Swagger\Client\Model\CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\CatalogCategory**](../Model/CatalogCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

