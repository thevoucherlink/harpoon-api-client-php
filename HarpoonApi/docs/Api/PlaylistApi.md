# Harpoon\Api\PlaylistApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistCount**](PlaylistApi.md#playlistCount) | **GET** /Playlists/count | Count instances of the model matched by where from the data source.
[**playlistCreate**](PlaylistApi.md#playlistCreate) | **POST** /Playlists | Create a new instance of the model and persist it into the data source.
[**playlistCreateChangeStreamGetPlaylistsChangeStream**](PlaylistApi.md#playlistCreateChangeStreamGetPlaylistsChangeStream) | **GET** /Playlists/change-stream | Create a change stream.
[**playlistCreateChangeStreamPostPlaylistsChangeStream**](PlaylistApi.md#playlistCreateChangeStreamPostPlaylistsChangeStream) | **POST** /Playlists/change-stream | Create a change stream.
[**playlistDeleteById**](PlaylistApi.md#playlistDeleteById) | **DELETE** /Playlists/{id} | Delete a model instance by {{id}} from the data source.
[**playlistExistsGetPlaylistsidExists**](PlaylistApi.md#playlistExistsGetPlaylistsidExists) | **GET** /Playlists/{id}/exists | Check whether a model instance exists in the data source.
[**playlistExistsHeadPlaylistsid**](PlaylistApi.md#playlistExistsHeadPlaylistsid) | **HEAD** /Playlists/{id} | Check whether a model instance exists in the data source.
[**playlistFind**](PlaylistApi.md#playlistFind) | **GET** /Playlists | Find all instances of the model matched by filter from the data source.
[**playlistFindById**](PlaylistApi.md#playlistFindById) | **GET** /Playlists/{id} | Find a model instance by {{id}} from the data source.
[**playlistFindOne**](PlaylistApi.md#playlistFindOne) | **GET** /Playlists/findOne | Find first instance of the model matched by filter from the data source.
[**playlistPrototypeCountPlaylistItems**](PlaylistApi.md#playlistPrototypeCountPlaylistItems) | **GET** /Playlists/{id}/playlistItems/count | Counts playlistItems of Playlist.
[**playlistPrototypeCreatePlaylistItems**](PlaylistApi.md#playlistPrototypeCreatePlaylistItems) | **POST** /Playlists/{id}/playlistItems | Creates a new instance in playlistItems of this model.
[**playlistPrototypeDeletePlaylistItems**](PlaylistApi.md#playlistPrototypeDeletePlaylistItems) | **DELETE** /Playlists/{id}/playlistItems | Deletes all playlistItems of this model.
[**playlistPrototypeDestroyByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeDestroyByIdPlaylistItems) | **DELETE** /Playlists/{id}/playlistItems/{fk} | Delete a related item by id for playlistItems.
[**playlistPrototypeFindByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeFindByIdPlaylistItems) | **GET** /Playlists/{id}/playlistItems/{fk} | Find a related item by id for playlistItems.
[**playlistPrototypeGetPlaylistItems**](PlaylistApi.md#playlistPrototypeGetPlaylistItems) | **GET** /Playlists/{id}/playlistItems | Queries playlistItems of Playlist.
[**playlistPrototypeUpdateAttributesPatchPlaylistsid**](PlaylistApi.md#playlistPrototypeUpdateAttributesPatchPlaylistsid) | **PATCH** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateAttributesPutPlaylistsid**](PlaylistApi.md#playlistPrototypeUpdateAttributesPutPlaylistsid) | **PUT** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeUpdateByIdPlaylistItems) | **PUT** /Playlists/{id}/playlistItems/{fk} | Update a related item by id for playlistItems.
[**playlistReplaceById**](PlaylistApi.md#playlistReplaceById) | **POST** /Playlists/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistReplaceOrCreate**](PlaylistApi.md#playlistReplaceOrCreate) | **POST** /Playlists/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistUpdateAll**](PlaylistApi.md#playlistUpdateAll) | **POST** /Playlists/update | Update instances of the model matched by {{where}} from the data source.
[**playlistUpsertPatchPlaylists**](PlaylistApi.md#playlistUpsertPatchPlaylists) | **PATCH** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertPutPlaylists**](PlaylistApi.md#playlistUpsertPutPlaylists) | **PUT** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertWithWhere**](PlaylistApi.md#playlistUpsertWithWhere) | **POST** /Playlists/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playlistCount**
> \Swagger\Client\Model\InlineResponse2001 playlistCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playlistCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistCreate**
> \Swagger\Client\Model\Playlist playlistCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | Model instance data

try {
    $result = $api_instance->playlistCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistCreateChangeStreamGetPlaylistsChangeStream**
> \SplFileObject playlistCreateChangeStreamGetPlaylistsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playlistCreateChangeStreamGetPlaylistsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistCreateChangeStreamGetPlaylistsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistCreateChangeStreamPostPlaylistsChangeStream**
> \SplFileObject playlistCreateChangeStreamPostPlaylistsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->playlistCreateChangeStreamPostPlaylistsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistCreateChangeStreamPostPlaylistsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistDeleteById**
> object playlistDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playlistDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistExistsGetPlaylistsidExists**
> \Swagger\Client\Model\InlineResponse2003 playlistExistsGetPlaylistsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playlistExistsGetPlaylistsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistExistsGetPlaylistsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistExistsHeadPlaylistsid**
> \Swagger\Client\Model\InlineResponse2003 playlistExistsHeadPlaylistsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->playlistExistsHeadPlaylistsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistExistsHeadPlaylistsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistFind**
> \Swagger\Client\Model\Playlist[] playlistFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playlistFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\Playlist[]**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistFindById**
> \Swagger\Client\Model\Playlist playlistFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->playlistFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistFindOne**
> \Swagger\Client\Model\Playlist playlistFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->playlistFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeCountPlaylistItems**
> \Swagger\Client\Model\InlineResponse2001 playlistPrototypeCountPlaylistItems($id, $where)

Counts playlistItems of Playlist.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Playlist id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->playlistPrototypeCountPlaylistItems($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeCountPlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Playlist id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeCreatePlaylistItems**
> \Swagger\Client\Model\PlaylistItem playlistPrototypeCreatePlaylistItems($id, $data)

Creates a new instance in playlistItems of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Playlist id
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | 

try {
    $result = $api_instance->playlistPrototypeCreatePlaylistItems($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeCreatePlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Playlist id |
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeDeletePlaylistItems**
> playlistPrototypeDeletePlaylistItems($id)

Deletes all playlistItems of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Playlist id

try {
    $api_instance->playlistPrototypeDeletePlaylistItems($id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeDeletePlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Playlist id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeDestroyByIdPlaylistItems**
> playlistPrototypeDestroyByIdPlaylistItems($fk, $id)

Delete a related item by id for playlistItems.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$fk = "fk_example"; // string | Foreign key for playlistItems
$id = "id_example"; // string | Playlist id

try {
    $api_instance->playlistPrototypeDestroyByIdPlaylistItems($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeDestroyByIdPlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playlistItems |
 **id** | **string**| Playlist id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeFindByIdPlaylistItems**
> \Swagger\Client\Model\PlaylistItem playlistPrototypeFindByIdPlaylistItems($fk, $id)

Find a related item by id for playlistItems.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$fk = "fk_example"; // string | Foreign key for playlistItems
$id = "id_example"; // string | Playlist id

try {
    $result = $api_instance->playlistPrototypeFindByIdPlaylistItems($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeFindByIdPlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playlistItems |
 **id** | **string**| Playlist id |

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeGetPlaylistItems**
> \Swagger\Client\Model\PlaylistItem[] playlistPrototypeGetPlaylistItems($id, $filter)

Queries playlistItems of Playlist.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Playlist id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->playlistPrototypeGetPlaylistItems($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeGetPlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Playlist id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem[]**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeUpdateAttributesPatchPlaylistsid**
> \Swagger\Client\Model\Playlist playlistPrototypeUpdateAttributesPatchPlaylistsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Playlist id
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | An object of model property name/value pairs

try {
    $result = $api_instance->playlistPrototypeUpdateAttributesPatchPlaylistsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeUpdateAttributesPatchPlaylistsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Playlist id |
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeUpdateAttributesPutPlaylistsid**
> \Swagger\Client\Model\Playlist playlistPrototypeUpdateAttributesPutPlaylistsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Playlist id
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | An object of model property name/value pairs

try {
    $result = $api_instance->playlistPrototypeUpdateAttributesPutPlaylistsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeUpdateAttributesPutPlaylistsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Playlist id |
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistPrototypeUpdateByIdPlaylistItems**
> \Swagger\Client\Model\PlaylistItem playlistPrototypeUpdateByIdPlaylistItems($fk, $id, $data)

Update a related item by id for playlistItems.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$fk = "fk_example"; // string | Foreign key for playlistItems
$id = "id_example"; // string | Playlist id
$data = new \Swagger\Client\Model\PlaylistItem(); // \Swagger\Client\Model\PlaylistItem | 

try {
    $result = $api_instance->playlistPrototypeUpdateByIdPlaylistItems($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistPrototypeUpdateByIdPlaylistItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for playlistItems |
 **id** | **string**| Playlist id |
 **data** | [**\Swagger\Client\Model\PlaylistItem**](../Model/\Swagger\Client\Model\PlaylistItem.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlaylistItem**](../Model/PlaylistItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistReplaceById**
> \Swagger\Client\Model\Playlist playlistReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | Model instance data

try {
    $result = $api_instance->playlistReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistReplaceOrCreate**
> \Swagger\Client\Model\Playlist playlistReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | Model instance data

try {
    $result = $api_instance->playlistReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 playlistUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | An object of model property name/value pairs

try {
    $result = $api_instance->playlistUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistUpsertPatchPlaylists**
> \Swagger\Client\Model\Playlist playlistUpsertPatchPlaylists($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | Model instance data

try {
    $result = $api_instance->playlistUpsertPatchPlaylists($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistUpsertPatchPlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistUpsertPutPlaylists**
> \Swagger\Client\Model\Playlist playlistUpsertPutPlaylists($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | Model instance data

try {
    $result = $api_instance->playlistUpsertPutPlaylists($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistUpsertPutPlaylists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playlistUpsertWithWhere**
> \Swagger\Client\Model\Playlist playlistUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\PlaylistApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\Playlist(); // \Swagger\Client\Model\Playlist | An object of model property name/value pairs

try {
    $result = $api_instance->playlistUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaylistApi->playlistUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\Playlist**](../Model/\Swagger\Client\Model\Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\Playlist**](../Model/Playlist.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

