# Harpoon\Api\StripeInvoiceItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceItemCount**](StripeInvoiceItemApi.md#stripeInvoiceItemCount) | **GET** /StripeInvoiceItems/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceItemCreate**](StripeInvoiceItemApi.md#stripeInvoiceItemCreate) | **POST** /StripeInvoiceItems | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**](StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream) | **GET** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**](StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream) | **POST** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemDeleteById**](StripeInvoiceItemApi.md#stripeInvoiceItemDeleteById) | **DELETE** /StripeInvoiceItems/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**](StripeInvoiceItemApi.md#stripeInvoiceItemExistsGetStripeInvoiceItemsidExists) | **GET** /StripeInvoiceItems/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceItemExistsHeadStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemExistsHeadStripeInvoiceItemsid) | **HEAD** /StripeInvoiceItems/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceItemFind**](StripeInvoiceItemApi.md#stripeInvoiceItemFind) | **GET** /StripeInvoiceItems | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceItemFindById**](StripeInvoiceItemApi.md#stripeInvoiceItemFindById) | **GET** /StripeInvoiceItems/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceItemFindOne**](StripeInvoiceItemApi.md#stripeInvoiceItemFindOne) | **GET** /StripeInvoiceItems/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid) | **PATCH** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid) | **PUT** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceById**](StripeInvoiceItemApi.md#stripeInvoiceItemReplaceById) | **POST** /StripeInvoiceItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceOrCreate**](StripeInvoiceItemApi.md#stripeInvoiceItemReplaceOrCreate) | **POST** /StripeInvoiceItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpdateAll**](StripeInvoiceItemApi.md#stripeInvoiceItemUpdateAll) | **POST** /StripeInvoiceItems/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceItemUpsertPatchStripeInvoiceItems**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPatchStripeInvoiceItems) | **PATCH** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertPutStripeInvoiceItems**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPutStripeInvoiceItems) | **PUT** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertWithWhere**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertWithWhere) | **POST** /StripeInvoiceItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeInvoiceItemCount**
> \Swagger\Client\Model\InlineResponse2001 stripeInvoiceItemCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->stripeInvoiceItemCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemCreate**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | Model instance data

try {
    $result = $api_instance->stripeInvoiceItemCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**
> \SplFileObject stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**
> \SplFileObject stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemDeleteById**
> object stripeInvoiceItemDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeInvoiceItemDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**
> \Swagger\Client\Model\InlineResponse2003 stripeInvoiceItemExistsGetStripeInvoiceItemsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeInvoiceItemExistsGetStripeInvoiceItemsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemExistsGetStripeInvoiceItemsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemExistsHeadStripeInvoiceItemsid**
> \Swagger\Client\Model\InlineResponse2003 stripeInvoiceItemExistsHeadStripeInvoiceItemsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeInvoiceItemExistsHeadStripeInvoiceItemsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemExistsHeadStripeInvoiceItemsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemFind**
> \Swagger\Client\Model\StripeInvoiceItem[] stripeInvoiceItemFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeInvoiceItemFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem[]**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemFindById**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->stripeInvoiceItemFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemFindOne**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeInvoiceItemFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$id = "id_example"; // string | StripeInvoiceItem id
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeInvoiceItem id |
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$id = "id_example"; // string | StripeInvoiceItem id
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeInvoiceItem id |
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemReplaceById**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | Model instance data

try {
    $result = $api_instance->stripeInvoiceItemReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemReplaceOrCreate**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | Model instance data

try {
    $result = $api_instance->stripeInvoiceItemReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 stripeInvoiceItemUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoiceItemUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemUpsertPatchStripeInvoiceItems**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemUpsertPatchStripeInvoiceItems($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | Model instance data

try {
    $result = $api_instance->stripeInvoiceItemUpsertPatchStripeInvoiceItems($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemUpsertPatchStripeInvoiceItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemUpsertPutStripeInvoiceItems**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemUpsertPutStripeInvoiceItems($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | Model instance data

try {
    $result = $api_instance->stripeInvoiceItemUpsertPutStripeInvoiceItems($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemUpsertPutStripeInvoiceItems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeInvoiceItemUpsertWithWhere**
> \Swagger\Client\Model\StripeInvoiceItem stripeInvoiceItemUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeInvoiceItemApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeInvoiceItem(); // \Swagger\Client\Model\StripeInvoiceItem | An object of model property name/value pairs

try {
    $result = $api_instance->stripeInvoiceItemUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeInvoiceItemApi->stripeInvoiceItemUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeInvoiceItem**](../Model/\Swagger\Client\Model\StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeInvoiceItem**](../Model/StripeInvoiceItem.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

