# Harpoon\Api\UdropshipVendorApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**udropshipVendorCount**](UdropshipVendorApi.md#udropshipVendorCount) | **GET** /UdropshipVendors/count | Count instances of the model matched by where from the data source.
[**udropshipVendorCreate**](UdropshipVendorApi.md#udropshipVendorCreate) | **POST** /UdropshipVendors | Create a new instance of the model and persist it into the data source.
[**udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**](UdropshipVendorApi.md#udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream) | **GET** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**](UdropshipVendorApi.md#udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream) | **POST** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorDeleteById**](UdropshipVendorApi.md#udropshipVendorDeleteById) | **DELETE** /UdropshipVendors/{id} | Delete a model instance by {{id}} from the data source.
[**udropshipVendorExistsGetUdropshipVendorsidExists**](UdropshipVendorApi.md#udropshipVendorExistsGetUdropshipVendorsidExists) | **GET** /UdropshipVendors/{id}/exists | Check whether a model instance exists in the data source.
[**udropshipVendorExistsHeadUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorExistsHeadUdropshipVendorsid) | **HEAD** /UdropshipVendors/{id} | Check whether a model instance exists in the data source.
[**udropshipVendorFind**](UdropshipVendorApi.md#udropshipVendorFind) | **GET** /UdropshipVendors | Find all instances of the model matched by filter from the data source.
[**udropshipVendorFindById**](UdropshipVendorApi.md#udropshipVendorFindById) | **GET** /UdropshipVendors/{id} | Find a model instance by {{id}} from the data source.
[**udropshipVendorFindOne**](UdropshipVendorApi.md#udropshipVendorFindOne) | **GET** /UdropshipVendors/findOne | Find first instance of the model matched by filter from the data source.
[**udropshipVendorPrototypeCountCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCountCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/count | Counts catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/count | Counts harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/count | Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeCountPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCountPartners) | **GET** /UdropshipVendors/{id}/partners/count | Counts partners of UdropshipVendor.
[**udropshipVendorPrototypeCountUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCountUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeCountVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/count | Counts vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeCreateCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateCatalogProducts) | **POST** /UdropshipVendors/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**udropshipVendorPrototypeCreateConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateConfig) | **POST** /UdropshipVendors/{id}/config | Creates a new instance in config of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners) | **POST** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Creates a new instance in harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories) | **POST** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeCreatePartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCreatePartners) | **POST** /UdropshipVendors/{id}/partners | Creates a new instance in partners of this model.
[**udropshipVendorPrototypeCreateUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateUdropshipVendorProducts) | **POST** /UdropshipVendors/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**udropshipVendorPrototypeCreateVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateVendorLocations) | **POST** /UdropshipVendors/{id}/vendorLocations | Creates a new instance in vendorLocations of this model.
[**udropshipVendorPrototypeDeleteCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Deletes all harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Deletes all harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeDeletePartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDeletePartners) | **DELETE** /UdropshipVendors/{id}/partners | Deletes all partners of this model.
[**udropshipVendorPrototypeDeleteUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**udropshipVendorPrototypeDeleteVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations | Deletes all vendorLocations of this model.
[**udropshipVendorPrototypeDestroyByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Delete a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Delete a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeDestroyByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdPartners) | **DELETE** /UdropshipVendors/{id}/partners/{fk} | Delete a related item by id for partners.
[**udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeDestroyByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations/{fk} | Delete a related item by id for vendorLocations.
[**udropshipVendorPrototypeDestroyConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyConfig) | **DELETE** /UdropshipVendors/{id}/config | Deletes config of this model.
[**udropshipVendorPrototypeExistsCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeExistsCatalogProducts) | **HEAD** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**udropshipVendorPrototypeExistsPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeExistsPartners) | **HEAD** /UdropshipVendors/{id}/partners/rel/{fk} | Check the existence of partners relation to an item by id.
[**udropshipVendorPrototypeFindByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Find a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Find a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeFindByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdPartners) | **GET** /UdropshipVendors/{id}/partners/{fk} | Find a related item by id for partners.
[**udropshipVendorPrototypeFindByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeFindByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/{fk} | Find a related item by id for vendorLocations.
[**udropshipVendorPrototypeGetCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeGetCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts | Queries catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeGetConfig) | **GET** /UdropshipVendors/{id}/config | Fetches hasOne relation config.
[**udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Queries harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeGetPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeGetPartners) | **GET** /UdropshipVendors/{id}/partners | Queries partners of UdropshipVendor.
[**udropshipVendorPrototypeGetUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeGetUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeGetVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations | Queries vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeLinkCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeLinkCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**udropshipVendorPrototypeLinkPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeLinkPartners) | **PUT** /UdropshipVendors/{id}/partners/rel/{fk} | Add a related item by id for partners.
[**udropshipVendorPrototypeUnlinkCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**udropshipVendorPrototypeUnlinkPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkPartners) | **DELETE** /UdropshipVendors/{id}/partners/rel/{fk} | Remove the partners relation to an item by id.
[**udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid) | **PATCH** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid) | **PUT** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners) | **PUT** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Update a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories) | **PUT** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Update a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeUpdateByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdPartners) | **PUT** /UdropshipVendors/{id}/partners/{fk} | Update a related item by id for partners.
[**udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeUpdateByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdVendorLocations) | **PUT** /UdropshipVendors/{id}/vendorLocations/{fk} | Update a related item by id for vendorLocations.
[**udropshipVendorPrototypeUpdateConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateConfig) | **PUT** /UdropshipVendors/{id}/config | Update config of this model.
[**udropshipVendorReplaceById**](UdropshipVendorApi.md#udropshipVendorReplaceById) | **POST** /UdropshipVendors/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**udropshipVendorReplaceOrCreate**](UdropshipVendorApi.md#udropshipVendorReplaceOrCreate) | **POST** /UdropshipVendors/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**udropshipVendorUpdateAll**](UdropshipVendorApi.md#udropshipVendorUpdateAll) | **POST** /UdropshipVendors/update | Update instances of the model matched by {{where}} from the data source.
[**udropshipVendorUpsertPatchUdropshipVendors**](UdropshipVendorApi.md#udropshipVendorUpsertPatchUdropshipVendors) | **PATCH** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertPutUdropshipVendors**](UdropshipVendorApi.md#udropshipVendorUpsertPutUdropshipVendors) | **PUT** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertWithWhere**](UdropshipVendorApi.md#udropshipVendorUpsertWithWhere) | **POST** /UdropshipVendors/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **udropshipVendorCount**
> \Swagger\Client\Model\InlineResponse2001 udropshipVendorCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->udropshipVendorCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorCreate**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | Model instance data

try {
    $result = $api_instance->udropshipVendorCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**
> \SplFileObject udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**
> \SplFileObject udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorDeleteById**
> object udropshipVendorDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->udropshipVendorDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorExistsGetUdropshipVendorsidExists**
> \Swagger\Client\Model\InlineResponse2003 udropshipVendorExistsGetUdropshipVendorsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->udropshipVendorExistsGetUdropshipVendorsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorExistsGetUdropshipVendorsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorExistsHeadUdropshipVendorsid**
> \Swagger\Client\Model\InlineResponse2003 udropshipVendorExistsHeadUdropshipVendorsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->udropshipVendorExistsHeadUdropshipVendorsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorExistsHeadUdropshipVendorsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorFind**
> \Swagger\Client\Model\UdropshipVendor[] udropshipVendorFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->udropshipVendorFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor[]**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorFindById**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->udropshipVendorFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorFindOne**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->udropshipVendorFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCountCatalogProducts**
> \Swagger\Client\Model\InlineResponse2001 udropshipVendorPrototypeCountCatalogProducts($id, $where)

Counts catalogProducts of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->udropshipVendorPrototypeCountCatalogProducts($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCountCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**
> \Swagger\Client\Model\InlineResponse2001 udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners($id, $where)

Counts harpoonHpublicApplicationpartners of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**
> \Swagger\Client\Model\InlineResponse2001 udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories($id, $where)

Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCountPartners**
> \Swagger\Client\Model\InlineResponse2001 udropshipVendorPrototypeCountPartners($id, $where)

Counts partners of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->udropshipVendorPrototypeCountPartners($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCountPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCountUdropshipVendorProducts**
> \Swagger\Client\Model\InlineResponse2001 udropshipVendorPrototypeCountUdropshipVendorProducts($id, $where)

Counts udropshipVendorProducts of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->udropshipVendorPrototypeCountUdropshipVendorProducts($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCountUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCountVendorLocations**
> \Swagger\Client\Model\InlineResponse2001 udropshipVendorPrototypeCountVendorLocations($id, $where)

Counts vendorLocations of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->udropshipVendorPrototypeCountVendorLocations($id, $where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCountVendorLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCreateCatalogProducts**
> \Swagger\Client\Model\CatalogProduct udropshipVendorPrototypeCreateCatalogProducts($id, $data)

Creates a new instance in catalogProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | 

try {
    $result = $api_instance->udropshipVendorPrototypeCreateCatalogProducts($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCreateCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCreateConfig**
> \Swagger\Client\Model\HarpoonHpublicVendorconfig udropshipVendorPrototypeCreateConfig($id, $data)

Creates a new instance in config of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicVendorconfig(); // \Swagger\Client\Model\HarpoonHpublicVendorconfig | 

try {
    $result = $api_instance->udropshipVendorPrototypeCreateConfig($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCreateConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicVendorconfig**](../Model/\Swagger\Client\Model\HarpoonHpublicVendorconfig.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicVendorconfig**](../Model/HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners($id, $data)

Creates a new instance in harpoonHpublicApplicationpartners of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | 

try {
    $result = $api_instance->udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**
> \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories($id, $data)

Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory(); // \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory | 

try {
    $result = $api_instance->udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory**](../Model/\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory**](../Model/HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCreatePartners**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorPrototypeCreatePartners($id, $data)

Creates a new instance in partners of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | 

try {
    $result = $api_instance->udropshipVendorPrototypeCreatePartners($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCreatePartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCreateUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct udropshipVendorPrototypeCreateUdropshipVendorProducts($id, $data)

Creates a new instance in udropshipVendorProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendorProduct(); // \Swagger\Client\Model\UdropshipVendorProduct | 

try {
    $result = $api_instance->udropshipVendorPrototypeCreateUdropshipVendorProducts($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCreateUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/\Swagger\Client\Model\UdropshipVendorProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeCreateVendorLocations**
> \Swagger\Client\Model\HarpoonHpublicBrandvenue udropshipVendorPrototypeCreateVendorLocations($id, $data)

Creates a new instance in vendorLocations of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicBrandvenue(); // \Swagger\Client\Model\HarpoonHpublicBrandvenue | 

try {
    $result = $api_instance->udropshipVendorPrototypeCreateVendorLocations($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeCreateVendorLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicBrandvenue**](../Model/\Swagger\Client\Model\HarpoonHpublicBrandvenue.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicBrandvenue**](../Model/HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDeleteCatalogProducts**
> udropshipVendorPrototypeDeleteCatalogProducts($id)

Deletes all catalogProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDeleteCatalogProducts($id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDeleteCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**
> udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners($id)

Deletes all harpoonHpublicApplicationpartners of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners($id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**
> udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories($id)

Deletes all harpoonHpublicv12VendorAppCategories of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories($id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDeletePartners**
> udropshipVendorPrototypeDeletePartners($id)

Deletes all partners of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDeletePartners($id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDeletePartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDeleteUdropshipVendorProducts**
> udropshipVendorPrototypeDeleteUdropshipVendorProducts($id)

Deletes all udropshipVendorProducts of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDeleteUdropshipVendorProducts($id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDeleteUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDeleteVendorLocations**
> udropshipVendorPrototypeDeleteVendorLocations($id)

Deletes all vendorLocations of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDeleteVendorLocations($id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDeleteVendorLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDestroyByIdCatalogProducts**
> udropshipVendorPrototypeDestroyByIdCatalogProducts($fk, $id)

Delete a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDestroyByIdCatalogProducts($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDestroyByIdCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**
> udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners($fk, $id)

Delete a related item by id for harpoonHpublicApplicationpartners.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for harpoonHpublicApplicationpartners
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for harpoonHpublicApplicationpartners |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**
> udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories($fk, $id)

Delete a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for harpoonHpublicv12VendorAppCategories
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for harpoonHpublicv12VendorAppCategories |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDestroyByIdPartners**
> udropshipVendorPrototypeDestroyByIdPartners($fk, $id)

Delete a related item by id for partners.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for partners
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDestroyByIdPartners($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDestroyByIdPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for partners |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**
> udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts($fk, $id)

Delete a related item by id for udropshipVendorProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendorProducts
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendorProducts |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDestroyByIdVendorLocations**
> udropshipVendorPrototypeDestroyByIdVendorLocations($fk, $id)

Delete a related item by id for vendorLocations.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for vendorLocations
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDestroyByIdVendorLocations($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDestroyByIdVendorLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for vendorLocations |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeDestroyConfig**
> udropshipVendorPrototypeDestroyConfig($id)

Deletes config of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeDestroyConfig($id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeDestroyConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeExistsCatalogProducts**
> bool udropshipVendorPrototypeExistsCatalogProducts($fk, $id)

Check the existence of catalogProducts relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeExistsCatalogProducts($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeExistsCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| UdropshipVendor id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeExistsPartners**
> bool udropshipVendorPrototypeExistsPartners($fk, $id)

Check the existence of partners relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for partners
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeExistsPartners($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeExistsPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for partners |
 **id** | **string**| UdropshipVendor id |

### Return type

**bool**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeFindByIdCatalogProducts**
> \Swagger\Client\Model\CatalogProduct udropshipVendorPrototypeFindByIdCatalogProducts($fk, $id)

Find a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeFindByIdCatalogProducts($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeFindByIdCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| UdropshipVendor id |

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners($fk, $id)

Find a related item by id for harpoonHpublicApplicationpartners.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for harpoonHpublicApplicationpartners
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for harpoonHpublicApplicationpartners |
 **id** | **string**| UdropshipVendor id |

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**
> \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories($fk, $id)

Find a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for harpoonHpublicv12VendorAppCategories
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for harpoonHpublicv12VendorAppCategories |
 **id** | **string**| UdropshipVendor id |

### Return type

[**\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory**](../Model/HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeFindByIdPartners**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorPrototypeFindByIdPartners($fk, $id)

Find a related item by id for partners.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for partners
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeFindByIdPartners($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeFindByIdPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for partners |
 **id** | **string**| UdropshipVendor id |

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeFindByIdUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct udropshipVendorPrototypeFindByIdUdropshipVendorProducts($fk, $id)

Find a related item by id for udropshipVendorProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendorProducts
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeFindByIdUdropshipVendorProducts($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeFindByIdUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendorProducts |
 **id** | **string**| UdropshipVendor id |

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeFindByIdVendorLocations**
> \Swagger\Client\Model\HarpoonHpublicBrandvenue udropshipVendorPrototypeFindByIdVendorLocations($fk, $id)

Find a related item by id for vendorLocations.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for vendorLocations
$id = "id_example"; // string | UdropshipVendor id

try {
    $result = $api_instance->udropshipVendorPrototypeFindByIdVendorLocations($fk, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeFindByIdVendorLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for vendorLocations |
 **id** | **string**| UdropshipVendor id |

### Return type

[**\Swagger\Client\Model\HarpoonHpublicBrandvenue**](../Model/HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeGetCatalogProducts**
> \Swagger\Client\Model\CatalogProduct[] udropshipVendorPrototypeGetCatalogProducts($id, $filter)

Queries catalogProducts of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->udropshipVendorPrototypeGetCatalogProducts($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeGetCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct[]**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeGetConfig**
> \Swagger\Client\Model\HarpoonHpublicVendorconfig udropshipVendorPrototypeGetConfig($id, $refresh)

Fetches hasOne relation config.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$refresh = true; // bool | 

try {
    $result = $api_instance->udropshipVendorPrototypeGetConfig($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeGetConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicVendorconfig**](../Model/HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner[] udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners($id, $filter)

Queries harpoonHpublicApplicationpartners of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner[]**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**
> \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory[] udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories($id, $filter)

Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory[]**](../Model/HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeGetPartners**
> \Swagger\Client\Model\UdropshipVendor[] udropshipVendorPrototypeGetPartners($id, $filter)

Queries partners of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->udropshipVendorPrototypeGetPartners($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeGetPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor[]**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeGetUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct[] udropshipVendorPrototypeGetUdropshipVendorProducts($id, $filter)

Queries udropshipVendorProducts of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->udropshipVendorPrototypeGetUdropshipVendorProducts($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeGetUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct[]**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeGetVendorLocations**
> \Swagger\Client\Model\HarpoonHpublicBrandvenue[] udropshipVendorPrototypeGetVendorLocations($id, $filter)

Queries vendorLocations of UdropshipVendor.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$filter = "filter_example"; // string | 

try {
    $result = $api_instance->udropshipVendorPrototypeGetVendorLocations($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeGetVendorLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **filter** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicBrandvenue[]**](../Model/HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeLinkCatalogProducts**
> \Swagger\Client\Model\UdropshipVendorProduct udropshipVendorPrototypeLinkCatalogProducts($fk, $id, $data)

Add a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendorProduct(); // \Swagger\Client\Model\UdropshipVendorProduct | 

try {
    $result = $api_instance->udropshipVendorPrototypeLinkCatalogProducts($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeLinkCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/\Swagger\Client\Model\UdropshipVendorProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeLinkPartners**
> \Swagger\Client\Model\UdropshipVendorPartner udropshipVendorPrototypeLinkPartners($fk, $id, $data)

Add a related item by id for partners.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for partners
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendorPartner(); // \Swagger\Client\Model\UdropshipVendorPartner | 

try {
    $result = $api_instance->udropshipVendorPrototypeLinkPartners($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeLinkPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for partners |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendorPartner**](../Model/\Swagger\Client\Model\UdropshipVendorPartner.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorPartner**](../Model/UdropshipVendorPartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUnlinkCatalogProducts**
> udropshipVendorPrototypeUnlinkCatalogProducts($fk, $id)

Remove the catalogProducts relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeUnlinkCatalogProducts($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUnlinkCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUnlinkPartners**
> udropshipVendorPrototypeUnlinkPartners($fk, $id)

Remove the partners relation to an item by id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for partners
$id = "id_example"; // string | UdropshipVendor id

try {
    $api_instance->udropshipVendorPrototypeUnlinkPartners($fk, $id);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUnlinkPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for partners |
 **id** | **string**| UdropshipVendor id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | An object of model property name/value pairs

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | An object of model property name/value pairs

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateByIdCatalogProducts**
> \Swagger\Client\Model\CatalogProduct udropshipVendorPrototypeUpdateByIdCatalogProducts($fk, $id, $data)

Update a related item by id for catalogProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for catalogProducts
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\CatalogProduct(); // \Swagger\Client\Model\CatalogProduct | 

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateByIdCatalogProducts($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateByIdCatalogProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for catalogProducts |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\CatalogProduct**](../Model/\Swagger\Client\Model\CatalogProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CatalogProduct**](../Model/CatalogProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**
> \Swagger\Client\Model\HarpoonHpublicApplicationpartner udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners($fk, $id, $data)

Update a related item by id for harpoonHpublicApplicationpartners.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for harpoonHpublicApplicationpartners
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicApplicationpartner(); // \Swagger\Client\Model\HarpoonHpublicApplicationpartner | 

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for harpoonHpublicApplicationpartners |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/\Swagger\Client\Model\HarpoonHpublicApplicationpartner.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicApplicationpartner**](../Model/HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**
> \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories($fk, $id, $data)

Update a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for harpoonHpublicv12VendorAppCategories
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory(); // \Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory | 

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for harpoonHpublicv12VendorAppCategories |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory**](../Model/\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicv12VendorAppCategory**](../Model/HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateByIdPartners**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorPrototypeUpdateByIdPartners($fk, $id, $data)

Update a related item by id for partners.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for partners
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | 

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateByIdPartners($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateByIdPartners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for partners |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**
> \Swagger\Client\Model\UdropshipVendorProduct udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts($fk, $id, $data)

Update a related item by id for udropshipVendorProducts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for udropshipVendorProducts
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\UdropshipVendorProduct(); // \Swagger\Client\Model\UdropshipVendorProduct | 

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for udropshipVendorProducts |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/\Swagger\Client\Model\UdropshipVendorProduct.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendorProduct**](../Model/UdropshipVendorProduct.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateByIdVendorLocations**
> \Swagger\Client\Model\HarpoonHpublicBrandvenue udropshipVendorPrototypeUpdateByIdVendorLocations($fk, $id, $data)

Update a related item by id for vendorLocations.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$fk = "fk_example"; // string | Foreign key for vendorLocations
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicBrandvenue(); // \Swagger\Client\Model\HarpoonHpublicBrandvenue | 

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateByIdVendorLocations($fk, $id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateByIdVendorLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **string**| Foreign key for vendorLocations |
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicBrandvenue**](../Model/\Swagger\Client\Model\HarpoonHpublicBrandvenue.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicBrandvenue**](../Model/HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorPrototypeUpdateConfig**
> \Swagger\Client\Model\HarpoonHpublicVendorconfig udropshipVendorPrototypeUpdateConfig($id, $data)

Update config of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | UdropshipVendor id
$data = new \Swagger\Client\Model\HarpoonHpublicVendorconfig(); // \Swagger\Client\Model\HarpoonHpublicVendorconfig | 

try {
    $result = $api_instance->udropshipVendorPrototypeUpdateConfig($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorPrototypeUpdateConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| UdropshipVendor id |
 **data** | [**\Swagger\Client\Model\HarpoonHpublicVendorconfig**](../Model/\Swagger\Client\Model\HarpoonHpublicVendorconfig.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\HarpoonHpublicVendorconfig**](../Model/HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorReplaceById**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | Model instance data

try {
    $result = $api_instance->udropshipVendorReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorReplaceOrCreate**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | Model instance data

try {
    $result = $api_instance->udropshipVendorReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 udropshipVendorUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | An object of model property name/value pairs

try {
    $result = $api_instance->udropshipVendorUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorUpsertPatchUdropshipVendors**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorUpsertPatchUdropshipVendors($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | Model instance data

try {
    $result = $api_instance->udropshipVendorUpsertPatchUdropshipVendors($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorUpsertPatchUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorUpsertPutUdropshipVendors**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorUpsertPutUdropshipVendors($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | Model instance data

try {
    $result = $api_instance->udropshipVendorUpsertPutUdropshipVendors($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorUpsertPutUdropshipVendors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **udropshipVendorUpsertWithWhere**
> \Swagger\Client\Model\UdropshipVendor udropshipVendorUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\UdropshipVendorApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\UdropshipVendor(); // \Swagger\Client\Model\UdropshipVendor | An object of model property name/value pairs

try {
    $result = $api_instance->udropshipVendorUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UdropshipVendorApi->udropshipVendorUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\UdropshipVendor**](../Model/\Swagger\Client\Model\UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\UdropshipVendor**](../Model/UdropshipVendor.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

