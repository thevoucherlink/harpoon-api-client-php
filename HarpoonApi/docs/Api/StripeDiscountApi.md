# Harpoon\Api\StripeDiscountApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeDiscountCount**](StripeDiscountApi.md#stripeDiscountCount) | **GET** /StripeDiscounts/count | Count instances of the model matched by where from the data source.
[**stripeDiscountCreate**](StripeDiscountApi.md#stripeDiscountCreate) | **POST** /StripeDiscounts | Create a new instance of the model and persist it into the data source.
[**stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**](StripeDiscountApi.md#stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream) | **GET** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**](StripeDiscountApi.md#stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream) | **POST** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountDeleteById**](StripeDiscountApi.md#stripeDiscountDeleteById) | **DELETE** /StripeDiscounts/{id} | Delete a model instance by {{id}} from the data source.
[**stripeDiscountExistsGetStripeDiscountsidExists**](StripeDiscountApi.md#stripeDiscountExistsGetStripeDiscountsidExists) | **GET** /StripeDiscounts/{id}/exists | Check whether a model instance exists in the data source.
[**stripeDiscountExistsHeadStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountExistsHeadStripeDiscountsid) | **HEAD** /StripeDiscounts/{id} | Check whether a model instance exists in the data source.
[**stripeDiscountFind**](StripeDiscountApi.md#stripeDiscountFind) | **GET** /StripeDiscounts | Find all instances of the model matched by filter from the data source.
[**stripeDiscountFindById**](StripeDiscountApi.md#stripeDiscountFindById) | **GET** /StripeDiscounts/{id} | Find a model instance by {{id}} from the data source.
[**stripeDiscountFindOne**](StripeDiscountApi.md#stripeDiscountFindOne) | **GET** /StripeDiscounts/findOne | Find first instance of the model matched by filter from the data source.
[**stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid) | **PATCH** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid) | **PUT** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceById**](StripeDiscountApi.md#stripeDiscountReplaceById) | **POST** /StripeDiscounts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceOrCreate**](StripeDiscountApi.md#stripeDiscountReplaceOrCreate) | **POST** /StripeDiscounts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeDiscountUpdateAll**](StripeDiscountApi.md#stripeDiscountUpdateAll) | **POST** /StripeDiscounts/update | Update instances of the model matched by {{where}} from the data source.
[**stripeDiscountUpsertPatchStripeDiscounts**](StripeDiscountApi.md#stripeDiscountUpsertPatchStripeDiscounts) | **PATCH** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertPutStripeDiscounts**](StripeDiscountApi.md#stripeDiscountUpsertPutStripeDiscounts) | **PUT** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertWithWhere**](StripeDiscountApi.md#stripeDiscountUpsertWithWhere) | **POST** /StripeDiscounts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeDiscountCount**
> \Swagger\Client\Model\InlineResponse2001 stripeDiscountCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->stripeDiscountCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountCreate**
> \Swagger\Client\Model\StripeDiscount stripeDiscountCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | Model instance data

try {
    $result = $api_instance->stripeDiscountCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**
> \SplFileObject stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**
> \SplFileObject stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountDeleteById**
> object stripeDiscountDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeDiscountDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountExistsGetStripeDiscountsidExists**
> \Swagger\Client\Model\InlineResponse2003 stripeDiscountExistsGetStripeDiscountsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeDiscountExistsGetStripeDiscountsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountExistsGetStripeDiscountsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountExistsHeadStripeDiscountsid**
> \Swagger\Client\Model\InlineResponse2003 stripeDiscountExistsHeadStripeDiscountsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeDiscountExistsHeadStripeDiscountsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountExistsHeadStripeDiscountsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountFind**
> \Swagger\Client\Model\StripeDiscount[] stripeDiscountFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeDiscountFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount[]**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountFindById**
> \Swagger\Client\Model\StripeDiscount stripeDiscountFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->stripeDiscountFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountFindOne**
> \Swagger\Client\Model\StripeDiscount stripeDiscountFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeDiscountFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**
> \Swagger\Client\Model\StripeDiscount stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$id = "id_example"; // string | StripeDiscount id
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | An object of model property name/value pairs

try {
    $result = $api_instance->stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeDiscount id |
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**
> \Swagger\Client\Model\StripeDiscount stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$id = "id_example"; // string | StripeDiscount id
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | An object of model property name/value pairs

try {
    $result = $api_instance->stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeDiscount id |
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountReplaceById**
> \Swagger\Client\Model\StripeDiscount stripeDiscountReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | Model instance data

try {
    $result = $api_instance->stripeDiscountReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountReplaceOrCreate**
> \Swagger\Client\Model\StripeDiscount stripeDiscountReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | Model instance data

try {
    $result = $api_instance->stripeDiscountReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 stripeDiscountUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | An object of model property name/value pairs

try {
    $result = $api_instance->stripeDiscountUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountUpsertPatchStripeDiscounts**
> \Swagger\Client\Model\StripeDiscount stripeDiscountUpsertPatchStripeDiscounts($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | Model instance data

try {
    $result = $api_instance->stripeDiscountUpsertPatchStripeDiscounts($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountUpsertPatchStripeDiscounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountUpsertPutStripeDiscounts**
> \Swagger\Client\Model\StripeDiscount stripeDiscountUpsertPutStripeDiscounts($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | Model instance data

try {
    $result = $api_instance->stripeDiscountUpsertPutStripeDiscounts($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountUpsertPutStripeDiscounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeDiscountUpsertWithWhere**
> \Swagger\Client\Model\StripeDiscount stripeDiscountUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeDiscountApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeDiscount(); // \Swagger\Client\Model\StripeDiscount | An object of model property name/value pairs

try {
    $result = $api_instance->stripeDiscountUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeDiscountApi->stripeDiscountUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeDiscount**](../Model/\Swagger\Client\Model\StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeDiscount**](../Model/StripeDiscount.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

