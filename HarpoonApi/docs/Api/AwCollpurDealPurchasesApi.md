# Harpoon\Api\AwCollpurDealPurchasesApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealPurchasesCount**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCount) | **GET** /AwCollpurDealPurchases/count | Count instances of the model matched by where from the data source.
[**awCollpurDealPurchasesCreate**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreate) | **POST** /AwCollpurDealPurchases | Create a new instance of the model and persist it into the data source.
[**awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream) | **GET** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream) | **POST** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesDeleteById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesDeleteById) | **DELETE** /AwCollpurDealPurchases/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists) | **GET** /AwCollpurDealPurchases/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid) | **HEAD** /AwCollpurDealPurchases/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesFind**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFind) | **GET** /AwCollpurDealPurchases | Find all instances of the model matched by filter from the data source.
[**awCollpurDealPurchasesFindById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindById) | **GET** /AwCollpurDealPurchases/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesFindOne**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindOne) | **GET** /AwCollpurDealPurchases/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon) | **POST** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Creates a new instance in awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon) | **DELETE** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Deletes awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeGetAwCollpurCoupon) | **GET** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Fetches hasOne relation awCollpurCoupon.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid) | **PATCH** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid) | **PUT** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon) | **PUT** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Update awCollpurCoupon of this model.
[**awCollpurDealPurchasesReplaceById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceById) | **POST** /AwCollpurDealPurchases/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesReplaceOrCreate**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceOrCreate) | **POST** /AwCollpurDealPurchases/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpdateAll**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpdateAll) | **POST** /AwCollpurDealPurchases/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases) | **PATCH** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases) | **PUT** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertWithWhere**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertWithWhere) | **POST** /AwCollpurDealPurchases/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awCollpurDealPurchasesCount**
> \Swagger\Client\Model\InlineResponse2001 awCollpurDealPurchasesCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->awCollpurDealPurchasesCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesCreate**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | Model instance data

try {
    $result = $api_instance->awCollpurDealPurchasesCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**
> \SplFileObject awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**
> \SplFileObject awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesDeleteById**
> object awCollpurDealPurchasesDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurDealPurchasesDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**
> \Swagger\Client\Model\InlineResponse2003 awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**
> \Swagger\Client\Model\InlineResponse2003 awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesFind**
> \Swagger\Client\Model\AwCollpurDealPurchases[] awCollpurDealPurchasesFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awCollpurDealPurchasesFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases[]**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesFindById**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->awCollpurDealPurchasesFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesFindOne**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->awCollpurDealPurchasesFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon($id, $data)

Creates a new instance in awCollpurCoupon of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | AwCollpurDealPurchases id
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | 

try {
    $result = $api_instance->awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDealPurchases id |
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**
> awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon($id)

Deletes awCollpurCoupon of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | AwCollpurDealPurchases id

try {
    $api_instance->awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon($id);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDealPurchases id |

### Return type

void (empty response body)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurDealPurchasesPrototypeGetAwCollpurCoupon($id, $refresh)

Fetches hasOne relation awCollpurCoupon.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | AwCollpurDealPurchases id
$refresh = true; // bool | 

try {
    $result = $api_instance->awCollpurDealPurchasesPrototypeGetAwCollpurCoupon($id, $refresh);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeGetAwCollpurCoupon: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDealPurchases id |
 **refresh** | **bool**|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | AwCollpurDealPurchases id
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDealPurchases id |
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | AwCollpurDealPurchases id
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDealPurchases id |
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**
> \Swagger\Client\Model\AwCollpurCoupon awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon($id, $data)

Update awCollpurCoupon of this model.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | AwCollpurDealPurchases id
$data = new \Swagger\Client\Model\AwCollpurCoupon(); // \Swagger\Client\Model\AwCollpurCoupon | 

try {
    $result = $api_instance->awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| AwCollpurDealPurchases id |
 **data** | [**\Swagger\Client\Model\AwCollpurCoupon**](../Model/\Swagger\Client\Model\AwCollpurCoupon.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurCoupon**](../Model/AwCollpurCoupon.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesReplaceById**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | Model instance data

try {
    $result = $api_instance->awCollpurDealPurchasesReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesReplaceOrCreate**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | Model instance data

try {
    $result = $api_instance->awCollpurDealPurchasesReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 awCollpurDealPurchasesUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealPurchasesUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | Model instance data

try {
    $result = $api_instance->awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | Model instance data

try {
    $result = $api_instance->awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **awCollpurDealPurchasesUpsertWithWhere**
> \Swagger\Client\Model\AwCollpurDealPurchases awCollpurDealPurchasesUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\AwCollpurDealPurchasesApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\AwCollpurDealPurchases(); // \Swagger\Client\Model\AwCollpurDealPurchases | An object of model property name/value pairs

try {
    $result = $api_instance->awCollpurDealPurchasesUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AwCollpurDealPurchasesApi->awCollpurDealPurchasesUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/\Swagger\Client\Model\AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\AwCollpurDealPurchases**](../Model/AwCollpurDealPurchases.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

