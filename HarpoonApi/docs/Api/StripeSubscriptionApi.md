# Harpoon\Api\StripeSubscriptionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeSubscriptionCount**](StripeSubscriptionApi.md#stripeSubscriptionCount) | **GET** /StripeSubscriptions/count | Count instances of the model matched by where from the data source.
[**stripeSubscriptionCreate**](StripeSubscriptionApi.md#stripeSubscriptionCreate) | **POST** /StripeSubscriptions | Create a new instance of the model and persist it into the data source.
[**stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**](StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream) | **GET** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**](StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream) | **POST** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionDeleteById**](StripeSubscriptionApi.md#stripeSubscriptionDeleteById) | **DELETE** /StripeSubscriptions/{id} | Delete a model instance by {{id}} from the data source.
[**stripeSubscriptionExistsGetStripeSubscriptionsidExists**](StripeSubscriptionApi.md#stripeSubscriptionExistsGetStripeSubscriptionsidExists) | **GET** /StripeSubscriptions/{id}/exists | Check whether a model instance exists in the data source.
[**stripeSubscriptionExistsHeadStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionExistsHeadStripeSubscriptionsid) | **HEAD** /StripeSubscriptions/{id} | Check whether a model instance exists in the data source.
[**stripeSubscriptionFind**](StripeSubscriptionApi.md#stripeSubscriptionFind) | **GET** /StripeSubscriptions | Find all instances of the model matched by filter from the data source.
[**stripeSubscriptionFindById**](StripeSubscriptionApi.md#stripeSubscriptionFindById) | **GET** /StripeSubscriptions/{id} | Find a model instance by {{id}} from the data source.
[**stripeSubscriptionFindOne**](StripeSubscriptionApi.md#stripeSubscriptionFindOne) | **GET** /StripeSubscriptions/findOne | Find first instance of the model matched by filter from the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid) | **PATCH** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid) | **PUT** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceById**](StripeSubscriptionApi.md#stripeSubscriptionReplaceById) | **POST** /StripeSubscriptions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceOrCreate**](StripeSubscriptionApi.md#stripeSubscriptionReplaceOrCreate) | **POST** /StripeSubscriptions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpdateAll**](StripeSubscriptionApi.md#stripeSubscriptionUpdateAll) | **POST** /StripeSubscriptions/update | Update instances of the model matched by {{where}} from the data source.
[**stripeSubscriptionUpsertPatchStripeSubscriptions**](StripeSubscriptionApi.md#stripeSubscriptionUpsertPatchStripeSubscriptions) | **PATCH** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertPutStripeSubscriptions**](StripeSubscriptionApi.md#stripeSubscriptionUpsertPutStripeSubscriptions) | **PUT** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertWithWhere**](StripeSubscriptionApi.md#stripeSubscriptionUpsertWithWhere) | **POST** /StripeSubscriptions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeSubscriptionCount**
> \Swagger\Client\Model\InlineResponse2001 stripeSubscriptionCount($where)

Count instances of the model matched by where from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$where = "where_example"; // string | Criteria to match model instances

try {
    $result = $api_instance->stripeSubscriptionCount($where);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionCreate**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionCreate($data)

Create a new instance of the model and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | Model instance data

try {
    $result = $api_instance->stripeSubscriptionCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**
> \SplFileObject stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**
> \SplFileObject stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream($options)

Create a change stream.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$options = "options_example"; // string | 

try {
    $result = $api_instance->stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream($options);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **string**|  | [optional]

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionDeleteById**
> object stripeSubscriptionDeleteById($id)

Delete a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeSubscriptionDeleteById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionDeleteById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

**object**

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionExistsGetStripeSubscriptionsidExists**
> \Swagger\Client\Model\InlineResponse2003 stripeSubscriptionExistsGetStripeSubscriptionsidExists($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeSubscriptionExistsGetStripeSubscriptionsidExists($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionExistsGetStripeSubscriptionsidExists: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionExistsHeadStripeSubscriptionsid**
> \Swagger\Client\Model\InlineResponse2003 stripeSubscriptionExistsHeadStripeSubscriptionsid($id)

Check whether a model instance exists in the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$id = "id_example"; // string | Model id

try {
    $result = $api_instance->stripeSubscriptionExistsHeadStripeSubscriptionsid($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionExistsHeadStripeSubscriptionsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionFind**
> \Swagger\Client\Model\StripeSubscription[] stripeSubscriptionFind($filter)

Find all instances of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeSubscriptionFind($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription[]**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionFindById**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionFindById($id, $filter)

Find a model instance by {{id}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$id = "id_example"; // string | Model id
$filter = "filter_example"; // string | Filter defining fields and include

try {
    $result = $api_instance->stripeSubscriptionFindById($id, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionFindById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **filter** | **string**| Filter defining fields and include | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionFindOne**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionFindOne($filter)

Find first instance of the model matched by filter from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$filter = "filter_example"; // string | Filter defining fields, where, include, order, offset, and limit

try {
    $result = $api_instance->stripeSubscriptionFindOne($filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionFindOne: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$id = "id_example"; // string | StripeSubscription id
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | An object of model property name/value pairs

try {
    $result = $api_instance->stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeSubscription id |
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid($id, $data)

Patch attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$id = "id_example"; // string | StripeSubscription id
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | An object of model property name/value pairs

try {
    $result = $api_instance->stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| StripeSubscription id |
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionReplaceById**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionReplaceById($id, $data)

Replace attributes for a model instance and persist it into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$id = "id_example"; // string | Model id
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | Model instance data

try {
    $result = $api_instance->stripeSubscriptionReplaceById($id, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionReplaceById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Model id |
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionReplaceOrCreate**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionReplaceOrCreate($data)

Replace an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | Model instance data

try {
    $result = $api_instance->stripeSubscriptionReplaceOrCreate($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionReplaceOrCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionUpdateAll**
> \Swagger\Client\Model\InlineResponse2002 stripeSubscriptionUpdateAll($where, $data)

Update instances of the model matched by {{where}} from the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | An object of model property name/value pairs

try {
    $result = $api_instance->stripeSubscriptionUpdateAll($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionUpdateAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionUpsertPatchStripeSubscriptions**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionUpsertPatchStripeSubscriptions($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | Model instance data

try {
    $result = $api_instance->stripeSubscriptionUpsertPatchStripeSubscriptions($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionUpsertPatchStripeSubscriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionUpsertPutStripeSubscriptions**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionUpsertPutStripeSubscriptions($data)

Patch an existing model instance or insert a new one into the data source.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | Model instance data

try {
    $result = $api_instance->stripeSubscriptionUpsertPutStripeSubscriptions($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionUpsertPutStripeSubscriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| Model instance data | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stripeSubscriptionUpsertWithWhere**
> \Swagger\Client\Model\StripeSubscription stripeSubscriptionUpsertWithWhere($where, $data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: access_token
Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKey('access_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Harpoon\Api\Configuration::getDefaultConfiguration()->setApiKeyPrefix('access_token', 'Bearer');

$api_instance = new Harpoon\Api\Api\StripeSubscriptionApi();
$where = "where_example"; // string | Criteria to match model instances
$data = new \Swagger\Client\Model\StripeSubscription(); // \Swagger\Client\Model\StripeSubscription | An object of model property name/value pairs

try {
    $result = $api_instance->stripeSubscriptionUpsertWithWhere($where, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StripeSubscriptionApi->stripeSubscriptionUpsertWithWhere: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **string**| Criteria to match model instances | [optional]
 **data** | [**\Swagger\Client\Model\StripeSubscription**](../Model/\Swagger\Client\Model\StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**\Swagger\Client\Model\StripeSubscription**](../Model/StripeSubscription.md)

### Authorization

[access_token](../../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

