<?php
/**
 * AppColorTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Harpoon\Api;

/**
 * AppColorTest Class Doc Comment
 *
 * @category    Class */
// * @description AppColor
/**
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AppColorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "AppColor"
     */
    public function testAppColor()
    {

    }

    /**
     * Test attribute "appPrimary"
     */
    public function testPropertyAppPrimary()
    {

    }

    /**
     * Test attribute "appSecondary"
     */
    public function testPropertyAppSecondary()
    {

    }

    /**
     * Test attribute "textPrimary"
     */
    public function testPropertyTextPrimary()
    {

    }

    /**
     * Test attribute "textSecondary"
     */
    public function testPropertyTextSecondary()
    {

    }

    /**
     * Test attribute "radioNavBar"
     */
    public function testPropertyRadioNavBar()
    {

    }

    /**
     * Test attribute "radioPlayer"
     */
    public function testPropertyRadioPlayer()
    {

    }

    /**
     * Test attribute "radioProgressBar"
     */
    public function testPropertyRadioProgressBar()
    {

    }

    /**
     * Test attribute "menuBackground"
     */
    public function testPropertyMenuBackground()
    {

    }

    /**
     * Test attribute "radioTextSecondary"
     */
    public function testPropertyRadioTextSecondary()
    {

    }

    /**
     * Test attribute "radioTextPrimary"
     */
    public function testPropertyRadioTextPrimary()
    {

    }

    /**
     * Test attribute "menuItem"
     */
    public function testPropertyMenuItem()
    {

    }

    /**
     * Test attribute "appGradientPrimary"
     */
    public function testPropertyAppGradientPrimary()
    {

    }

    /**
     * Test attribute "feedSegmentIndicator"
     */
    public function testPropertyFeedSegmentIndicator()
    {

    }

    /**
     * Test attribute "radioPlayButton"
     */
    public function testPropertyRadioPlayButton()
    {

    }

    /**
     * Test attribute "radioPauseButton"
     */
    public function testPropertyRadioPauseButton()
    {

    }

    /**
     * Test attribute "radioPlayButtonBackground"
     */
    public function testPropertyRadioPlayButtonBackground()
    {

    }

    /**
     * Test attribute "radioPauseButtonBackground"
     */
    public function testPropertyRadioPauseButtonBackground()
    {

    }

    /**
     * Test attribute "radioPlayerItem"
     */
    public function testPropertyRadioPlayerItem()
    {

    }

}
