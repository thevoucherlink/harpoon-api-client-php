<?php
/**
 * AppTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Harpoon\Api;

/**
 * AppTest Class Doc Comment
 *
 * @category    Class */
// * @description App
/**
 * @package     Harpoon\Api
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AppTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "App"
     */
    public function testApp()
    {

    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {

    }

    /**
     * Test attribute "internalId"
     */
    public function testPropertyInternalId()
    {

    }

    /**
     * Test attribute "vendorId"
     */
    public function testPropertyVendorId()
    {

    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {

    }

    /**
     * Test attribute "category"
     */
    public function testPropertyCategory()
    {

    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {

    }

    /**
     * Test attribute "keywords"
     */
    public function testPropertyKeywords()
    {

    }

    /**
     * Test attribute "copyright"
     */
    public function testPropertyCopyright()
    {

    }

    /**
     * Test attribute "realm"
     */
    public function testPropertyRealm()
    {

    }

    /**
     * Test attribute "bundle"
     */
    public function testPropertyBundle()
    {

    }

    /**
     * Test attribute "urlScheme"
     */
    public function testPropertyUrlScheme()
    {

    }

    /**
     * Test attribute "brandFollowOffer"
     */
    public function testPropertyBrandFollowOffer()
    {

    }

    /**
     * Test attribute "privacyUrl"
     */
    public function testPropertyPrivacyUrl()
    {

    }

    /**
     * Test attribute "termsOfServiceUrl"
     */
    public function testPropertyTermsOfServiceUrl()
    {

    }

    /**
     * Test attribute "restrictionAge"
     */
    public function testPropertyRestrictionAge()
    {

    }

    /**
     * Test attribute "storeAndroidUrl"
     */
    public function testPropertyStoreAndroidUrl()
    {

    }

    /**
     * Test attribute "storeIosUrl"
     */
    public function testPropertyStoreIosUrl()
    {

    }

    /**
     * Test attribute "typeNewsFeed"
     */
    public function testPropertyTypeNewsFeed()
    {

    }

    /**
     * Test attribute "clientSecret"
     */
    public function testPropertyClientSecret()
    {

    }

    /**
     * Test attribute "grantTypes"
     */
    public function testPropertyGrantTypes()
    {

    }

    /**
     * Test attribute "scopes"
     */
    public function testPropertyScopes()
    {

    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {

    }

    /**
     * Test attribute "created"
     */
    public function testPropertyCreated()
    {

    }

    /**
     * Test attribute "modified"
     */
    public function testPropertyModified()
    {

    }

    /**
     * Test attribute "styleOfferList"
     */
    public function testPropertyStyleOfferList()
    {

    }

    /**
     * Test attribute "appConfig"
     */
    public function testPropertyAppConfig()
    {

    }

    /**
     * Test attribute "clientToken"
     */
    public function testPropertyClientToken()
    {

    }

    /**
     * Test attribute "multiConfigTitle"
     */
    public function testPropertyMultiConfigTitle()
    {

    }

    /**
     * Test attribute "multiConfig"
     */
    public function testPropertyMultiConfig()
    {

    }

    /**
     * Test attribute "appId"
     */
    public function testPropertyAppId()
    {

    }

    /**
     * Test attribute "parentId"
     */
    public function testPropertyParentId()
    {

    }

    /**
     * Test attribute "rssFeeds"
     */
    public function testPropertyRssFeeds()
    {

    }

    /**
     * Test attribute "rssFeedGroups"
     */
    public function testPropertyRssFeedGroups()
    {

    }

    /**
     * Test attribute "radioStreams"
     */
    public function testPropertyRadioStreams()
    {

    }

    /**
     * Test attribute "customers"
     */
    public function testPropertyCustomers()
    {

    }

    /**
     * Test attribute "appConfigs"
     */
    public function testPropertyAppConfigs()
    {

    }

    /**
     * Test attribute "playlists"
     */
    public function testPropertyPlaylists()
    {

    }

    /**
     * Test attribute "apps"
     */
    public function testPropertyApps()
    {

    }

    /**
     * Test attribute "parent"
     */
    public function testPropertyParent()
    {

    }

}
