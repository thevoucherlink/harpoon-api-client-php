<?php
/**
 * RadioShowApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Harpoon\Api;

use \Harpoon\Api\Configuration;
use \Harpoon\Api\ApiClient;
use \Harpoon\Api\ApiException;
use \Harpoon\Api\ObjectSerializer;

/**
 * RadioShowApiTest Class Doc Comment
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RadioShowApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test case for radioShowCount
     *
     * Count instances of the model matched by where from the data source..
     *
     */
    public function testRadioShowCount()
    {

    }

    /**
     * Test case for radioShowCreate
     *
     * Create a new instance of the model and persist it into the data source..
     *
     */
    public function testRadioShowCreate()
    {

    }

    /**
     * Test case for radioShowCreateChangeStreamGetRadioShowsChangeStream
     *
     * Create a change stream..
     *
     */
    public function testRadioShowCreateChangeStreamGetRadioShowsChangeStream()
    {

    }

    /**
     * Test case for radioShowCreateChangeStreamPostRadioShowsChangeStream
     *
     * Create a change stream..
     *
     */
    public function testRadioShowCreateChangeStreamPostRadioShowsChangeStream()
    {

    }

    /**
     * Test case for radioShowDeleteById
     *
     * Delete a model instance by {{id}} from the data source..
     *
     */
    public function testRadioShowDeleteById()
    {

    }

    /**
     * Test case for radioShowExistsGetRadioShowsidExists
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testRadioShowExistsGetRadioShowsidExists()
    {

    }

    /**
     * Test case for radioShowExistsHeadRadioShowsid
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testRadioShowExistsHeadRadioShowsid()
    {

    }

    /**
     * Test case for radioShowFind
     *
     * Find all instances of the model matched by filter from the data source..
     *
     */
    public function testRadioShowFind()
    {

    }

    /**
     * Test case for radioShowFindById
     *
     * Find a model instance by {{id}} from the data source..
     *
     */
    public function testRadioShowFindById()
    {

    }

    /**
     * Test case for radioShowFindOne
     *
     * Find first instance of the model matched by filter from the data source..
     *
     */
    public function testRadioShowFindOne()
    {

    }

    /**
     * Test case for radioShowPrototypeCountRadioPresenters
     *
     * Counts radioPresenters of RadioShow..
     *
     */
    public function testRadioShowPrototypeCountRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeCountRadioShowTimes
     *
     * Counts radioShowTimes of RadioShow..
     *
     */
    public function testRadioShowPrototypeCountRadioShowTimes()
    {

    }

    /**
     * Test case for radioShowPrototypeCreateRadioPresenters
     *
     * Creates a new instance in radioPresenters of this model..
     *
     */
    public function testRadioShowPrototypeCreateRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeCreateRadioShowTimes
     *
     * Creates a new instance in radioShowTimes of this model..
     *
     */
    public function testRadioShowPrototypeCreateRadioShowTimes()
    {

    }

    /**
     * Test case for radioShowPrototypeDeleteRadioPresenters
     *
     * Deletes all radioPresenters of this model..
     *
     */
    public function testRadioShowPrototypeDeleteRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeDeleteRadioShowTimes
     *
     * Deletes all radioShowTimes of this model..
     *
     */
    public function testRadioShowPrototypeDeleteRadioShowTimes()
    {

    }

    /**
     * Test case for radioShowPrototypeDestroyByIdRadioPresenters
     *
     * Delete a related item by id for radioPresenters..
     *
     */
    public function testRadioShowPrototypeDestroyByIdRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeDestroyByIdRadioShowTimes
     *
     * Delete a related item by id for radioShowTimes..
     *
     */
    public function testRadioShowPrototypeDestroyByIdRadioShowTimes()
    {

    }

    /**
     * Test case for radioShowPrototypeExistsRadioPresenters
     *
     * Check the existence of radioPresenters relation to an item by id..
     *
     */
    public function testRadioShowPrototypeExistsRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeFindByIdRadioPresenters
     *
     * Find a related item by id for radioPresenters..
     *
     */
    public function testRadioShowPrototypeFindByIdRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeFindByIdRadioShowTimes
     *
     * Find a related item by id for radioShowTimes..
     *
     */
    public function testRadioShowPrototypeFindByIdRadioShowTimes()
    {

    }

    /**
     * Test case for radioShowPrototypeGetPlaylistItem
     *
     * Fetches belongsTo relation playlistItem..
     *
     */
    public function testRadioShowPrototypeGetPlaylistItem()
    {

    }

    /**
     * Test case for radioShowPrototypeGetRadioPresenters
     *
     * Queries radioPresenters of RadioShow..
     *
     */
    public function testRadioShowPrototypeGetRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeGetRadioShowTimes
     *
     * Queries radioShowTimes of RadioShow..
     *
     */
    public function testRadioShowPrototypeGetRadioShowTimes()
    {

    }

    /**
     * Test case for radioShowPrototypeGetRadioStream
     *
     * Fetches belongsTo relation radioStream..
     *
     */
    public function testRadioShowPrototypeGetRadioStream()
    {

    }

    /**
     * Test case for radioShowPrototypeLinkRadioPresenters
     *
     * Add a related item by id for radioPresenters..
     *
     */
    public function testRadioShowPrototypeLinkRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeUnlinkRadioPresenters
     *
     * Remove the radioPresenters relation to an item by id..
     *
     */
    public function testRadioShowPrototypeUnlinkRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeUpdateAttributesPatchRadioShowsid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testRadioShowPrototypeUpdateAttributesPatchRadioShowsid()
    {

    }

    /**
     * Test case for radioShowPrototypeUpdateAttributesPutRadioShowsid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testRadioShowPrototypeUpdateAttributesPutRadioShowsid()
    {

    }

    /**
     * Test case for radioShowPrototypeUpdateByIdRadioPresenters
     *
     * Update a related item by id for radioPresenters..
     *
     */
    public function testRadioShowPrototypeUpdateByIdRadioPresenters()
    {

    }

    /**
     * Test case for radioShowPrototypeUpdateByIdRadioShowTimes
     *
     * Update a related item by id for radioShowTimes..
     *
     */
    public function testRadioShowPrototypeUpdateByIdRadioShowTimes()
    {

    }

    /**
     * Test case for radioShowReplaceById
     *
     * Replace attributes for a model instance and persist it into the data source..
     *
     */
    public function testRadioShowReplaceById()
    {

    }

    /**
     * Test case for radioShowReplaceOrCreate
     *
     * Replace an existing model instance or insert a new one into the data source..
     *
     */
    public function testRadioShowReplaceOrCreate()
    {

    }

    /**
     * Test case for radioShowUpdateAll
     *
     * Update instances of the model matched by {{where}} from the data source..
     *
     */
    public function testRadioShowUpdateAll()
    {

    }

    /**
     * Test case for radioShowUploadFile
     *
     * Upload File to Radio Show.
     *
     */
    public function testRadioShowUploadFile()
    {

    }

    /**
     * Test case for radioShowUpsertPatchRadioShows
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testRadioShowUpsertPatchRadioShows()
    {

    }

    /**
     * Test case for radioShowUpsertPutRadioShows
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testRadioShowUpsertPutRadioShows()
    {

    }

    /**
     * Test case for radioShowUpsertWithWhere
     *
     * Update an existing model instance or insert a new one into the data source based on the where criteria..
     *
     */
    public function testRadioShowUpsertWithWhere()
    {

    }

}
