<?php
/**
 * RadioStreamApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Harpoon\Api;

use \Harpoon\Api\Configuration;
use \Harpoon\Api\ApiClient;
use \Harpoon\Api\ApiException;
use \Harpoon\Api\ObjectSerializer;

/**
 * RadioStreamApiTest Class Doc Comment
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RadioStreamApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test case for radioStreamCount
     *
     * Count instances of the model matched by where from the data source..
     *
     */
    public function testRadioStreamCount()
    {

    }

    /**
     * Test case for radioStreamCreate
     *
     * Create a new instance of the model and persist it into the data source..
     *
     */
    public function testRadioStreamCreate()
    {

    }

    /**
     * Test case for radioStreamCreateChangeStreamGetRadioStreamsChangeStream
     *
     * Create a change stream..
     *
     */
    public function testRadioStreamCreateChangeStreamGetRadioStreamsChangeStream()
    {

    }

    /**
     * Test case for radioStreamCreateChangeStreamPostRadioStreamsChangeStream
     *
     * Create a change stream..
     *
     */
    public function testRadioStreamCreateChangeStreamPostRadioStreamsChangeStream()
    {

    }

    /**
     * Test case for radioStreamDeleteById
     *
     * Delete a model instance by {{id}} from the data source..
     *
     */
    public function testRadioStreamDeleteById()
    {

    }

    /**
     * Test case for radioStreamExistsGetRadioStreamsidExists
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testRadioStreamExistsGetRadioStreamsidExists()
    {

    }

    /**
     * Test case for radioStreamExistsHeadRadioStreamsid
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testRadioStreamExistsHeadRadioStreamsid()
    {

    }

    /**
     * Test case for radioStreamFind
     *
     * Find all instances of the model matched by filter from the data source..
     *
     */
    public function testRadioStreamFind()
    {

    }

    /**
     * Test case for radioStreamFindById
     *
     * Find a model instance by {{id}} from the data source..
     *
     */
    public function testRadioStreamFindById()
    {

    }

    /**
     * Test case for radioStreamFindOne
     *
     * Find first instance of the model matched by filter from the data source..
     *
     */
    public function testRadioStreamFindOne()
    {

    }

    /**
     * Test case for radioStreamPrototypeCountRadioShows
     *
     * Counts radioShows of RadioStream..
     *
     */
    public function testRadioStreamPrototypeCountRadioShows()
    {

    }

    /**
     * Test case for radioStreamPrototypeCreateRadioShows
     *
     * Creates a new instance in radioShows of this model..
     *
     */
    public function testRadioStreamPrototypeCreateRadioShows()
    {

    }

    /**
     * Test case for radioStreamPrototypeDeleteRadioShows
     *
     * Deletes all radioShows of this model..
     *
     */
    public function testRadioStreamPrototypeDeleteRadioShows()
    {

    }

    /**
     * Test case for radioStreamPrototypeDestroyByIdRadioShows
     *
     * Delete a related item by id for radioShows..
     *
     */
    public function testRadioStreamPrototypeDestroyByIdRadioShows()
    {

    }

    /**
     * Test case for radioStreamPrototypeFindByIdRadioShows
     *
     * Find a related item by id for radioShows..
     *
     */
    public function testRadioStreamPrototypeFindByIdRadioShows()
    {

    }

    /**
     * Test case for radioStreamPrototypeGetRadioShows
     *
     * Queries radioShows of RadioStream..
     *
     */
    public function testRadioStreamPrototypeGetRadioShows()
    {

    }

    /**
     * Test case for radioStreamPrototypeUpdateAttributesPatchRadioStreamsid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testRadioStreamPrototypeUpdateAttributesPatchRadioStreamsid()
    {

    }

    /**
     * Test case for radioStreamPrototypeUpdateAttributesPutRadioStreamsid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testRadioStreamPrototypeUpdateAttributesPutRadioStreamsid()
    {

    }

    /**
     * Test case for radioStreamPrototypeUpdateByIdRadioShows
     *
     * Update a related item by id for radioShows..
     *
     */
    public function testRadioStreamPrototypeUpdateByIdRadioShows()
    {

    }

    /**
     * Test case for radioStreamRadioShowCurrent
     *
     * .
     *
     */
    public function testRadioStreamRadioShowCurrent()
    {

    }

    /**
     * Test case for radioStreamRadioShowSchedule
     *
     * .
     *
     */
    public function testRadioStreamRadioShowSchedule()
    {

    }

    /**
     * Test case for radioStreamReplaceById
     *
     * Replace attributes for a model instance and persist it into the data source..
     *
     */
    public function testRadioStreamReplaceById()
    {

    }

    /**
     * Test case for radioStreamReplaceOrCreate
     *
     * Replace an existing model instance or insert a new one into the data source..
     *
     */
    public function testRadioStreamReplaceOrCreate()
    {

    }

    /**
     * Test case for radioStreamUpdateAll
     *
     * Update instances of the model matched by {{where}} from the data source..
     *
     */
    public function testRadioStreamUpdateAll()
    {

    }

    /**
     * Test case for radioStreamUpsertPatchRadioStreams
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testRadioStreamUpsertPatchRadioStreams()
    {

    }

    /**
     * Test case for radioStreamUpsertPutRadioStreams
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testRadioStreamUpsertPutRadioStreams()
    {

    }

    /**
     * Test case for radioStreamUpsertWithWhere
     *
     * Update an existing model instance or insert a new one into the data source based on the where criteria..
     *
     */
    public function testRadioStreamUpsertWithWhere()
    {

    }

}
