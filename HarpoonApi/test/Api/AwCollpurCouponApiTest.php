<?php
/**
 * AwCollpurCouponApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Harpoon\Api;

use \Harpoon\Api\Configuration;
use \Harpoon\Api\ApiClient;
use \Harpoon\Api\ApiException;
use \Harpoon\Api\ObjectSerializer;

/**
 * AwCollpurCouponApiTest Class Doc Comment
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AwCollpurCouponApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test case for awCollpurCouponCount
     *
     * Count instances of the model matched by where from the data source..
     *
     */
    public function testAwCollpurCouponCount()
    {

    }

    /**
     * Test case for awCollpurCouponCreate
     *
     * Create a new instance of the model and persist it into the data source..
     *
     */
    public function testAwCollpurCouponCreate()
    {

    }

    /**
     * Test case for awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream
     *
     * Create a change stream..
     *
     */
    public function testAwCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream()
    {

    }

    /**
     * Test case for awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream
     *
     * Create a change stream..
     *
     */
    public function testAwCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream()
    {

    }

    /**
     * Test case for awCollpurCouponDeleteById
     *
     * Delete a model instance by {{id}} from the data source..
     *
     */
    public function testAwCollpurCouponDeleteById()
    {

    }

    /**
     * Test case for awCollpurCouponExistsGetAwCollpurCouponsidExists
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testAwCollpurCouponExistsGetAwCollpurCouponsidExists()
    {

    }

    /**
     * Test case for awCollpurCouponExistsHeadAwCollpurCouponsid
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testAwCollpurCouponExistsHeadAwCollpurCouponsid()
    {

    }

    /**
     * Test case for awCollpurCouponFind
     *
     * Find all instances of the model matched by filter from the data source..
     *
     */
    public function testAwCollpurCouponFind()
    {

    }

    /**
     * Test case for awCollpurCouponFindById
     *
     * Find a model instance by {{id}} from the data source..
     *
     */
    public function testAwCollpurCouponFindById()
    {

    }

    /**
     * Test case for awCollpurCouponFindOne
     *
     * Find first instance of the model matched by filter from the data source..
     *
     */
    public function testAwCollpurCouponFindOne()
    {

    }

    /**
     * Test case for awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testAwCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid()
    {

    }

    /**
     * Test case for awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testAwCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid()
    {

    }

    /**
     * Test case for awCollpurCouponReplaceById
     *
     * Replace attributes for a model instance and persist it into the data source..
     *
     */
    public function testAwCollpurCouponReplaceById()
    {

    }

    /**
     * Test case for awCollpurCouponReplaceOrCreate
     *
     * Replace an existing model instance or insert a new one into the data source..
     *
     */
    public function testAwCollpurCouponReplaceOrCreate()
    {

    }

    /**
     * Test case for awCollpurCouponUpdateAll
     *
     * Update instances of the model matched by {{where}} from the data source..
     *
     */
    public function testAwCollpurCouponUpdateAll()
    {

    }

    /**
     * Test case for awCollpurCouponUpsertPatchAwCollpurCoupons
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testAwCollpurCouponUpsertPatchAwCollpurCoupons()
    {

    }

    /**
     * Test case for awCollpurCouponUpsertPutAwCollpurCoupons
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testAwCollpurCouponUpsertPutAwCollpurCoupons()
    {

    }

    /**
     * Test case for awCollpurCouponUpsertWithWhere
     *
     * Update an existing model instance or insert a new one into the data source based on the where criteria..
     *
     */
    public function testAwCollpurCouponUpsertWithWhere()
    {

    }

}
