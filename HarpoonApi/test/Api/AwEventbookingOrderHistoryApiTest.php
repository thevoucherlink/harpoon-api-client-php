<?php
/**
 * AwEventbookingOrderHistoryApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * harpoon-api
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Harpoon\Api;

use \Harpoon\Api\Configuration;
use \Harpoon\Api\ApiClient;
use \Harpoon\Api\ApiException;
use \Harpoon\Api\ObjectSerializer;

/**
 * AwEventbookingOrderHistoryApiTest Class Doc Comment
 *
 * @category Class
 * @package  Harpoon\Api
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AwEventbookingOrderHistoryApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryCount
     *
     * Count instances of the model matched by where from the data source..
     *
     */
    public function testAwEventbookingOrderHistoryCount()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryCreate
     *
     * Create a new instance of the model and persist it into the data source..
     *
     */
    public function testAwEventbookingOrderHistoryCreate()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream
     *
     * Create a change stream..
     *
     */
    public function testAwEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream
     *
     * Create a change stream..
     *
     */
    public function testAwEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryDeleteById
     *
     * Delete a model instance by {{id}} from the data source..
     *
     */
    public function testAwEventbookingOrderHistoryDeleteById()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testAwEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid
     *
     * Check whether a model instance exists in the data source..
     *
     */
    public function testAwEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryFind
     *
     * Find all instances of the model matched by filter from the data source..
     *
     */
    public function testAwEventbookingOrderHistoryFind()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryFindById
     *
     * Find a model instance by {{id}} from the data source..
     *
     */
    public function testAwEventbookingOrderHistoryFindById()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryFindOne
     *
     * Find first instance of the model matched by filter from the data source..
     *
     */
    public function testAwEventbookingOrderHistoryFindOne()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testAwEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid
     *
     * Patch attributes for a model instance and persist it into the data source..
     *
     */
    public function testAwEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryReplaceById
     *
     * Replace attributes for a model instance and persist it into the data source..
     *
     */
    public function testAwEventbookingOrderHistoryReplaceById()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryReplaceOrCreate
     *
     * Replace an existing model instance or insert a new one into the data source..
     *
     */
    public function testAwEventbookingOrderHistoryReplaceOrCreate()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryUpdateAll
     *
     * Update instances of the model matched by {{where}} from the data source..
     *
     */
    public function testAwEventbookingOrderHistoryUpdateAll()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testAwEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories
     *
     * Patch an existing model instance or insert a new one into the data source..
     *
     */
    public function testAwEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories()
    {

    }

    /**
     * Test case for awEventbookingOrderHistoryUpsertWithWhere
     *
     * Update an existing model instance or insert a new one into the data source based on the where criteria..
     *
     */
    public function testAwEventbookingOrderHistoryUpsertWithWhere()
    {

    }

}
